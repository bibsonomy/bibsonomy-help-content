<!-- de: Typo3 -->

## Integrate into TYPO3
--------------------

For Typo3 v1.0-v5.9 you can use [BibSonomy Typo3-Extension](http://typo3.org/extensions/repository/view/ext_bibsonomy)
to integrate lists of publications from ${project.name} into your
pages. In Typo3 v6.0 there are some major changes, so we implemented a
new extension based on Typo3 Flow. If you are using Typo3 v6.0 or above,
please use the following extension: [BibSonomy CSL (ext\_bibsonomy\_csl)](http://typo3.org/extensions/repository/view/ext_bibsonomy_csl).

With our new extension, you can add lists of publications from
${project.name} to your pages and you can style your bibliography with
CSL (Citation Style Language) stylesheets. To get started quickly, you
can choose from a set of pre-installed stylesheets. But you can also
change these pre-installed styles or create your own stylesheets.
Additionally, you have the option to add your ${project.name} tag cloud
to your website.

--------------

### Administration

After you have installed the Typo3 plugin, there are several options for administration. Information on installing the plugin and managing CSL styles can be found [here](Typo3/Typo3Admin "wikilink").

------------------------------------------------------------------------

### Add publication lists with the Frontend Plugin

After you installed and activated the extension, you can create publication lists. Visit [this page](Typo3/Typo3Publist "wikilink") for help.

------------------------------------------------------------------------

### Add your Tag Cloud with the Frontend Plugin

You can also add your ${project.name} tag cloud to your pages. To do so, [this page](Typo3/Typo3Tagcloud "wikilink") might help you.

-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").