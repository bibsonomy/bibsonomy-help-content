<!-- de: Synchronisation -->

## Synchronization between BibSonomy and PUMA
------------------------------------------

If you want to use your data from BibSonomy and [PUMA](http://www.bibsonomy.org/help_en/Our%20Projects) in both systems,
you can use the synchronization function. For this, you need to be
**registered as a user in both systems**. Complete the following steps:  

-  Log in to BibSonomy with your user data.

-   Go to the menu **"settings"** and click on the tab **"synchronization"**.

-   Enter the address of the **synchronization client**, e.g.
    <http://puma.uni-kassel.de/>.
    
-   Choose a **synchronization direction** or choose to synchronize in
    both directions.
    
-   Save the settings by clicking on **"Save"**.

-   Finally, click on the link **"synchronization page"** and request a
    **synchronization plan** to get a preview for synchronizations.
    Complete the synchronization by clicking on **"OK"**, if the preview
    is to your liking. Otherwise, click on **"settings"** to return to
    the synchronization start page and adjust the synchronization.


![ <File:sync.png>](synchronisation/sync.png  "fig: File:sync.png")

-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").