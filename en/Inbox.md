<!-- de: Eingang -->

## Inbox
------------------------

In the [ inbox](${project.home}inbox "wikilink"), all entries are stored that were sent to you by your friends.

![ <File:inbox.png>](inbox/inbox.png  "fig: File:inbox.png")  

---------------------------------

### Send entries

To send a bookmark or a publication to another ${project.name} user
(for example *xyz*), you can use the [ system tag](SystemTags "wikilink") `send:xyz`.
The entry will then be tagged with `from:<YourUserName>` and sent
to the user *xyz*'s inbox. To avoid spam in the inbox, the receiving user has to

-   either have added you as a friend
-   or be a member in a group where you are a member, too.

After the entry has been sent successfully, it will be tagged with
`sent:xyz`.

![ <File:sendtag.png>](inbox/sendtag.png  "fig: File:sendtag.png")

----------------------------

### Receive and use entries

In the inbox, you can see all entries sent to you by your friends. Use
**copy this publication/bookmark to your repository** to save the entry
into your collection.

![ <File:copypublication.png>](inbox/copypublication.png  "fig: File:copypublication.png")


With **remove this publication/bookmark from your
inbox**, you can remove the entry from your inbox. By clicking on the
**clear inbox** button, you can delete all entries in your inbox.

![ <File:removepublication.png>](inbox/removepublication.png  "fig: File:removepublication.png")

-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").

