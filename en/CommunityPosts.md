<!-- de: CommunityPosts -->

## Community posts
---------------

### What is a community post?

${project.theme == "bibsonomy"}{

In ${project.name}, bookmarks and publications are automatically stored
in two different representation ways. If you save a bookmark/publication to your collection, a **private
entry** will be created that belongs to your collection and cannot be
edited by other users, only by yourself.  
Additionally, a **public representation** for this bookmark/publication
will be created, a so called **community post**. This post
provides an overview on all availabe data of different users belonging
to this post. Also, the metadata from this post can be edited by all
other users.  
Ideally, the community of ${project.name} can provide all relevant information on the particular publication.

------------------------------------------------------------------------

### How can I view a community post?

To get to the **community post page view** of a post, just click on a
post, e.g. in your collection or in the ${project.name} feed. Then,
click on the **black arrow** on the right and finally on **community post**.  
  
![ <File:arrow.png>](communityposts/arrow.png  "fig: File:arrow.png")  

------------------------------------------------------------------------

### Community post - page overview

In this chapter, the different sections of the community post page are described.  

-   **Action buttons:**  
    On the very top of the community post page, there are buttons displayed.  
    ![ <File:buttons.png>](communityposts/buttons.png  "fig: File:buttons.png")  
    - With the **pencil**, you can **edit the community post** (enter
    general and detailed information, add comments and notes).
    -   The **three lines** button shows you **the history of the post**
        (only displayed if the post has a history).
    -   If the post is not yet in your collection, you can **add it to
        your collection** with the **green button**.  
        If the post is already in your collection, another button will
        be displayed to **view the post in your collection** (not the
        community post).  
        ![
        <File:buttonsCollection.png>](communityposts/buttonsCollection.png  "fig: File:buttonsCollection.png")  

<!-- -->

-   **General information:**  
    The most important information about a post, e.g. **title**,
    **authors**, **journal** and **year**.

<!-- -->

-   **Abstract:**  
    A short summary of the publication. Click on **(more)** to view the
    whole abstract.

<!-- -->

-   **Links and resources:**  
    Provided links to this post, e.g. **URL**, **DOI** or a **document**.

<!-- -->

-   **Tags:**  
    [ Tags](Tags "wikilink") that this post has been tagged with (from
    all users who have stored this post in their collection).  
    ![ <File:tags.png>](communityposts/tags.png  "fig: File:tags.png")  

<!-- -->

-   **Users:**  
    Users who have saved this post to their collection will be displayed here. Click on a
    user's **picture** to get to their user page. Click on the **book
    symbol** below to view this post in the user's collection.  
    ![ <File:users.png>](communityposts/users.png  "fig: File:users.png")  

<!-- -->

-   **Comments and Reviews:**  
    Comments, reviews and ratings for this post (if existing). Also, you
    can [ write your own comment or rating](DiscussionReviewsComments "wikilink").  
    ![ <File:comments.png>](communityposts/comments.png  "fig: File:comments.png")  

<!-- -->

-   **Cite this publication:**  
    Citation of this post in different **citation styles**, e.g.
    Harvard, APA, BibTeX and EndNote.

}

${project.theme == "puma"}{

In ${project.name}, bookmarks and publications are automatically stored
in two different representation ways. If you save a bookmark/publication to your collection, a **private
entry** will be created that belongs to your collection and cannot be
edited by other users, only by yourself.  
Additionally, a **public representation** for this bookmark/publication
will be created, a so called **community post**. This post
provides an overview on all availabe data of different users belonging
to this post. Also, the metadata from this post can be edited by all
other users.  
Ideally, the community of ${project.name} can provide all relevant information on the particular publication.

------------------------------------------------------------------------

### How can I view a community post?

To get to the **community post page view** of a post, just click on a
post, e.g. in your collection or in the ${project.name} feed. Then,
click on the **black arrow** on the right and finally on **community post**.  
  
![ <File:arrow.png>](communityposts/arrow.png  "fig: File:arrow.png")  

------------------------------------------------------------------------

### Community post - page overview

In this chapter, the different sections of the community post page are described.  

-   **Action buttons:**  
    On the very top of the community post page, there are buttons displayed.  
    ![ <File:buttons.png>](communityposts/buttons.png  "fig: File:buttons.png")  
    - With the **pencil**, you can **edit the community post** (enter
    general and detailed information, add comments and notes).
    -   The **three lines** button shows you **the history of the post**
        (only displayed if the post has a history).
    -   If the post is not yet in your collection, you can **add it to
        your collection** with the **green button**.  
        If the post is already in your collection, another button will
        be displayed to **view the post in your collection** (not the
        community post).  
        ![
        <File:buttonsCollection.png>](communityposts/buttonsCollection.png  "fig: File:buttonsCollection.png")  

<!-- -->

-   **General information:**  
    The most important information about a post, e.g. **title**,
    **authors**, **journal** and **year**.

<!-- -->

-   **Abstract:**  
    A short summary of the publication. Click on **(more)** to view the
    whole abstract.

<!-- -->

-   **Links and resources:**  
    Provided links to this post, e.g. **URL**, **DOI** or a **document**.

<!-- -->

-   **Tags:**  
    [ Tags](Tags "wikilink") that this post has been tagged with (from
    all users who have stored this post in their collection).  
    ![ <File:tags.png>](communityposts/tags.png  "fig: File:tags.png")  

<!-- -->

-   **Users:**  
    Users who have saved this post to their collection will be displayed here. Click on a
    user's **picture** to get to their user page. Click on the **book
    symbol** below to view this post in the user's collection.  
    ![ <File:users.png>](communityposts/users.png  "fig: File:users.png")  

<!-- -->

-   **Comments and Reviews:**  
    Comments, reviews and ratings for this post (if existing). Also, you
    can [ write your own comment or rating](DiscussionReviewsComments "wikilink").  
    ![ <File:comments.png>](communityposts/comments.png  "fig: File:comments.png")  

<!-- -->

-   **Cite this publication:**  
    Citation of this post in different **citation styles**, e.g.
    Harvard, APA, BibTeX and EndNote.

}

${project.theme == "wueresearch"}{

In ${project.name}, bookmarks and publications are automatically stored
in two different representation ways. If you save a bookmark/publication to your collection, a **private
entry** will be created that belongs to your collection and cannot be
edited by other users, only by yourself.  
Additionally, a **public representation** for this bookmark/publication
will be created, a so called **community post**. This post
provides an overview on all availabe data of different users belonging
to this post. Also, the metadata from this post can be edited by all
other users.  
Ideally, the community of ${project.name} can provide all relevant information on the particular publication.

------------------------------------------------------------------------

### How can I view a community post?

To get to the **community post page view** of a post, just click on a
post, e.g. in your collection or in the ${project.name} feed. Then,
click on the **black arrow** on the right and finally on **community post**.  
  
![ <File:arrow.png>](communityposts/arrow.png  "fig: File:arrow.png")  

------------------------------------------------------------------------

### Community post - page overview

In this chapter, the different sections of the community post page are described.  

-   **Action buttons:**  
    On the very top of the community post page, there are buttons displayed.  
    ![ <File:buttonswue.png>](communityposts/buttonswue.png  "fig: File:buttonswue.png")  
    - With the **pencil**, you can **edit the community post** (enter
    general and detailed information, add comments and notes).
    -   The **three lines** button shows you **the history of the post**
        (only displayed if the post has a history).
    -   If the post is not yet in your collection, you can **add it to
        your collection** with the blue **"copy" button**.  
        If the post is already in your collection, another button will
        be displayed to **view the post in your collection** (not the
        community post).  
        ![
        <File:buttonsCollectionwue.png>](communityposts/buttonsCollectionwue.png  "fig: File:buttonsCollectionwue.png")  

<!-- -->

-   **General information:**  
    The most important information about a post, e.g. **title**,
    **authors**, **journal** and **year**.

<!-- -->

-   **Abstract:**  
    A short summary of the publication. Click on **(more)** to view the
    whole abstract.

<!-- -->

-   **Links and resources:**  
    Provided links to this post, e.g. **URL**, **DOI** or a **document**.

<!-- -->

-   **Tags:**  
    [ Tags](Tags "wikilink") that this post has been tagged with (from
    all users who have stored this post in their collection).  
    ![ <File:tagswue.png>](communityposts/tagswue.png  "fig: File:tagswue.png")  

<!-- -->

-   **Users:**  
    Users who have saved this post to their collection will be displayed here. Click on a
    user's **picture** to get to their user page. Click on the **book
    symbol** below to view this post in the user's collection.  
    ![ <File:userswue.png>](communityposts/userswue.png  "fig: File:userswue.png")  

<!-- -->

-   **Comments and Reviews:**  
    Comments, reviews and ratings for this post (if existing). Also, you
    can [ write your own comment or rating](DiscussionReviewsComments "wikilink").  
    ![ <File:commentswue.png>](communityposts/commentswue.png  "fig: File:commentswue.png")  

<!-- -->

-   **Cite this publication:**  
    Citation of this post in different **citation styles**, e.g.
    Harvard, APA, BibTeX and EndNote.

}

-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").
  

