<!-- de: Follower -->

## Followers
---------

When looking for interesting bookmarks and publicatons in ${project.name},
a natural way is to search for other users who have similar interests.
To make this process easier for you, there is a possibility that you can **follow**
one or more users. When you follow a user, it is easy to keep track of all his interesting
posts.
The list of followers can be understood as a list of people with similar interests as the ones 
you have.


---------------------------------------------------------------------------------

### Follow users


![ <File:follow.png>](followers/follow.png  "fig: File:follow.png")


To follow a user, you just need to click the **"follow"** button you find in the sidebar
of each userpage beneath the user's name. 


![ <File:unfollow.png>](followers/unfollow.png  "fig: File:unfollow.png")


The names of the users you are following are shown in the sidebar on the right on the [followers page](https://www.bibsonomy.org/followers). You can find this page by clicking **"myBibSonomy"** in the left main menu. Then choose **"followed posts".**


To **unfollow** a user, click the red **"delete"** button behind the username.
In the sidebar you can also find users who follow you and simliar users to you
who could be interesting to follow.

----------------------------------------------------------------------------------

### Find users to follow

There are several ways to find a user who could be interesting for you to follow. 


 -  If you know their username you can search it directly via **search option**.
	In the search bar on the upper right side, select the option **"user"**, then type 
	in the name of the user you want to find.
    

![ <File:searchname.png>](followers/searchname.png  "fig: File:searchname.png")


 -  You can check the usernames of users who have created entries that are interesting 
	for you in the list of publications and bookmarks.
    

![ <File:namepublication.png>](followers/namepublication.png  "fig: File:namepublication.png")


 -  You can also find more people with similar interests if you click on the name of a publication. 
	In the right sidebar you can see a list of people's usernames who have added this publication 
    to their collection as well.
    

![ <File:collection.png>](followers/collection.png  "fig: File:collection.png")

-----------------------------------------------------------------------------------

### Entries of followed users

The [followers page](https://www.bibsonomy.org/followers) summarizes the recent posts of all users you are following.
The most relevant entries for you are shown on top of the list.

If you only want to see the entries of one specific person you follow, click
on their username that you will find in the right sidebar where the users you are 
following are listed: now only entries of the selected user will be shown.


![ <File:sort.png>](followers/sort.png  "fig: File:sort.png")


You can also change the applied **ranking method** of the publications by clicking the **symbol with the arrow with four bars**: in the appearing menu you can choose to sort them by date, name or ranking.


![ <File:ranking.png>](followers/ranking.png  "fig: File:ranking.png")


If you choose the option **"ranking"**, you will find the sections **"settings"** in the right sidebar, where you can choose between the ranking methods [**"tf-idf"**](https://en.wikipedia.org/wiki/Tf%E2%80%93idf) or **"tag overlap"**.
You can also choose whether to apply normalization during the ranking computation.

To confirm your selections, click the **"go"** button.

-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").