<!-- de: MitNutzeraccountAnmelden -->

## Sign in with a ${project.name} user account
--------------------------------------------

**Goal:** This tutorial shows you how to sign in to ${project.name}
with an existing user account.

**Requirements:** You need to have a user account on
${project.name}. If you don't have an account yet, this guide shows
you [how to create a new user account](CreateNewAccount "wikilink").

-------------------------

### Instructions

1.  Click on the **"sign-in"**-button on the homepage.

    ![ <File:1.png>](signinwithuseraccount/1.png  "fig: File:1.png")
    
2.  A new window will pop up. Enter your username and your password.
 
    ![ <File:2.png>](signinwithuseraccount/2.png  "fig: File:2.png")

3.  Finally, click on the **"sign in"**-button. If your username and
    your password have been accepted, then the login box disappears and
    you will be redirected to ${project.name}.

------------------------------------------------------------------------

[Click here to go back to beginner's area and learn more about the basic functions](MainBeginner "wikilink").
