<!-- de: TagLayout -->

## The layout of your tag box
--------------------------

To change the layout of your tagbox, click on the person icon in the [
right main menu](User_Interface "wikilink"), click on [ settings](Settings "wikilink") and then on the tab **"settings"**. Under
**"layout of your tag box and post lists"**, you have several options to
edit your tagbox' layout.  
  
![ <File:formular.png>](taglayout/formular.png  "fig: File:formular.png")  

- **show tags as:** Tags can either be displayed as **cloud** or as **list**.   
![<File:tagcloud-list.png>](taglayout/tagcloud-list.png  "fig: File:tagcloud-list.png")  

- **sort tags by:** Tags can be sorted by **alphabet** or their **frequency**.    
![<File:tagalpha-freq.png>](taglayout/tagalpha-freq.png  "fig: File:tagalpha-freq.png")

- **tooltips for tags:** A tooltip can be displayed when moving the
    mouse cursor over a tag. The options here are **yes** or **no**.  
  
- **choice of tags:** The amount of displayed tags can be limited
    according to your choice. There are two options:

    -   **top X:** Only the X most frequent tags are displayed.
    -   **min Frequency:** Only the tags with a frequency of at least
        the minimum frequency X are displayed.  
        You can set the tag threshold X below.  
          
-   **tag threshold:** This value has different meanings depending on
    your choice of tags.
   
    -   **top X:** The amount of tags that are displayed.  
        Example: For a tag threshold value of X = 5, only the five most
        frequent tags are displayed.  
    -   **min Frequency:** The minimum frequency of a tag.  
        Example: For a tag threshold value of X = 5, only the tags with
        a frequency greater or equal to five are displayed.

-    **posts per page:** This value shows the maximum number of posts that are displayed on a page.  
  
-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").