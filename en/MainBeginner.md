<!-- de: MainEinsteiger -->

## Help for beginners
------------------

This is the **help page for beginners**. For further information, please visit the
[ help page for advanced users](MainAdvanced "wikilink") and the [ help page for software developers](MainDeveloper "wikilink"). 


${project.name} is a free service to manage your bibliographic
references and publications. In addition, ${project.name} allows you
and your colleagues to work together in [groups](GroupFunctions "wikilink") (e.g. exchange
interesting bookmarks and publications). This page shows you all basic
functions of ${project.name}.

----

${project.theme == "puma"}{

![ <File:Race-Flag.png>](mainbeginner/Race-Flag.png  " File:Race-Flag.png")

### Unlock your library card for PUMA services


To use PUMA, you only need to unlock your library card for PUMA
services. The following instructions show you [ how to use your library
card for authentication](How-toUnlockLibraryCardFor_PUMA "wikilink").

}

${project.theme == "bibsonomy"}{

![ <File:Race-Flag.png>](mainbeginner/Race-Flag.png  " File:Race-Flag.png")

### Sign in to BibSonomy / Create a new BibSonomy account


There are two ways how to sign in to BibSonomy:  

-   [ Use BibSonomy with an existing OpenID account](SignInWithOpenIDAccount "wikilink")

<!-- -->

-   [ Sign in with a BibSonomy user account](SignInWithUseraccount "wikilink")


If you *haven't got a BibSonomy user account yet*, read here how to  

- [ Create a new BibSonomy user account](CreateNewAccount "wikilink")  
  

Further help on user accounts:  

-   [ How to recover your password](PasswordRecovery "wikilink")  

<!-- -->

-   [ How to delete my BibSonomy account](DeleteAccount "wikilink")

}

----

![<File:Student-Read-04.png>](mainbeginner/Student-Read-04.png  " File:Student-Read-04.png")

### The basic functions of ${project.name}


Here you can find quick and easy explanations for the most important
functions in ${project.name}.  

-   [ The User Interface](User_Interface "wikilink")

<!-- -->

-   [ Manage literature and bookmarks (add, store & search)](BookmarkPublicationManagement "wikilink")

<!-- -->

-   [ Use tags to optimize your search](Tags "wikilink")

<!-- -->

-   [Bookmark import](ImportBookmarks "wikilink")

<!-- -->

-   [Bookmark export](ExportBookmarks "wikilink")

<!-- -->

-   [ Bibliography import (e.g. from Citavi, EndNote, etc.)](AddPublications "wikilink")

<!-- -->

-   [ Bibliography export (e.g. to EndNote, RDF, HTML or BibTeX)](ExportData "wikilink")

<!-- -->

-   [ Install Browser Plugin (Google Chrome, Mozilla Firefox or Apple Safari)](${project.home}buttons "wikilink")

<!-- -->

-   [ Use the Discovery Service of your library](DiscoveryService "wikilink")


----

![ <File:News.png>](mainbeginner/News.png  " File:News.png")

### Press Distribution


Information for media representatives and other interested parties on
${project.name} and related projects:  

-   [ Our Projects](Projects "wikilink")

<!-- -->

-   [ Press Distribution](Press "wikilink")


----

${project.theme == "bibsonomy"}{


![<File:Messages-Information-01.png>](mainbeginner/Messages-Information-01.png  " File:Messages-Information-01.png")

### Contact the developers of ${project.name}


You need help? You have questions about ${project.name}? You need a
special feature or you have an idea for improvement? Contact us! Here is
our contact data. We appreciate your feedback.  

------------------------

![<File:Message-Mail.png>](mainbeginner/Message-Mail.png  "fig: File:Message-Mail.png")
<webmaster@bibsonomy.org>

-   Contact us via email.  

-----------------------------------

![<File:WordPress.png>](mainbeginner/WordPress.png  "fig: File:WordPress.png")
[blog.bibsonomy.org](http://blog.bibsonomy.org)

-   Feature of the week
-   Releases-News
-   Questions, feature-requests and many more

-------------------------

![ <File:Twitter+Bird.png](mainbeginner/Twitter+Bird.png  "fig: File:Twitter+Bird.png")
[Join "BibSonomyCrew" on Twitter](https://twitter.com/BibSonomyCrew)

-   Updates via Tweet

----------------------------

![<File:Conference-Call.png>](mainbeginner/Conference-Call.png  "fig: File:Conference-Call.png")
For all friends of the classic mailing list:
<bibsonomy-discuss@cs.uni-kassel.de>

-   Discuss with us about new features
-   Discussions are public and all Users can see the questions and
    answers

}

--------------------

Here you can see the **help page for beginners**. For further information, please visit the
[ help page for advanced users](MainAdvanced "wikilink") and the [ help page for software developers](MainDeveloper "wikilink"). 

  


