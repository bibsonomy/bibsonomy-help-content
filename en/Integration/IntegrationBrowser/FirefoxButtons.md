<!-- de: Integration/IntegrationBrowser/FirefoxButtons -->

---------------------------

[`Back to Integration in any browser.`](Integration/IntegrationBrowser "wikilink")

---------------------

## Integration in Firefox with ${project.name} Buttons
----------------------------------

You can download a free add-on for Firefox, which implements some functions of ${project.name} in your browser.
Open your add-on tab (about:addons or Ctrl+Shift+A) and search for ${project.name} **"Buttons"**. You can also follow [this link](https://addons.mozilla.org/en-US/firefox/addon/bibsonomy-buttons/?src=search). 

${project.theme == "puma"}{

If you would like to install the add-on for the system "PUMA" instead, follow [this link](https://www.bibsonomy.org/puma_files/firefox/puma_buttons-latest.xpi). At the moment, it is not possible to provide the add-on via the official Firefox store.

}

After adding it to Firefox, its surface is expanded by a new button.

-------------

![ <File:MainButton.png>](FirefoxButtons/MainButton.png  "fig: File:MainButton.png")

This button leads to a small menu with four different buttons, which offer following functions:

1. **Navigate to ${project.name}**:
By using this button, you are redirected to your own ${project.name}-site.

2. **Store a bookmark in ${project.name}**:
Use this button to create a bookmark for ${project.name}. The URL, title and description of your currently visited website are automatically saved.

3. **Store a publication in ${project.name}**:
Use this button to store a publicaton in ${project.name}. As much metadata as possible is gathered from the publication.

4. **Settings**: 
	Here you can change the following settings:
	- First, you can decide if the desired function should be opened in a new tab. Check this box if you still need your current tab. Then, a new tab will take you to the appropriate ${project.name}-page. Here you can then continue with your desired action. 
	- Second, it is possible to decide whether posts should be visible or invisible. Check **public** if you want to share your new bookmarks and publications with other users of $ {project.name}. If you do not want this, check **private**.
	- ![ <File:ButtonSettingsEN.png>](FirefoxButtons/ButtonSettingsEN.png  "fig: File:ButtonSettingsEN.png")

