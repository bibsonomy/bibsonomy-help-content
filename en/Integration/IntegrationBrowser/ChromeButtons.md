<!-- de: Integration/IntegrationBrowser/ChromeButtons -->

---------------------------

[`Back to Integration in any browser.`](Integration/IntegrationBrowser "wikilink")

---------------------

## Integration in Google Chrome with ${project.name} Buttons
----------------------------------

${project.theme == "bibsonomy"}{

You can download a free add-on for Chrome, which implements some functions of ${project.name} in your browser.
Open your add-on tab (chrome://extensions) and search for **"BibSonomy Buttons"**. You can also follow [this link](https://chrome.google.com/webstore/detail/bibsonomy-buttons/lbbnooihfnhphbgeajgmpmaedkdjgeid?hl=en). 
}

${project.theme == "puma"}{
You can download a free add-on for Chrome, which implements some functions of ${project.name} in your browser.
Open your add-on tab (chrome://extensions) and search for **"PUMA Buttons"**.
You can also follow [this link](https://chrome.google.com/webstore/detail/puma-buttons/emdfjhapplcngnmhllpdmaecgoafjjmc?hl=en).
}

After adding it to Chrome, its surface is expanded by a new button.

![ <File:MainButtonChrome.png>](ChromeButtons/MainButtonChrome.png  "fig: File:MainButtonChrome.png")

This button leads to a small menu with four different buttons, which offer following functions:

1. **Navigate to ${project.name}**:
By using this button, you are redirected to your own ${project.name}-site.

2. **Store a bookmark in ${project.name}**:
Use this button to create a bookmark for ${project.name}. The URL, title and description of your currently visited website are automatically saved.

3. **Store a publication in ${project.name}**:
Use this button to store a publicaton in ${project.name}. As much metadata as possible is gathered from the publication.

4. **Settings**: 
	Here you can change the following settings:
	- You can decide if the desired function should be done in a new tab. Check the first box if you still need your current tab. Then, a new tab will take you to the appropriate ${project.name}-page. Here you can  continue with your desired action. 
	- It is also possible to decide whether posts should be visible or invisible. Check **public** if you want to share your new bookmarks and publications with other users of $ {project.name}. If you do not want this, check **private**.
	- ![ <File:ButtonSettingsEN.png>](ChromeButtons/ButtonSettingsEN.png  "fig: File:ButtonSettingsEN.png")

