<!-- de: Integration/IntegrationProgramme/IntegrationBibLaTeX -->

---------------------------

[`Back to Integration in any program.`](Integration/IntegrationProgram "wikilink")

---------------------

## Integration with BibLaTeX
--------------------------------------------

[BibLaTex](http://mirrors.rit.edu/CTAN/help/Catalogue/entries/biblatex.html) is an extension for BibTex. 
It gives you many more options to easily configure your bibliographies and citations. 
It is e.g. now possible to just add the following two lines to the LaTeX project header to 
not having to bother with different keys and inconsistent bibliographies:

        \usepackage[backend=biber]{biblatex}
        \addbibresource[location=remote]
        {http://www.bibsonomy.org/bib/user/thoni/myown}

Now every time the PDF file is compiled, the list of publications in BibSonomy with the tag "myown" 
from the user "thoni" will be downloaded. 

To print your bibliography, you just need to type

        \printbibliography

You can find a more detailed tutorial [here](https://www.sharelatex.com/blog/2013/07/31/getting-started-with-biblatex.html). 
