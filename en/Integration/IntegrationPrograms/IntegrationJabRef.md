<!-- de: Integration/IntegrationProgramme/IntegrationJabRef -->

---------------------------

[`Back to Integration in any program.`](Integration/IntegrationProgram "wikilink")

---------------------

## Integration with JabRef
--------------------------------------------

[JabRef](http://www.jabref.org/) is open source literature management
software and uses the BibTeX format as the standard format.

**${project.name} plugin**  
*Caution: Since JabRef version 2.12, plugins are no longer supported in
JabRef!*  
To exchange literature data between ${project.name} and JabRef easier
and faster, you can use the ${project.name} plugin for JabRef. Read
more information on [installation](JabrefInstall "wikilink") and [usage](Jabref "wikilink").

**Extract Jabref-layouts from Jar-files**  
In JabRef, you can export your literature data in user-defined formats.
These formats are called layouts und are defined in layout files. On our
[developer page](http://dev.bibsonomy.org/maven2/org/bibsonomy/bibsonomy-layout/), you can download the currently available layouts as a JAR file. You can
open this file using an unzip tool like WinZip. You will find the
layouts under the path `\\org\\bibsonomy\\layout\\jabref`.

To use a layout, you have to extract its `'.begin'` and `'.end'` files
in combination with the respective layout-file. For example, if you want
to use the Harvard HTML JabRef layout, you have to extract the files
`'harvard.layout'`, '`harvard.begin.layout'` and `'harvard.end.layout'`.