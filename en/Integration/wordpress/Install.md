<!-- de: Integration/wordpress/Installation -->

## BibSonomy CSL Installation
----------------

To install **Bibsonomy CSL**, log into your WordPress installation as an administrator 
and search in the plugin install menu for BibSonomy CSL.
Install the "BibSonomy CSL and BibSonomy Tag Cloud Widget" in the install menu. 
(Alternatively you can download the plugin from [here](http://wordpress.org/extend/plugins/bibsonomy-csl/), and unzip it and upload it by using a FTP client).

After the installation is complete, you will find the new item **BibSonomy CSL** in the settings menu. 
Click it and enter your ${project.name} username and the [BibSonomy API key](https://www.bibsonomy.org/settings?selTab=1). Save the settings.

![ <File:1.png>](Install/1.png  "fig: File:1.png")

Now you can create a new blog post or a new static page in the usual way.