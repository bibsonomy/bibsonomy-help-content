<!-- de: Integration/wordpress/Tagwolken -->


## Add Tagclouds to your WordPress blog
--------------------

Adding a **${project.name} Tag Cloud** in your blog is very easy. 
Switch to the widget page using the design menu item.
  
 
![ <File:3.png>](Tagclouds/3.png  "fig: File:3.png") 
  
  
Drag the ${project.name} Tag Cloud Widget to the prefered position, fill out the form, choose a layout style and save the settings. 