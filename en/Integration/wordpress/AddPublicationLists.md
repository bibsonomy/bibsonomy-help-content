<!-- de: Integration/wordpress/Publikationslisten -->

## Add publication lists to your WordPress blog

--------------------------

When you create a new blog post or a new static page in the usual way, you can find a new Meta Box 
called **"Add ${project.name} Publications"** at the bottom.
You can choose between **user**, **group** or **viewable** to select the content from ${project.name}. 


Here is an example: Assume, you want to publish your own publications in a blog post 
(and in ${project.name} all own publications are tagged with [myown](http://blog.bibsonomy.org/search/label/myown) ). 
Choose **"user"** as content source type and enter your ${project.name} username as **content type value**. 
Now you have to filter your selection by using the tag **"myown"**. 
For this, enter "myown" in the input field for tags. 
If you want to select more than one tag, you have to separate them by a space character. 
Optionally, you can limit the number of publications.

Now you can choose one of the pre-installed citation styles to layout your publication list. 
If your desired CSL style isn't contained in the default selection, then you can select a custom style
by entering the url of the stylesheet. 
The Citation Style Language (CSL) is an open XML-based language to describe the formatting of citations and bibliographies. 
A large list of free available styles can be found [here](http://www.zotero.org/styles/).


![ <File:1.png>](AddPublicationLists/1.png  "fig: File:1.png")


To manipulate the look you can customize the CSS. An example is set by default. If you don't like it, simply delete it.


![ <File:2.png>](AddPublicationLists/2.png  "fig: File:2.png")


Save your post and take a preview. If it's necessary you can customize your settings or CSS definitions.

*Warning: Be aware, that you are using your own account to retrieve the posts from BibSonomy. That means, that all posts, that are visible to you in ${project.name} (you private ones too), will be made visible on your blog, if they fit the description in the Meta Box. (In our example: all posts you have tagged with **"myown"**.)*


