<!-- de: Integration/IntegrationWebseiten -->

---------------------------

[`Back to Integration overview page.`](Integration "wikilink")

---------------------

## Integration with Websites
--------------------------------------------

### Confluence

An interesting tool from Christian Schenk is the [Confluence plugin](http://www.christianschenk.org/projects/confluence-bibsonomy-plugin/). The plugin allows you to show a tag list/cloud and the most recent
BibTeX posts on any page in [Confluence](http://www.atlassian.com/software/confluence).

--------------------------------------------

### GoogleDocs

This ${project.name} addon for Google Docs allows you to cite directly
from ${project.name} in Google Docs. Follow [these instructions](AddonGoogleDocs "wikilink") to learn how to do so.

--------------------------------------------

### GoogleScholar

With the ${project.name} Scholar Plugin for Chrome, you can connect
${project.name} and GoogleScholar. It is then possible to manage
publications directly in the GoogleScholar web interface. The extension
can be added at the [Chrome Web Store](https://chrome.google.com/webstore/detail/bibsonomy-scholar/nfncjdnkilenkgnhchaapiiboaimpoon).
More information on the GoogleScholar extension can be found in the
${project.name} [blog](http://blog.bibsonomy.org/2015/12/feature-of-week-bibsonomy-scholar.html).

--------------------------------------------

### Moodle

Moodle is a popular platform for E-Learning. ${project.name} can
enhance course descriptions and E-Learning projects by offering
literature, e.g. in form of RSS feeds. A module description as well as
instructions for course administrators and users can be found on the [Moodle help page](Moodle "wikilink") and [here](https://moodle.org/plugins/mod_pbm).