<!-- de: Integration/IntegrationEigeneWebseite -->

---------------------------

[`Back to Integration overview page.`](Integration "wikilink")

---------------------

## Integration with your own website
--------------------------------------------

There are several ways to incorporate ${project.name} content or links
to ${project.name} into your own website.

--------------------------------------------

### Bookmark Links

Many websites provide links or buttons on their web pages to allow
visitors to share it easily in social networks or bookmarking services.
You can add such a link for ${project.name} on your own website or blog
using a short JavaScript snippet that can be found on the [ Browser
Add-ons & Bookmarklets page](${project.home}buttons "wikilink") and on the [JavaScript codesnippet page](JS-Codesnippet "wikilink").

--------------------------------------------

### Tag Clouds

${project.name} offers the possibility to integrate its tag clouds, for
example for your publications, into your private website. For this
purpose ${project.name} provides a JSON feed for tags which is
processed by JavaScript snippets. At the moment this feature is only
available for the BibTeX page, but will be extended to all pages
offering tag clouds in the near future.  
Have a look at the example and test the different arguments in the form.
The tag cloud belongs to the following publication:  

*Andreas Hotho and Robert Jäschke and Christoph Schmitz and Gerd Stumme.
[BibSonomy: A Social Bookmark and Publication Sharing System](${project.home}/bibtex/1d28c9f535d0f24eadb9d342168836199 "wikilink").
Proceedings of the Conceptual Structures Tool Interoperability Workshop, 2006.*

Proceed as follows to integrate a tag cloud into your page:

-   Insert these lines into your HTML code where the tag cloud shall be
    shown:

        <div id="tags"> 
        <div class="title">${project.name} Tags</div> 
        <ul id="tagbox" class="tagcloud"></ul> 
        </div>

-   Include a reference to the processing JavaScript file (The
    JavaScript file can be found [here](TagCloud.js "wikilink")):

        <script type="text/javascript" src="tagCloud.js"> 
        </script>

-   If you want to format your tag cloud like in the example above, use
    this [ CSS file](TagCloud.css "wikilink") and include it as follows.
    If you do not like this particular style or it just does not fit to
    your existing website style, you can of course use your
    own stylesheets.

        <link rel="stylesheet" type="text/css" href="tagCloud.css" />

-   Finally, you need to activate the JavaScript function
    `getTags()`. You have to enter three arguments.
    -   First, the URL providing the JSON feed. For example
        `/json/tags/bibtex/1d28c9f535d0f24eadb9d342168836199` for all
        tags related to that publication.
        
    -   The second argument is the limit of tags to retrieve.
    -   Finally, the order of the tags. You can choose between an
        alphabetic order using the keyword `alpha` or a tagcount order
        using the keyword `frequency`.  
        
        The following example retrieves 25 tags related to that
        publication in alphabetic order. The tag cloud will be filled in
        dynamically with the retrieved tags.  
        `getTags(/json/tags/bibtex/1d28c9f535d0f24eadb9d342168836199, 25, "alpha")`

You can find another tutorial on how to integrate tag clouds into your
website [ on this page](Flash-JavaScript-Tag-Cloud "wikilink").  

--------------------------------------------

### Post Lists

Embed a list of posts (e.g. your own publications) on your own website
in the same way they look in ${project.name}. Just include an iframe in
your HTML code that looks like this:

    <iframe name='my publications' 
        src='<URL>' 
        height='400' 
        width='100%' 
        style='border: none;'>
    <p>
      Unfortunately, your browser is not capable of showing embedded frames (iframes).
    </p>
    </iframe>

The URL can be any ${project.name} page, e.g. your user page or the page of your group. It
is important to add the URL parameter `format=embed`. For example,
<http://www.bibsonomy.org/user/jaeschke/myown?items=1000&resourcetype=publication&sortPage=year&sortPageOrder=desc&format=embed>
displays all (up to 1000) publication posts of the user *jaeschke* that
are tagged with "myown", sorted by year descending.

--------------------------------------------

### JSON feed

For every ${project.name} page you can get a [JSON feed](http://www.json.org/) of it by prepending `json/`
to the path part of the URL, e.g., to get the JSON feed for [/tag/json](${project.home}tag/json "wikilink"), use the page [/json/tag/json](${project.home}json/tag/json "wikilink").

This gives you an [Exhibit](http://www.simile-widgets.org/exhibit/) compatible JSON feed including all bookmark
and publication posts of the respective page. To include the JSON feed into
your Exhibit, add a link to it into the header of your Exhibit HTML code.

${project.theme == "bibsonomy"}{

    <link href="http://www.bibsonomy.org/tag/json?callback=cb" type="application/jsonp" rel="exhibit/data" ex:jsonp-callback="cb" />

}

${project.theme == "puma"}{

    <link href="http://puma.uni-kassel.de/tag/json?callback=cb" type="application/jsonp" rel="exhibit/data" ex:jsonp-callback="cb" />

}

Have a look at this [list of publications](http://www.kde.cs.uni-kassel.de/hotho/publication_json.html) to see what is possible using JSON and Exhibit
with only some lines of HTML.

Users of our existing JSON feed should note that the format was slightly
changed:

-   The tags are now contained in the list with the key `items` and not
    `tags`.
-   The name of the tag is expressed using the key `label` and not
    `name`.
-   There are two more lists (`types` and `properties`) to allow easier
    Exhibit integration.

--------------------------------------------

### TYPO3

Our [TYPO3 extension BibSonomy CSL](http://typo3.org/extensions/repository/view/ext_bibsonomy_csl)
allows you to create publication lists containing bibliographic
references stored at ${project.name} or tag clouds from your
${project.name} tags. The extension helps researchers to easily
organize publication lists in TYPO3, both for personal home pages or to
list publications of a research group or project. With BibSonomy CSL you
can display publication lists/tag clouds choosing one of many available
bibliographic styles from the Citation Style Language (CSL). Individual
styles can easily be added. Furthermore, you may choose to make
documents (attached to the ${project.name} posts) available for
download, to provide preview images and to reference BibTeX or Endnote.
To install the extension please visit the [official BibSonomy CSL page](http://typo3.org/extensions/repository/view/ext_bibsonomy_csl) in
the TYPO3 extension repository. The TYPO3 extension is an [open source
project](https://bitbucket.org/bibsonomy/bibsonomy-typo3-csl).  
For more information, read the [ tutorial about TYPO3 integration](Typo3 "wikilink").

${project.theme == "puma"}{

--------------------------------------------

### VuFind

The open source project [VuFind](http://vufind-org.github.io/vufind/)
provides cataloguing, search, and repositorial facilities to libraries.
PUMA can be added to a running VuFind replacing its favorites list, thus
adding such benefits as tagging and filtering the favorite resources and
offering various export formats. To install PUMA at your own institution
or to connect it to your VuFind installation please contact the [PUMA team](http://www.academic-puma.de/uber-uns/kontakt/).

}

--------------------------------------------

### WordPress

For WordPress there is a plugin to create publication lists from posts
stored in ${project.name}. To render the list, you can choose one of
many available bibliographic styles from the Citation Style Language
(CSL). Individual styles can easily be added. The plugin allows the
direct integration with ${project.name}: You can integrate publication
lists into your blog posts or pages. It allows you to offer your
documents (attached to your posts) for download and to render preview
images.  
For further information and download, read our [WordPress tutorial](wordpress "wikilink").

--------------------------------------------

### XWiki

To integrate ${project.name} into a XWiki page, proceed as follows:

1.  Make sure the XWiki RSS Feed Plugin is installed in your XWiki
    instance by editing your `xwiki.cfg` file (it is installed
    by default).
2.  Edit the wiki page where you want to display ${project.name} data.
3.  Enter the following code activating the XWiki RSS macro  
    `rss:feed=/publrssN/user/username?items=1000`  
    where the feed parameter is linked to the RSS data you want to get
    displayed on that page.
4.  Save the page.

The ${project.name} data should then be displayed on your page.

If you want to customize the way each feed item gets displayed, you can
use XWiki Velocity or Groovy Scripting features as shown in this Groovy
example: 

    feed =}} 
    {{xwiki.feed.getFeeds("http://www.bibsonomy.org/publrssN/user/username?items=1000")}} 
    {{for (entry in feed.entries) {}} \\ {{desc = entry.getDescription().value}} 
    {{println("{pre}")}} 
    {{println(desc)}} 
    {{println("{/pre}")}} 
    {{ } 


Example: [Nepomuk publications](${project.home}user/nepomuk/nepomuk "wikilink")

--------------------------------------------

### Zope

You can integrate ${project.name} content into the content management
system of [Zope](http://www.zope.org/).

**Posts**  
Lists of posts can be displayed using the ${project.name} [RSS feed](Integration#RSS "wikilink") on your Zope page. Have a look at the detailed description of the [RDF Summary](http://www.zope.org/Members/EIONET/RDFSummary/) product of Zope.  

**Tag clouds**  
In Zope, tag clouds can be created following our [tutorial](TagcloudOnZopePages "wikilink").
