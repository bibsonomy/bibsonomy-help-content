<!-- de: Integration/IntegrationProgramme -->

---------------------------

[`Back to Integration overview page.`](Integration "wikilink")

---------------------

## Integration with Programs
--------------------------------------------

You can integrate ${project.name} in several programs.
Use the following links for instructions:

* [BibLaTeX](Integration/IntegrationPrograms/IntegrationBibLaTeX)

* [Citavi](Integration/IntegrationPrograms#Citavi)

* [Emacs](Integration/IntegrationPrograms#Emacs)

* [JabRef](Integration/IntegrationPrograms/IntegrationJabRef)

* [KBibTeX](Integration/IntegrationPrograms#KBibTeX)

* [Sublime Text](Integration/IntegrationPrograms#SublimeText)

* [TeXlipse](Integration/IntegrationPrograms#TeXlipse)

<br/>
<br/>

--------------------------------------------

### Citavi

You can exchange articles between the reference manager software
[Citavi](http://www.citavi.de/de/index.html) and ${project.name}.
Instructions for that are given in [this tutorial](Citavi "wikilink").

--------------------------------------------

### Emacs

[Emacs](http://www.gnu.org/software/emacs/) is a powerful, customizable text editor. The package [AUCTeX](http://www.gnu.org/software/auctex/)
turns Emacs into a comfortable LaTeX environment. In our [blog post](http://blog.bibsonomy.org/2011/09/feature-of-week-access-your-posts-in.html)
we describe how to setup Emacs to automatically make use of BibTeX
references stored in ${project.name}.

--------------------------------------------

### KBibTeX

[KBibTeX](http://home.gna.org/kbibtex/) is a KDE application to manage your publications offline similar to
[JabRef](http://jabref.sourceforge.net/). It integrates several online services and allows to store the found data.
It also supports searching in ${project.name}.

--------------------------------------------

### SublimeText

The text editor [Sublime Text](http://www.sublimetext.com/) has a LaTeX type setting plugin called [Latexing](http://www.latexing.com/). The
plugin offers integration with ${project.name} and is explained in a [tutorial](http://docs.latexing.com/stable/tutorials/setup-bibsonomy-with-latexing.html).

--------------------------------------------

### TeXlipse

[TeXlipse](http://texlipse.sourceforge.net/) is the LaTeX development plugin for [Eclipse](https://www.eclipse.org/). The TeXlipse
${project.name} extension adds ${project.name} support to that plugin
to manage your references. Installation and usage are documented in this
[tutorial](TeXlipseBibSonomyExtension "wikilink").
