<!-- de: Integration/IntegrationAPI -->

---------------------------

[`Back to Integration overview page.`](Integration "wikilink")

---------------------

## Integration with REST API
--------------------------------------------

${project.name} provides a webservice using Representational State
Transfer (REST), a software architectural style for distributed
hypermedia systems. The REST API is intended for developers who want to
develop applications which interact with ${project.name}.
You can use the provided client library, written in Java to access the
API or you can directly interact with the webservice if you would like
to write a client in a language other than Java.  
To get access to the API, you will need your API key. You can find it on
the [ settings page](${project.home}settings "wikilink") in the tab "settings".  

-   [REST-API repository](http://dev.bibsonomy.org/maven2/org/bibsonomy/bibsonomy-rest-client/)
-   Instructions: [Documentation on BitBucket](https://bitbucket.org/bibsonomy/bibsonomy/wiki/documentation/api/REST%20API), [web presentation](http://www.academic-puma.de/rest-training/#/)
-   [XML Schema](https://bitbucket.org/bibsonomy/bibsonomy/wiki/documentation/api/XML%20Schema)
-   [Javadoc documentation](http://www.bibsonomy.org/help/doc/rest-client-javadoc/index.html)  

--------------------------------------------

### Java

You can access the REST API by the programming language Java.  

- [Examples](https://bitbucket.org/bibsonomy/bibsonomy/wiki/documentation/api/Java%20API%20Examples)
-   [Source code](https://bitbucket.org/bibsonomy/bibsonomy/src/tip/bibsonomy-rest-client/?at=default)  

--------------------------------------------

### PHP

Restclient PHP is a package of PHP scripts containing a RESTClient and
some utilities that are useful for the development of PHP applications
that shall interact with the ${project.name} REST API. The RESTClient
maps functions provided by the ${project.name} REST API.  

-   [BitBucket repository](https://bitbucket.org/bibsonomy/restclient-php)
-   [Demo files and examples](http://www.academic-puma.de/rest-training/restclient_training.zip)

--------------------------------------------

### Python

There is an API client to retrieve posts using the programming language
[Python](http://www.python.org/).  
The provided [export function](https://bitbucket.org/bibsonomy/bibsonomy-python/src/tip/onefile.py?fileviewer=file-view-default) allows you to download posts and store them in a file. 
Read more in the [Instructions](https://bitbucket.org/bibsonomy/bibsonomy-python).

-   [Download](https://pypi.python.org/pypi/bibsonomy)
-   [Instructions](https://bitbucket.org/bibsonomy/bibsonomy-python)
