<!-- de: Integration/IntegrationBrowser -->

---------------------------

[`Back to Integration overview page.`](Integration "wikilink")

---------------------

## Integration with Browser
--------------------------------------------

### Chrome / Chromium

Add three useful ${project.name} control buttons to your
Chrome/Chromium browser by installing this add-on. One will be a quick
link to your my ${project.name} page. The other two buttons enable you
to store bookmarks and publications in ${project.name} by only clicking
a button or by using keyboard shortcuts. You can find the description
and the download on the [Browser Add-ons & Bookmarklets page](http://www.bibsonomy.org/buttons).
For instructions on how to use the add-on, visit [this page](Integration/IntegrationBrowser/ChromeButtons "wikilink").

--------------------------------------------

### Firefox

**Add on**  
Add three useful ${project.name} control buttons to your Firefox
browser by installing this add-on. One will be a quick link to your
my ${project.name} page. The other two enable you to store bookmarks and
publications in ${project.name} by only clicking a button or by using
keyboard shortcuts. Please find description and download on the [Browser
Add-ons & Bookmarklets page](http://www.bibsonomy.org/buttons).
For instructions on how to use the add-on, visit [this page](Integration/IntegrationBrowser/FirefoxButtons "wikilink").

**Search**  
You can search for content from ${project.name} directly in the Firefox
address bar.  

-  First, you have to create a new bookmark. To do so, click on **"Show
    your bookmarks"** (on the right next to the bookmark star symbol) and
    then on **"Show All Bookmarks"**. On the left, click on **"Bookmarks
    Toolbar"** (or on any of the other options), then, above, click on
    **"Organize"** und finally on **"New Bookmark..."**.

-   **Search in your ${project.name} account**  
    When you want to search in your own ${project.name} account, enter
    the following into the input fields:  
    Name: `Search My BibSonomy`  
    Location: `www.bibsonomy.org/user/YOUR_USERNAME/%s`  
    Keyword: `bs`  
    Now you can enter the following search request in the Firefox
    address bar:  
    **bs myown 2015**, to find all entries of yours with the tags
    *myown* and *2015*.
    
-   **Global Search in ${project.name}**  
    When you want to search for entries in the entire ${project.name}
    system, enter the following into the input fields:  
    Name: `Global Search BibSonomy`  
    Location: `www.bibsonomy.org/search/%s`  
    Keyword: `as`  
    Now you can enter the following search request in the Firefox
    address bar:  
    **as java**, to find all entries in ${project.name} that are tagged
    with *java*.

    ![ <File:lesezeichen.png>](IntegrationBrowser/lesezeichen.png "File:lesezeichen.png")

**Zotero**  
[Zotero](http://www.zotero.org/) is an extension for Firefox and helps
you to collect, manage and cite publications. We support exporting
citation information from ${project.name} to Zotero as well as
importing entries from the Zotero library to ${project.name}. [ Click
here](Zotero "wikilink") to find out how to do so.

--------------------------------------------

### Safari

Add three useful ${project.name} control buttons to your Safari browser
by installing this add-on. One will be a quick link to your
my ${project.name} page. The other two buttons enable you to store
bookmarks and publications in ${project.name} by only clicking a button
or by using keyboard shortcuts. You can find the description and the
download on the [Browser Add-ons & Bookmarklets page](http://www.bibsonomy.org/buttons).
