<!-- de: Werkzeug -->

## Old System Tools
----------------

### GoogleSonomy (Firefox add on)

**This add on is only working with Firefox version 1.5 - 3.6.**  
GoogleSonomy is a Firefox extension that shows you ${project.name}
search results along your Google search. In the current version four
search modes are available: tag search as well as fulltext search (in
each case global and on user base). Furthermore GoogleSonomy adds a
bookmarkIt link to the google search results for fast posting in
${project.name}.

In the settings window below you can specify whether you want to
retrieve only bookmarks, publications or both and how many entries per
list should be displayed. You can download this extension from [Mozillas
Addon Page](https://addons.mozilla.org/de/firefox/addon/8855).

----------------

### Collect Bibtex entries from Latex aux files

**The perl script isn't available anymore.**  
The perl script you can download below collects all the BibTeX entries
referenced in a particular LaTeX file in aux format. It then requests
the corresponding BibTeX entries from ${project.name}.

Command to start the script:

`grabbib.pl username LaTeX aux file name > bibtexfile.bib`

[ Download](${project.home}help/tools/toolbox/downloads/grabbib.pl "wikilink")

-------------------

### Gnome deskbar integration

**The development for the gnome deskbar ended in 2010.**  
The goal of the deskbar is to provide one search interface to different
search systems such as Amazon, eBay, or your file system. You can
integrate ${project.name} into the [Gnome Deskbar](http://projects.gnome.org/deskbar-applet/) with the [ Gnome
Deskbar handler](${project.home}help/tools/toolbox/downloads/bibsonomy.py "wikilink") for ${project.name}, licensed under the GPL 2.
(Thanks to Jörn Dreyer). Just follow the steps below:

-   Download the script.
-   Move the script to `$HOME/.gnome2/deskbar-applet/handlers`.
-   Right click on the deskbar item and enable
    ${project.name} Bookmarks.
-   Click on *more*. A menu pops up in which you need to enter your
    ${project.name} username (see picture below).

[ Gnome Deskbar Handler](${project.home}help_de/attach/Werkzeug/Deskbar.png "wikilink")

When you enter a term in the deskbar it might take a few seconds before
the result of ${project.name} will appear.

--------------------

### Citesmart

**CiteSmart's website isn't available anymore.**  
[CiteSmart](http://www.miresoft.net/citesmart/) is a citation software supported by ${project.name}. This
software provides an easy way to extract data from our website and to
establish references in various formats for articles written in Word.
In this way, it facilitates the process of writing scientific articles and
supports the scientific work of researchers.

-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").