<!-- de: GruppenfunktionenAdmin/GruppeAdminMitgliederliste -->

---------------------------

[`Back to overview page on groupfunctions for admins.`](GroupFunctionsAdmin "wikilink")

---------------------

## Manage member list
-------------------------

Regular members of a group are only able to see the member list. They
cannot change anything.  

![<File:userMembers.png>](GroupAdminMembers/userMembers.png  "fig: File:userMembers.png")  

Admins of the group are allowed to **invite users** to the group,
**manage join requests** and **assign roles** to the group members.  

Available roles:

-   **Administrator:** This user has the most rights in a group. They
    are able to change group settings and manage join requests.
-   **Moderator:** Moderators are able to manage join requests, but they
    cannot change any group settings.
-   **User:** Regular users can use all availabe group functions, but
    they cannot change any group settings or manage join requests.