<!-- de: GruppenfunktionenAdmin/GruppeAdminTags-->

---------------------------

[`Back to overview page on groupfunctions for admins.`](GroupFunctionsAdmin "wikilink")

---------------------

## Define predefined tags
-----------------------------------

By selecting the tab "Groups" -> GROUPNAME -> Settings, group admins can select the tab "Predefined tag list":

![<File:TagsAnlegen.png>](GroupAdminTags/TagsAnlegen.png "fig: File:TagsAnlegen.png") 

Tags can be added and described in the input mask. The description is displayed later as a mouseover. 
As soon as you have entered all the information, click on Add.
The predefined tag then appears in the list.
You have the option of editing existing predefined tags by selecting a specific tag in the "Edit predefined tags" drop-down list and then editing it directly.
With "Update" the changed information is saved.