<!-- de: GruppenfunktionenAdmin/GruppeAdminAufloesen -->

---------------------------

[`Back to overview page on groupfunctions for admins.`](GroupFunctionsAdmin "wikilink")

---------------------

## Disband group
---------------------

To disband a group, type **"yes"** into the text field next to "Are you
sure?". **Be careful:** If you disband a group, **all group documents** will be lost.  

![ <File:delete.png>](GroupAdminDisband/delete.png  "fig: File:delete.png")  