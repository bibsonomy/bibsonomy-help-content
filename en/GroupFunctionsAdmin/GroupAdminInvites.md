<!-- de: GruppenfunktionenAdmin/GruppeAdminEinladungen -->

---------------------------

[`Back to overview page on groupfunctions for admins.`](GroupFunctionsAdmin "wikilink")

---------------------

## Group invites
---------------------

In the tab **member list** administrators and moderators of the group 
are allowed to **invite users** to the group and **manage join requests**.


![ <File:adminJoin.png>](GroupAdminInvites/adminJoin.png  "fig: File:adminJoin.png")  


**Group invites** can be viewed, accepted or rejected in the settings in the tab **"groups"**.  

![ <File:invite.png>](GroupAdminInvites/invite.png  "fig: File:invite.png") 