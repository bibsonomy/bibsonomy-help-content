<!-- de: GruppenfunktionenAdmin/GruppeAdminEinstellungen -->

---------------------------

[`Back to overview page on groupfunctions for admins.`](GroupFunctionsAdmin "wikilink")

---------------------

## Change settings
--------------------

Only the admin of a group is able to change the group's settings.

![<File:settings_neu.png>](GroupAdminSettings/settings_neu.png  "fig: File:settings_neu.png")  

-  Add **general information** and **contact data**

-   **Group settings**
    -   Member list: You can choose if the member list should be public,
        private or public for members.
    -   Shared documents: Choose if group members should have access to
        other members' documents (enabled) or not (disabled).
    -   User join request: Choose if other users should be able to send
        join requests (enabled) or not (disabled).
    
-   Access the group's **API key**

-   Change the group's **picture**