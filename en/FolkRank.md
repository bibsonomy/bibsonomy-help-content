<!-- de: FolkRank -->

## FolkRank
--------

From your previous experience with ${project.name} you may know that a
typical way of browsing is to select one or more tags to retrieve a list
of resources (e.g. bookmarks) **ordered by date**. This helps to
discover the **most recent** posts; but it does not present the **most
relevant** information considering the specific tag.

For finding highly relevant resources for a given tag, our research
group has developed an algorithm called [FolkRank](${project.home}bibtex/24d8b4f79814691fbe6db8357d63206a1/stumme "wikilink").
Its idea is similar to Google's PageRank algorithm, i.e. it analyzes the
link structure between users, tags and resources iteratively in order to
**calculate relevance**.

FolkRank can be accessed by adding `?order=folkrank` to the URL or
by clicking **"order by FolkRank"** under the search bar on a tag page.
For instance, when you analyze the results for the tag "www", you can
see that highly relevant bookmarks (in a research community) like the
WWW conference websites are found at the very top of the retrieved
list.  
  
Furthermore, FolkRank is able to calculate **highly "relevant" users**
for a given tag:

![ <File:relatedusers.png>](folkrank/relatedusers.png  " File:relatedusers.png")

The relevant users may be experts in a certain research area and a good
starting point in networking with colleagues interested in similar
topics as you are.

-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").
  

