<!-- de: DigitaleBibliotheken -->

## Digital Libraries
-----------------

With ${project.name}, users of digital library catalogues (Open Public
Access Catalogues, OPAC) are able to collect and manage the information
they retrieved from the catalogue.

------------------------------------------------------------------------

### Why combine social bookmarking with digital libraries?

Social bookmarking enhances search, collection and management of digital
information. It allows participants to contribute and share information.
Users can store the bibliographic information which they retrieved from
the OPAC library catalogue and annotate it with their personal comments.
They can also explore the resources of others, enlarging their own
collection with interesting objects and being guided through the vast
amount of information sources.

Here are some examples of how to integrate ${project.name} with OPAC
library catalogues.

------------------------------------------------------------------------

### ${project.name} at the library of the University of Cologne

The [digital library of the University of Cologne](http://kug3.ub.uni-koeln.de/portal/opac?view=all) promotes the
idea of integrating libraries with user-created bookmarking systems.
They offer a simple link to the ${project.name} publication metadata
interface where you can add tags and cross-check the reference data
fields. By clicking on the **green ${project.name} symbol (on the right
next to an entry)** you get redirected to ${project.name}. There, you
can edit the data and add information.

![ <File:koeln1.png>](digitallibraries/koeln1.png  "fig: File:koeln1.png")  

------------------------------------------------------------------------

### ${project.name} at the library of the University of Heidelberg

The [digital library of the University of Heidelberg](http://katalog.ub.uni-heidelberg.de/cgi-bin/search.cgi)
offers to store the metadata of books and other publications in
${project.name}, too. To extract literature data of an entry to
${project.name}, click on an entry in the result list. On the upper
right side on the grey bar, click on **"Exportieren/Zitieren"**, then,
choose **${project.name}**. You get redirected to ${project.name}
where you can edit the data before saving.

![ <File:heidelberg.png>](digitallibraries/heidelberg.png  "fig: File:heidelberg.png")  
  

-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").
  

