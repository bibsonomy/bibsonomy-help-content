<!-- de: Programmoberfläche -->

## User Interface - Overview
-------------

${project.theme == "bibsonomy"}{

**Goal:** This guide will give you an outline of the ${project.name}
user interface. After these instructions, you know where you can find
the important parts (e.g. mainmenu or searchbox).

![user_interface/user_interface_eng_markierungen.png](user_interface/user_interface_eng_markierungen.png  "fig: /File:user_interface_eng_markierungen.png")  

1. **searchbox** - with the searchbox, you can search for all available
    information in ${project.name}. Click on the blue arrow next to
    *"search"* - a menu appears. Click on these categories to select what
    you want to search (e.g. select *"author"* to search for a specific
    author). Then enter your search term in the white textbox. Finally,
    click on the magnifying glass on the right or press *"Enter"* on your
    keyboard. A few seconds later, ${project.name} shows you all available
    results.  
    On the [ search help page](SearchPageHelp "wikilink"), you can find
    further information on the options you have when searching in
    ${project.name}.  
  
2.  **language bar** - you can choose between three available language
    versions. Click on *"en"* to use ${project.name} in English, click on
    *"de"* to switch to the German version or click on *"ru"* to use the
    Russian version.  
  
3.  **left main menu** - provides you with all important features in
    ${project.name}. When you click on the arrows next to the menu items,
    you can see the sub menus. In the following section, the menu items of
    the main menu are described (from left to right, with sub menus):

    -   **Home:** here you get to the home page of ${project.name}.
      
    -   **my ${project.name}:** your personal home page. Subitems
        printed in *italics* can only be seen in advanced view mode. How
        to activate the advanced view [ is described here](UnlockAdvancedMode "wikilink").
        -   **my posts:** shows your posts.
        
        -   ***private posts:*** shows all posts that only you are able
            to see.
            
        -   ***posts for friends:*** shows the posts that only you or
            your friends are able to see.
            
        -   ***documents:*** if you have [attached documents](AttachDocuments "wikilink") (e.g. in
            PDF format) to your posts, you get an overview of these
            documents here.
            
        -   ***duplicates:*** shows posts that may be duplicates. This
            way you can clear up your bibliography easily.
            
        -   ***concepts:*** [ concepts](Concepts "wikilink") allow you
            to group several tags.
            
        -   **discussed posts:** shows the posts in which you
            contributed to the [discussion](DiscussedPosts "wikilink").
            
        -   **followed posts:** shows posts of the users you [ followed](Followers 			 "wikilink").

        -   **posts of friends:** shows all posts of users which have added you to 			 their friend list and which are visible only to friends.
            
        -   ***curriculum vitae:*** here you can provide a
            [text/curriculum vitae](ChangeCV "wikilink") of yourself that other users of
            ${project.name} can access.
            
        -   ***browse publications:*** browse through your publication
            list by using the filter options. You can find instructions
            in [this tutorial](BrowsePublications "wikilink").
            
        -   ***export BibTeX:*** exports your literature data in
            BibTeX format.
        
    -   **add post:** this menu helps you to add new bookmarks/publications.
    
        -   **add bookmark:** add a new bookmark. [ This tutorial](BookmarkPublicationManagement "wikilink") shows
            you how to manage bookmarks and publications.
            
        -   **add publication:** add a new publication. [ This tutorial](BookmarkPublicationManagement "wikilink") shows
            you how to manage bookmarks and publications.
            
        -   **import bookmarks:** You can import bookmarks from your
            browser or [Delicious](https://delicious.com/) (view instructions [ here](ImportBookmarks "wikilink")).
            
        -   **import publications:** You can import publications by uploading an 				existing BibTeX or EndNote file.    
        
    -   **groups:** shows you the [ functions related to groups](GroupFunctions "wikilink").
    
        -   **all groups:** shows you an overview of all groups existing
            in ${project.name}.
            
        -   **create new group:** here you can create a new group.
        
    -   **popular:** allows you to discover the currently most popular
        items in ${project.name}.
        
        -   **posts:** shows the most popular posts.
        
        -   **tags:** shows a tag cloud with the most popular [tags](Tags "wikilink") in ${project.name}. The bigger a tag
            is displayed, the more popular it is.
            
        -   **authors:** shows the most popular authors.
        
        -   **concepts:** shows the most popular [concepts](Concepts "wikilink") and their mappings.
            
        -   **discussions:** shows the most popular [discussions](DiscussedPosts "wikilink")
            regarding bookmarks and publications.  
    
4.  **right main menu** - here you can find more important features
    of ${project.name}.

    -   **your username:** you are currently using ${project.name} with
        this username. Click on your username to get to your [ user
        page](UserPage "wikilink") and to view your bookmarks and publications.
        
    -   **person icon:** Click on this symbol on the right of your
        username to see more features.
        
        -   **inbox:** your [ inbox for
            bookmarks/publications](Inbox "wikilink"). Your friends and
            groups are able to send bookmarks/publications to you, these
            will be collected here.
            
        -   **clipboard:** the clipboard allows you to [ collect
            publications and bookmarks](Clipboard "wikilink").
            
        -   **friends:** shows your [ friends](AddFriends "wikilink").
        
        -   **edit tags:** here you can edit your tags and [concepts](Concepts "wikilink").
        
        -   **settings:** here you can [ view and edit your personal
            user settings](Settings "wikilink").
            
        -   **blog:** redirects you to the [ ${project.name} blog](http://bibsonomy.blogspot.com/).
        
        -   **help:** shows you the help pages of ${project.name}.
        
        -   **logout:** click here to log out from ${project.name}.  
    
5.  **content area** - in this section you can see currently available
    bookmarks and publications. To learn more about this section,
    especially to learn more about bookmarks and publications, read [
    this tutorial](BookmarkPublicationManagement "wikilink").  
    
6.  **news** - here, ${project.name} developers publish the latest news
    (including new features or important system notifications).  
    
7.  **busy tags** - displays the currently most popular [tags](Tags "wikilink").

}


${project.theme == "puma"}{

**Goal:** This guide will give you an outline of the ${project.name}
user interface. After these instructions, you know where you can find
the important parts (e.g. mainmenu or searchbox).

![user_interface/user_interface_eng_markierungen.png](user_interface/user_interface_eng_markierungen.png  "fig: /File:user_interface_eng_markierungen.png")  

1. **searchbox** - with the searchbox, you can search for all available
    information in ${project.name}. Click on the blue arrow next to
    *"search"* - a menu appears. Click on these categories to select what
    you want to search (e.g. select *"author"* to search for a specific
    author). Then enter your search term in the white textbox. Finally,
    click on the magnifying glass on the right or press *"Enter"* on your
    keyboard. A few seconds later, ${project.name} shows you all available
    results.  
    On the [ search help page](SearchPageHelp "wikilink"), you can find
    further information on the options you have when searching in
    ${project.name}.  
  
2.  **language bar** - you can choose between three available language
    versions. Click on *"en"* to use ${project.name} in English, click on
    *"de"* to switch to the German version or click on *"ru"* to use the
    Russian version.  
  
3.  **left main menu** - provides you with all important features in
    ${project.name}. When you click on the arrows next to the menu items,
    you can see the sub menus. In the following section, the menu items of
    the main menu are described (from left to right, with sub menus):

    -   **Home:** here you get to the home page of ${project.name}.
      
    -   **my ${project.name}:** your personal home page. Subitems
        printed in *italics* can only be seen in advanced view mode. How
        to activate the advanced view [ is described here](UnlockAdvancedMode "wikilink").
        -   **my posts:** shows your posts.
        
        -   ***private posts:*** shows all posts that only you are able
            to see.
            
        -   ***posts for friends:*** shows the posts that only you or
            your friends are able to see.
            
        -   ***documents:*** if you have [attached documents](AttachDocuments "wikilink") (e.g. in
            PDF format) to your posts, you get an overview of these
            documents here.
            
        -   ***duplicates:*** shows posts that may be duplicates. This
            way you can clear up your bibliography easily.
            
        -   ***concepts:*** [ concepts](Concepts "wikilink") allow you
            to group several tags.
            
        -   **discussed posts:** shows the posts in which you
            contributed to the [discussion](DiscussedPosts "wikilink").
            
        -   **followed posts:** shows posts of the users you [ followed](Followers 			 "wikilink").

        -   **posts of friends:** shows all posts of users which have added you to 			 their friend list and which are visible only to friends.
            
        -   ***curriculum vitae:*** here you can provide a
            [text/curriculum vitae](ChangeCV "wikilink") of yourself that other users of
            ${project.name} can access.
            
        -   ***browse publications:*** browse through your publication
            list by using the filter options. You can find instructions
            in [this tutorial](BrowsePublications "wikilink").
            
        -   ***export BibTeX:*** exports your literature data in
            BibTeX format.
        
    -   **add post:** this menu helps you to add new bookmarks/publications.
    
        -   **add bookmark:** add a new bookmark. [ This tutorial](BookmarkPublicationManagement "wikilink") shows
            you how to manage bookmarks and publications.
            
        -   **add publication:** add a new publication. [ This tutorial](BookmarkPublicationManagement "wikilink") shows
            you how to manage bookmarks and publications.
            
        -   **import bookmarks:** You can import bookmarks from your
            browser or [Delicious](https://delicious.com/) (view instructions [ here](ImportBookmarks "wikilink")).
            
        -   **import publications:** You can import publications by uploading an 				existing BibTeX or EndNote file.    
        
    -   **groups:** shows you the [ functions related to groups](GroupFunctions "wikilink").
    
        -   **all groups:** shows you an overview of all groups existing
            in ${project.name}.
            
        -   **create new group:** here you can create a new group.
        
    -   **popular:** allows you to discover the currently most popular
        items in ${project.name}.
        
        -   **posts:** shows the most popular posts.
        
        -   **tags:** shows a tag cloud with the most popular [tags](Tags "wikilink") in ${project.name}. The bigger a tag
            is displayed, the more popular it is.
            
        -   **authors:** shows the most popular authors.
        
        -   **concepts:** shows the most popular [concepts](Concepts "wikilink") and their mappings.
            
        -   **discussions:** shows the most popular [discussions](DiscussedPosts "wikilink")
            regarding bookmarks and publications.  
    
4.  **right main menu** - here you can find more important features
    of ${project.name}.

    -   **your username:** you are currently using ${project.name} with
        this username. Click on your username to get to your [ user
        page](UserPage "wikilink") and to view your bookmarks and publications.
        
    -   **person icon:** Click on this symbol on the right of your
        username to see more features.
        
        -   **inbox:** your [ inbox for
            bookmarks/publications](Inbox "wikilink"). Your friends and
            groups are able to send bookmarks/publications to you, these
            will be collected here.
            
        -   **clipboard:** the clipboard allows you to [ collect
            publications and bookmarks](Clipboard "wikilink").
            
        -   **friends:** shows your [ friends](AddFriends "wikilink").
        
        -   **edit tags:** here you can edit your tags and [concepts](Concepts "wikilink").
        
        -   **settings:** here you can [ view and edit your personal
            user settings](Settings "wikilink").
            
        -   **blog:** redirects you to the [ ${project.name} blog](http://bibsonomy.blogspot.com/).
        
        -   **help:** shows you the help pages of ${project.name}.
        
        -   **logout:** click here to log out from ${project.name}.  
    
5.  **content area** - in this section you can see currently available
    bookmarks and publications. To learn more about this section,
    especially to learn more about bookmarks and publications, read [
    this tutorial](BookmarkPublicationManagement "wikilink").  
    
6.  **news** - here, ${project.name} developers publish the latest news
    (including new features or important system notifications).  
    
7.  **busy tags** - displays the currently most popular [tags](Tags "wikilink").

}

${project.theme == "wueresearch"}{

**Goal:** This guide will give you an outline of the ${project.name}
user interface. After these instructions, you know where you can find
the important parts (e.g. mainmenu or searchbox).

![user_interface/userinterfacewueen.png](wueresearch/User_Interface/userinterfacewueen.png  "fig: /File:userinterfacewueen.png")  

1. **searchbox** - with the searchbox, you can search for all available
    information in ${project.name}. Click on the blue arrow next to
    *"search"* - a menu appears. Click on these categories to select what
    you want to search (e.g. select *"author"* to search for a specific
    author). Then enter your search term in the white textbox. Finally,
    click on the magnifying glass on the right or press *"Enter"* on your
    keyboard. A few seconds later, ${project.name} shows you all available
    results.  
    On the [ search help page](SearchPageHelp "wikilink"), you can find
    further information on the options you have when searching in
    ${project.name}.  
  
2.  **language bar** - you can choose between two available language
    versions. Click on *"en"* to use ${project.name} in english or click on
    *"de"* to switch to the german version.
  
3.  **left main menu** - provides you with all important features in
    ${project.name}. When you click on the arrows next to the menu items,
    you can see the sub menus. Subitems
    printed in *italics* can only be seen in advanced view mode. How
    to activate the advanced view [ is described here](UnlockAdvancedMode "wikilink").

    In the following section, the menu items of
    the main menu are described (from left to right, with sub menus):

    -   **Home:** here you get to the home page of ${project.name}.
      
    -   **my ${project.name}:** your personal home page.

        -   **my posts:** shows your posts.
        
        -   ***private posts:*** shows all posts that only you are able
            to see.
            
        -   ***posts for friends:*** shows the posts that only you or
            your friends are able to see.
            
        -   ***documents:*** if you have [attached documents](AttachDocuments "wikilink") (e.g. in
            PDF format) to your posts, you get an overview of these
            documents here.
            
        -   ***duplicates:*** shows posts that may be duplicates. This
            way you can clear up your bibliography easily.
            
        -   ***concepts:*** [ concepts](Concepts "wikilink") allow you
            to group several tags.
            
        -   **discussed posts:** shows the posts in which you
            contributed to the [discussion](DiscussedPosts "wikilink").
            
        -   **followed posts:** shows posts of the users you [ followed](Followers 			 "wikilink").

        -   **posts of friends:** shows all posts of users which have added you to 			 their friend list and which are visible only to friends.
            
        -   ***curriculum vitae:*** here you can provide a
            [text/curriculum vitae](ChangeCV "wikilink") of yourself that other users of
            ${project.name} can access.
            
        -   ***browse publications:*** browse through your publication
            list by using the filter options. You can find instructions
            in [this tutorial](BrowsePublications "wikilink").
            
        -   ***export BibTeX:*** exports your literature data in
            BibTeX format.
        
    -   **add post:** this menu helps you to add new bookmarks/publications.
    
        -   **add bookmark:** add a new bookmark. [ This tutorial](BookmarkPublicationManagement "wikilink") shows
            you how to manage bookmarks and publications.
            
        -   **add publication:** add a new publication. [ This tutorial](BookmarkPublicationManagement "wikilink") shows
            you how to manage bookmarks and publications.
            
        -   **import bookmarks:** You can import bookmarks from your
            browser or [Delicious](https://delicious.com/) (view instructions [ here](ImportBookmarks "wikilink")).
            
        -   **import publications:** You can import publications by uploading an 				existing BibTeX or EndNote file.    

    -   **organizations:** Here, you can find a list of all institutions at the university. You can search them and get access to additional informations.

    -   **groups:** shows you the [ functions related to groups](GroupFunctions "wikilink").
    
        -   **all groups:** shows you an overview of all groups existing
            in ${project.name}.
            
        -   **create new group:** here you can create a new group.
        
    -   **persons:** Here, you can find a list of all [person entities](PersonEntities "wikilink") at ${project.name}. You can search for specific names and access additional information.

    - **publications:** Here, you can find the latest publications added to 
    ${project.name}. You can also search for specific entries and filter your results.
             
4.  **right main menu** - here you can find more important features
    of ${project.name}.

    -   **your username:** you are currently using ${project.name} with
        this username. Click on your username to get to your [ user
        page](UserPage "wikilink") and to view your bookmarks and publications.
        
    -   **person icon:** Click on this symbol on the right of your
        username to see more features.
        
        -   **inbox:** your [ inbox for
            bookmarks/publications](Inbox "wikilink"). Your friends and
            groups are able to send bookmarks/publications to you, these
            will be collected here.
            
        -   **clipboard:** the clipboard allows you to [ collect
            publications and bookmarks](Clipboard "wikilink").
            
        -   **friends:** shows your [ friends](AddFriends "wikilink").
        
        -   **edit tags:** here you can edit your tags and [concepts](Concepts "wikilink").
        
        -   **settings:** here you can [ view and edit your personal
            user settings](Settings "wikilink").
            
        -   **blog:** redirects you to the [ ${project.name} blog](http://bibsonomy.blogspot.com/).
        
        -   **help:** shows you the help pages of ${project.name}.
        
        -   **logout:** click here to log out from ${project.name}.  

5.  **content area** - in this section you can see currently available
    bookmarks and publications. To learn more about this section,
    especially to learn more about bookmarks and publications, read [
    this tutorial](BookmarkPublicationManagement "wikilink").      
}

------------------------------------------------------------------------

[Click here to go back to beginner's area and learn more about the basic functions](MainBeginner "wikilink").
