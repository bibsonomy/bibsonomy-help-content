
[ Overview on help topics](Overview "wikilink")

### Registration/Sign-in

${project.theme == "bibsonomy"}{

-   [ Sign in with OpenID account](SignInWithOpenIDAccount "wikilink")
-   [ Sign in with user account](SignInWithUseraccount "wikilink")
-   [ Create a new user account](CreateNewAccount "wikilink")
-   [ How to recover your password](PasswordRecovery "wikilink")

}

${project.theme == "puma"}{

-   [ How to use your library card for authentication](How-toUnlockLibraryCardForPUMA "wikilink")

}

### Basic Functions

${project.theme == "bibsonomy"}{

-   [ User Interface - Overview](User_Interface "wikilink")
-   [ Manage literature and bookmarks](BookmarkPublicationManagement "wikilink")
-   [ Use the tag system](Tags "wikilink")
-   [ Add a single bookmark](BookmarkPublicationManagement/BookmarkManagement "wikilink")
-   [ Import multiple bookmarks](ImportBookmarks "wikilink")
-   [ Export bookmarks](ExportBookmarks "wikilink")
-   [ Edit boookmarks/publications](EditOwnEntries "wikilink")
-   [ Add a single publication](AddPublications "wikilink")
-   [ Import multiple publications](AddPublications/ImportMultiplePublications "wikilink")
-   [ Export publications](ExportData "wikilink")
-   [ Install Browser-PlugIn](${project.home}buttons)
-   [ Discovery Service](DiscoveryService "wikilink")

}

${project.theme == "puma"}{

-   [ User Interface - Overview](User_Interface "wikilink")
-   [ Manage literature and bookmarks](BookmarkPublicationManagement "wikilink")
-   [ Use the tag system](Tags "wikilink")
-   [ Add a single bookmark](BookmarkPublicationManagement/BookmarkManagement "wikilink")
-   [ Import multiple bookmarks](ImportBookmarks "wikilink")
-   [ Export bookmarks](ExportBookmarks "wikilink")
-   [ Edit boookmarks/publications](EditOwnEntries "wikilink")
-   [ Add a single publication](AddPublications "wikilink")
-   [ Import multiple publications](AddPublications/ImportMultiplePublications "wikilink")
-   [ Export publications](ExportData "wikilink")
-   [ Install Browser-PlugIn](http://puma.uni-kassel.de/buttons)
-   [ Discovery Service](DiscoveryService "wikilink")

}

### Advanced Features

-   [How to change my CV](ChangeCV "wikilink")
-   [Person Entities](PersonEntities "wikilink")
-   [Use RSS-Feeds](RSS-FeedsSubscribe "wikilink")
-   [Browse publications](BrowsePublications "wikilink")
-   [Use the clipboard](Clipboard "wikilink")
-   [Attach a private document](AttachDocuments "wikilink")
-   [URL-Syntax](URL-Syntax "wikilink")
-   (Beta) [Addon for Google Docs](AddonGoogleDocs "wikilink")

### Social Features

-   [Add friends/contacts](AddFriends "wikilink")
-   [Group functions](GroupFunctions "wikilink")
-   [Follow users](Followers "wikilink")
-   [Discussion: Reviews and Comments](DiscussionReviewsComments "wikilink")
-   [Community Posts](CommunityPosts "wikilink")
-   [Use the URL-Resolver](OpenURL "wikilink")


${project.theme == "puma"}{

### Developers' Garden

-   [Developers' Corner](https://bibsonomy.bitbucket.io/)
-   [Integration with other websites](Integration "wikilink")
-   [Javascript Code Snippets](JS-Codesnippet "wikilink")

}

${project.theme == "bibsonomy"}{

### Developers' Garden

-   [Developers' Corner](https://bibsonomy.bitbucket.io/)
-   [REST-API](https://bitbucket.org/bibsonomy/bibsonomy/wiki/documentation/api/REST%20API)
-   [Integration with other websites](Integration "wikilink")
    -   [Integrate into TYPO3](Typo3 "wikilink")
    -   [Javascript Code Snippets](JS-Codesnippet "wikilink")
-   [Anonymised records for
    research](https://www.kde.cs.uni-kassel.de/bibsonomy/dumps/)

}

### press release

-   [ Project Overview](Projects "wikilink")
-   [ Articles about BibSonomy/PUMA](Press "wikilink")  


### Additional resources

-   [BibSonomy/PUMA Developer-Blog](http://bibsonomy.blogspot.com/)

-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").