<!-- de: URL-Syntax -->

## URL-Syntax
----------

**Advice:** This page contains information intended for developers and technically experienced users.

This page contains all information about the ${project.name} URL
syntax. You can click on all the links on this page to see an example of the outputs.

------------------------

### General pages

-   **[ /](${project.home} "wikilink")**  
    Home page of ${project.name}, shows the 50 recently posted public
    entries.  
      
-   **[ /popular](${project.home}popular "wikilink")**  
    Shows the 100 most often posted entries of the last 100.000 public
    posts.  
      
-   **[ /help\_en](${project.home}help_en "wikilink")**  
    The help homepage.  
      
-   **[ /help\_en/FAQ](${project.home}help_en/FAQ "wikilink")**  
    A page with the frequently asked questions (FAQ) and their answers.  
      
-   **[ /user/jaeschke](${project.home}user/jaeschke "wikilink")**  
    Shows all public posts of the user *jaeschke*.  
      
-   **[ /user/jaeschke/web](${project.home}user/jaeschke/web "wikilink")**  
    Shows all public posts with the tag *web* of the user *jaeschke*.  
      
-   **[/user/jaeschke/web%20api](${project.home}user/jaeschke/web%20api "wikilink")**  
    Shows all public posts with the tag *web* and the tag *api* of the
    user *jaeschke*.  
      
-   **[ /myHome](${project.home}myBibSonomy "wikilink")**  
    Link to the list of your own bookmarks and bibliographic
    references.  
      
-   **[ /myBibTeX](${project.home}myBibTeX "wikilink")**  
    Link to the BibTeX list of all your own bibliographic references.  
      
-   **[ /myRelations](${project.home}myRelations "wikilink")**  
    Link to the list of your own relations.  
      
-   **[ /myDocuments](${project.home}myPDF "wikilink")**  
    Link to your own collection of full texts (PDF, PS, TXT, DJV or DJVU).  
      
-   **[ /myDuplicates](${project.home}myDuplicates "wikilink")**  
    Link to your own duplicates.  
      
------------------

### Parameters for sorting

When you see a page with publication/bookmark lists in ${project.name},
you are able to sort them by adding one or more of the following
parameters behind the URL. Here's a list of existing parameters:  
  
-  **items** - How many items should be displayed?  

	(For example: **?items=30** will show 30 items.)  
	*Note: If you do not add the **item-parameter** with a certain number, a default number 
	of maximum 20 items will be shown.*

-  **sortPage** - How is the list sorted?
    -   **values (can be linked with a | ):**
        -   **author** - the author's name
        -   **editor** - the editor's name
        -   **year** - year of publication
        -   **entrytype** - type of publication
        -   **title** - title
        -   **booktitle** - the book's title (especially in collections)
        -   **journal** - the journal's name
        -   **school** - the university's name

-   **sortPageOrder** - order of sorting
    -   **values:**
        -   **asc** - ascending
        -   **desc** - descending

-   **duplicates**
    -   **values:**
        -   **yes** - duplicates allowed
        -   **no** - no duplicates in result list

  
**Example:**  
**?sortPage=year&sortPageOrder=asc&duplicates=no**  
Sort by year (sortPage=year) ascending (sortPageOrder=asc) and remove
all duplicates (duplicates=no).  

------------

### Administrative pages

-   **[ /settings](${project.home}settings "wikilink")**  
    On this page you can:
    -   add a user to your group,
    -   find your API key and generate a new key,
    -   import your del.icio.us data,
    -   change your password,
    -   change your account settings (email, name of your homepage),
    -   synchronize your data between [BibSonomy](http://www.bibsonomy.org/) and [PUMA](http://puma.uni-kassel.de/).

<!-- -->

-   **[ /postBookmark](${project.home}postBookmark "wikilink")**  
    On this page you can:
    -   enter a URL to post a bookmark.  
          

<!-- -->

-   **[ /postPublication](${project.home}postPublication "wikilink")**  
    On this page you can:
    -   enter type, title, author, publisher and year to post a
        publication,
    -   insert a selected BibTeX snippet into a text box to post one or
        more BibTeX entries,
    -   upload a BibTeX file to post one or more BibTeX entries.  
          
------------

### Author pages

${project.name} offers a way to retrieve publications by the names of
their authors. There are several options to filter these search results.
Currently, the filters include the year of publication, a special tag
and the username of the person who stored the post.  
  
- **[ /author/hotho](${project.home}author/hotho "wikilink")**  
    Shows all public posts with the author Hotho.  
  
- **[/author/stumme+hotho+schmitz](${project.home}author/stumme+hotho+schmitz "wikilink")**  
    Shows all publications published by these authors.  
  
- **[/author/stumme+hotho+!schmitz](${project.home}author/stumme+hotho+!schmitz "wikilink")**
    or **[/author/stumme+hotho+%21schmitz](${project.home}author/stumme+hotho+%21schmitz "wikilink")**  
    Specifies names of an author who should not be part of the searched publications, e. g. show all publications written by Stumme and Hotho, but not by Schmitz.
 
- **[/author/hotho/clustering](${project.home}author/hotho/clustering "wikilink")**  
Shows all posts with the tag 'clustering' and the author Hotho. 

- **[/author/stumme/sys:user:hotho](${project.home}author/stumme/sys:user:hotho "wikilink")**  
    Shows all publications of the author 'Stumme' in the collection of the user 'hotho'.
 
- **[/author/stumme+hotho+!schmitz/sys:year:2002-2007%20sys:user:hotho%20folksonomy](${project.home}author/stumme+hotho+!schmitz/sys:year:2002-2007%20sys:user:hotho%20folksonomy "wikilink")**
    This combination of search results returns all publications of the authors Stumme and Hotho but not Schmitz between the years 2002 and 2007 in user Hotho's collection marked with the tag 'folksonomy'.
 
- **[/author/stumme/sys:group:kde](${project.home}author/stumme/sys:group:kde "wikilink")**
    Shows all publications of the author Stumme in the collections of all group members of the group 'kde'.

---------------

#### Author search: year of publication

A [ system tag](SystemTags "wikilink") can restrict your (author) search
result to a specific year or range of years. Four formats are
available:  
  

-   **[/author/stumme/sys:year:2007](${project.home}author/stumme%20sys:year:2007 "wikilink")**  
    Returns all publications of the author Stumme of the year 2007.  
      
-   **[/author/stumme/sys:year:2003-2007](${project.home}author/stumme/sys:year:2003-2007 "wikilink")**  
    Returns all publications of the author Stumme between 2003 and 2007.  
      
-   **[/author/stumme/sys:year:-2005](${project.home}author/stumme/sys:year:-2005 "wikilink")**  
    Returns all publications of the author Stumme until the year 2005.  
      
-   **[/author/stumme/sys:year:1997-](${project.home}author/stumme/sys:year:1997- "wikilink")**  
    Returns all publications of the author Stumme since 1997.  
      
----------

### Friends pages

You get an introduction about visibility on the page [
friends](AddFriends "wikilink"). In the following section, the most
important URLs about friends are listed.  
  

-   **[ /friends](${project.home}friends "wikilink")**  
    Shows all posts which are set visible for friends of all users which
    declared you as their friend. Additionally you can
    -   add a friend to your friends list,
    -   remove a friend from your friends list.  
          
-   **[ /friend/jaeschke](${project.home}friend/jaeschke "wikilink")**  
    Shows all posts which are set visible for friends of the user
    *jaeschke*. You can only see them if *jaeschke* declared you a
    friend.  
      
-   **[ /friend/jaeschke/web](${project.home}friend/jaeschke/web "wikilink")**  
    Shows all posts with the tag 'web' which are set visible for friends
    of the user *jaeschke*. You can only see them if *jaeschke* declared
    you a friend.  
      
-   **[/friend/jaeschke/web%20api](${project.home}friend/jaeschke/web%20api "wikilink")**  
    Shows all posts with the tag 'web' and the tag 'api' which are set
    visible for friends of the user *jaeschke*. You can only see them if
    *jaeschke* declared you a friend.  

-----------

### Groups

You get an introduction about visibility on the page [ group
functions](GroupFunctions "wikilink"). In the following section, the
most important URLs about groups are listed.  
  
- **[ /groups](${project.home}groups "wikilink")**  
Shows all groups of the system.  
  
- **[ /group/kde](${project.home}group/kde "wikilink")**  
Shows all posts of members of the group 'kde', if you are a member of
the group.  
  
- **[ /group/kde/web](${project.home}group/kde/web "wikilink")**  
Shows all posts (which you are allowed to see) with the tag 'web' of
members of the group 'kde', if you are a member of the group.  
  
- **[ /group/kde/web%20api](${project.home}group/kde/web%20api "wikilink")**  
Shows all posts with the tag 'web' and the tag 'api' of the members of
the group 'kde', if you are a member of the group.  
  
- **[ /relevantfor/group/kde](${project.home}relevantfor/group/kde "wikilink")**  
Shows all posts relevant for the members of the group.  
  
- **[ /followers](${project.home}followers "wikilink")**  
Shows the recent posts of all users you are following and applies a
ranking algorithm to show the most relevant ones for you on top of the
list.  
  
-------------------

### Tag pages

-   **[ /tag/web](${project.home}tag/web "wikilink")**  
    Shows all public posts with the tag 'web'.  
      
-   **[ /tag/web%20api](${project.home}tag/web%20api "wikilink")**  
    Shows all public posts with the tag 'web' and the tag 'api'.  
      
-   **[/tag/web?order=folkrank](${project.home}tag/web?order=folkrank "wikilink")**  
    Shows all public posts with the tag 'web' and orders the results by
    the [FolkRank algorithm](https://www.bibsonomy.org/bibtex/24d8b4f79814691fbe6db8357d63206a1/stumme).  

-------------

### Relations and Tags

-   **[/api/tags/data](http://www.bibsonomy.org/api/tags/data)**  
    Shows all related tags to 'data'.  
      
-   **[/api/tags/data?relation=related](http://www.bibsonomy.org/api/tags/data?relation=related)**  
    Shows all related tags to 'data'.  
      
-   **[/api/tags/data?relation=similar](http://www.bibsonomy.org/api/tags/data?relation=similar)**  
    Shows all similar tags to 'data'.  

-------------

### Relations and concept pages

-   **[ /relations/schmitz](${project.home}relations/schmitz "wikilink")**  
    Shows all relations of the user *schmitz*.  
      
-   **[/concept/user/schmitz/event](${project.home}concept/user/schmitz/event "wikilink")**  
    Shows all bookmarks and publications of the user *schmitz* which
    have 'event' or one of the relation's subtags as tags.  

---------

### Search pages

-   **[ /search/web](${project.home}search/web "wikilink")**  
    Shows all public posts which contain the word 'web' (in the text and
    in the tags!). In case of a bookmark, the full text contains the
    URL, the title and the description. In case of a publication, the
    title, the description and all BibTeX fields are included.  
      
-   **[ /search/web%20api](${project.home}search/web%20api "wikilink")**  
    Shows all public posts which contain the word 'web' and the word
    'api' (in the text, not in the tags!). In case of a bookmark, the
    full text contains the URL, the title and the description. In case
    of a publication, the title, the description and all BibTeX fields
    are included.  
      
-   **[ /search/web%20-api](${project.home}search/web%20-api "wikilink")**  
    Shows all public posts which contain the word 'web' but not the word
    'api' (in the text, not in the tags!). In case of a bookmark, the
    full text contains the URL, the title and the description. In case
    of a publication, the title, the description and all BibTeX fields
    are included.  
      
-   **[/search/web%20user:jaeschke](${project.home}search/web%20user:jaeschke "wikilink")**  
    Shows all public posts of the user *jaeschke* which contain the word
    'web' (in the text, not in the tags!). In case of a bookmark, the
    full text contains the URL, the title and the description. In case
    of a publication, the title, the description and all BibTeX fields
    are included.  
      
-   **[/search/web%20api%20user:jaeschke](${project.home}search/web%20api%20user:jaeschke "wikilink")**  
    Shows all public posts of the user *jaeschke* which contain the word
    'web' and the word 'api' (in the text, not in the tags!). In case of
    a bookmark, the full text contains the URL, the title and
    the description. In case of a publication the title, the description
    and all BibTeX fields are included.  
      
-   **[/search/web%20-api%20user:jaeschke](${project.home}search/web%20-api%20user:jaeschke "wikilink")**  
    Shows all public posts of the user *jaeschke* which contain the word
    'web' but not the word 'api' (in the text, not in the tags!). In
    case of a bookmark, the full text contains the URL, the title and
    the description. In case of a publication, the title, the
    description and all BibTeX fields are included.  
      
-   **[ /mySearch](${project.home}mySearch "wikilink")**  
    This page offers a quick search in your collection.  
      

For an overview on the different search options ${project.name} offers, check the [search help page](SearchPageHelp "wikilink"). 

-----------

### Viewable pages

You get an introduction about visibility on the pages [
friends](AddFriends "wikilink") and [ group functions](GroupFunctions "wikilink"). In the following section, the
most important URLs about visibility are listed.  
  

-   **[ /viewable/public](${project.home}viewable/public "wikilink")**  
    Shows all your posts which you set viewable for *public*.  
      
-   **[ /viewable/public/web](${project.home}viewable/public/web "wikilink")**  
    Shows all your posts with the tag 'web' which you set viewable for
    *public*.  
      
-   **[/viewable/public/web%20api](${project.home}viewable/public/web%20api "wikilink")**  
    Shows all your posts with the tag 'web' and the tag 'api' which you
    set viewable for *public*.  
      
-   **[ /viewable/private](${project.home}viewable/private "wikilink")**  
    Shows all your posts which you set viewable for *private*.  
      
-   **[ /viewable/private/web](${project.home}viewable/private/web "wikilink")**  
    Shows all your posts with the tag 'web' which you set viewable for
    *private*.  
      
-   **[/viewable/private/web%20api](${project.home}viewable/private/web%20api "wikilink")**  
    Shows all your posts with the tag 'web' and the tag 'api' which you
    set viewable for *private*.  
      
-   **[ /viewable/friends](${project.home}viewable/friends "wikilink")**  
    Shows all your posts which you set viewable for *friends*.  
      
-   **[ /viewable/friends/web](${project.home}viewable/friends/web "wikilink")**  
    Shows all your posts with the tag 'web' which you set viewable for
    *friends*.  
      
-   **[/viewable/friends/web%20api](${project.home}viewable/friends/web%20api "wikilink")**  
    Shows all your posts with the tag 'web' and the tag 'api' which you
    set viewable for *friends*.  
      
-   **[ /viewable/kde](${project.home}viewable/kde "wikilink")**  
    Shows all posts (if you are allowed to see them) which were set
    viewable for the group 'kde'.  
      
-   **[ /viewable/kde/web](${project.home}viewable/kde/web "wikilink")**  
    Shows all posts (if you are allowed to see them) with the tag 'web'
    which were set viewable for the group 'kde'.  
      
-   **[/viewable/kde/web%20api](${project.home}viewable/kde/web%20api "wikilink")**  
    Shows all posts (if you are allowed to see them) with the tag 'web'
    and the tag 'api' which were set viewable for the group 'kde'.  

-------------

### Handling duplicates

On several pages, entries (publications) are displayed which come from
different users. As an example, if two or more users within a group have
posted the same publication, this publication will appear twice on the
corresponding group page.  
If this is not desired, the handling of such duplicate entries can be
controlled with the parameter *duplicates*:  
  

-   **[ /group/kde/myown](${project.home}group/kde/myown "wikilink")**  
    Shows all posts of all group members of the group 'kde' that are
    tagged with 'myown' (including duplicates).  
      
-   **[/group/kde/myown?duplicates=no](${project.home}group/kde/myown?duplicates=no "wikilink")**  
    Shows all posts of all group members of the group 'kde' that are
    tagged with 'myown', not including duplicates: For each set of
    identical publications, only the first publication is displayed.  
      
-   **[/group/kde/myown?duplicates=merged](${project.home}group/kde/myown?duplicates=merged "wikilink")**  
    Shows all posts of all group members of the group 'kde' that are
    tagged with 'myown', with duplicates merged: For each set of
    identical publications, all tags are aggregated into a single post,
    which is displayed.  
 
-------------

### Export pages

-   **[ /clipboard](${project.home}clipboard "wikilink")**  
    Here you can manage publication posts you picked with the "pick"
    button.  
      
-   **[ /export/](${project.home}export/ "wikilink")**  
    Shows a variety of formats which you can select to export
    publication metadata.  
      

The following URLs are shortcuts which can also be clicked on the export page.  

------------

#### RSS-Feeds

-   **[ /publrss/](${project.home}publrss/ "wikilink")**  
    Presents an RSS feed of the main publication page.
-   **[ /burst/](${project.home}burst/ "wikilink")**  
    ${project.name} [BuRST](http://www.cs.vu.nl/pmika/research/burst/BuRST.html) feed for the main publication page.  
      
-   **[ /aparss/](${project.home}aparss/ "wikilink")**  
    RSS feed in [APA](http://www.apa.org/) style.  

-----------

#### Reference metadata and formatting

-   **[ /bib/](${project.home}bib/ "wikilink")**  
    BibTeX format of all publications of the main publication page.  
      
-   **[ /bib/user/beate](${project.home}bib/user/beate "wikilink")**  
    BibTeX format of all publications of the publication page of the
    user beate.  
      
-   **[ /endnote/](${project.home}endnote/ "wikilink")**  
    EndNote format of the publications of the main page.  

----------

#### HTML formatting

-   **[ /publ/](${project.home}publ/ "wikilink")**  
    A simple layout where entries are shown as a list in a table.  
      
-   **[ /publ/?notags=1](${project.home}publ/?notags=1 "wikilink")**  
    Suppresses the ${project.name} tags in the HTML output.  
      
-   **[ /publ/user/beate](${project.home}publ/user/beate "wikilink")**  
    A publication list of the user *beate*.  
      
-   **[/publ/user/beate/myown](${project.home}publ/user/beate/myown "wikilink")**  
    A publication list of the user *beate* with the tag *myown*.  

--------

#### Semantic web formatting

-   **[ /swrc/](${project.home}swrc/ "wikilink")**  
    RDF output according to the [SWRC ontology](http://ontoware.org/swrc/).  

--------

### URL or BibTeX pages

-   **[/url/d1bb7b3f6cafafa7b418f9f356ff2e83](${project.home}url/d1bb7b3f6cafafa7b418f9f356ff2e83 "wikilink")**  
    Shows all public bookmark posts of the URL with the MD5 hash
    d1bb7b3f6cafafa7b418f9f356ff2e83.  
      
-   **[/url/d1bb7b3f6cafafa7b418f9f356ff2e83/jaeschke](${project.home}url/d1bb7b3f6cafafa7b418f9f356ff2e83/jaeschke "wikilink")**  
    Shows the bookmark posts (if you are allowed to see it) of the user
    *jaeschke* with the MD5 hash d1bb7b3f6cafafa7b418f9f356ff2e83.  
      
-   **[/bibtex/1d28c9f535d0f24eadb9d342168836199](${project.home}bibtex/1d28c9f535d0f24eadb9d342168836199 "wikilink")**  
    Shows all public publication posts with the [
    hashkey](${project.home}help/doc/inside.html "wikilink") 1d28c9f535d0f24eadb9d342168836199.
    The used hash is the Inter-Hash.  
      
-   **[/bibtex/25854a71547051543dd3d3d5e2e2f2b67/steff83](${project.home}bibtex/25854a71547051543dd3d3d5e2e2f2b67/steff83 "wikilink")**  
    Shows the publication post (if you are allowed to see it) of the
    user *steff83* with the hashkey 25854a71547051543dd3d3d5e2e2f2b67.
    The used hash is the Intra-Hash. ${project.name} provides a Tag
    JSON Feed belonging to a BibTeX post.  
      
-   **[/json/tags/bibtex/1d28c9f535d0f24eadb9d342168836199](${project.home}json/tags/bibtex/1d28c9f535d0f24eadb9d342168836199 "wikilink")**  
    Provides A JSON output containing all tags related to the [
    publication](${project.home}bibtex/1d28c9f535d0f24eadb9d342168836199 "wikilink") with the Inter-Hash 1d28c9f535d0f24eadb9d342168836199.
    ${project.name} offers the possibility to retrieve a publication by
    its BibTeX key.  
      
-   **[ /bibtexkey/Wille82](${project.home}bibtexkey/Wille82 "wikilink")**  
    Returns publications which have the BibTeX key *Wille82*.  
      
-   **[/bibtexkey/wille82+sys:user:Stumme](${project.home}bibtexkey/wille82+sys:user:Stumme "wikilink")**  
    Returns publications which have the BibTeX key *Wille82* from the
    collection of the user *Stumme*.  
      
-   **[/bibtexkey/hjss06bibsonomy+sys%3Auser%3Ajaeschke](${project.home}bibtexkey/hjss06bibsonomy+sys%3Auser%3Ajaeschke "wikilink")**  
    Shows all posts (which you are allowed to see) with the specified
    BibTeX key `hjss06bibsonomy` of the user *jaeschke*. If you have
    more than one entry with the same BibTeX key, then a list of all
    hits will be given.  
      
-   **[ /bibtexkey/journals/jacm/HopcroftU69/dblp](${project.home}bibtexkey/journals/jacm/HopcroftU69/dblp "wikilink")**  
    You can use BibTeX semantics to refer to entries that we mirror from
    DBLP, once you know how DBLP generates its BibTeX keys.  

---------

### Content negotiation pages

Content negotiation helps to show a source with the same URL in
different ways, according to the settings of the user agent. Adding the
keyword `uri` to your URL enables content negotiation. This is available
for the following pages:  
  

-   URL and BibTeX pages
-   author pages
-   BibTeX-Key pages  
      

**Examples:**  
  

-   **[/uri/url/d1bb7b3f6cafafa7b418f9f356ff2e83](${project.home}uri/url/d1bb7b3f6cafafa7b418f9f356ff2e83 "wikilink")**  
    Shows all bookmarks with the specified hash.  
      
-   **[/uri/bibtex/2b8b87c78e9e27a44aacde0402c642bff](${project.home}uri/bibtex/2b8b87c78e9e27a44aacde0402c642bff "wikilink")**  
    Shows all BibTeX entries with the specified hash.  
      
-   **[/uri/bibtexkey/hjss06bibsonomy/jaeschke](${project.home}uri/bibtexkey/hjss06bibsonomy/jaeschke "wikilink")**  
    Shows all BibTeX entries with the specified key of the user
    *jaeschke*.
    
-   **[ /uri/author/hotho](${project.home}uri/author/hotho "wikilink")**  
    Shows all BibTeX entries of the author *hotho*.  
      

The current supported output formats are HTML, XML, RSS, RDF, and
BibTeX. The user agent specifies a preferred output format in the HTML
accept header defining the priority order with the help of *q-values*.  
For example, the header definition

`Accept:text/xml;q=1,text/html;q=0.9,text/plain;q=0.8,image/png;q=1,*/*;q=0.5`

would trigger the following priority:

-   text/xml
-   image/png
-   text/html
-   text/plain
-   \*/*

------------

### Jabref layouts

The [ export page](${project.home}export/ "wikilink") gives an overview on all available Jabref layouts.  

-   **[ /layout/simplehtml/](${project.home}layout/simplehtml/ "wikilink")**  
    HTML layout without any header or footer. Useful for integrating
    publication lists into other HTML pages.  
      
-   **[ /layout/html/](${project.home}layout/html/ "wikilink")**  
    A simple layout where entries are shown as a list in a table.  
      
-   **[ /layout/tablerefs/](${project.home}layout/tablerefs/ "wikilink")**  
    HTML output with each entry shown as a list in a table and an
    additional JavaScript search function.  
      
-   **[/layout/tablerefsabsbib/](${project.home}layout/tablerefsabsbib/ "wikilink")**  
    Similar to TableRefs. Also includes the BibTeX source and the
    abstract of the publication.  
      
-   **[ /layout/docbook/](${project.home}layout/docbook/ "wikilink")**  
    This is an XML output according to the DocBook schema.  
      
-   **[ /layout/endnote/](${project.home}layout/endnote/ "wikilink")**  
    Output in RIS format which is used by EndNote, a bibliography
    management tool.  
      
-   **[ /layout/dblp/](${project.home}layout/dblp/ "wikilink")**  
    DBLP exports your records to a DBLP conformable XML structure.  
      
-   **[ /layout/text/](${project.home}layout/text/ "wikilink")**  
    BibTeX output.  
      
-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").