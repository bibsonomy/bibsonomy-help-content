<!-- de: LetzteÄnderungen -->

## Recent changes
--------------

The following changes on the wiki pages where made within the last 30
days. To see a complete list, go to [ All changes](FullRecentChanges "wikilink").

This server is located in the time zone *[ {INSERT CurrentTimePlugin
format=zzzz}]({INSERT_CurrentTimePluginFormat=zzzz} "wikilink")*, where
currently, it's the [ {INSERT CurrentTimePlugin format='dd. MMMM yyyy
\\', \\' HH:mm
\\'Uhr\\''}]({INSERT_CurrentTimePluginFormat='dd._MMMMYyyy_\',_\'_HH:mm_\'Uhr\''} "wikilink")
ist.

[ {INSERT com.ecyrd.jspwiki.plugin.RecentChangesPlugin
since=30}]({INSERTCom.ecyrd.jspwiki.plugin.RecentChangesPluginSince=30} "wikilink")

-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").