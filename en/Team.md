<!-- de: Team -->

## Team
----

We are working tirelessly to bring you the best free and open-source web-based
service to organise your bookmarks and publications!

--------------------

### Andreas Hotho

![<File:http://www.bibsonomy.org/picture/user/hotho>](http://www.bibsonomy.org/picture/user/hotho  " File:http://www.bibsonomy.org/picture/user/hotho")

[ Andreas Hotho](${project.home}user/hotho "wikilink") is the Project lead of
BibSonomy. He earned his Master\`s Degree in information systems at the
University of Braunschweig (Germany) in 1998. He worked from 1999 to 2004 at the
Institute of Applied Informatics and Formal Description Methods (AIFB) at the
University of Karlsruhe in the areas of text, data, and web mining, semantic web
and information retrieval where he also received his PhD. Since 2009 he is
professor for data science at the Julius-Maximilians-Universität Würzburg. His
focus is on the combination of machine learning/data mining and semantic web,
called semantic web mining, and especially on text clustering/classification
with background knowledge.  He was involved in organizing several Knowledge
Discovery and Ontology Learning workshops in conjunction with the ECML/PKDD and
KDD conferences.

----------------

### Robert Jäschke

![https://amor.cms.hu-berlin.de/~jaeschkr/img/me.jpg](https://amor.cms.hu-berlin.de/~jaeschkr/img/me.jpg  " http://www.kbs.uni-hannover.de/~jaeschke/img/me.jpg")

[ Robert Jäschke](${project.home}user/jaeschke "wikilink") is the former head of
our developer team and still active developer. His research focuses is on
integration of algorithms for community detection, ranking and recommendations
into folksonomy systems.

-----------------

### Gerd Stumme

![ <File:Stumme.jpg>](team/Stumme.jpg  " File:Stumme.jpg")

[ Gerd Stumme](${project.home}user/stumme "wikilink") is Full Professor of
Computer Science. He is leading the Hertie Chair on Knowledge and Data
Engineering at the University of Kassel, and full member of the Research
Center L3S. Gerd Stumme earned his PhD in 1997 at Darmstadt University
of Technology, and his Habilitation at the Institute Institute of
Applied Informatics and Formal Description Methods (AIFB) of the
University of Karlsruhe in 2002. In 1999/2000 he was Visiting Professor
at the University of Clermont-Ferrand, France, and Substitute Professor
for Machine Learning and Knowledge Discovery at the University of
Magdeburg in 2003. Gerd Stumme published over 80 articles at national
and international conferences and in journals, and chaired several
workshops and conferences. He is member in the Editorial Boards of the
Intl. Journal on Data Warehousing and Mining and of the International
Conference on Conceptual Structures, and was also member of several
conference and workshop Program Committees. Gerd Stumme is leading and
led several national and European projects.

-------------------------------

### Daniel Zoller

![ <File:csm_zoller_17d1047376.jpg>](team/csm_zoller_17d1047376.jpg)

[ Daniel Zoller](${project.home}user/nosebrain "wikilink") is one of the senior
developers of BibSonomy as well as a PhD candidate at the chair for data science
of the Julius-Maximilians-Universität Würzburg. After his master’s degree in
computer science from the University of Kassel in 2013, he started his PhD
project. His research focuses on recommender systems and information
retrieval. Further topics of his research interest include deep learning,
machine learning in general and software engineering.

---------------------------

### Tom Hanika

![ <File:hanika.jpg>](team/hanika_small.jpg)

[ Tom Hanika](${project.home}user/tomhanika "wikilink") is one of the senior
developers of BibSonomy as well as a PostDoc at the research unit Knowledge &
Data Engineering of the University of Kassel.  His research is mainly focused on
knowledge discovery in bipartite structures, in particular their relation
to formal concept analysis, and the application of Algebra, Geometric Measure
Theory, Topology and Logic, at the intersection of Knowledge Discovery, Machine
Learning, and Reasoning.  He is also senior developer and maintainer of the
[conexp-clj] (https://github.com/tomhanika/conexp-clj), a tool for Conceptual
Data Analysis.

---------------------------

### Mario Holtmüller

![ <File:Holtmueller.jpg>](team/Holtmueller_small.jpg)

[ Mario Holtmueller](${project.home}user/mho "wikilink") is one of the senior
developers of BibSonomy. He is also maintainer and developer for the related
[PUMA](https://www.academic-puma.de/ "https://www.academic-puma.de/") project. 

-------------------------------


### Former Scientific Assistants

-------------------

### Stephan Doerfel

![ <File:doerfel.png>](team/doerfel.png  " File:doerfel.png")

[ Stephan Doerfel](${project.home}user/sdo "wikilink") achieved his diploma degree
in mathematics after studies in Dresden (Germany) and Lund (Sweden). He
focussed on Algebraic Structures and Formal Concept Analysis. Since 2009
he is a Ph.D.-student at the Knowledge and Data Engineering Group in
Kassel (Germany).

------------------

### Christoph Scholz

![ <File:scholz.png>](team/scholz.png  " File:scholz.png")

-------------------------------

### Björn-Elmar Macek

![<File:bjoern-elmar_macek.png>](team/bjoern-elmar_macek.png  " File:bjoern-elmar_macek.png")

[ Björn-Elmar Macek](${project.home}user/macek "wikilink") studied Computer
Science at the Technical University of Ilmenau with major focus on
complexity theory, databases and data streams. After his graduation he
got experienced in the field of industrial web development and
consulting and the design and implementation of databases. From September
2009 until 2014 he was a PhD student at the Knowledge and Data Engineering team.

---------------------------

### Martin Atzmüller

![ <File:martin.png>](team/martin.png  " File:martin.png")

[ Martin Atzmueller](${project.home}user/atzmueller "wikilink") He earned his
habilitation (Dr.  habil.) in 2013 at the University of Kassel, and received his
Ph.D. in Computer Science from the University of Würzburg in 2006. He studied
Computer Science at the University of Texas at Austin (USA) and at the
University of Wuerzburg where he completed his MSc in Computer Science.

-------------------------------

### Dominik Benz

![ <File:benz.png>](team/benz.png  " File:benz.png")

[ Dominik Benz](${project.home}user/dbenz "wikilink") obtained his Diploma in
Computer Science with minor Psychology at the University of Freiburg,
Germany in 2007. He was employed currently as a Ph.D. candidate in
Computer Science at the University of Kassel, Germany, in the research
unit Knowledge and Data Engineering. His main research interests lie in
the area of collaborative knowledge management, ontology learning and
collaborative tagging systems.

------------------

### Folke Mitzlaff

![ <File:folke.png>](team/folke.png  " File:folke.png")

-----------------

### Beate Navarro Bullock

![ <File:krause.png>](team/krause.png  " File:krause.png")

[ Beate Krause](${project.home}user/beate "wikilink") studied Information Systems
(Wirtschaftsinformatik) at the Berufsakademie Stuttgart (BA) and at the
Humboldt University of Berlin (M.Sc.). 

--------------------


### Students

The following students helped us in the past or at present in developing
our application:

-   [ Mohammed Abed](${project.home} "wikilink") ![<File:MohammedAbed.jpg>](team/MohammedAbed.jpg  "fig: File:MohammedAbed.jpg")
-   [ Philipp Beau](${project.home} "wikilink") ![<File:PhilippBeau.png>](team/PhilippBeau.png  "fig: File:PhilippBeau.png")
-   [ Waldemar Biller](${project.home} "wikilink") ![<File:waldemar_biller.png>](team/waldemar_biller.png  "fig: File:waldemar_biller.png")
-   [ Manuel Bork](${project.home} "wikilink")
-   [ Christian Claus](${project.home}user/christian_claus "wikilink") ![<File:claus.jpg>](team/claus.jpg  "fig: File:claus.jpg")
- 	[ Jennifer Häfner](${project.home} "wikilink")
-   [ Lukas Hanke](${project.home} "wikilink") ![ <File:Lukas+Hanke.jpg](team/Lukas+Hanke.jpg  "fig: File:Lukas+Hanke.jpg")
-   [ Johannes Hirth](${project.home}"wikilink") ![ <File:johannes.png](team/johannes.png  "fig: File:johannes.png")
-   [ Jens Illig](${project.home} "wikilink")
-   [ Lutful Kabir](${project.home} "wikilink") ![ <File:Lutful+Kabir.jpg](team/Lutful+Kabir.jpg  "fig: File:Lutful+Kabir.jpg")
-   [ Christian Kramer](${project.home} "wikilink")
-   [ Ibrahim Mahmoud](${project.home} "wikilink") ![ <File:Ibrahim+Mahmoud.jpg](team/Ibrahim+Mahmoud.jpg  "fig: File:Ibrahim+Mahmoud.jpg")
-   [ Markus Matz](${project.home}user/cupodmac "wikilink") ![<File:matz.jpg>](team/matz.jpg  "fig: File:matz.jpg")
-   [ Nils Raabe](${project.home} "wikilink") ![<File:Nils_Raabe_96x133.jpg>](team/Nils_Raabe_96x133.jpg  "fig: File:Nils_Raabe_96x133.jpg")
-   [ Serak Rezane](${project.home}user/siko "wikilink") ![<File:rezane.jpg>](team/rezane.jpg  "fig: File:rezane.jpg")
-   [ Jan Oliver Rüdiger](${project.home} "wikilink") ![ <File:Jan+Oliver+Ruediger.jpg](team/Jan+Oliver+Ruediger.jpg  "fig: File:Jan+Oliver+Ruediger.jpg")
-   [ Bastian Schäfermeier](${project.home} "wikilink") ![<File:Bastian_schaefermeier_klein2.jpg>](team/Bastian_schaefermeier_klein2.jpg  "fig: File:Bastian_schaefermeier_klein2.jpg")
-   [ Christian Schenk](${project.home}user/cschenk "wikilink")
-   [ Sven Stefani](${project.home} "wikilink") ![<File:stefani.jpg>](team/stefani.jpg  "fig: File:stefani.jpg")
-   [ Thomas Steuber](${project.home}user/thomas "wikilink")
-   [ Stefan Stützer](${project.home}user/steff83 "wikilink") ![<File:stuetzer.jpg>](team/stuetzer.jpg  "fig: File:stuetzer.jpg")
-   [ Daniil Tanygin](${project.home} "wikilink") ![ <File:Daniil+Tanygin.jpg](team/Daniil+Tanygin.jpg  "fig: File:Daniil+Tanygin.jpg")
-   [ Bernd Terbrack](${project.home} "wikilink")
-   [ Michael Velimirov](${project.home} "wikilink") ![<File:michael_velimirov.jpg>](team/michael_velimirov.jpg  "fig: File:michael_velimirov.jpg")
-   [ Michael Wagner](${project.home}user/michi "wikilink") ![<File:wagner.jpg>](team/wagner.jpg  "fig: File:wagner.jpg")

-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").