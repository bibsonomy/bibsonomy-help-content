# Legal Notice

> Note: The English text below is intended solely as a convenience to non-German-reading users of this website. Any discrepancies or differences that may arise in the translation of the official German version shall not be legally binding.

## Website Owner

Julius-Maximilians-Universität Würzburg  
Postal address: Sanderring 2, 97070 Würzburg, Germany  

Phone: +49 931 31-0  
Fax: +49 931 31-82600  
<info@uni-wuerzburg.de>  

## Legal form and representation

Julius-Maximilians-Universität Würzburg is a corporation under public law and, at the same time, a state institution pursuant to art. 1 (1) Bayerisches Hochschulgesetz (Bavarian Higher Education Act, BayHSchG). The legal representative of Julius-Maximilians-Universität Würzburg is its President, Prof. Dr. Paul Pauli.

## Competent supervisory authority

Bavarian State Ministry of Science and the Arts  
Postal address: Salvatorstraße 2, 80327 München, Germany  

## VAT identification number

The University’s VAT identification number as per section 27a Umsatzsteuergesetz (Value Added Tax Act, UStG) is DE 134187690.

## Disclaimer

Julius-Maximilians-Universität Würzburg does not accept any liability or responsibility for the accuracy, correctness, completeness or accessibility of any information posted on this website; in particular, it disclaims all liability and responsibility for any loss or damage incurred by any user that may arise out of or in connection with the use of the website. This includes, but is not limited to, any damage to, or loss of, information technology, equipment or data that users may incur through downloading material from this website.

In addition, Julius-Maximilians-Universität Würzburg does not accept any liability or responsibility for any content provided on third party websites that can be accessed through links found on the University’s website or for any comments posted by third parties, in particular on the social media channels of Julius-Maximilians-Universität Würzburg. The content of the external websites linked to from this site is solely the responsibility of the providers of those external sites.

## Social Media Channels

This legal notice also covers all of the central social media accounts of the University of Würzburg:

<https://www.facebook.com/uniwue>  
<https://www.twitter.com/uni_wue>  
<https://www.instagram.com/uniwuerzburg>  
<https://www.youtube.com/uniwuerzburg>  
<https://www.linkedin.com/school/julius-maximilians-universitat-wurzburg/>  
<https://www.pinterest.de/uniwue/>  

In addition, this legal notice also covers all of the official social media accounts associated with the University, including, but not limited to, the accounts held by University institutions, faculties, departments, research projects and chairs on the following platforms:

<https://www.facebook.com/>  
<https://www.twitter.com/>  
<https://www.instagram.com/>  
<https://www.youtube.com/>  
<https://de.linkedin.com/>  
<https://tiktok.com/>  
<https://uninow.de/>  
<https://www.pinterest.de/>  
