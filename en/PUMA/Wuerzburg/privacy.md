# Privacy Policy and Data Processing Information

This privacy policy applies to the websites of the domains of the University of Würzburg.

And as a supplement as far as we process personal data in our own responsibility for:

## General information

Data protection is our concern and our legal obligation. In order to adequately protect the security of your data during transmission, we use state-of-the-art encryption procedures (e.g. SSL/TLS) and secure technical systems.

Javascript is used as the active component for the web pages. If you have deactivated this function in your browser, you will receive a corresponding message to activate the function.

### Logging

Out of consideration for events which may affect security, relevant access data is logged each time the web server of the domain uni-wuerzburg.de is accessed. The web servers of Julius-Maximilians-Universität Würzburg are maintained by the Information Technology Centre of the University. Additional web servers may be maintained by the individual Faculties; these are obliged to adhere to the regulations regarding university web servers.

Depending on the access protocol used, the log data includes:

- IP address of the requesting computer
- date and time of request
- access method/function required by the requesting computer
- input values (e. g. file name) transferred by the requesting computer
- web server access status (file transferred, file not found, command not executed, etc.)
- URL from which the file was requested/the function was initiated
- browser identifier.

#### Purpose of logging

Recorded data is used to identify and track illegal access and access attempts to the web server and to create anonymised access statistics for web content optimisation. No user profiles are created.

#### Retention period

Logged data is stored for three months and then deleted provided there is no known web attack leading to civil or criminal prosecution of the attacker.

#### Analysis

The data is analysed by the respective web server operator authorised by Julius-Maximilians-Universität Würzburg.

#### Active components

Cookies set by the University of Würzburg only serve the purpose of logging user-related configuration settings for the duration of the session (e. g. preferred language) on the client side. You have the right to reject our cookies and can do so by setting your browser accordingly; of course, you will still be able to use all information offered.

#### Data communication security

When you send us an email, your email address will only be used for correspondence with you.

We cannot warrant the security of any information transferred via internet and email due to the risk that information, including confidential information, sent in the email and its attachments may be destroyed or intercepted during transmission, may get lost or may arrive late. Although the University of Würzburg endeavours to reduce this risk by putting in place appropriate measures, we cannot warrant that information, emails and their attachments forwarded by the University of Würzburg are free of viruses, worms or other damaging elements.

### Disclaimer

The website of Julius-Maximilians-Universität Würzburg includes information made available by cross references (‘links’) to other sites, which are marked as follows: (Link-New-Window) [or:  [Example link to Google](https://www.google.de/).].

With the cross reference, Julius-Maximilians-Universität Würzburg only provides access to this content (Section 9 Telemediengesetz (Telemedia Act)). The University is not responsible for these ‘foreign’ contents as it does not initiate the transmission of the information, select the receiver of the transmission or select or modify the information contained in the transmission.

Due to the call-up and link method used, there is not even an automatic, intermediate and transient storage of this ‘foreign information’ by Julius-Maximilians-Universität Würzburg. Thus, Julius-Maximilians-Universität Würzburg assumes no liability for these external contents.

When a link to such internet content is created for the first time, however, Julius-Maximilians-Universität Würzburg does verify whether that foreign content may result in possible civil or criminal legal liability.