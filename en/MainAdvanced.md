<!-- de: MainFortgeschrittene -->

## Help for advanced users
-----------------------

This is the **help page for advanced users**. For further
information, please visit the [ help page for beginners](MainBeginner "wikilink") and the
[ help page for software developers](MainDeveloper "wikilink").

${project.name} is a free service to manage your bibliographic
references and publications. In addition, ${project.name} allows you
and your colleagues to work together in groups (e.g. exchange
interesting bookmarks and publications). This page shows you all
functions for advanced users.

----

![<File:Student-Laptop.png>](mainadvanced/Student-Laptop.png  " File:Student-Laptop.png")

### Advanced features

In this section, you will find instructions for the advanced features of
${project.name}. Before you start with these tutorials, we recommend to
**[ unlock the advanced features](UnlockAdvancedMode "wikilink")**, so you have even more ways to work effectively
with your publications.  

-  [ How to change my CV](ChangeCV "wikilink")

-   [ Use RSS-Feeds to always be up to date](RSS-FeedsSubscribe "wikilink")

<!-- -->

-   [ Browse publications](BrowsePublications "wikilink")

<!-- -->

-   [ Use the clipboard to highlight interesting publications](Clipboard "wikilink")

<!-- -->

-   [ Attach private documents to publications](AttachDocuments "wikilink")

<!-- -->

-   [ URL-Syntax](URL-Syntax "wikilink")

<!-- -->

-   (Beta) [ Addon for google docs](AddonGoogleDocs "wikilink")


----

![ <File:User-Group.png>](mainadvanced/User-Group.png  " File:User-Group.png")

### Social features

${project.name} allows any user to associate with each other. Linked
users are called *friends*. In addition, users can form *groups* and
*discuss* publications.  

- [ Add friends/contacts](AddFriends "wikilink")
<!-- -->

-   [ Join a group with the same reading interests](GroupFunctions "wikilink")
<!-- -->
  
-   [ Follow users](Followers "wikilink")
<!-- -->

-   [ Discussion: Reviews and Comments](DiscussionReviewsComments "wikilink")

<!-- -->

-   [ Use the URL-Resolver](OpenURL "wikilink")


----

![ <File:News.png>](mainadvanced/News.png  " File:News.png")

### Press Distribution


Information for media representatives and other interested parties on
${project.name} and related projects:  

-   [ Our Projects](Projects "wikilink")

<!-- -->

-   [ Press Distribution](Press "wikilink")


----

${project.theme == "bibsonomy"}{

![<File:Messages-Information-01.png>](mainadvanced/Messages-Information-01.png  " File:Messages-Information-01.png")

### Contact the developers of ${project.name}

You need help? You have questions about ${project.name}? You need a
special feature or you have an idea for improvement? Contact us! Here is
our contact data. We appreciate your feedback.  

------------------------

![<File:Message-Mail.png>](mainadvanced/Message-Mail.png  "fig: File:Message-Mail.png")
<webmaster@bibsonomy.org>

-   Contact us via email.  

-----------------------------------

![<File:WordPress.png>](mainadvanced/WordPress.png  "fig: File:WordPress.png")
[blog.bibsonomy.org](http://blog.bibsonomy.org)

-   Feature of the week
-   Releases-News
-   Questions, feature-requests and many more

-------------------------

![ <File:Twitter+Bird.png](mainadvanced/Twitter+Bird.png  "fig: File:Twitter+Bird.png")
[Join "BibSonomyCrew" on Twitter](https://twitter.com/BibSonomyCrew)

-   Updates via Tweet

----------------------------

![<File:Conference-Call.png>](mainadvanced/Conference-Call.png  "fig: File:Conference-Call.png")
For all friends of the classic mailing list:
<bibsonomy-discuss@cs.uni-kassel.de>

-   Discuss with us about new features
-   Discussions are public and all Users can see the questions and
    answers

}

--------------------

This is the **help page for advanced users**. For further information, please visit the
[ help page for beginners](MainBeginner "wikilink") and the [ help page for software developers](MainDeveloper "wikilink"). 


