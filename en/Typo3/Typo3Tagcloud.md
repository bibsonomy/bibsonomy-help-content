<!-- de: Typo3/Typo3Tagcloud -->



---------------------------

[`Back to Typo3 page.`](Typo3 "wikilink")

---------------------

## Typo3 - Tag Clouds
------------------

You can also add your ${project.name} tag cloud to your pages. 
To do so, add a new plugin on the page where the tag cloud should be displayed.
Choose **"Tag Cloud from ${project.name}"** from the list. Typically, a tag
cloud will be placed in the sidebar, so you can choose the right or left
content area for inserting.  

-------------------------

![ <File:tagcloudplugin.png>](Typo3Tagcloud/tagcloudplugin.png  "fig: File:tagcloudplugin.png")  

-------------------------

Like in ["Add list of publications"](Typo3/Typo3Publist "wikilink"), you can choose between different options here.  

You can also adapt the tag cloud to your specific needs. To do this, visit the instructions for the tabs [Content Selection](Typo3/TypoTagContent "wikilink") and [Login](Typo3/TypoTagLogin "wikilink").

-------------------------

The result might look like this:

![ <File:tagcloudex.png>](Typo3Tagcloud/tagcloudex.png  "fig: File:tagcloudex.png")  

---------------------------------------