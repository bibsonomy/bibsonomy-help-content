<!-- de: Typo3/Typo3Admin -->



---------------------------

[`Back to Typo3 page.`](Typo3 "wikilink")

---------------------

## Typo3 - Administration
------------------------

On this page, you will find help regarding the administration of the Typo3 plugins.
This includes instructions for installation and managing CSL styles.

---------------------------

### Installation

To install ${project.name} CSL, log into your Typo3 installation as an
administrator and in **Extension Manager =&gt; Import Extensions**,
search for the extension **ext\_bibsonomy\_csl** and import it.  

![ <File:1.png>](Typo3Admin/1.png  "fig: File:1.png")  

After the import is finished, the extension will be displayed in the
section **"Available Extensions"**. Press the **"+"-symbol** for activation.  

![ <File:2.png>](Typo3Admin/2.png  "fig: File:2.png")  

-----------------------------

### Manage your CSL styles with the Backend Module

Typo3 extensions are classified into frontend plugins and backend
modules. You have already learned about the frontend plugins **"add
publication lists"** and **"add tag cloud"**. To manage your CSL
stylesheets you can use the backend module **"CSL Styles"**.

By default there are a lot of pre-installed stylesheets. To add your own
custom styles, create a new folder **"CSL Styles"** in the page tree.
Select this new file, click on **"new"** and then select **"Add a custom style"**.

There are three ways to add a new stylesheets:

1.  **Direct input:** Enter the XML source code of your stylesheet in the
    text area and click on **"Save"**.
2.  **Import from URL:** Enter the URL of the CSL stylesheet file and click
    on **"Import"**.
3.  **Upload CSL file:** Select a CSL XML file from your filesystem and
    click on **"Upload"**.

![ <File:9.png>](Typo3Admin/9.png  "fig: File:9.png")  

Click on **"Show Styles"** to get a preview.  

![ <File:10.png>](Typo3Admin/10.png  "fig: File:10.png")  

To **delete** styles, click on the **trash can icon** on the left of the style.

---------------------------------------