<!-- de: Typo3/TypoGroupsort-->



---------------------------

[`Back to Typo3 page.`](Typo3 "wikilink")

---------------------

## Typo3 - Grouping and Sorting
--------------------

After you successfully inserted the plugin, you can now work on its settings.
The tab **"Grouping and Sorting"** offers the following possibilites:

--------------------

![ <File:plugingrouping.jpg>](TypoGroupSort/plugingrouping.jpg "fig: File:plugingrouping.jpg") 

---------------------------------

1. First, you can choose if you want to group the publications and if yes, by what measure. You can choose between "none", "year ascending", "year descending", "type" and "DBLP".
2. Then, you can choose by what the publications should be sorted. If grouped, they will be sorted within groups. Choose between "year", "title", "author", "type" and "month".
3. Here, you can switch the sorting order to ascending or descending.
4. If you want to show publications of a single year, you can specify it here.
5. The following checkmarks allow you to include non-numeric year specifications and to enable inline filtering.
6. Finally, you can choose between a soft, hard and no filter at all. This is especially useful, if you have chosen "type" at the beginning.

--------------------------

Do you need more information on how ${project.name} uses hashes? Head over to our [helppage](InterIntraHash "wikilink").