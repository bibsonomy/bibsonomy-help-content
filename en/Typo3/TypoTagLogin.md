<!-- de: Typo3/TypoTagLogin -->



---------------------------

[`Back to Typo3 page.`](Typo3 "wikilink")

---------------------

## Typo3 - Tags Login
------------------

After you successfully inserted the plugin, you can now work on its settings.

-------------------------

![ <File:tagcloudlogin.png>](TypoTagLogin/tagcloudlogin.png  "fig:File:tagcloudlogin.png")  

-------------------------

Here, you have to enter your ${project.name} API credentials. You can find your API data [here](${project.home}settings "wikilink"). Your username is displayed there aswell.

---------------------------------------