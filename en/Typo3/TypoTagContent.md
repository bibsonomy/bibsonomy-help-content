<!-- de: Typo3/TypoTagContent -->



---------------------------

[`Back to Typo3 page.`](Typo3 "wikilink")

---------------------

## Typo3 - Tags Content Selection
------------------

After you successfully inserted the plugin, you can now work on its settings.
The tab **"Content Selection"** offers multiple possibilities.

-------------------------

![ <File:tagcloudcontent.png>](TypoTagContent/tagcloudcontent.png  "fig:File:tagcloudcontent.png")  

-------------------------

First, select the plugin's source of data which it uses to create the tag cloud. You can choose between a user and a group. Then, insert the ID of your previous choice.
You can then limit the selection of the tags displayed by only displaying certain tags, ignoring some tags and limiting the number of tags.

---------------------------------------