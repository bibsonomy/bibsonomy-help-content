<!-- de: Typo3/TypoLayout-->



---------------------------

[`Back to Typo3 page.`](Typo3 "wikilink")

---------------------

## Typo3 - Layout
------------------

After you successfully inserted the plugin, you can now work on its settings.
The tab **"Layout"** offers multiple possibilities to define the layout of the publication list.

--------------------

![ <File:pluginlayout1.jpg>](TypoLayout/pluginlayout1.jpg  "fig: File:pluginlayout1.jpg") 

---------------------------------

1. First, you can choose your preferred Bibliography Style. There is already a long list of Styles.
2. Second, choose your preferred language.
3. You can style your publication lists by using Citation Style Language (CSL), an open source XML-based markup language. A large list of freely available stylesheets can be found [here](http://www.zotero.org/styles/).
4. The two checkboxes allow you to show links for a publication abstract and to BibTeX below each listed publication.

-------------------------

![ <File:pluginlayout2.jpg>](TypoLayout/pluginlayout2.jpg  "fig: File:pluginlayout2.jpg") 

-------------------------------

1. The same is possible with links to EndNote, a URL field, a DOI field and to the post in the publication hose (e.g. ${project.name}).
2. An experimental mode that allows you to have personal author links appear directly in the publication. CSL must be processed by the CSL-Processor with a specific author HTML span-class and use semi-colons to seperate each author. 
3. Here, you can define personal websites and e-mails of authors.

-------------------------

![ <File:pluginlayout3.jpg>](TypoLayout/pluginlayout3.jpg  "fig: File:pluginlayout3.jpg") 

-------------------------------

1. If you want to show notes of the publications, check this box.
2. Here, you can insert a list of keys from misc-fields which contain URLs.
3. Choose, if and how thumbnails and links to private documents in your publications should be shown. (Remember the warning at the [Login page](Typo3/TypoLogin "wikilink").)
4. You can specify how curly braces and backlashes should be handled by the plugin. Also BibTex can clean up special characters.
5. Finally, you can define layout modifications by using CSS.

-------------------------

