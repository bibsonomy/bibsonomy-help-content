<!-- de: Typo3/TypoInhalt-->



---------------------------

[`Back to Typo3 page.`](Typo3 "wikilink")

---------------------

## Typo3 - Content Selection
------------------

After you successfully inserted the plugin, you can now work on its settings.
The tab **"Content Selections"** offers multiple possibilities.

--------------------

![ <File:plugincontent.png>](TypoContent/plugincontent.png  "fig: File:plugincontent.png") 

---------------------------------

1. Select content source: Here, you choose the plugins source of data which it uses to create the list of publications. You can show the publications of a specific user, a group or any viewable.
2. Now you have to insert the ID of your previous choice. 

    **Example:** You want to include the publications of the group "testgroup". Therefore, choose "group" as content source and insert the id "testgroup".

3. Now you can filter your selection by using the tags. To select just your own publications,
choose the tag **"myown"**. If you want to select more than one tag, you
have to separate them by a space character. Optionally, you can limit
the number of publications or filter with free text.

4. There is also the possibility to filter the content by using the fulltext
search field.

-------------------------------------