<!-- de: Typo3/Typo3Publist -->



---------------------------

[`Back to Typo3 page.`](Typo3 "wikilink")

---------------------

## Typo3 - list of publications
------------------

After you installed and activated the extension, you can create
publication lists with the Frontend Plugin. 

-----------------------

For help regarding the different columns, go to our respective helppages:
[Content Selection](Typo3/TypoContent "wikilink"), [Grouping and Sorting](Typo3/TypoGroupsort "wikilink"), [Layout](Typo3/TypoLayout "wikilink"), [Details](Typo3/TypoDetails "wikilink") and [Login](Typo3/TypoLogin "wikilink").)

-------------------------

To create a list of publications follow these steps:

First, go to the page where the publication list shall be
displayed. There, you have to add a new plugin by choosing "Insert Plugin" as **Content Element** type.

--------------------------

Now choose **"Publication List from ${project.name}"** from the list.  

![ <File:selectplugin.png>](Typo3Publist/selectplugin.png  "fig: File:selectplugin.png")  

--------------------------

After correctly inserting the list to your page, you have numerous possibilities to adjust its appearance and its function.
There are five columns, which provide multiple settings.

![ <File:pluginoptions.jpg>](Typo3Publist/pluginoptions.jpg  "fig: File:pluginoptions.jpg") 

--------------------------------------

For help regarding the different columns, go to our respective helppages:
[Content Selection](Typo3/TypoContent "wikilink"), [Grouping and Sorting](Typo3/TypoGroupsort "wikilink"), [Layout](Typo3/TypoLayout "wikilink"), [Details](Typo3/TypoDetails "wikilink") and [Login](Typo3/TypoLogin "wikilink").

**Warning:** Some of them are obligatory for the plugin to work properly.

-------------------------

It might also useful to insert a title for your publication list in the tab **"General"** as header.  

![ <File:plugingeneral.jpg>](Typo3Publist/plugingeneral.jpg  "fig: File:plugingeneral.jpg")  

------------------------------

**Warning:** Be aware that you are using your own account to retrieve
the posts from ${project.name}. That means that all posts that are
visible to you in ${project.name} (your **private** ones too as well as
**attached documents**) will be made visible on your page if they fit
to the description in the plugin record. As a result, you can get
copyright issues. We recommend to create an additional ${project.name}
account just for Typo3 publication management.  

-----------------------