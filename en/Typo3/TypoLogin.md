<!-- de: Typo3/TypoLogin-->



---------------------------

[`Back to Typo3 page.`](Typo3 "wikilink")

---------------------

## Typo3 - Login
--------------------

After you successfully inserted the plugin, you can now work on its settings.
Here, you have to enter your ${project.name} API credentials. You can find your API data [here](${project.home}settings "wikilink"). Your username is displayed there aswell.

--------------------

![ <File:pluginlogin.jpg>](TypoLogin/pluginlogin.jpg "fig: File:pluginlogin.jpg") 

---------------------------------

**Warning:** Be aware that you are using your own account to retrieve
the posts from ${project.name}. That means that all posts that are
visible to you in ${project.name} (your **private** ones too as well as
**attached documents**) will be made visible on your page if they fit
to the description in the plugin record. As a result, you can get
copyright issues. We recommend to create an additional ${project.name}
account just for Typo3 publication management.  

-----------------------



