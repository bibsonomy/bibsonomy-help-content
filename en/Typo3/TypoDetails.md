<!-- de: Typo3/TypoDetails-->



---------------------------

[`Back to Typo3 page.`](Typo3 "wikilink")

---------------------

## Typo3 - Details
------------------

After you successfully inserted the plugin, you can now work on its settings.
The tab **"Details"** offers the following possibility:

--------------------

![ <File:plugindetails.jpg>](TypoDetails/plugindetails.jpg "fig: File:plugindetails.jpg") 

---------------------------------

By checking the box, the plugin creates a link to a details page below each publication. That page should show special information on it.

---------------------


