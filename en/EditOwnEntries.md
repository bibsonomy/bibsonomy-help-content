<!-- de: EigeneEinträgeBearbeiten -->

## Edit own entries
--------------

**Goal:** These instructions show you how to edit several bookmarks/publications at once.  

**Note:** You are only able to edit **your own entries**
(the entries you have copied to your collection). Entries that are not
in your collection **cannot be edited**.


------------------------------------------------------------------------

### Overview

If you don't want to edit only one bookmark/publication, but several at
once, go to your **user page** (e.g. by clicking on your username on the
right hand side of the blue bar) and click on the **pen symbol** next
to the column with the bookmarks or publications, respectively.  

${project.theme == "bibsonomy"}{

![ <File:menu.png>](editownentries/menu.png  "fig: File:menu.png")  

You will be redirected to a page structured like this:  

![ <File:page_neu.png>](editownentries/page_neu.png  "fig: File:page_neu.png")  

}

${project.theme == "puma"}{

![ <File:menu.png>](editownentries/menu.png  "fig: File:menu.png")  

You will be redirected to a page structured like this:  

![ <File:page_neu.png>](editownentries/page_neu.png  "fig: File:page_neu.png")  

}

${project.theme == "wueresearch"}{

![ <File:menuwueen.png>](wueresearch/editownentries/menuwueen.png  "fig: File:menuwueen.png")  

You will be redirected to a page structured like this:  

![ <File:page_neuwueen.png>](wueresearch/editownentries/page_neuwueen.png  "fig: File:page_neuwueen.png")  

}
1. **Choose action:** Here you can choose what you want to change in your
    entries.

2.  **Add tags:** Here you can enter tags that should be added to
    your entries.
    
3.  **Update privacy:** Select the visibility of your entries.

4.  **Your entries:** Here, your entries are listed. On the right, an
    entry's tags are displayed. Select or deselect an entry by clicking
    on the check box on the left.
    
5.  **Next page:** If your entries don't fit on one page, you can
    navigate to the next page here. You can set the amount of entries
    that are displayed on one page in the **[settings](${project.home}settings "wikilink")** in the tab **"settings"**.  

**Note:** Some fields on this page are **inactive** initially (that
means, they cannot be used and are **displayed in grey**). After you
have **chosen an action**, the respective fields will be activated.

------------------------------------------------------------------------

### Actions

Next to **"Please select action"**, there are **five possible actions**
to edit entries. After choosing an action, **select the entries** to
which the action should be applied. At the end, confirm the action by
clicking on **"update"**.

![ <File:Actions.png>](editownentries/Actions.png  "fig: File:Actions.png") 

#### Add tags to all the selected posts
Enter the [tags](Tags "wikilink") that should be added to the selected entries into the field **"add tags"**.  


#### Edit each post's tags separately

You can edit your posts' [tags](Tags "wikilink") **separately** by entering the tag changes into the field next to an entry **("Your posts' tags")**.  


#### Normalize BibTeX Key

You can normalize the BibTeX keys of the selected entries. That means, the posts' keys will be set to the pattern ***nameYearTitle***.  


#### Delete post(s)

Select the entries that should be **deleted** from your collection.  


#### Update privacy
Here you can choose who should be able to see your
entries. In the field **"Update privacy (Viewable for:)"**, choose
between the options **public**, **private** and **other** (e.g. friends
or groups).  

-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").
