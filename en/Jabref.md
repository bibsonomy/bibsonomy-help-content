<!-- de: Jabref -->

## JabRef plugin - data exchange
-----------------------------

**Caution:** Since JabRef version 2.12, **plugins are no
longer supported** in JabRef. So in newer versions the ${project.name}
plugin is no longer working.

To exchange data between JabRef and ${project.name}, you have to
install the ${project.name} plugin first. You can find information on
that [ here](JabrefInstall "wikilink"). If you have questions regarding the plugin, visit our [ contact page](Contact "wikilink").


------------------------------------------------------------------------

### The ${project.name} sidebar

To start working in JabRef, open a new database or open an existing one.  
The ${project.name} plugin for JabRef contains a ${project.name}
sidebar that provides you with important functions. To open the sidebar,
click on the *BibSonomy button* in the JabRef toolbar.

![<File:jabrefSidebar.png>](jabref/jabrefSidebar.png  "fig: File:jabrefSidebar.png") 

In the sidebar, you can do several actions.

-   **Search**  
    Here you can search for publications that are stored
    in ${project.name}. Just enter the search term into the text field.
    You can choose between searching for *Full text* or a *Tag*. Then,
    click on the *search button*.

<!-- -->

-   **Tags**  
    The plugin provides you with a tagcloud with the most used tags by
    you, all users or a certain group you are a member of. Reloading the
    tagcloud can be achieved by clicking on the *Refresh button*. By
    clicking on a tag, the entries that are tagged with it will
    be displayed.

<!-- -->

-   **Import posts**  
    Here you can choose whether you want to import only your own posts
    from ${project.name} or the posts from all users. Click on the
    *Refresh button* to update the tagcloud.

<!-- -->

-   **Settings**  
    Click here to view and change the settings for the
    ${project.name} plugin.

------------------------------------------------------------------------

### Import data from ${project.name} to JabRef

You can import your posts that you collected in ${project.name} to JabRef.

1.  In the menu *Plugins*, click on *BibSonomy*. Choose the function
    *Import all my posts*.  
    
    ![<File:importPosts.png>](jabref/importPosts.png  "fig: File:importPosts.png")  
    
2.  Select the entries in the list that you want to import. Finally,
    confirm by clicking on *OK*. The entries will be imported to your
    JabRef database.

------------------------------------------------------------------------

### Export data from JabRef to ${project.name}

You can export your posts that you collected in JabRef to
${project.name}.

1.  In JabRef, choose the files that should be exported
    to ${project.name}. You can select several files by keeping the
    CTRL key pressed and then clicking on the files.  
    
2.  Then in the menu *Plugins*, click on *BibSonomy* and then on *Export
    selected entries*. Already existing posts will be updated.  
    
    ![<File:exportPosts.png>](jabref/exportPosts.png  "fig: File:exportPosts.png")  
    

**Note**: If you didn't enter a tag for an entry you'll get the error
message "The following selected entries have no keywords assigned". If
you confirm with "yes", the tag "noKeywordAssigned" will be entered as a
tag automatically. If you click on "no", you can enter your own tags.

In JabRef, tags are usually separated by commas. When exporting from
JabRef to ${project.name}, the commas will be replaced by spaces.  

---------------------------------

### Change the delimiter in JabRef

In JabRef, you can choose the delimiter symbol for tags that are entered
into the keyword field. To change the delimiter, go to *Options &gt;
Preferences &gt; Groups*.  

![<File:delimiter.png>](jabref/delimiter.png  "fig: File:delimiter.png")  

There you can enter your preferred delimiter symbol into the text field
*When adding/removing keywords, separate them by*. Please note that in
${project.name}, blank spaces within a tag are not allowed. Therefore,
follow these steps when wanting to upload an entry to ${project.name}:

1.  Separate the content of the tag field according to your JabRef
    delimiter symbol
2.  Delete all remaining blank spaces

We generally suggest to use blank spaces as a delimiter symbol for tags.

------------------------------------------------------------------------

### Delete entries

With this plugin, it is possible to delete entries from your own
${project.name} publication collection.

1.  In JabRef, choose the files that should be deleted. You can select
    several files by keeping the CTRL key pressed and then clicking on
    the files.  
    
2.  Then in the menu *Plugins*, click on *BibSonomy* and then on *Delete
    selected entries*. Please note that the entry will be deleted from
    your ${project.name} collection - the local copy of the entry
    within JabRef will still persist!

------------------------------------------------------------------------

### Synchronize entries

Since JabRef version 1.5 it is possible to synchronize your local and
remote BiBTeX entries with a singles click. To perform the
synchronization simply go to *Plugins -&gt; Bibsonomy -&gt; Synchronize*.  

If there is any difference between local and remote publications a
dialog pops up with both the local and the remote source of the
publication and non equal fields will be highlighted in a bright yellow
color.

![<File:synchronize.png>](jabref/synchronize.png  "fig: File:synchronize.png") 

By choosing *Keep local* or *Always keep local* your local publication
will be exported to ${project.name}. Also *Always keep local* remebers
your decision for the current session.  
If you choose *Keep remote* or *Always keep remote* your local BibTeX
entry will be overwritten by the remote entry from ${project.name}.
*Always keep remote* also remebers your decision for the whole session.

------------------------------------------------------------------------

### Private documents

With JabRef version 2.5 the handling of documents changed completely.
All documents will be downloaded and uploaded automatically as long as
you activated the options in the settings dialogue *(Plugins -&gt; BibSonomy -&gt; Settings)*.

To avoid importing all your entries again you can select *Download my
documents* from the BibSonomy menu if you deactivated the download
option in the settings dialog before.

To upload newly added documents just export the certain entry, but don't
forget to activate the option in the settings dialogue.

------------------------------------------------------------------------

### Proxy settings

If you are using JabRef from a setting behind a proxy, you can still use
the ${project.name} plugin. It makes use of the same proxy mechanism as
JabRef itself; this means, when you start JabRef with the options

`java -Dhttp.proxyHost="hostname" -Dhttp.proxyPort="portnumber"`

then the ${project.name} plugin also uses these parameters (replace
hostname with the your proxy's hostname and portnumber with your proxy's
portnumber).

-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").
