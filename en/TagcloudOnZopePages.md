<!-- de: TagwolkenAufZope-Seiten -->

## Tag clouds on Zope pages
------------------------

${project.name} tags can be displayed on a [Zope](http://zope.org/) page. An example can be found [here](http://www.kde.cs.uni-kassel.de/macek).

-   You will need to grab a ${project.name} page from within Zope. For
    that, you need the product [Kebas Data](http://sourceforge.net/projects/kebasdata/).
    
-   For each tag cloud you want do display, you need a KebasData object.
    Configure it as follows (of course, replacing the user name etc.):  
      
    ![ <File:kebas_config.png>](tagcloudonzopepages/kebas_config.png  "fig: File:kebas_config.png")  
      
    This will display all the tags in your tag cloud which are located
    between the &lt;ul ...> and the &lt;/ul> start and end patterns.

-   You need to fix the URLs that ${project.name} shows though, as they
    are relative to the ${project.name} root and not to your site. For
    this, add a "Script (Python)" object named **`render_fixbaseurl`**
    in Zope anywhere above the folder containing your tag cloud. Let it
    have two parameters (the names don't matter) and make it look like
    this (you may have to replace "http://www.bibsonomy.org/" with the
    ${project.name} address):

        ul = context.match[0]
        ul = ul.replace('href="/', 'href="http://www.bibsonomy.org/')
        print ul
        return printed
        some code block

-   To display your tag cloud from *DTML*, use these lines of code:

        <ul class="tagbox">
        <dtml-var tagcloud>
        </ul>

-   To display your tag cloud from a *Page Template*, you can use this
    piece of code:

        <ul class="tagbox">
        <div tal:replace="structure here/tagcloud"/>
        </ul>

-   Use CSS to format the tag cloud to your liking. Here's what we use;
    note that this hides the low-frequency tags. You can replace the
    **`display: none`** with **`display: inline`** to display the
    low-frequency tags.

        ul.tagbox { list-style: none; text-align: justify; }
        ul.tagbox li { display: inline; }
        ul.tagbox li a { display: none; text-decoration: none; color: #e05698; font-size: 60% }
        ul.tagbox li.tagone a {  display: none; text-decoration: none; color: #a3004e; font-size: 80% } 
        ul.tagbox li.tagten a {  display: inline; text-decoration: none; color: #830030; font-size: 100% }
  

-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").