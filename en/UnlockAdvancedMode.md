<!-- de: ErweiterteFunktionenFreischalten -->

## Unlock advanced mode
--------------------

In order to achieve high usability of ${project.name}, we have decided
to only display relevant features for different user types. Starting
with ${project.name} you only see the basic functionality. However, the extended
features might be useful for advanced users.  
  
**Goal:** This tutorial shows you how to unlock the advanced features of
${project.name} in five easy steps.

  
![ <File:1.png>](unlockadvancedmode/1.png  "fig: File:1.png")  
  
Instructions:

1.  Click on the user menu.

2.  Click on menu entry **"settings"**.  
      
    ![ <File:2.png>](unlockadvancedmode/2.png  "fig: File:2.png")  
    
3.  Then click on the tab **"settings"**.

4.  Under **"appearance"**, select the **"advanced"** option.

5.  Save your selection by clicking on **"set layout"**.

  
The extended layout is now activated.

-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").
  

