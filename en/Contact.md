<!-- de: Impressum -->

## Contact
-------

**_Note:_** If you need help using ${project.name}, please visit the [help pages](Overview "wikilink") first. The following contact options are not intended for this. However, if you have any further questions, do not hesitate to contact us per email ${project.email} .

${project.theme == "bibsonomy"}{

BibSonomy is a bookmarking-service offered and maintained by the following three institutions:  

- [Knowledge and Data Engineering Group](https://www.kde.cs.uni-kassel.de/)
of the University of Kassel, Germany
- [Data Mining and Information Retrieval Group](http://www.dmir.uni-wuerzburg.de/)
of the University of Würzburg, Germany
- [Library and Information Science](https://www.ibi.hu-berlin.de/en/) Information Processing and Analytics,
Humboldt University of Berlin, Germany  

We are not responsible for the content of the users.

Responsible for the website and service are:  

Julius-Maximilians-Universität Würzburg (public corporation)  
Am Hubland  
D-97074  Würzburg  
Tel: +49 931 / 31 - 89675  
Fax: +49 931 / 31 - 86732  
${project.email}

Authorized representative  
for Data Mining and Information Retrieval Group at LS VI  
Prof. Dr. Andreas Hotho  

Value added tax identification number:  
The value added tax identification number according to § 27a Value Added Tax Act is DE 134187690  

Competent supervisory authority:  
Bayerisches Staatsministerium für Wissenschaft und Kunst  
Postal address:  
Salvatorstraße 2  
80327 München  

-----------------------------------

Universität Kassel (public corporation)  
Wilhelmshöher Allee 73  
34121 Kassel  
Tel.: +49 561 804 - 6250  
Fax: +49 561 804 - 6259  
${project.email} 

Authorized representative
Fachbereich Elektrotechnik/Informatik  
Fachgebiet Wissensverarbeitung  
Prof. Dr. Gerd Stumme 

Value added tax identification number:  
The value added tax identification number according to § 27a Value Added Tax Act is DE 113057055  

Competent supervisory authority:  
Hessisches Ministerium für Wissenschaft und Kunst  
Rheinstraße 23-25  
65185 Wiesbaden  

----------------------------------

Humboldt University of Berlin (public corporation)  
Unter den Linden 6  
10099 Berlin  
Tel: +49 30 2093–0  
Fax: +49 30 2093–2770  

Value added tax identification number:  
The value added tax identification number according to § 27a Value Added Tax Act is DE 137176824  

------------------------------------------------------------------------

### Addresses

[Prof. Dr. Andreas Hotho](http://www.dmir.uni-wuerzburg.de/staff/hotho/)  
Julius-Maximilians-Universität Würzburg  
Data Mining and Information Retrieval Group am LS VI  
Am Hubland  
D-97074 Würzburg  
Tel: +49 931 / 31 - 89675  
Fax: +49 931 / 31 - 86732  
${project.email}

[Prof. Dr. Robert Jäschke](https://amor.cms.hu-berlin.de/~jaeschkr/)  
Humboldt-Universität Berlin
Institut für Bibliotheks- und Informationswissenschaft 
Sachgebiet Information Processing and Analytics
Dorotheenstraße 26  
10099 Berlin  
Phone.: +49 30 / 2093 - 70960  
Fax: +49 30 / 2093 - 4335  
${project.email}

[Prof. Dr. Gerd Stumme](http://www.kde.cs.uni-kassel.de/stumme)  
Universität Kassel  
Fachbereich Elektrotechnik/Informatik  
Fachgebiet Wissensverarbeitung  
Wilhelmshöher Allee 73  
34121 Kassel  
Tel.: +49 561 / 804 - 6250  
Fax: +49 561 / 804 - 6259  
${project.email}

------------------------------------------------------------------------

### Mailing lists

These are the mailing lists that have been established for this project.
For each list, there is a subscribe, unsubscribe, a post and an archive link.

**BibSonomy Discuss**  
For BibSonomy users, also for questions regarding the JabRef plugin  
[Subscribe](https://mail.cs.uni-kassel.de/mailman/listinfo/bibsonomy-discuss)  
[Unsubscribe](https://mail.cs.uni-kassel.de/mailman/listinfo/bibsonomy-discuss)  
[Post](mailto:bibsonomy-discuss@cs.uni-kassel.de)  
Archive: [mail.cs.uni-kassel.de](https://mail.cs.uni-kassel.de/pipermail/bibsonomy-discuss)

**Genealogy**  
List for users of the BibSonomy genealogy  
[Subscribe](https://mail.cs.uni-kassel.de/mailman/listinfo/genealogie)  
[Unsubscribe](https://mail.cs.uni-kassel.de/mailman/listinfo/genealogie)  
[Post](mailto:genealogie@cs.uni-kassel.de)  
Archive: [mail.cs.uni-kassel.de](https://mail.cs.uni-kassel.de/pipermail/genealogie/)

------------------------------------------------------------------------

### Repository

BibSonomy is hosted on [Bitbucket](https://bitbucket.org). It is part of
the [BibSonomy team's repository](https://bitbucket.org/bibsonomy/).

-   [Repository](https://bitbucket.org/bibsonomy/bibsonomy)
-   [Source code](https://bitbucket.org/bibsonomy/bibsonomy/src)
-   [Issues](https://bitbucket.org/bibsonomy/bibsonomy/issues?status=new&status=open)
-   [Wiki](https://bitbucket.org/bibsonomy/bibsonomy/wiki/Home)
-   [Licenses](https://bitbucket.org/bibsonomy/bibsonomy/wiki/License)

}

${project.theme == "puma"}{

This service is offered by the [Universitätsbibliothek (university library) of Kassel, Germany](http://www.ub.uni-kassel.de/)
and has been developed in cooperation with the [Knowledge and Data Engineering
Group](http://www.kde.cs.uni-kassel.de/) of the University of Kassel, Germany and the [Data Mining and Information Retrieval
Group](http://www.dmir.uni-wuerzburg.de/) of the University of Würzburg, Germany.

We offer the bookmarking service but we are not responsible for the
content of the users.

------------------------------------------------------------------------

### Addresses

Universitätsbibliothek Kassel

Diagonale 10 34127 Kassel

Telefon: 0561-804-2117

Telefax: 0561-804-2125

E-Mail: direktion(at)bibliothek.uni-kassel.de

Internet: [www.ub.uni-kassel.de](http://www.ub.uni-kassel.de)


The University Library Kassel is a facility of the University Kassel,
represented by the Head Librarian Dr. Axel Halle. Legal representative
of University Kassel is President Prof. Dr. Postlep.  
Competent supervisory authority:

Hessisches Ministerium für Wissenschaft und Kunst (Hessian Ministry of
Science and Art)  
Rheinstr. 23 – 25  
65185 Wiesbaden

------------------------------------------------------------------------

### Mailing lists

These are the mailing lists that have been established for this project.
For each list, there is a subscribe, unsubscribe, and an archive link.
The mailing lists below belong to PUMA's sister project BibSonomy, but
can also be used for questions regarding PUMA.

**BibSonomy Discuss**  
For BibSonomy users, also for questions regarding the JabRef plugin  
[Subscribe](https://mail.cs.uni-kassel.de/mailman/listinfo/bibsonomy-discuss)  
[Unsubscribe](https://mail.cs.uni-kassel.de/mailman/listinfo/bibsonomy-discuss)  
[Post](mailto:bibsonomy-discuss@cs.uni-kassel.de)  
Archive: [mail.cs.uni-kassel.de](https://mail.cs.uni-kassel.de/pipermail/bibsonomy-discuss)

**Genealogy**  
List for users of the BibSonomy genealogy  
[Subscribe](https://mail.cs.uni-kassel.de/mailman/listinfo/genealogie)  
[Unsubscribe](https://mail.cs.uni-kassel.de/mailman/listinfo/genealogie)  
[Post](mailto:genealogie@cs.uni-kassel.de)  
Archive: [mail.cs.uni-kassel.de](https://mail.cs.uni-kassel.de/pipermail/genealogie/)

------------------------------------------------------------------------

### Repository

PUMA is hosted on [Bitbucket](https://bitbucket.org). It is part of the
[BibSonomy team's repository](https://bitbucket.org/bibsonomy/).

-   [Repository](https://bitbucket.org/bibsonomy/puma/overview)
-   [Source code](https://bitbucket.org/bibsonomy/puma/src)
-   [Issues](https://bitbucket.org/bibsonomy/puma/issues?status=new&status=open)
-   [Wiki](https://bitbucket.org/bibsonomy/puma/wiki/Home)
-   [License](https://bitbucket.org/bibsonomy/puma/src/stable/bibsonomy-webapp-puma/LICENSE.txt)

}

-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").