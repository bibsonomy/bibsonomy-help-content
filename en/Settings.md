<!-- de: Einstellungen -->

## Settings
--------

![ <File:0.png>](settings/0.png  "fig: File:0.png")

To access the [ settings](${project.home}settings "wikilink") page, click on the person icon in the right [ main menu](User_Interface "wikilink"), then
click on "settings". Now, you can choose between different tabs to view
and edit your personal user settings.  

![ <File:1.png>](settings/1.png  "fig: File:1.png")  

-   **my profile:** You can enter general personal information about
    yourself, like real name, birthday, your contact data, your
    interests, and you can upload a profile picture. The information you
    entered here will also be displayed on your [ CV page](ChangeCV "wikilink").  
    In addition, you can also change your profile's visibility here
    (public, private or friends).  
    
-   **settings:** Here you can change the [ layout of your tag box and
    post lists](TagLayout "wikilink"), view your API key, change logging
    and deleting options, set your [favourite export formats] (FavouriteExportFormats "wikilink"), change your password and [ delete your
    account](DeleteAccount "wikilink").  
    
-   **layout files:** Here you can upload your layout files
    to customize your publication list. You can find further information on the internal help pages for [CSL layouts](https://www.bibsonomy.org/help_en/settings/CSLLayouts) or [JabRef layouts](https://www.bibsonomy.org/help_en/settings/JabRefLayouts) and on the official pages of [JabRef](http://jabref.sourceforge.net/help/CustomExports.php)
    and [CSL](http://docs.citationstyles.org/en/stable/primer.html).  
    
-   **curriculum vitae:** You can view and edit the information that is
    displayed on your CV page, also you can change the layout ([view
    instructions here](ChangeCV "wikilink")).  
    
-   **OAuth consumers:** An overview of all consumers that get an
    [OAuth](http://oauth.net/) authorisation by you to your account. To
    learn how to access ${project.name} from your application using
    OAuth, [ click here](OAuth "wikilink").  
    
-   **groups:** Here you can create a new [group](GroupFunctions "wikilink") and get an overview
on the groups you are a member of.  
    
-   **synchronization:** To keep your bookmarks and publications in sync
    between two systems, you can add a [ new synchronization
    client](Synchronisation "wikilink") here.

-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").