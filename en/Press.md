<!-- de: Presse -->

${project.theme == "bibsonomy"}{

## Press Coverage
--------------

We are in the news! Click on the links below to read several articles
about ${project.name} in newspapers and magazines online.  

- Take a look at [BibSonomy/PUMA news](https://www.academic-puma.de/aktuelles/)!

-   Berufsverband Information Bibliothek :: 2014 :: [PUMA als
    Schnittstelle zwischen Discovery Service, Institutional Repository und eLearning](http://puma.uni-kassel.de/bibtex/24c4916e0eab92e3e6465711a2f978885/seboettg)

<!-- -->

-   VuFind Anwendertreffen :: September 2013 :: [Tag-basierte Merkliste
    in VuFind mit PUMA - einer Plattform für akademisches
    Publikationsmanagement](https://www.tub.tuhh.de//vufind-anwendertreffen/files/2013/08/PUMA-VuFind.pdf)

<!-- -->

-   Technologiewerte.de :: November 2012 :: [BibSonomy zeigt, wie
    Datenschutz im Web 2.0 geht](http://www.kde.cs.uni-kassel.de/presse/Technologiewerte-de.pdf)

<!-- -->

-   ZEIT Campus :: Juni 2012 :: [Diese Programme und Online-Dienste
    erleichtern das Arbeiten im Team](http://books.google.de/books?id=kS-4AAAAQBAJ&pg=PT70&lpg=PT70&dq=zeit+campus+Gemeinsam+die+Zeit+verdoodeln&source=bl&ots=iosBCbkgW7&sig=b7BEmo4BAIyUNWbmfUoC-CWydtY&hl=de&sa=X&ei=YoVjU7_IOKGp4gTDy4CYBQ&ved=0CCEQ6AEwAA)

<!-- -->

-   publik :: Februar 2011 :: [Publikationen clever verwalten -
    Online-Plattform zur Literaturorganisation](http://www.kde.cs.uni-kassel.de/presse/publik-Artikel_Puma_20110201.pdf)

<!-- -->

-   UNIK Press :: Juli 2010 :: [PUMA - wissenschaftliche Publikationen
    clever verwalten!](http://www.kde.cs.uni-kassel.de/presse/mitteilung20100712.pdf)

<!-- -->

-   eduCampus :: Mai 2010 :: [Wissenschaftliche Veröffentlichungen:
    Unterstützung für die Wissenschaftler der Uni Kassel](http://educampus.uni-kassel.de/blog/2010/05/14/wissenschaftliche-veroffentlichung-unterstutzung-fur-die-wissenschaftler-der-uni-kassel/)

<!-- -->

-   HNA :: Dezember 2008 :: [Bücher jetzt einfacher verwalten](http://www.kde.cs.uni-kassel.de/presse/HNA-Artikel-12-08)

<!-- -->

-   e-teaching.org :: August 2007 :: [e-teaching im Gespräch mit Robert Jäschke](http://www.e-teaching.org/news/eteaching_blog/et_showEntries?permalink=1188305918)

<!-- -->

-   netConnect - Library Journal :: Oktober 2006 :: Product Pipeline;
    Melissa L. Rethlefsen looks at social reference managers and what
    they mean for librarians

<!-- -->

-   Kefk :: Juni 2006 :: [Kasseler Informatiker entwickeln komfortable
    Literatur- und Lesezeichenverwaltung](http://kefk.org/information.retrieval/kasseler.informatiker.entwickeln.komfortable.literatur.und.lesezeichenverwaltung)

<!-- -->

-   UNIK Press :: Juni 2006 :: [Soziale Komponente für das Web: Kasseler
    Informatiker entwickeln komfortable Literatur- und Lesezeichenverwaltung](http://www.kde.cs.uni-kassel.de/presse/mitteilung20060627.html)

<!-- -->

-   c\`t 25/2006 :: April 2006 :: [Uni-Bibliothek - www.bibsonomy.org](http://www.heise.de/kiosk/archiv/ct/2006/25/238)

<!-- -->

-   UNIK Press :: März 2006 :: [Neues Lesezeichen-Verwaltungssystem
    hilft, Webseiten und Bücher leichter zu verwalten](http://www.kde.cs.uni-kassel.de/presse/mitteilung20060321.html)

}

-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").