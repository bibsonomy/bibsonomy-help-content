<!-- de: LiteraturlisteExportieren -->

## Bibliography Export
-------------------

With ${project.name}, you are able to export your lists of
bookmarks/publications in order to use them with other programs. The
internationally standardized data format that is supported by all major
bibliographic management programs is called BibTeX. The export takes
place in two steps. First, you have to export the bookmarks/publications
from ${project.name}, then you have to import the BibTeX data into the
requested program.  

-------------


### Bibliography Export in general

#### Copy citations

${project.theme == "bibsonomy"}{

In order to cite a single publication, you can copy the corresponding citation to your
local clipboard.

There are two options to do so: 

**A:** Click on the downwards-pointing arrow in the menu located in 
the right corner at the bottom of each publication entry.
You can now choose from your [favourite export formats](FavouriteExportFormats"wikilink").
After you selected the format you like, a new window appears.
Click the button at the very bottom to copy the citation to your local clipboard.
You can now import the citation into your requested program.

![ <File:cit1.png>](exportdata/cit1.png  "fig: File:cit1.png")

**B:** Click on the name of the publication you want to export.
At the bottom of the page with the publication details, you will find the option to
**cite this publication** by clicking the button at the very bottom. 

----------------

### How to build individual bibliography out of your publications

**Goal:** This tutorial shows you how to build bibliographies out of
your publications. You'll also learn how to save the bibliographies in
${project.name} and/or export them.

**Requirements:** If you don’t know how to search and view bookmarks and
publications in ${project.name} yet, please read the following tutorial
before you continue: [ Manage literature and bookmarks](BookmarkPublicationManagement "wikilink"). 

---------------

If you want to export a list of several publications you need to add them
to your bibliography first. Follow these steps to do so:

1.  After you have used the search bar (at the top on the right) to
    search for publications, the resulting bookmarks are displayed on
    the left-hand side and publications are displayed on the
    right-hand side. With every publication comes a
    corresponding toolbar.
    
2.  Click on the folder symbol (if you
    hover the mouse cursor over the symbol, the message **“add this
    publication to your clipboard”** appears).  
    
    ![ <File:1_new.png>](exportdata/1_new.png  "fig: File:1_new.png")  
    
3.  The publication is now stored in your clipboard. To view the
    clipboard, click on the person icon in the right [main menu](User_Interface "wikilink") and in the
    following sub-menu, click on **“clipboard”**. In addition, you can
    see a red circle with the number of publications in your clipboard
    next to the person icon.  
    
    ![ <File:2_new.png>](exportdata/2_new.png  "fig: File:2_new.png")  

Repeat the steps 1 to 3 for all publications that shall be added to your
bibliography. To export your bibliography, go to your clipboard (as
described above in step 3).

-----------------------------

### How to export your bibliography

   ![ <File:3_new.png>](exportdata/3_new.png  "fig: File:3_new.png") 

1.  In the clipboard, click on the arrow symbol on the very right – the export options are shown.

2.  Choose your preferred export format for
    exporting your bibliography. ${project.name} shows you the most
    common export formats (RSS, BibTeX, RDF). 
    Click on **“more…”** to choose other export formats (e.g. HTML). 
    
    **A:** Choose an export format from the box or your favorite layouts. 
    
    **B:** Choose the number of posts you want to export. You can either choose a number of items from the boxes **before** you choose an export format 	or you can add `?items=variousnumber` to the end of the URL **after** choosing an export format.
    
    ( **For example:** If you want to export 10 items, add `?items=10` to the end of the URL, if you want to export 20 items `?items=20` and so on.)
     ![ <File:exportformats.png>](exportdata/exportformats.png  "fig: File:exportformats.png")

    
    **Advice**: We recommend to export your bibliography in
    BibTeX format. This format is so widely spread that you can call it
    the standard format for reference management software.  
    
3.  After you clicked on an export format, sometimes you directly will
    be asked where you want to store the file, but sometimes (depending
    on the format) your bibliography will be displayed in the
    corresponding format. Click on the right mouse button and select the
    option **“Save as…”** to store your bibliography.  
    
    ![ <File:export1.png>](exportdata/export1.png  "fig: File:export1.png")  
    
4.  To clear your clipboard for the next text research, click on the
    folder symbol in the clipboard, then click on **“clear clipboard”**.
    Please keep in mind that this action cannot be undone, so you should
    make a backup of any data before.

-----------------------------

}

${project.theme == "puma"}{

In order to cite a single publication, you can copy the corresponding citation to your
local clipboard.

There are two options to do so: 

**A:** Click on the downwards-pointing arrow in the menu located in 
the right corner at the bottom of each publication entry.
You can now choose from your [favourite export formats](FavouriteExportFormats"wikilink").
After you selected the format you like, a new window appears.
Click the button at the very bottom to copy the citation to your local clipboard.
You can now import the citation into your requested program.

![ <File:cit1.png>](exportdata/cit1.png  "fig: File:cit1.png")

**B:** Click on the name of the publication you want to export.
At the bottom of the page with the publication details, you will find the option to
**cite this publication** by clicking the button at the very bottom. 

----------------

### How to build individual bibliography out of your publications

**Goal:** This tutorial shows you how to build bibliographies out of
your publications. You'll also learn how to save the bibliographies in
${project.name} and/or export them.

**Requirements:** If you don’t know how to search and view bookmarks and
publications in ${project.name} yet, please read the following tutorial
before you continue: [ Manage literature and bookmarks](BookmarkPublicationManagement "wikilink"). 

---------------

If you want to export a list of several publications you need to add them
to your bibliography first. Follow these steps to do so:

1.  After you have used the search bar (at the top on the right) to
    search for publications, the resulting bookmarks are displayed on
    the left-hand side and publications are displayed on the
    right-hand side. With every publication comes a
    corresponding toolbar.
    
2.  Click on the folder symbol (if you
    hover the mouse cursor over the symbol, the message **“add this
    publication to your clipboard”** appears).  
    
    ![ <File:1_new.png>](exportdata/1_new.png  "fig: File:1_new.png")  
    
3.  The publication is now stored in your clipboard. To view the
    clipboard, click on the person icon in the right [main menu](User_Interface "wikilink") and in the
    following sub-menu, click on **“clipboard”**. In addition, you can
    see a red circle with the number of publications in your clipboard
    next to the person icon.  
    
    ![ <File:2_new.png>](exportdata/2_new.png  "fig: File:2_new.png")  

Repeat the steps 1 to 3 for all publications that shall be added to your
bibliography. To export your bibliography, go to your clipboard (as
described above in step 3).

-----------------------------

### How to export your bibliography

   ![ <File:3_new.png>](exportdata/3_new.png  "fig: File:3_new.png") 

1.  In the clipboard, click on the arrow symbol on the very right – the export options are shown.

2.  Choose your preferred export format for
    exporting your bibliography. ${project.name} shows you the most
    common export formats (RSS, BibTeX, RDF). 
    Click on **“more…”** to choose other export formats (e.g. HTML). 
    
    **A:** Choose an export format from the box or your favorite layouts. 
    
    **B:** Choose the number of posts you want to export. You can either choose a number of items from the boxes **before** you choose an export format 	or you can add `?items=variousnumber` to the end of the URL **after** choosing an export format.
    
    ( **For example:** If you want to export 10 items, add `?items=10` to the end of the URL, if you want to export 20 items `?items=20` and so on.)
     ![ <File:exportformats.png>](exportdata/exportformats.png  "fig: File:exportformats.png")

    
    **Advice**: We recommend to export your bibliography in
    BibTeX format. This format is so widely spread that you can call it
    the standard format for reference management software.  
    
3.  After you clicked on an export format, sometimes you directly will
    be asked where you want to store the file, but sometimes (depending
    on the format) your bibliography will be displayed in the
    corresponding format. Click on the right mouse button and select the
    option **“Save as…”** to store your bibliography.  
    
    ![ <File:export1.png>](exportdata/export1.png  "fig: File:export1.png")  
    
4.  To clear your clipboard for the next text research, click on the
    folder symbol in the clipboard, then click on **“clear clipboard”**.
    Please keep in mind that this action cannot be undone, so you should
    make a backup of any data before.

-----------------------------

}

${project.theme == "wueresearch"}{

In order to cite a single publication, you can copy the corresponding citation to your
local clipboard.

There are three options to do so: 

**A:** Click on the downwards-pointing arrow in the menu located in 
the right corner at the bottom of each publication entry.
You can now choose from your [favourite export formats](FavouriteExportFormats"wikilink").
After you selected the format you like, a new window appears.
Click the button at the very bottom to copy the citation to your local clipboard.
You can now import the citation into your requested program.

![ <File:maincitwueen.png>](wueresearch/ExportData/maincitwueen.png  "fig: File:maincitwueen.png")

**B:** Click on the name of the publication you want to export.
At the bottom of the page with the publication details, you will find the option to
**cite this publication** by clicking the button at the very bottom. 

**C:** If the publication is part of your personal clipboard or bibliography, you may alternatively click on the downward-pointing arrow, which is located to the right under each entry. However, you can only access a smaller selection of citation formats.

----------------

### How to build individual bibliography out of your publications

**Goal:** This tutorial shows you how to build bibliographies out of
your publications. You'll also learn how to save the bibliographies in
${project.name} and/or export them.

**Requirements:** If you don’t know how to search and view bookmarks and
publications in ${project.name} yet, please read the following tutorial
before you continue: [ Manage literature and bookmarks](BookmarkPublicationManagement "wikilink"). 

---------------

If you want to export a list of several publications you need to add them
to your bibliography first. Follow these steps to do so:

1.  After you have used the search bar (at the top on the right) to
    search for publications, the resulting bookmarks are displayed on
    the left-hand side and publications are displayed on the
    right-hand side. With every publication comes a
    corresponding toolbar.
    
2.  Click on the folder symbol (if you
    hover the mouse cursor over the symbol, the message **“add this
    publication to your clipboard”** appears).  
    
    ![ <File:addclipwueen.png>](wueresearch/ExportData/addclipwueen.png  "fig: File:addclipwueen.png")  
    
3.  The publication is now stored in your clipboard. To view the
    clipboard, click on the person icon in the right [main menu](User_Interface "wikilink") and in the
    following sub-menu, click on **“clipboard”**. In addition, you can
    see a red circle with the number of publications in your clipboard
    next to the person icon.  
    
    ![ <File:clipboardwueen.png>](wueresearch/ExportData/clipboardwueen.png  "fig: File:clipboardwueen.png")  

Repeat the steps 1 to 3 for all publications that shall be added to your
bibliography. To export your bibliography, go to your clipboard (as
described above in step 3).

-----------------------------

### How to export your bibliography

   ![ <File:exportclipwueen.png>](wueresearch/ExportData/exportclipwueen.png  "fig: File:exportclipwueen.png") 

1.  In the clipboard, click on the arrow symbol on the very right – the export options are shown.

2.  Choose your preferred export format for
    exporting your bibliography. ${project.name} shows you the most
    common export formats (RSS, BibTeX, RDF). 
    Click on **“more…”** to choose other export formats (e.g. HTML). 
    
    **A:** Choose an export format from the box or your favorite layouts. 
    
    **B:** Choose the number of posts you want to export. You can either choose a number of items from the boxes **before** you choose an export format 	or you can add `?items=variousnumber` to the end of the URL **after** choosing an export format.
    
    ( **For example:** If you want to export 10 items, add `?items=10` to the end of the URL, if you want to export 20 items `?items=20` and so on.)
     ![ <File:exportformats.png>](exportdata/exportformats.png  "fig: File:exportformats.png")

    
    **Advice**: We recommend to export your bibliography in
    BibTeX format. This format is so widely spread that you can call it
    the standard format for reference management software.  
    
3.  After you clicked on an export format, sometimes you directly will
    be asked where you want to store the file, but sometimes (depending
    on the format) your bibliography will be displayed in the
    corresponding format. Click on the right mouse button and select the
    option **“Save as…”** to store your bibliography.   
    
4.  To clear your clipboard for the next text research, click on the
    folder symbol in the clipboard, then click on **“clear clipboard”**.
    Please keep in mind that this action cannot be undone, so you should
    make a backup of any data before.

-----------------------------

}

For a documentation on how to export data from ${project.name} to commonly used software like Microsoft Word or Bibliographix, [click here](ExportDataSpecific "wikilink").


-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").