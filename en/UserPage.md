<!-- de: UserSeite -->

## User page
---------------

This page is **your personal user page**, where only the publications
and bookmarks in your collection are displayed.  
To access your user page, just click on your username in the [ right
main menu](User_Interface "wikilink") or enter the URL
`http://www.bibsonomy.org/user/your_username`.  
  
You can also access the user page of **any other user** by inserting
their username into the previous shown URL or by entering their username
into the search bar on the upper right side.  

![ <File:overview1.png>](userpage/overview1.png  "fig: File:overview1.png")  

1. **A user's bookmarks/publications** - Here you can view the bookmarks
    and publications that are stored in a user's personal collection.  
  
2. **Add as a friend/follow/CV** - Click on the button "add as friend" to add
    the user to your [ friends list](AddFriends "wikilink") or "follow" to [ follow](Followers "wikilink") them. Click on the
    button "CV" to view the users [ Curriculum Vitae](ChangeCV "wikilink").
    Click on the arrow next to the button and then on "show personalized
    posts" to get a personalized view of the user's posts.  
  
3. **Discussion** - Here you can view the posts that were recently [
    discussed](DiscussionReviewsComments "wikilink") by the user.  
  
4. **Concepts** - The list of [ concepts](Concepts "wikilink") of the user.  
  
5. **Similar users** - A list of users that are similar to this user. By
    clicking on "more...", you can change the algorithm that calculates
    similarity between users ([ FolkRank](FolkRank "wikilink") (default), [Jaccard](https://en.wikipedia.org/wiki/Jaccard_index), [Cosine](https://en.wikipedia.org/wiki/Cosine_similarity) and [TF-IDF](https://en.wikipedia.org/wiki/Tf%E2%80%93idf)).  
  
6. **Tags** - The list of [ tags](Tags "wikilink") of the user.  
  
-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").