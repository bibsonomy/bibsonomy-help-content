<!-- de: RSS-FeedsAbonnieren -->

## Subscribe to RSS feeds
----------------------

**RSS** (Really Simple Syndication) feeds allow you to always stay up to
date and to never miss any news. Information about RSS technology can be
found for example on [Wikipedia](http://en.wikipedia.org/wiki/RSS). In ${project.name}, you can use RSS feeds to
subscribe to your own/other publication lists including the publication lists
of your groups. You will be informed about all news/updates immediatly.  

![ <File:1.png>](rss-feedssubscribe/1.png  "fig: File:1.png")  

1.  Click on the black **arrow symbol (export options)** on the very right in the "publications/bookmarks"
    column you want to subscribe to. A menu appears.

2.  Now click on **"RSS"**. The RSS feed will be
    generated and forwarded to your RSS reader (often, this will be your
    webbrowser).  
      
    **Advice:** The next steps are just an example, they
    depend both on your browser and your RSS reader. In this case, we
    show you how to use "Mozilla Firefox" as browser and as
    RSS reader.
    
    After you have clicked on **"RSS"**, Mozilla Firefox shows you
    some options. Choose **"dynamic bookmarks"** and click on
    **"subscribe now"**. A pop up window appears.  
      
    ![ <File:RssFeed2.png>](rss-feedssubscribe/RssFeed2.png  "fig: File:RssFeed2.png")  
    
3.  Choose a **name** for your RSS feed. ${project.name} always
    generates a name automatically, in this case, there are publications
    (publications for ...) by a specific user (/user/...).
   
4.  Then choose a **folder** to store the feed.

5.  Click on **"Subscribe"** to save the new RSS feed.

6.  The following screenshot shows the subscribed RSS feed as
    an example. Compare the left (orange marked) side, this is the RSS
    feed reader view with the view on the right, this is the latest
    version in ${project.name}.
  
![ <File:RssFeed3.png>](rss-feedssubscribe/RssFeed3.png  "fig: File:RssFeed3.png")  
  
-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").