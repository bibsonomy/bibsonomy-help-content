<!-- de: AddonGoogleDocs -->

## Addon for Google Docs
---------------------

**Note:** This feature is currently still in development.
We are still working on a more user-friendly solution in the moment. We
appreciate your feedback.

This ${project.name} addon for Google Docs allows you to cite directly
from ${project.name} in Google Docs.

**Important:** Before performing the instructions, read them completely,
so you do not have to jump back to this page.

**Requirements:** You need a Google account.

1.  Click on the [this link](https://docs.google.com/document/d/1O9h5zqcd8UAOnJszDnilr4-_0uBfCk2Pw-qdDtphtPE/edit?usp=sharing) - it will take you to Google Docs.
You may have to log in to Google Docs.

2.  You see the document prepared by us. It contains additional scripts
    that allow access to BibSonomy. The document will be automatically
    added to your library.
3.  In order to work with the document, you need to copy it. The
    original file is protected. Just click on **"File"** - **"Make a copy"**.  
    
    ![ <File:1.png>](addongoogledocs/1.png  "fig: File:1.png")  
    
4.  To cite a publication from ${project.name} click on **"addons"** -
    **"BibSonomy"** - **"cite from BibSonomy"**.  
    
    ![ <File:2.png>](addongoogledocs/2.png  "fig: File:2.png")  
    
5.  When you log in the first time, you need to authenticate.

*More information:* <https://bitbucket.org/fwhkoenig/bibsonomy-googledocs-add-on/wiki/Home>

-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").