<!-- de: Projekte -->

Our Projects
------------

The following projects are involved in ${project.name}:

${project.theme == "bibsonomy"}{

### Akademisches Publikationsmanagement (PUMA)

![<File:puma-logo_schrift-web.png>](projects/puma-logo_schrift-web.png  "fig: File:puma-logo_schrift-web.png")

[PUMA](http://puma.uni-kassel.de/) can be used as social bookmarking
system and as online publication management software, just like
BibSonomy. However, it has additional features that are optimized for
usage in universities/university libraries and scientific institutions
in general.

PUMA was supported by the Deutsche Forschungsgemeinschaft (DFG) from
2009 to 2011. A second period of support has started in 2013. PUMA
stands for "Akademisches Publikationsmanagement".  
  
You can find more information on PUMA here: <http://www.academic-puma.de/> 

--------------------------------------------

}

### Nepomuk

We take part in the European research project [Nepomuk](http://nepomuk.semanticdesktop.org/xwiki/bin/view/Main1/). The
project brings together researchers, industrial software developers, and
representative industrial users to develop a comprehensive solution for
extending the personal desktop into a collaboration environment which
supports both the personal information management and the sharing and
exchange across social and organizational relations. This solution is
called the Social Semantic Desktop.

![ <File:bubbles2.png>](projects/bubbles2.png  " File:bubbles2.png")

[Nepomuk](http://nepomuk.semanticdesktop.org/xwiki/bin/view/Main1/) intends to realize and deploy a comprehensive solution such as methods,
data structures, and a set of tools for extending the personal computer
into a collaborative environment, which improves the state-of-the-art in
online collaboration and personal data management and augments the
intellect of people by providing and organizing information created by
single or group efforts.

----------------------------------

### TAGora

The TAGora project, a [STREP](http://cordis.europa.eu/fp6/instr_strp.htm) project funded by
the European Commission deals with *Simulating Emergent Properties in
Complex Systems*.

![<File:tagora_artwork1.png>](projects/tagora_artwork1.png  " File:tagora_artwork1.png")

The collaborative character underlying many Web 2.0 applications puts
them in the spotlight of complex systems science, since the problem of
linking the low-level scale of user behavior with the high-level scale
of global applicative goals is a typical problem tackled by the science
of complexity: understanding how an observed emergent structure arises
from the activity and interaction of many globally uncoordinated agents.
The large number of users involved, together with the fact that their
activity is occurring on the web, provide for the first time a unique
opportunity to monitor the "microscopic" behavior of users and link it
to the high-level features of applications (for example the global
properties of a folksonomy) by using formal tools and concepts from the
science of complexity. The TAGora
project aims at exploiting these unique opportunities offered by the
increasing popularity of computer-mediated social interaction in a
variety of contexts.

--------------------------

### Microsoft Accelerating Search

We won one of the [Microsoft Accelerating Search in Academic Research
2006 RFP awards](http://research.microsoft.com/en-us/collaboration/).
The objective of this RFP is to support Live Labs' collaboration with
the academic research community and is focused on the Internet Search
research area. Our project dealt with enhancing link-based search with
social search in order to provide enhanced functionality and multiple
search paradigms for the Web.

![ <File:ms_logo.png>](projects/ms_logo.png  " File:ms_logo.png")

---------------

### Informational self-determination in the Web 2.0

The new generation of the internet ("Web 2.0" or "social internet") is
characterized by a very liberal provision of information through the
users. Against this background, this DFG project's goal is to explore
and to shape the opportunities and risks of the new Web 2.0 technologies
in a selected scenario and in close interaction between scientists and
lawyers.

After a review of the situation and subsequently the creation of
medium-term scenarios, the project will analyze the technical and legal
opportunities and risks related to typed roles. Generic concepts will be
developed for the design of applications complying with data protection
law (identity management, avoidance of personal reference and
educational profile, responsibilities). Honouring these concepts,
algorithms and procedures for two specific tasks will be developed:
Recommender systems for cooperative tagging systems and collaborative
spam detection methods for such systems. They are evaluated using real
data. The most successful approaches will be implemented in the
collaborative publication management system ${project.name} and will be
evaluated in the current operation. Finally, it will be analyzed to
which extent, on account of the new complex of problems of Web 2.0,
dogmatics and interpretation of data protection law have to be modified,
and if possibly legislative activities are necessary or advisable.

  
-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").

