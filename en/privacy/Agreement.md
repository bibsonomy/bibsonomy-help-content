<!--de: datenschutz/Vereinbarung-->

${project.theme == "bibsonomy"}{


Agreement for joint controllers
-----------------------


We offer a bookmarking-service that is run as a joint project for everyone.  
We, that is:  

- The Data Mining and Information Retrieval Group of the University of Würzburg, 
- The Knowledge and Data Engineering team of the University of Kassel 
- The Institut für Bibliotheks- und Informationswissenschaft Sachgebiet Information Processing and Analytics of the Humboldt-Universität Berlin.
  
------------------------------

**University of Würzburg (public corporation)**  
Am Hubland  
D-97074  
Würzburg  
Tel: +49 931 / 31 – 86731  
Fax: +49 931 / 31 - 86732  
${project.email}  
Authorized representative for Data Mining and Information Retrieval Group at LS VI  
and this service is Prof. Dr. Andreas Hotho.  

**University of Kassel (public corporation)**  
Wilhelmshöher Allee 73  
34121 Kassel  
Tel.: +49 561 804 - 6250  
Fax: +49 561 804 - 6259  
${project.email}
Authorized representative for the Knowledge and Data Engineering team is Prof. Dr. Gerd Stumme   

**Humboldt University of Berlin (public corporation)**  
Unter den Linden 6  
10099 Berlin  
Telefon: +49 30 2093–0  
Fax: +49 30 2093–2770  
${project.email}  
Authorized representative for the Institut für Bibliotheks- und Informationswissenschaft  
Sachgebiet Information Processing and Analytics is Prof. Dr. Robert Jäschke.


Our Data Protection Officers are 

- Appointed Data Protection Officer of the University of Würzburg
- Appointed Data Protection Officer of the University of Kassel
- Appointed Data Protection Officer of the Humboldt University of Berlin
  
--------------------------------------

We pursue the following purposes:  
BibSonomy is run as a research project to develop and improve functions for recommendation,  
spam detection and ranking and advance the organization of social bookmarking-systems in compliance with data protection.

The system is introduced in the publication

> Benz, D. et al.: The Social Bookmark and Publication Management System BibSonomy. The VLDB Journal. 19, 6, 849--875 (2010).

In this publication you can also find examples from research.  
You can find aditional reports about recent research findings in our blog.  
You can find a list of research projects [here](Projects "wikilink").  

The methods we use for data processing include (amongst others) statistical analyses
and machine learning.  
The concept for "privacy by design" is implemented by all involved authorities.  
Default privacy settings are implemented by all involved authorities.  
All involved authorities are responsible for the implementation of data privacy,  
reports of security issues and the notification of those affected.
Information requirements are available for those affected on the website 
of the portal by creating a user account.  
All involved authorities are responsible for the design.  
The right to data portability is implemented by all involved authorities.  
The contact point for those affected by privacy law is implemented by all involved  
authorities.  
You can direct your questions to  
${project.email}  

This agreement will be published at https://www.bibsonomy.de/help_en/privacy/Agreement
}