<!-- de: Gruppenfunktionen -->

## Group functions
---------------

The group concept in ${project.name} allows you to meet people with the
same reading interests. In groups, you can easily work on
projects/articles or you can share interesting publications with your
colleagues at your workplace. 

On this page, you will find links to short instructions for all group functions.
Some group functions only are available for administrators. Learn more about 
those functions [here](GroupFunctionsAdmin "wikilink").

------------------------

### Search for a group and join it

If you would like to search for a group and join it, follow [these steps](GroupFunctions/GroupSearch "wikilink").

------------------------------------------------------------------------

### Create a new group

[This guide](GroupFunctions/GroupCreate "wikilink") shows you how to create a new group.

------------------------------------------------------------------------

### The group page

The group page offers several functions. Compare this [guide](GroupFunctions/GroupPage "wikilink"). 

------------------------------------------------------------------------

### Share bookmarks/publications with the group

${project.name} offers the possibility to share bookmarks and publications with your group. Follow these  [steps](GroupFunctions/GroupBookmarks "wikilink").

------------------------------------------------------------------------

### Group discussions

[This helppage](GroupFunctions/GroupDiscussions "wikilink") explains how to start a group discussion with your colleagues

------------------------------------------------------------------------

### Leave a group

To leave a group, follow [these steps](GroupFunctions/GroupLeave "wikilink").

-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").