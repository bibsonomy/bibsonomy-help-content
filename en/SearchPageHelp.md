<!-- de: SuchenHilfe -->

## Search Options
--------------

This page will give you an overview on the different options you have
with searching in ${project.name}. You can find more details at [Apache
Lucene - Query ParserSyntax](https://lucene.apache.org/core/5_2_1/queryparser/org/apache/lucene/queryparser/classic/package-summary.html#package_description).  
To search, enter your search term into the search bar **on the upper
right side**.

-----------------

  ![ <File:searchbar.png>](searchpagehelp/searchbar.png  "fig: File:searchbar.png")  

### Overview table

Term | Meaning | Example
-- | -- | -- | --
term | find one word | `hello` 
"..." | find phrases | `"hello dolly"`
 ? | any single character (the question mark cannot be at the beginning of a search term) | `te?t` 
* | any multiple characters (the star cannot be at the beginning of a search term) | `test*` 
OR | finds a matching document if either one of the terms exist | `"hello dolly" hello` `"hello dolly" OR hello` 
AND | finds a matching document if all of the terms exist | `"hello dolly" AND "dolly lucy"`
+ | the term after the '+' symbol has to exist | `+hello dolly` 
-/NOT/! | the term after the '-' symbol mustn't exist Alternatives: NOT or ! | `"hello dolly" -"dolly lucy"` / `"hello dolly" NOT "dolly lucy"` 
(...) | Grouping of clauses | `(hello OR dolly) AND website` 
\~ | *fuzzy* search terms. Finds words that are spelled similarly. | `roam~`
\~n | *Proximity Search*. Finding words that are within a specific distance away. | `"hello dolly"~10`
\^n | *Boosting factor* to control the relevance of a document by boosting its term. | `"hello dolly"^4 "dolly lucy"`  
\\ | *Escaping* of special characters: + - && &#124;&#124; ! ( ) { } [ ] \^ " \* ? : \\ | `\(1\+1\)\:2` for (1+1):2

------------------------------------------------------------------------

### Examples

#### Search with one word

To search with a single word, just enter the word into the search bar.
**It doesn't matter if you use upper case or lower case letters**. 

**Example:** `java` finds all entries with the topic "java".  

------------------------------------------------------------------------

#### Search with phrases

To search with more than one word (phrases), put **quotes** in front of
and after the phrase. 

**Example:** `"programming language"` finds all entries with the
phrase "programming language".  

------------------------------------------------------------------------

#### Wildcards (? and \*)

When you aren't sure how a word is spelled or you want to search for
more versions of a word, you can use wildcards. **The wildcase must
never stand in the beginning of a search term.**  

**?:** A question mark stands for any single character.  
**Example:** With the search term `te?t`, you can find all entries with
the terms "test" as well as "text".  

**\*:** A star stands for any multiple characters.  
**Example:** With the search term `test*`, you can find all entries with
the terms "test", "tests", "tester" and "testing". 

**Combination:** It is possible to use mutliple words aswell.
**Example:** By searching `te?t foo?`, you can find all entries with any combination of the expression. For example, "text food" and "text foot" will be found, but also "test food" and "test foot".

------------------------------------------------------------------------

#### Regular Expressions

In ${project.name}, you can also search with [regular
expressions](https://en.wikipedia.org/wiki/Regular_expression). The search term has to stand between two **slashes**. 

**Example:** With `/[bl]ook/`, you can search for "book" and "look".  

------------------------------------------------------------------------

#### AND, OR, NOT, + and -

The operators AND, OR and NOT belong to the [Boolean
operators](https://en.wikipedia.org/wiki/Logical_connective). They allow terms to be combined through logic operators.  

**AND:** When two search terms are combined by AND, only entries will be
found that contain **both** search terms.  
**Example:** `honey AND bee` shows entries that contain "honey" as
well as "bee".  

**OR:** When two search terms are combined by OR, only entries will be
found that contain **one of the two** search terms.  
**Example:** `"computer science" OR computer` finds entries with
the terms "computer science" or "computer".  

**NOT:** With the operator NOT, you can exclude terms that shouldn't be
contained in the found entries.  
**Example:** `"computer science" NOT computer` only finds entries
with the term "computer science" and excludes entries with the term
"computer".  

**'+' and '-':** A '+' in front of a search term means that this term
**has to exist**, while a '-' means that this word **mustn't exist**.
When an entry includes words that are marked with '-' as well as words
marked with '+', this entry won't be shown in the list of results.  
**Example:** `+java -script` finds entries where the term "java" has
to exist, but the term "script" mustn't exist.  

**Combination:** These operators can be combined with Wildcards and Regular Expressions.
**Example:** Search for `global AND foo?` to find entries with **both** words "global" and
"food"/"foot"/...

------------------------------------------------------------------------

#### Grouping

With **paranthesis**, you can group your search terms and arrange them.
This way, it is always clear which operator will be executed first. 

**Example:** The term `(user OR person) AND test` searches for
entries where either the words "user test" or the words "person test"
exist.  

The term `user:hotho (tags:web OR tags:data)` searches for entries of the user "hotho"
that are tagged with "web" or "data".

------------------------------------------------------------------------

#### Fuzzy Search

Fuzzy searches mean that you can search for words where one or more
letters are **changed** from the original search term. That means, you
can search for words that are written similarly.  
A number after the '~' symbol shows how many letters may be changed
(numbers **between 0 and 2** are allowed). If no number is given after
the '~' symbol, the default allowed changes will be 2.  

**Example:** The search term `site~` will also find entries with the
words "side" or "kate". The search term `site~1` won't be able to find
entries with the word "kate" anymore.  

------------------------------------------------------------------------

#### Proximity search

${project.name} supports finding words that are within a specific
distance away from each other. To do so, put a '~' symbol and a
number after a search phrase to determine the maximum distance. 

**Example:** The search phrase `"java training"~10` only finds entries
where the distance between the words "java" and "training" is no more
than ten words long.  

------------------------------------------------------------------------

#### Boosting factor

When one term is more relevant for the search than another, you can
boost it. To do so, put a '^' symbol and a positive number after the
term that shall be boosted. The higher the number, the more the term
will be boosted.  
**Example:** In the term `java^4 script`, the word "java" is more
relevant than the word "script".  

------------------------------------------------------------------------

#### Escaping

Escaping means to suppress the function of a special character und to
print it as a normal character. To escapte special characters
**( + - && &#124;&#124; ! ( ) { } [ ] \^ " \* ? : \\ )**, put a **'\\**' in front of the character.  

**Example:** The term `\(1\+1\)\:2` stands for the search query "(1+1):2".  

------------------------------------------------------------------------

#### Field search

To make your searching more precise, you can search in specific
categories (tag, user, group, author, concept, BibTeX key). To do so,
click on the **blue arrow** next to the field **search** and choose the
corresponding category by clicking (recommended), or enter the category
directly into the search bar.

Category | Description | Example
-- | -- | -- | --
tags:*term* | only searches in the list of tags | `tags:java` 
user:*term* | only searches in the list of users | `user:hotho` 
group:*term* | only searches in the list of groups | `group:kde` 
author:*term* | only searches in the list of authors | `author:gabbard`

You can also combine the different categories in a search.

**Example:** If you search for the term `user:hotho`, you will find entries of the user "hotho".

**Combination:** You can combine categories with eachother.
**Example:** If you enter the term `user:hotho (tags:web OR tags:data)` into the full text search,
the shown results will be entries of the user "hotho" that are tagged with "web" or "data".

------------------------------------------------------------------------
#### Range search

You have the possibility to search for entries in a specific range. To
do so, you first have to choose the **category** where the search shall
be conducted (e.g. title, author,...). Then, determine the range.
Using **square brackets**, the borders of the range will be included into the
search.
Using **curved brackets**, they will be excluded. 

**Example:** The search term `title: [java TO perl]` finds all entries whose title
alphabetically is between "java" and "perl" ("java" and "perl" are included). 

The search term `author: {gabbard TO grasset}` finds all entries
whose author alphabetically is between "gabbard" and "grasset"
("gabbard" and "grasset" are excluded).  

------------------------------------------------------------------------

#### Advanced search

In addition to the specific categories (tag, user, group, author, concept BibTeX key) you
can find in the **field search** (by clicking the blue arrow next to 
the search bar) you have the possibility to make an even more specific search by searching 
in the following categories.
Just enter one of the categories, followed by a colon and the term you want to search for, into the search bar.

**Example:** `journal: European Economic Review`

**Combination:** By using specific categories and other methods of searching, you can improve the results.
**Example:** Searching `journal: Historische Zeitschrift (rezension OR imperial)` will show entries by the journal "Historische Zeitschrift", which contain **either** "rezension" **or** "imperial".


Category | Description | 
-- | -- | -- | -- 
doi: | DOI  of a publication
isbn: | ISBN  of a publication
issn: | ISSN of a publication
journal: | full (unabbreviated) journal title
volume: | volume of a journal or book of a multi-volume series
year: | year of the publication, typically consisting of four numbers
publisher: | name of the publisher
adress: | address of publisher or institution
edition: | edition of a book, usually written in full (as "second")
institution: | name of the sponsoring institution for a technical report
organization: | name of the sponsoring organization for a conference or manual
series: | name of a series or a set of books
school: | name of academic institution where a thesis was written
language: | language of an entry
misc: | information of imported non-standard fields for additional information that doesn't fit the categories of the predefined ones


-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").