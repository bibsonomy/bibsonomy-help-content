<!-- de: Zotero -->

## Zotero
-----------

[Zotero](http://www.zotero.org/) is an extension for Firefox and helps
you to collect, manage and cite publications. We support exporting
citation information from ${project.name} to Zotero as well as
importing entries from the Zotero library to ${project.name}.

When Zotero recognizes literature publication items on a web page, an
icon is added in the Firefox address bar. A user can click on the icon
and Zotero saves the reference information in one of the user's
libraries. Also on ${project.name} pages that contain BibTeX entries,
the icon is shown for Zotero users.

------------------------------------------------------------------------

### Export from ${project.name} to Zotero

When you want to add a publication from a ${project.name} page (like
the homepage or your userpage) to your Zotero library, click on the
**black arrow** next to the Zotero symbol and then on **"Save to Zotero
using unAPI"**. By clicking on this icon, a **window pops up** which
allows you to select the entries you want to export. Click OK and the
selected references will appear in your Zotero library.

![ <File:speichern1.png>](zotero/speichern1.png  " File:speichern1.png")

![ <File:zoteroentry.png>](zotero/zoteroentry.png  " File:zoteroentry.png")

------------------------------------------------------------------------

### Import from Zotero to ${project.name}

Unfortunately, this process is not fully automized yet for Zotero has to
be configured for ${project.name} first. Open the Zotero settings. To
do so, click on the **"Z" symbol** in Firefox on the upper right hand
side, then, Zotero will be opened in a window at the bottom of the
browser. There, click on the **gear symbol** and finally on
**"Preferences"**. A window pops up. Choose the menu item **"Export"**.
Click on the '+' button below the **"Site-Specific Settings"** area to
add a new setting. In the popup window, enter `bibsonomy.org` and
select `BibTeX` as output format. Confirm by clicking on OK.

![<File:einstellungen.png>](zotero/einstellungen.png  " File:einstellungen.png")

![<File:zoterosettings.png>](zotero/zoterosettings.png  "fig: File:zoterosettings.png")  

Now you can log in to ${project.name} and in the [ left main menu](User_Interface "wikilink"),
click on **"add post"** and then on **"post publication"**. Choose the tab
**"BibTeX/EndNote snippet"**. On the following page, you can drag and drop
an entry from your Zotero library directly into the field **"Select"** (click on the Zotero entry,
keep the left mouse button pressed, move the mouse cursor into the field
and then, release the left mouse button). By clicking on **"post"**, the
data from the Zotero entry will be extracted and automatically put into
the respective fields.

![<File:zoterobibsonomy.png>](zotero/zoterobibsonomy.png  "fig: File:zoterobibsonomy.png")  

-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").