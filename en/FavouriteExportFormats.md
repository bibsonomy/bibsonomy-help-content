<!-- de: BevorzugteExportformate -->

## Set favourite export formats
----------------------------

![ <File:1.png>](favouriteexportformats/1.png  "fig: File:1.png")

To set your favourite export formats, click on the person icon in the [right main menu](User_Interface "wikilink"),
click on [settings](Settings "wikilink") and then on the tab **"settings"**.

1.	Under **“favourite export formats“** you can add your preferred export formats by typing them 
	in the **“add export format“** section.

2.	If you want to remove one, click on the red **“delete button“** behind its name.
  
3.	When you are finished choosing your favourite export formats, confirm your selections by clicking 
	the **“set layout“** button at the end of the layout section.

![ <File:2.png>](favouriteexportformats/2.png  "fig: File:2.png")

You can now display the publications in the formats you selected.
To do so, click on the downwards-pointing arrow in the menu located in the right corner 
at the bottom of each publication entry. 
Your favourite export formats are now shown at the bottom of the appearing menu. 

-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").