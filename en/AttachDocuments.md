<!-- de: DokumentAnhängen -->

## Attach a private document to publications
-----------------------------------------

You can upload file attachments for your publications. The file upload
is limited to 50 MB per file and also you can only upload files with the
following file extensions: *pdf, ps, djv, djvu, txt, tex, doc, docx,
ppt, pptx, xls, xlsx, ods, odt, odp, jpg, jpeg, svg, tif, tiff, png,
htm, html, epub*. For example, the attachment could contain some
personal notes about the publication or a short summary of the content.
The documents in the attachments are only visible for you. It is also
possible to share attachments with your research colleagues.
  
![ <File:1.png>](attachdocuments/1.png  "fig: File:1.png")  
  
To upload a document, click on the title of the publication.
Now click on the big **document symbol with the plus sign** on the right.
An **upload button** appears underneath the document symbol, after clicking
it you can select the document you want to attach.
  
![ <File:2.png>](attachdocuments/2.png  "fig: File:2.png")  
  
After the upload is complete, you can see a preview of your document.
Beneath you will see the name of the file as well as the options to
edit, delete or download it.
You can upload further files as well.

-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").