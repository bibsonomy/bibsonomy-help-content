<!-- de: NeuenNutzeraccountAnlegen -->

## How to create a new user account
--------------------------------

**Goal:** The following instructions show you how to create a new
${project.name} user account.

1.  [Click here](${project.home}register) to register a new user account.
    
2.  Fill in all required text boxes regarding **"Personal Information"**.  

    ![ <File:1.png>](createnewaccount/1.png  "fig: File:1.png")  
    
3.  **"Captcha"**: Check the checkbox to verify that you are no a robot:
	a green checkmark will appear.

4.  You need to accept the Terms of Use of ${project.name}. Click on
    the gray box next to "I accept the privacy statement and the general
    terms and conditions of ${project.name}".
    
5.  Click on the **"register"** button at the bottom.  
    
    ![ <File:2.png>](createnewaccount/2.png  "fig: File:2.png")  
    
6.  If you have forgotten to enter important data or certain data is
    incorrect, you receive an error message. You
    must correct this error. Once the error has been corrected, click on
    the **"register"** button again.
    
7.  If all data is correct, your new user account will be generated.

8.  **Important**! - Before you can start properly, please check your
    e-mail inbox. There you will find a **confirmation e-mail**. This e-mail
    contains a link you must click on. Thus, we ensure that your e-mail
    address is correct. When your e-mail address has been verified, you
    can use ${project.name}.

With your new user account you can login to the system and use all
features of ${project.name} for free.

-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").