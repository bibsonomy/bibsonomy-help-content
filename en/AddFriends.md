<!-- de: FreundeHinzufügen -->

## Friends
-------------

In ${project.name}, you can add other users to your friends list to
facilitate interaction with them. On this page, you can learn how to add
friends to your contact list and how to send them publications or
bookmarks.

------------------------------------------------------------------------

### Add friends and colleagues

${project.name} offers you two different ways to add friends and
colleagues to your contact list.  
![ <File:1.png>](addfriends/1.png  "fig: File:1.png") 

-  **A**: Click on a username (they always begin with an "@"). A
    username is for example displayed with an article that a user has posted
    (go to **C** - see below).

-   **B**: You know the username. In this case you can directly go to
    the user's profile by using the search function.

    1.  Click on **"search"** and select **"user"**.
    2.  Enter the username into the searchbox.
    3.  Press Enter or click on the magnifying glass icon (go to **C** -
        see below).

-   **C:** In the right blue column of the user's profile, click on
    **"add as friend"**.

------------------------------------------------------------------------

### Send bookmarks/publications

You can easily send bookmarks and publications to your friends by just
adding the [ system tag](SystemTags "wikilink") `send:<username>` (e.g.:
`send:hotho`) to the bookmark/publication you want to send.  
  
The easiest way to do this is to go to **your userpage** (just click on
your username in the right main menu). Then, on the bookmark/publication
you want to send, click on the **small blue pencil icon** to edit the
tags.  

![ <File:editTags.png>](addfriends/editTags.png  "fig: File:editTags.png")  
  
Here you can add the tag `send:<username>` (replace &lt;username> by the
username of your friend) and then click on **save**.  

![ <File:editTags3.png>](addfriends/editTags3.png  "fig: File:editTags3.png")  

The bookmark/publication is now being sent to your friend. Also, a new
icon will appear next to the small blue pencil, a **small blue star
icon**. When you click on this icon, you can see the **hidden system
tags** (e.g. `sent:<username>`).  
  
![ <File:sent.png>](addfriends/sent.png  "fig: File:sent.png")  
  
You can find bookmarks/publications that were sent to you by your
friends in your [ inbox](Inbox "wikilink"). To get to your inbox, click
on the **person icon** in the right main menu and then on **inbox**. You
can now see the items that were sent to you, tagged with
`from:<username>`.  
  
![ <File:inbox.png>](addfriends/inbox.png  "fig: File:inbox.png")  

-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").
  

