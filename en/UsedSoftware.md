<!-- de: VerwendeteSoftware -->

## Used Software
-----------------------------------------

  -   [Apache HTTPD](http://httpd.apache.org/) -  a free and open-source web server

  -   [Apache Tomcat](https://tomcat.apache.org/) -  an open source implementation of the Java Servlet, JavaServer Pages, Java Expression Language and Java WebSocket technologies
  
  -   [CSL](http://citationstyles.org/),  [CSL Style Repository](https://github.com/citation-style-language/styles) - an open XML-based language to describe the formatting of citations and bibliographies
  
  -   [Elasticsearch](https://www.elastic.co/guide/en/elasticsearch/reference/current/index.html) - an open-source, RESTful, distributed search and analytics engine built on Apache Lucene
  
  -   [HAProxy](http://www.haproxy.org/) - a free, open source software that provides a high availability load balancer and proxy server for TCP and HTTP-based applications that spreads requests across multiple servers to improve the performance and reliability of a server environment 
  
  -   [Java](https://www.java.com/en/download/faq/whatis_java.xml) - an object-oriented programming language and runtime environment for the development of cross-platform applications
  
  -   [MySQL](https://dev.mysql.com/doc/) - an open-source relational database management system (RDBMS)
  
  -   [Ubuntu](https://www.ubuntu.com/) - a Linux-distribution, open source software operating system 

-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").