<!-- de: OpenURL-Resolver -->

## How to use the OpenURL-Resolver of my library
---------------------------------------------

**Goal:** These instructions help you to connect your ${project.name}
user profile with your library's URL resolver. This way all URLs, also
in publications, can be resolved via your library.  

**Prerequisite:** You have to know the URL of the library
OpenURL-Resolver. You can ask your librarian or you can open this URL in
your library's/university's network: [find out the OpenURL-Resolver](http://www.openurl.de/info/).  
  
You will get the following output (as an example):  

![<File:Resolver.png>](openurl/Resolver.png  "fig: File:Resolver.png")  

If you know the resolver's URL, complete the following steps:  
  
![ <File:Resolver1.png>](openurl/Resolver1.png  "fig: File:Resolver1.png")  
  
1.  Open your **"personal menu"** in the right [ main menu](User_Interface "wikilink").

2.  Click on **"settings"**.

3.  Under **"contact"**, enter the URL-Resolver's URL into the field
    **"OpenURL"**.
    
4.  Click on **"save changes"** at the end of the page.

  
-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").