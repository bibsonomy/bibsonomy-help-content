<!-- de: JS-Codeschnipsel -->

## Javascript Code Snippets
------------------------

The code below is meant to be incorporated into existing websites in
order to ease posting bookmarks and publications to ${project.name}. It
will display a link which leads directly to the appropriate bookmark or
publication posting site in ${project.name}.

    <!-- post bookmark to link code -->
    <script type="text/JavaScript">
      //<!--
        var url=encodeURIComponent(document.location.href);
        var title=encodeURIComponent(document.title);
        document.write("<a href=http://www.bibsonomy.org/ShowBookmarkEntry?c=b&jump=yes&url="+url+ "&description="+title +"\" title=\"Bookmark this page\">Bookmark!</a>");
      //-->
    </script>
    <!-- end post bookmark to link code --> 

-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").