<!-- de: EintragPublikationen/EintragScan -->

---------------------------

[`Back to overview page on adding publications.`](AddPublications "wikilink")

---------------------

## Addition by scanning the ISBN code
---------------------

${project.theme == "bibsonomy"}{

**Prerequisite:** Your computer must have a built-in webcam or must be
connected to an external webcam before you can start. If that is not possible, choose another method to add your publication to ${project.name}. You can find all of them [here](AddPublications "wikilink").

-------------------

To add a publication by scanning the ISBN code with your webcam, follow these steps:

1.  Move the pointer over the menu item **"add post"** in the
[main menu](User_Interface "wikilink"). A sub-menu appears.

2.  Click on **"add publication"** in the menu by pressing the left
    mouse button.
    
3.  Click on **"scan code"**. Your browser probably displays a warning
    message (e.g. "The website ${project.name} attempts to access
    your webcam"). You have to allow it to access to continue.
    
4.  Hold the publication's ISBN barcode nearly and clearly visible to
    the camera. If ${project.name} recognizes the ISBN barcode you can
    hear a sound as if a camera took a photo.
    
5.  The auto-detected data will be displayed. Check the data and
    complete/correct it if necessary. Finally, click on **"save"**.

-----------------------------

![ <File:Scan.jpg>](AddScan/Scan.jpg  "fig: File:Scan.jpg")

}

${project.theme == "puma"}{

**Prerequisite:** Your computer must have a built-in webcam or must be
connected to an external webcam before you can start. If that is not possible, choose another method to add your publication to ${project.name}. You can find all of them [here](AddPublications "wikilink").

-------------------

To add a publication by scanning the ISBN code with your webcam, follow these steps:

1.  Move the pointer over the menu item **"add post"** in the
[main menu](User_Interface "wikilink"). A sub-menu appears.

2.  Click on **"add publication"** in the menu by pressing the left
    mouse button.
    
3.  Click on **"scan code"**. Your browser probably displays a warning
    message (e.g. "The website ${project.name} attempts to access
    your webcam"). You have to allow it to access to continue.
    
4.  Hold the publication's ISBN barcode nearly and clearly visible to
    the camera. If ${project.name} recognizes the ISBN barcode you can
    hear a sound as if a camera took a photo.
    
5.  The auto-detected data will be displayed. Check the data and
    complete/correct it if necessary. Finally, click on **"save"**.

-----------------------------

![ <File:Scan.jpg>](AddScan/Scan.jpg  "fig: File:Scan.jpg")

}

${project.theme == "wueresearch"}{

**Warning:** Adding publications by scanning their ISBN code is currently disabled. 
You can find several alternatives [here](AddPublications "wikilink").

}

------------------------------------------------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").
