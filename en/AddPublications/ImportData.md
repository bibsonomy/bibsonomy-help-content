<!-- de: EintragPublikationen/LiteraturlisteImportieren -->

---------------------------

[`Back to overview page on adding publications.`](AddPublications "wikilink")

---------------------

## Bibliography import
-------------------

${project.name} allows you to import bookmarks and publications from
other programs. The internationally standardized data format that is
supported by all major bibliographic management programs is called
BibTeX. The import takes place in two steps. First, you have to export
the publications from your previous reference management software, then
you have to import the BibTeX data into ${project.name}.  

------------------

### BibTeX export from your previous reference management software

You can perform a BibTeX export from your previous reference management software. 
Please refer to the instructions for [Citavi](AddPublications/listimportcitavi "wikilink") 
and [Bibliographix](AddPublications/listimportbibliographix "wikilink").

**Advice:** Please note that apart from the different programs there
are also different program versions, so there may be two or more ways to
export data in one program. Therefore you should try all the options if
necessary.

------------------------------------------------------------------------

### BibTeX import from the clipboard

Have you exported data from your previous bibliographic management program in BibTeX format?
and copied to the clipboard? Then you can import them into ${project.name}. Follow 
[this guide](AddPublications/listimportclipboard "wikilink").

**Precondition:** First, export your list of references
(publications/bookmarks) from your previous reference management program
to the clipboard. You can find instructions for this on this unter the caption
**BibTeX export from my previous reference management software** on this page.  



