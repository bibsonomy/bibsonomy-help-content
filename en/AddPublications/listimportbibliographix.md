 <!-- de: EintragPublikationen/listeimportbibliographix -->

---------------------------

[`Back to overview page on bibliography import.`](AddPublications/ImportData "wikilink")

---------------------

## BibTeX export from Bibliographix
------------------

  1.  Open Bibliographix and load the project you want to export.
  
  2.  Click on the tab *"mehr..."*, then on *"Daten austauschen"*.  
          
      ![ <File:biblio1.png>](listimportbibliographix/biblio1.png  "fig: File:biblio1.png")  
      
  3.  In the left menue, choose the option *"Export"*.
  
  4.  Finally, click on *"Datenbank im RIS-Format exportieren!"*.  
          
      ![ <File:biblio2.png>](listimportbibliographix/biblio2.png  "fig: File:biblio2.png")
      
  5.  You can now import the exported file with the BibTeX data
      to ${project.name}. In the left main menu, click on **“add
      post”** and then on **“post publication”**. Then choose the tab
      **“upload file”**'. You can now choose the file from your
      computer, also you can determine who should be able to see
      the publications. By clicking on **"post"**, the file is uploaded.
