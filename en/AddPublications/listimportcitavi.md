<!-- de: EintragPublikationen/listeimportcitavi -->

---------------------------

[`Back to overview page on bibliography import.`](AddPublications/ImportData "wikilink")

---------------------

## BibTeX export from Citavi
------------------

   1.  Click on *"File"* in the upper menu, then on *"Export"*.
   
   2.  A dialog appears. Choose if you want to export only some
       articles or all articles. Then in the bottom of the dialogue,
       click on *"Next"*.
       
   3.  You will be asked about your preferred export format. Select
       *"BibTeX (without extension)"* and click on *"Next"*.
       
   4.  Next you will be asked where you want to save the file. Select
       *"Save text file into the clipboard"* and click on *"Next"*.
   
   5.  Finally you will be asked if you want to save your export
       settings.  
       **Advice:** We recommend the following procedure:
       1.  Select *"Yes, under the following name"*.
       2.  In the textbox, enter *"BibTeX"*.
       3.  Check the box with *"Automatic Export on Save"*.  
           **Reason:** If you follow this advice, Citavi will
           automatically export all data when you save the Citavi
           project (e.g. *"File"* &gt; *"Save"*).
       
   4.  Click on *"Next"* - You will get the message *"Export completed successfully"*.

    Confirm this message with *"OK"*.

  **Result:** Your exported data is now stored in the clipboard. Now you can continue with the instructions under the caption **BibTeX import from the clipboard** which you will find further down below on this page.