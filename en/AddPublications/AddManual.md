<!-- de: EintragPublikationen/EintragManuell -->

---------------------------

[`Back to overview page on adding publications.`](AddPublications "wikilink")

---------------------

## Manual addition of publications
-------------------

${project.name} allows you to add publications by entering information yourself. Click in the main menu on the buttons "add post" and "add publication".  Then, follow the steps below.

------------------

1. Click on the button "manual" to choose the manual addition of publications. Multiple fields will appear.

2. Choose  the entry type of the publication (e. g. article, book, collection, ... ).

3. Enter the title of the publication that you want to add.

4. Enter the author(s) of the publication. Cite each author on a seperate line and format their names like "lastname, firstname" (e. g. Hotho, Andreas). If the author is an institution or another type that does not fit this format, surround it with curly brackets (e. g. {Ministry of education} ). By that, it will not be seperated.

5. If you are an author or editor of the publication, you can check the box. The system tag "myown" will automatically be added to the post.

6. The field "editor(s)" is only necessary, if no author is given. Again, please put each editor on a seperate line and format their names like "lastname, firstname".

7. Finally, name the year of the publication, typically consisting of four numbers (e. g. 2021) and press the button "post". Your publication is now added to ${project.name}.

----------------

![ <File:ENManual.png>](AddManual/ENManual.png  "fig: File:ENManual.png")

------------------------------------------------------------------------


[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").
