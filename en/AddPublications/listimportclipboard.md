<!-- de: EintragPublikationen/listeimportzwischenablage -->

---------------------------

[`Back to overview page on bibliography import.`](AddPublications/ImportData "wikilink")

---------------------

## Import from your clipboard
--------------------------------------



1.  Click on **"add post"** in the [main menu ](User_Interface "wikilink"). A sub-menu appears.



2.  Click on **"add publication"** by pressing the left mouse button.



3.  Select the tab **"BibTeX / EndNote snippet"**.



4.  Paste the text from the clipboard into the large text field named **"snippet"**.

    **Advice:** You can paste text from the clipboard by pressing the

    right mouse button on the large textbox and selecting *"Paste"* in

    the appearing menu.

    

5.  Click on **"post"**.



6.  ${project.name} shows you an overview of all data. Check if the

    data was successfully imported.

    

7.  Click on **"Save"** to complete the import.

${project.theme == "bibsonomy"}{

![ <File:1.png>](listimportclipboard/1.png  "fig: File:1.png")

}

${project.theme == "puma"}{

![ <File:1.png>](listimportclipboard/1.png  "fig: File:1.png")

}

${project.theme == "wueresearch"}{

![ <File:clipboardwueen.png>](wueresearch/AddPublications/listimportclipboard/clipboardwueen.png  "fig: File:clipboardwueen.png")

}