<!-- de: EintragPublikationen/EintragMehrerePublikationen -->

---------------------------

[`Back to overview page on adding publications.`](AddPublications "wikilink")

---------------------

## Importing numerous publications
-------------------------------------

Publications can also be imported to ${project.name} in larger quantities by creating BibTeX or EndNote files.
Those are generated in most programs for literature management. If you need help to export your desired list of publications, [this instruction](AddPublications/listimportcitavi "wikilink") might help.
If you already have access to such a file, please follow these steps.

----------------------------

1. Click on the button **"add post"** and then **"import publications"** on the topmenu of ${project.name}.
Alternatively, you can also follow [this link](https://www.bibsonomy.org/import/publications).

2. Here you need to choose the destination of you file on your computer with the button **"Durchsuchen"**. Then, you can decide, if the import should be visible to everyone (public) or not (private). With **"other"**, you can also choose a group, whose members are allowed to see it (e.g. friends).

3. Now you can enact the following decisions:
	- If you set a cross at **"edit before import"**, the imports will not be saved directly. Instead, you will have the opportunity to edit the publications before adding them.
	- If you check the box below, you will overwrite any entry referencing the same publication.
	- For **"delimiter"**, you have to choose which sign is used to seperate the entries. E.g. whitespace, comma or semicolon.
	- If you do not choose "whitespace", you can now edit the delimiter yourself. 
	- Last, you have to specify the encoding of your file. It is mostly "UTF-8".

4. Did you choose every setting? Finalize the import by pressing the **"post"**-button.

