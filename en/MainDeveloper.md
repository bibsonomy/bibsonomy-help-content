<!-- de: MainEntwickler -->

## Help for software developers
----------------------------

This is the **help page for software developers**. For further information, please visit the
[ help page for beginners](MainBeginner "wikilink") and the [ help page for advanced users](MainAdvanced "wikilink").

${project.name} is a free service to manage your bibliographic
references and publications. In addition, ${project.name} allows you
and your colleagues to work together in groups (e.g. exchange
interesting bookmarks and publications). This page shows you all
functions for software developers.

----

![ <File:Binary-Code.png>](maindeveloper/Binary-Code.png  " File:Binary-Code.png")

### ${project.name} - developers garden


You want to integrate ${project.name} into your website/webapp? No
problem. We provide you the following interfaces for free.

- [Developers' Corner at BitBucket](https://bibsonomy.bitbucket.io/)


- [REST-API](https://bitbucket.org/bibsonomy/bibsonomy/wiki/documentation/api/REST%20API)

<!-- -->

- [Integration with other systems and software - Overview](Integration "wikilink")

<!-- -->

- [Javascript Code Snippets](JS-Codesnippet "wikilink")

<!-- -->

-   [Anonymised records for your own research purposes](https://www.kde.cs.uni-kassel.de/bibsonomy/dumps/)


----

![ <File:News.png>](maindeveloper/News.png  " File:News.png")

### Press Distribution

Information for media representatives and other interested parties on
${project.name} and related projects:

- [Our Projects](Projects "wikilink")

<!-- -->

-   [ Press Distribution](Press "wikilink")


----

${project.theme == "bibsonomy"}{

![<File:Messages-Information-01.png>](maindeveloper/Messages-Information-01.png  " File:Messages-Information-01.png")

### Contact the developers of ${project.name}


You need help? You have questions about ${project.name}? You need a
special feature or you have an idea for improvement? Contact us! Here is
our contact data. We appreciate your feedback.  

------------------------

![<File:Message-Mail.png>](maindeveloper/Message-Mail.png  "fig: File:Message-Mail.png")
<webmaster@bibsonomy.org>

-   Contact us via email.  

-----------------------------------

![<File:WordPress.png>](maindeveloper/WordPress.png  "fig: File:WordPress.png")
[blog.bibsonomy.org](http://blog.bibsonomy.org)

-   Feature of the week
-   Releases-News
-   Questions, feature-requests and many more

-------------------------

![ <File:Twitter+Bird.png](maindeveloper/Twitter+Bird.png  "fig: File:Twitter+Bird.png")
[Join "BibSonomyCrew" on Twitter](https://twitter.com/BibSonomyCrew)

-   Updates via Tweet

----------------------------

![<File:Conference-Call.png>](maindeveloper/Conference-Call.png  "fig: File:Conference-Call.png")
For all friends of the classic mailing list:
<bibsonomy-discuss@cs.uni-kassel.de>

-   Discuss with us about new features
-   Discussions are public and all Users can see the questions and
    answers

}

--------------------


This is the **help page for software developers**. For further information, please visit the
[ help page for beginners](MainBeginner "wikilink") and the [ help page for advanced users](MainAdvanced "wikilink"). 


