<!-- de: LiteraturlisteExportierenSpezifisch -->

## Bibliography Export - program specific
----------------------------------------------

In this section you learn how to export data from ${project.name} to commonly used software.  


### Microsoft Word

1.  First, click on the arrow symbol on the very right of the clipboard - a
    sub-menu appears.
    
2.  Under **"export"**, select the option **"more ..."**.

3.  A page with all available export formats is displayed. Select the number of posts you want to export and then **"MSOffice XML"**. Then save this file on your hard disk.  
    
    ![ <File:word1.png>](exportdata/word1.png  "fig: File:word1.png")  
    
4.  In Microsoft Word, you can import this file by clicking on
    **"References"** and then on **"Manage Sources"**. In the dialogue
    you can click on the **"Browse..."** button and load the saved file
    from step 3.  
    
    ![ <File:word2.png>](exportdata/word2.png  "fig: File:word2.png")  
    
    ![ <File:word3.png>](exportdata/word3.png  "fig: File:word3.png")  


------------

### Bibliographix

1.  Export your data from ${project.name} as BibTeX file with the
    extension .bib or .txt.
    
2.  Start Bibliographix. Click on the tab *"mehr..."*, then on *"Daten
    austauschen"*.  
    
    ![ <File:biblio1.png>](exportdata/biblio1.png  "fig: File:biblio1.png")  
    
3.  In the left menu, choose the option *"Import“* and then click on
    *"auswählen“* to select your saved file with the extension .txt
    oder .bib.
    
4.  Finally, click on *"Import!"*  
    
    ![ <File:biblio2.png>](exportdata/biblio2.png  "fig: File:biblio2.png")  

  
-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").