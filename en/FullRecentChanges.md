<!-- de: SämtlicheÄnderungen -->

## All changes
-----------

This is a list of all changes ever made. A shorter list can be found
under [ Recent changes](RecentChanges "wikilink").

This server is located in the time zone *[ {INSERT CurrentTimePlugin
format=zzzz}]({INSERT_CurrentTimePluginFormat=zzzz} "wikilink")*, where
currently it's the [ {INSERT CurrentTimePlugin format='dd. MMMM yyyy
\\', \\' HH:mm
\\'Uhr\\''}]({INSERT_CurrentTimePluginFormat='dd._MMMMYyyy_\',_\'_HH:mm_\'Uhr\''} "wikilink").

[ {INSERT
com.ecyrd.jspwiki.plugin.RecentChangesPlugin}]({INSERTCom.ecyrd.jspwiki.plugin.RecentChangesPlugin} "wikilink")

  
-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").

