<!-- de: LebenslaufÄndern -->

## Change your Curriculum Vitae (CV)
-------------------------

The CVWiki is a feature of ${project.name} which allows you to create
or manipulate your own CV page by using either predefined templates or
by creating your CV in your personal style. In your CV, you may provide
information about yourself and your interests (scientific/private).
Other users can view this information.


------------------------------------------------------------------------

### View your CV

![ <File:1.png>](changecv/1.png  "fig: File:1.png")  

1.  Click on your **username** in the [Main Menu](User_Interface "wikilink").

2.  Click on the **CV-button** next to your profile picture.

3.  Your CV will be displayed (as other users see it).  
    
    ![ <File:2.png>](changecv/2.png  "fig: File:2.png")  
    
4.  To change your CV, click on the **gear symbol**.

5.  Click on **"edit curriculum vitae"**. Here you are free to create
    your own CV page. For this, choose a layout unter **"edit layout"** and click on the **"edit layout"** button.
    Now you can use the
    [MediaWiki-syntax](http://en.wikipedia.org/wiki/Help:Wiki_markup) that is known from Wikipedia to edit your CV page.


**Alternative**: You can also edit your CV by clicking on the person
icon in the right main menu, then clicking on **"settings"** and then
choosing the tab **"curriculum vitae"**.  


------------------------------------------------------------------------

### Custom tags

We provide several XHTML tags so you can save time. However, some of the tags can only be used
for a user CV respectively a group CV. 
In the following table you can see which tags you can use for the CV of groups and/or users.

Tag |Meaning | Group | User 
--|--|--|--
`<name />` | Your name, linking to your personal ${project.name} page | yes | yes
`<image />` | Your profile picture | no | yes
`<homepage/>` | Your personal homepage | yes | yes
`<publications />` | A list of all the publications you tagged with "myown" | yes | yes
`<bookmarks />` | A list of all bookmarks you tagged with "myown" | yes | yes
`<profession />` | Your profession | no | yes 
`<institution />` | Your institution you work with | no | yes 
`<hobbies />` | A list of your hobbies | no | yes
`<interests />` | Your personal interests | no | yes 
`<birthday />` | Your date of birth | no | yes 
`<location />` | Your current location | no | yes
`<regdate />` | Your date of registration with ${project.name} | no | yes
`<members/>` | List of all group members | yes | no
`<groupimage/>` | The group icon | yes | no
`<tags />` | Your tags | yes | yes


For `<publications />`, `<bookmarks />` and `<tags />`, you
can also specify other tags as selectors for your resources if you add
`tags="tag1 tag2\[ ...\]"` as parameter.
For example, `<publications tags="data mining" />` returns all publications in your list which you
tagged with both `data` and `mining`. 

For `<tags />` you can use the following selectors: `style` with the
options `cloud` and `list`; `order` with the options `alpha` and `freq`, `minfreq`
with a number as the parameter, and `type` with the options `bookmarks` and
`publications`.

-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").