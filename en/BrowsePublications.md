<!-- de: PublikationenDurchstöbern -->

## Browse publications/bookmarks
-----------------------------

We have already shown you how to [ search and add bookmarks and
publications](BookmarkPublicationManagement "wikilink") in ${project.name}. In this tutorial you will learn how to browse your own
bookmark/publication list in order to get an overview of your bibliography.  
  
**Requirements:** To use this tutorial, you have to unlock the [
advanced features ](UnlockAdvancedMode "wikilink") of ${project.name}.  
  
![ <File:1.png>](browsepublications/1.png  "fig: File:1.png")
![ <File:2.png>](browsepublications/2.png  "fig: File:2.png")  
  
How to browse publications/bookmarks:  

1.  Click on **"my ${project.name}"**. A menu appears. Then click on
    **"browse publications"** (Note: This menu item is only visible if you
    have activated the [ advanced features](UnlockAdvancedMode "wikilink")).

2.  The **"search options"** allow you to choose different **tags**
    and/or **authors**, for which you want to display all entries. In
    order to select more than just one list item, keep pressing the CTRL
    key during the mouse click.
    
3.  You can use the **"and/or"** buttons to set up the search query in
    different ways.
    
4.  Under **"search results"** you will find all results that match the
    selection of 2. and 3.
    
5.  The textbox **"filter"** allows you to filter the results from 4.

-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").
  

