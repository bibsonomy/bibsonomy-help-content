<!-- de: DiskutierteEinträge -->

## Discussed Posts
---------------

Two important aspects of working with literature are the processes of
sharing it and of exchanging ideas and thoughts about it among
colleagues. The [ discussion features](DiscussionReviewsComments "wikilink") in ${project.name}
provide support in doing so.

The discussed posts pages give an overview on resources that have
recently been discussed by a specific user, by any user in general or by
members of a specific group ([more on discussions within groups](GroupFunctions "wikilink")).

----------------------------------

### Popular discussions

To find entries that have recently been discussed, click on **popular**
in the [ left main menu](User_Interface "wikilink") and then on **discussions**.
By clicking on the star ratings of an entry, you get to see the detailed view
where you can view and write comments and reviews.

--------------------------------

### Discussions of a user

To find the resources that were discussed by a specific user, first go
to their user page (by clicking on the user's name **or** by using the
search function). In the sidebar on the right, click on **show posts
recently discussed by &lt;username>**.  

![ <File:user.png>](discussedposts/user.png  "fig: File:user.png")  

To view your **own discussed entries**, click on **my ${project.name}**
in the [ left main menu](User_Interface "wikilink") and then on **discussed posts**.

---------------------------------------------

### Discussion of a group

Similar to the user-centered version these pages show resources that
were discussed by members of a particular group. To view the discussed
entries of a group, first go to their [ group page](GroupFunctions "wikilink") (by clicking on the group's name **or**
by using the search function). In the sidebar on the right, click on
**show posts recently discussed by members of &lt;groupname>**.  

![ <File:group.png>](discussedposts/group.png  "fig: File:group.png")  

**Note:** On each of the presented discussed posts pages,
of course **privacy constraints** are respected. For example on the
discussed page of another user, resources are not listed if the reviews
and comments by that user were all posted anonymously. 

------------------

### Discussion Statistics

On every page where discussed posts of a user, of a group or of
${project.name} users in general are listed, there is a **statistic
box** in the sidebar on the right. There, the rating distribution of all
reviews of the user/the group/the system is displayed. This way, you can
compare reviews easier.  
  
![ <File:statistic.png>](discussedposts/statistic.png  "fig: File:statistic.png")

-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").
  

