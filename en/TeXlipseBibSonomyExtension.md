<!-- de: TeXlipseBibSonomyExtension -->

## TeXlipseBibSonomyExtension
----------------

By using the TeXlipseBibSonomyExtension you can import the publications
from your BibSonomy account directly in [TeXlipse](http://texlipse.sourceforge.net/) and make use of them.

For installation use the Eclipse Update mechanism with the [update site](https://texlipse.bibsonomy.org/) `https://texlipse.bibsonomy.org/`. Please note, that older versions of
the extension are also available from the TeXlipse update site, newer
versions only from the bibsonomy.org update site. Alternatively copy the
file below in your eclipse dropins folder.

Please note: You need the TeXlipse plugin in version 1.5 or above to use
the TeXlipseBibSonomyExtension.

----

### Features

#### Preferences

Use the preference page to adjust your BibSonomy username and API key.
Additionally enable or disable the plugin.  
No account yet? Get one for free at [BibSonomy](https://www.bibsonomy.org/).  

![preferences.png](texlipsebibsonomyextension/pref.png)  

-----------------

#### Cite Function

Import publication entries from the BibSonomy server into the .bib file
of your LaTeX project.  
There are:

-   entries existent in the .bib file only (yellow)
-   entries existent on the server only (red)
-   entries in the .bib file and on the server (blue)


![preferences.png](texlipsebibsonomyextension/cite.png)  

-------------

#### Use Tags

Filter the entries-to-fetch with one or more tag(s).  

![preferences.png](texlipsebibsonomyextension/tags.png)  

----------------------

#### Synchronization

This function enables the synchronization of the .bib file with the
server (not completely working until now).  

![preferences.png](texlipsebibsonomyextension/sync.png)  

------------------------------------------------------------------------

### Current release

[TeXlipseBibSonomyExtension\_0.1.1.1](https://bitbucket.org/bibsonomy/bibsonomy-texlipse/downloads/org.bibsonomy.texlipseextension_0.1.1.1.jar)

------------------

### Older releases

[TeXlipseBibSonomyExtension\_0.1](https://bitbucket.org/bibsonomy/bibsonomy-texlipse/downloads/org.bibsonomy.texlipseextension_0.1.0.jar)

---------------------

### Known issues

-   Sometimes the .bib file and autocomplete container lose sync. In
    that case re-import the file with the `\bibliography` command.
    
-   The synchronization between server and file is only temporary and
    will be replaced in future releases.

-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").