<!-- de: EintragPublikationen -->

## Adding publications
-----------------------------

Every user can add publications to ${project.name}. Thus, our database  continues to grow with its users.
This instruction shows multiple ways to achieve this.

---------------------

### Automatic addition by the ${project.name}-Add-on

The ${project.name}-addon can help you with that. It automatically collects metadata from various systems. [This manual](Integration/IntegrationBrowser "wikilink") explains how to install it.

---------------------

### Manual addition by entering information by hand

You can also add publications by entering information yourself. Click in the main menu on the buttons **"add post"** and **"add publication"**. Here, you can also enter the ISBN-, DOI- or ISSN-number. Follow [this instruction](AddPublications/AddManual "wikilink") for more information.

---------------------

### Import of publications by BibTeX

It is possible to import a publication from other literature management software (e.g. Citavi). For that,
you will first need to create a BibTeX-dataset and secondly import it to ${project.name}. Follow [this instruction](AddPublications/ImportData "wikilink") for more information.

---------------------

### Addition by scanning the ISBN code

If you have access to a webcam, this way might be suitable for you. It is possible to scan the ISBN code of your publication with it and thereby adding it to ${project.name}. For help with that, visit [this instruction](AddPublications/AddScan "wikilink").

---------------------

### Adding numerous publications

Would you like to import multiple publications at once? Follow [this instruction](AddPublications/ImportMultiplePublications "wikilink") to learn, how to use a BibTeX- or EndNote-file to import a list of publications.

-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").  