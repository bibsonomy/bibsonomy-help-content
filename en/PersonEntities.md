<!-- de: Personeneinträge -->

## Person Entities
---------------

${project.theme == "bibsonomy"}{

### What are "person entities"?

Person entities are representations of **authors of publications** in
the system of ${project.name}. Note that person entities do **not
represent users** of ${project.name}, but rather the persons that are
associated with publications.  
For example, any students might be users of ${project.name}. 
They registered for a personal account and have access to their own [user page](UserPage "wikilink").
If they have not yet published any publication, they are however not recognised as **authors** by ${project.name}.
In that case, they are not represented by a unique **person entity**.

------------------------------------------------------------------------

### Overview

To view an author's page, just click on their name that is displayed
with their publication. To search for an author, use the [
searchbar](SearchPageHelp "wikilink") on the upper right hand side,
select the field "author" and then enter the author's name. You can then
click on the author's name that is displayed with the publications.  


![<File:searchName.png>](personentities/searchName.png  "fig: File:searchName.png")

![<File:searchBar.png>](personentities/searchBar.png  "fig: File:searchBar.png")

------------------------------------------------------------------------

### Add new persons

After clicking on an author's name, you get redirected to a page where
you can associate the post (publication) with a person entity (author).  
If the author isn't registered by a person entity in the system yet, you
can **add a new person** with the name of the author. Just click on the
respective button at the bottom of the page.  

![<File:NoPersonFound.png>](personentities/NoPersonFound.png  "fig: File:NoPersonFound.png") 

After clicking on the button, a popup window appears and you are asked
if you want to create a **new person entity** or if you want to connect
the author's name with a **person with another name**.  
*(This can be the case if the author previously only has published under
another name (an alias name or a former name). In this case, click on
"Person with Another Name" and then, search the person you want to
connect the author's name with)*.  

![<File:AddOrSelectPerson.png>](personentities/AddOrSelectPerson.png  "fig: File:AddOrSelectPerson.png") 

------------------------------------------------------------------------

### Associate authors with publications

After clicking on an author's name, you get redirected to a page where
you can associate the post (publication) with a person entity (author).  
If the author's name is already represented by a person entity in the
system, you can see a **list of authors** with this name.  

![<File:PersonFound.png>](personentities/PersonFound.png  "fig: File:PersonFound.png")

In this list, you can select the person entity that fits to the author.
You then will be asked in a popup window if you want to **save the
connection** of the author and the person entity in the system ("Yes,
save") or if you **only want to display the person's page** without
saving ("No, show person only").  

![<File:CommunityChoice.png>](personentities/CommunityChoice.png  "fig: File:CommunityChoice.png")

If no person in this list fits to the author, click on "other person" to
**create a new person entity** for the author's name.

------------------------------------------------------------------------

### The author's page

After having created a new person entity or having connected the
author's name to an existing entity, the author's page will be
displayed.  
You can also access this page by URL:  
`/person/<first letter of first name>.<last name>`, e.g. [
/person/f.jannidis](${project.home}person/f.jannidis "wikilink").  

![<File:overview.png>](personentities/overview.png  "fig: File:overview.png")

On this page, you get an overview on the author's academic degree, the
alternative name, their own/related thesis and other publications
associated with the person.  
You are also allowed to add missing information. If this person entity
is you, you can claim this person by clicking on the button "That's me" which 
you will find on the right.

}

${project.theme == "puma"}{

### What are "person entities"?

Person entities are representations of **authors of publications** in
the system of ${project.name}. Note that person entities do **not
represent users** of ${project.name}, but rather the persons that are
associated with publications.  
For example, any students might be users of ${project.name}. 
They registered for a personal account and have access to their own [user page](UserPage "wikilink").
If they have not yet published any publication, they are however not recognised as **authors** by ${project.name}.
In that case, they are not represented by a unique **person entity**.

------------------------------------------------------------------------

### Overview

To view an author's page, just click on their name that is displayed
with their publication. To search for an author, use the [
searchbar](SearchPageHelp "wikilink") on the upper right hand side,
select the field "author" and then enter the author's name. You can then
click on the author's name that is displayed with the publications.  


![<File:searchName.png>](personentities/searchName.png  "fig: File:searchName.png")

![<File:searchBar.png>](personentities/searchBar.png  "fig: File:searchBar.png")

------------------------------------------------------------------------

### Add new persons

After clicking on an author's name, you get redirected to a page where
you can associate the post (publication) with a person entity (author).  
If the author isn't registered by a person entity in the system yet, you
can **add a new person** with the name of the author. Just click on the
respective button at the bottom of the page.  

![<File:NoPersonFound.png>](personentities/NoPersonFound.png  "fig: File:NoPersonFound.png") 

After clicking on the button, a popup window appears and you are asked
if you want to create a **new person entity** or if you want to connect
the author's name with a **person with another name**.  
*(This can be the case if the author previously only has published under
another name (an alias name or a former name). In this case, click on
"Person with Another Name" and then, search the person you want to
connect the author's name with)*.  

![<File:AddOrSelectPerson.png>](personentities/AddOrSelectPerson.png  "fig: File:AddOrSelectPerson.png") 

------------------------------------------------------------------------

### Associate authors with publications

After clicking on an author's name, you get redirected to a page where
you can associate the post (publication) with a person entity (author).  
If the author's name is already represented by a person entity in the
system, you can see a **list of authors** with this name.  

![<File:PersonFound.png>](personentities/PersonFound.png  "fig: File:PersonFound.png")

In this list, you can select the person entity that fits to the author.
You then will be asked in a popup window if you want to **save the
connection** of the author and the person entity in the system ("Yes,
save") or if you **only want to display the person's page** without
saving ("No, show person only").  

![<File:CommunityChoice.png>](personentities/CommunityChoice.png  "fig: File:CommunityChoice.png")

If no person in this list fits to the author, click on "other person" to
**create a new person entity** for the author's name.

------------------------------------------------------------------------

### The author's page

After having created a new person entity or having connected the
author's name to an existing entity, the author's page will be
displayed.  
You can also access this page by URL:  
`/person/<first letter of first name>.<last name>`, e.g. [
/person/f.jannidis](${project.home}person/f.jannidis "wikilink").  

![<File:overview.png>](personentities/overview.png  "fig: File:overview.png")

On this page, you get an overview on the author's academic degree, the
alternative name, their own/related thesis and other publications
associated with the person.  
You are also allowed to add missing information. If this person entity
is you, you can claim this person by clicking on the button "That's me" which 
you will find on the right.

}

${project.theme == "wueresearch"}{

### What are "person entities"?

Person entities are representations of **authors of publications** in
the system of ${project.name}. Note that person entities do **not
represent users** of ${project.name}, but rather the persons that are
associated with publications.  
For example, any students might be users of ${project.name}. 
They registered for a personal account and have access to their own [user page](UserPage "wikilink").
If they have not yet published any publication, they are however not recognised as **authors** by ${project.name}.
In that case, they are not represented by a unique **person entity**.

------------------------------------------------------------------------

### Overview

To view an author's page, just click on their name that is displayed
with their publication. To search for a particular author, click on the field "**People**" in the top bar. There, you can search for a specific name in the search field or choose it from the alphabetical list. You may also decide not to show only the authors linked to the University by using the button on the bottom right.   

![ <File:searchwue.png>](personeneinträge/searchwue.png  "fig: File:searchwue.png")

Alternatively, use the [searchbar](SearchPageHelp "wikilink") on the upper right hand side. Select the field "author" and then enter the author's name. Then click on the author's name that is displayed with the publications.  

![<File:searchName.png>](personentities/searchName.png  "fig: File:searchName.png")

![<File:searchBar.png>](personentities/searchBar.png  "fig: File:searchBar.png")

------------------------------------------------------------------------

### Add new persons

After clicking on an author's name without an exisiting entry, you will be redirected to a page on which
you can associate the post (publication) with a person entry (author).  
If the author isn't registered by a person entity in the system yet, you
can **add a new person** with the name of the author. Just click on the
respective button at the bottom of the page.  

![<File:PersonChoosewue.png>](personentities/PersonChoosewue.png  "fig: File:PersonChoosewue.png") 

After clicking on the button, a popup window appears and you are asked
if you want to create a **new person entity** or if you want to connect
the author's name with a **person with another name**.  
*(This can be the case if the author previously only has published under
another name (an alias name or a former name). In this case, click on
"Person with Another Name" and then, search the person you want to
connect the author's name with)*.  

![<File:AddOrSelectPerson.png>](personentities/AddOrSelectPerson.png  "fig: File:AddOrSelectPerson.png") 

------------------------------------------------------------------------

### Associate authors with publications

After clicking on an author's name, you will be redirected to a page where
you can link the post (publication) to a person entity (author).  
If the author's name is already represented by a person entity in the
system, you can see a **list of authors** with this name. If you need more information about the names, click on the eye symbol to the right of the entries to go to the associated person page.

![<File:PersonChoosewue.png>](personentities/PersonChoosewue.png  "fig: File:PersonChoosewue.png") 

In this list, you can select the person entity that fits to the author.
You then will be asked in a popup window if you want to **save the
connection** of the author and the person entity in the system ("Yes,
save") or if you **only want to display the person's page** without
saving ("No, show person only").  

![<File:CommunityChoice.png>](personentities/CommunityChoice.png  "fig: File:CommunityChoice.png")

If no person in this list fits to the author, click on "other person" to
**create a new person entity** for the author's name (see above).

------------------------------------------------------------------------

### The author's page

After having created a new person entity or having linked the
author's name to an existing entry, the author's page will be
displayed.  
You can also access this page by URL:  
`/person/<first letter of first name>.<last name>`, e.g. [/person/a.hotho](${project.home}person/a.hotho "wikilink").  

![<File:overviewwue.png>](personentities/overviewwue.png  "fig: File:overviewwue.png")

This page gives you access to the following personal information:
* Name of the institution with possible link to the organization
* E-mail adress
* home page
* possibly ORCID as a unique identification number

In addition, all publications associated with the person are listed. You can filter these by publication type.
Here, you may also add missing information the author's page.

}

-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").