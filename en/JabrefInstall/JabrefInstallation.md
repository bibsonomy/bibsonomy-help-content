<!-- de: JabrefInstallieren/JabrefInstallation -->

---------------------------

[`Back to JabRef page.`](JabrefInstall "wikilink")

---------------------

##  JabRef - Installation
------------------

In order to use the plugin, you need a ${project.name} useraccount.  
Just follow these simple steps to experience the synergies of JabRef and
${project.name}:  

1. **Download JabRef**  
  To use the ${project.name} plugin, you need an older version of JabRef.
  You can find one on this page: <http://www.oldfoss.com/JabRef.html>  
  Choose a version that is older than 2.12, for example 2.10:  
  ![<File:downloadJabref.png>](JabrefInstallation/downloadJabref.png  "fig: File:downloadJabref.png")  
  Download the suitable file for your operating system and execute it.
  JabRef will then be installed.  
  
2. **Download the ${project.name} plugin**  
  You can download the plugin here:
  <https://gforge.cs.uni-kassel.de/frs/download.php/76/jabref-bibsonomy-plugin-2.5.2-bin.jar>  
  
3. **Move the plugin jar file**  
  Put the plugin jar file into a subfolder called *Plugins* in your JabRef
  installation directory.  
  
4. **Start JabRef**  
Start JabRef and you can start working with the Plugin!

------------------------------------------------------------------------