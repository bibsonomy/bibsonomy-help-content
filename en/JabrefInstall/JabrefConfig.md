<!-- de: JabrefInstallieren/JabrefKonfiguration -->

---------------------------

[`Back to JabRef page.`](JabrefInstall "wikilink")

---------------------

##  JabRef - Configuration
------------------

Before you can start working with the plugin, you have to enter your
${project.name} username and your API key. Do the following steps:

1.  **Create a new database**  
    Open JabRef and create a new database first. To do so, click on
    *File* and then on *New database*.  
    
2.  **Open the plugin settings**  
    In the menu, click on *Plugins*, then on *BibSonomy* and finally on
    *Settings*.  
    
    ![<File:jabrefSettings.png>](JabrefConfig/jabrefSettings.png  "fig: File:jabrefSettings.png")  
    
3.  **Enter your data**  
    Enter your ${project.name} username and your API key. You can find
    your API key on the [settings page](${project.home}settings "wikilink") in
    the tab "settings". To store the API key, select the checkbox *Store
    API key*. You can select all the other fields as you wish. Save your
    data by clicking on *Save*.  
    
    ![<File:jabrefSettings2.png>](JabrefConfig/jabrefSettings2.png  "fig: File:jabrefSettings2.png")  

Now, the ${project.name} plugin is configured and you can start using
it. You can find further information on data exchange between JabRef and
${project.name} on [JabRef- data exchange](Jabref "wikilink"). If you
have questions regarding the plugin, visit our [contact page](Contact "wikilink").

------------------------