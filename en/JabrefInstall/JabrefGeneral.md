<!-- de: JabrefInstallieren/JabrefAllgemein -->

---------------------------

[`Back to JabRef page.`](JabrefInstall "wikilink")

---------------------

##  JabRef - General information
------------------

[JabRef](http://jabref.sourceforge.net/) is an open source bibliography reference manager based on the
BibTeX-format (like ${project.name}).
Starting with version 2.4b1, JabRef can be extended using a [plugin
system](http://jabref.sourceforge.net/help/Plugin.php) based on the Java Plugin Framework ([JPF](http://jpf.sourceforge.net/)). We have now built
a ${project.name} plugin for JabRef that allows a user to directly
access their publications from ${project.name}.  
This plugin enables you to:

-   Fetch publication entries directly from ${project.name},
-   Upload publications into your personal account on ${project.name},
-   Delete entries from your collection.

For example, you can do the following:

-   Download your complete ${project.name} collection into JabRef, use
    its built-in bibtex-key generator to unify the bibtex keys of your
    publicatons, and upload all entries again.
-   Download your complete ${project.name} collection into JabRef,
    perform integrity checks on it (missing fields, etc), and upload all
    entries again.
-   Use JabRef's web search facility to import entries (e.g, from
    PubMed), edit them locally to your needs, and bulk-upload them
    to ${project.name}.

All of the above integrates seamlessly into the usual workflow of JabRef

- this means you don't have to visit the ${project.name} web interface
  during the whole process.
-------------------