<!-- de: Gruppenfunktionen/GruppeGruenden -->

---------------------------

[`Back to overview page on groupfunctions.`](GroupFunctions "wikilink")

---------------------

## Create a new group
--------------------------

**Goal**: This guide shows you how you can create a new group.  

![ <File:4.png>](GroupCreate/4.png "fig: File:4.png")  

1. Click on **"groups"** in the [main menu](User_Interface "wikilink") - a menu appears.
2. Select **"create new group"**.
3. Fill out the form. It's important to describe the purpose of the new
   group in the field **"reason for request"**. This is because our
   administrators will check each group request manually to prevent
   spam groups.
4. Finally click on **"request group"**.
5. The administrators will check your group request. You will receive
   an email after our administrators accepted your request.