<!-- de: Gruppenfunktionen/GruppenDiskussionen -->

---------------------------

[`Back to overview page on groupfunctions.`](GroupFunctions "wikilink")

---------------------

## Group discussions
--------------------------

When working in science, it is important to share thoughts and ideas
with colleagues. In ${project.name}, you can [discuss](DiscussionReviewsComments "wikilink") posts publicly, but also
within groups.  

When writing a comment on a post, you can restrict the comment's
**visibility** for the group. To do so, click on "other" next to
"visibility" and then choose the group's name. Your comment is now only
visible for group members, even **not** for the owner of the entry
(however, the average rating is always public).  
This way, you can discuss relevant scientific publications within groups
or recommend interesting articles to the group's members. To see which
posts are discussed within a group, go to the [group page](GroupFunctions/GroupPage "wikilink") and click on **show posts
recently discussed by members of &lt;group name>**.