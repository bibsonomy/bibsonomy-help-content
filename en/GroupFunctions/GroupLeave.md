<!-- de: Gruppenfunktionen/GruppeAustreten -->

---------------------------

[`Back to overview page on groupfunctions.`](GroupFunctions "wikilink")

---------------------

## Leave a group
--------------------------

To leave a group, go to the **group page** (click on the name of the
group OR in the left main menu, click on "groups", then on the group's
name and then on "posts").  
In the sidebar on the right, there is a **green button** with the text
**"member"**. When you hover the mouse over this button, it will turn
**red** and the text changes to **"leave group"**. Click on it if you
are sure you don't need this group any more.  

![ <File:leave.png>](GroupLeave/leave.png "fig: File:leave.png")