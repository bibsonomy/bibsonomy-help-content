<!-- de: Gruppenfunktionen/GruppenSeite -->

---------------------------

[`Back to overview page on groupfunctions.`](GroupFunctions "wikilink")

---------------------

## The group page
---------------------------

When you click on the name of a group, you get directed to the **group
page**. There, you can see publications/bookmarks of the group, the
group members and used tags.  

![<File:overview.png>](GroupPage/overview.png "fig: File:overview.png")  

- **Recently discussed posts:** In the sidebar, click on **show posts
  recently discussed by members of &lt;group name>.** On the following page,
  you can see all posts that were recently rated or commented by this
  group. Also, you can see the **rating distribution** of the posts.
- **CV:** In the sidebar, click on the **CV button**. On the following
  page, you get an overview on the group (name, members, recently
  added bookmarks/publications).