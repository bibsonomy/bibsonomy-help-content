<!-- de: Gruppenfunktionen/GruppeSuchen -->

---------------------------

[`Back to overview page on groupfunctions.`](GroupFunctions "wikilink")

---------------------

## Search for a group and join it
----------------------------------

![ <File:1.png>](GroupSearch/1.png "fig: File:1.png")  

1. Click on **"groups"** in the [main menu](User_Interface "wikilink") - a menu will be displayed.

2. Select **"all groups"** in the menu.

3. You see all groups and some information about the group (name
   and description) in the first column.

4. The last column (which you only can see when you are logged in
   to ${project.name}) shows the button **"join"** for each group.
   Click on the corresponding **"join"** button to apply for membership.  

   ![<File:joinrequest1.png>](GroupSearch/joinrequest1.png "fig: File:joinrequest1.png")

5. In the field **"reason"**, enter some text where you describe why
   you want to join this group (this information will be read by the
   group admin).

6. Next to **"captcha"**, check the box to make sure you are not
   a machine/robot.

7. If you want other group members to get access to your documents,
   check the box next to "Share your documents with the group".

8. Finally, click on **"send request"**.

9. The group administrator receives a message that you want to be a
   member of his/her group. So it's important to write a good explanation
   why you want to join. Only the group administrator can decide
   whether you will be accepted to the group or not.
