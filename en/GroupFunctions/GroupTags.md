<!-- de: Gruppenfunktionen/GruppenTags -->

---------------------------

[`Back to overview page on groupfunctions.`](GroupFunctions "wikilink")

---------------------

## Predefined group tags
--------------------------

Predefined group tags allow tags to be selected in a group, that a group admin has previously created (Gruppenfunktionen "wikilink"). 

When entering an individual publication, with the option "send to" <groupname>, group tags can be selected:

![<File:TagsAuswählen.png>](GroupTags/TagsAuswählen.png "fig: File:TagsAuswählen.png") 


Here you can select the group to which the entry is to be copied and the predefined tags appear.
The tags can be added and removed again with a click; selected tags are greyed out.
The mouseover shows the stored description of the individual tags.
Only the predefined group tags are added to the group entry; the tags are not displayed in the own collection.
In this way, it is possible that only a defined set of tags is added to the entries in the group, while only the own tags are displayed in the own collection.