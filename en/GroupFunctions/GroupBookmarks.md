<!-- de: Gruppenfunktionen/GruppeLesezeichen -->

---------------------------

[`Back to overview page on groupfunctions.`](GroupFunctions "wikilink")

---------------------

## Share bookmarks/publications with the group
--------------------------------------------------

In the dialogue **"Edit your publication/bookmark post"** which appears
when you [add or edit](BookmarkPublicationManagement "wikilink") a (new) bookmark/publication, you have the possibility
to share the entry with several groups/friends.  

![ <File:3.png>](GroupBookmarks/3.png "fig: File:3.png") 

1. Under **"post visibility"**, choose the option **"other"**.
2. Choose the group you want to share the publication/bookmark with.
3. Click on **"save"** at the end of the page.

**Another way** to share a bookmark/publication with a group is to tag
the specific bookmark/publication with the [system tag](SystemTags "wikilink") `for:<group name>`.
This way, it will be copied to the group's collection.

**Note:** When a member of a group **deletes** their
${project.name} account, their publications/bookmarks tagged with the
system tag `for:<group name>` will still be available for the group.
Otherwise, it will be lost for the group.  
Also, if an entry shall be shared with **several groups**, it is useful
to use the system tag.