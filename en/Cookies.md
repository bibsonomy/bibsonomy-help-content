<!-- de: Cookies -->

## Cookies
-------

${project.name} is using so-called *cookies*. They are used to make
${project.name} more user-friendly, effective and secure. Cookies are
small text files that are stored by your browser on your computer. Most
of the cookies ${project.name} uses are so-called *session cookies*.
They are deleted automatically at the end of your visit, except when you
have selected the option "stay logged in" when signing in. In that case
the cookie is deleted after one year, except you delete it manually.

For internal statistics about web page visits ${project.name} might use
*Piwik*, a software to analyse web server access logs.

Piwik also uses cookies which allow us to analyse the usage of
${project.name}. The data about your visits that is produced by cookies
is only stored on ${project.name} servers. The data is used only for
internal statistics and deleted afterwards. When your browser supports
the *do not track* technology and when you have enabled it, Piwik will
not log your visits.

You can disable cookies by an appropriate setting of your web browser.
However, we point out that in that case not all functionality of
${project.name} is usable.

For questions about cookies you can contact us via the e-mail address
${project.email}.

-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").