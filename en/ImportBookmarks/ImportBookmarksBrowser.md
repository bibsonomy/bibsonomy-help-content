<!-- de: LesezeichenImportieren/LesezeichenImportierenBrowser -->

---------------------------

[`Back to the overview on import of bookmarks.`](ImportBookmarks "wikilink")

---------------------

## Import Bookmarks from your Browser
--------------------

### Chrome

Using Chrome, you can export bookmarks into an HTML file
by clicking on the menue on the right, choosing *Bookmarks* and then
clicking on *Bookmark manager*. There, click on *Organize* and finally
on *Export bookmarks to HTML file...*.  

![ <File:Chrome1.png>](ImportBookmarksBrowser/Chrome1.png  "fig: File:Chrome1.png")  

![ <File:Chrome2.png>](ImportBookmarksBrowser/Chrome2.png  "fig: File:Chrome2.png")  

-----------

### Firefox

Using Firefox, you can export bookmarks into an HTML file by clicking on
the bookmarks symbol next to the search bar and then clicking on *show
all bookmarks*. Next, you have to choose *Import and Backup* where you
can click on *Export Bookmarks to HTML...*.  

![ <File:Firefox1.png>](ImportBookmarksBrowser/Firefox1.png  "fig: File:Firefox1.png")  

![ <File:Firefox2.png>](ImportBookmarksBrowser/Firefox2.png  "fig: File:Firefox2.png")  

-------------

### Import an HTML file into ${project.name}

Stored HTML files can be imported into ${project.name} on the [ imports page](${project.home}import/bookmarks "wikilink").
In the upper section of this page, you can upload the exported HTML file which you just exported from
your browser. You can also define whether the imported bookmarks should
be viewable for all users **(public)** or just you **(private)**.  

![ <File:Bib2.png>](ImportBookmarksBrowser/Bib2.png  "fig: File:Bib2.png")  

