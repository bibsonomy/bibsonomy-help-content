<!-- de: LesezeichenImportieren/LesezeichenImportierenDelicious -->

---------------------------

[`Back to the overview on import of bookmarks.`](ImportBookmarks "wikilink")

---------------------

## Import Bookmarks from Delicious
-------------------------------------

In order to import your bookmarks at the [imports](${project.home}import/bookmarks "wikilink") page, you have
to enter your [Delicious](http://www.delicious.com/) account information. In case you want to import your **bookmarks**,
choose the corresponding option. Along with your bookmarks the corresponding tags
and visibility definitions will be imported.

You can also have your **tag bundles** be imported as *relations* within
${project.name} by choosing the other option.  

![ <File:Bib1.png>](ImportBookmarksDelicious/Bib1.png  "fig: File:Bib1.png")  