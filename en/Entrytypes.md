<!-- de: Eintragstypen -->

## Entry types
-----------

Entry types are used to classify BibTeX entries according to their type.
Currently, ${project.name} supports the following 24 types of publications:  

-   **article** (a short entry representing an article from a journal or
magazine)
-   **book** (a longer entry representing a book with an
    explicit publisher)
-   **booklet** (a work that is printed and bound, but without a named
    publisher or sponsoring institution)
-   **collection** (a collection)
-   **conference** (a contribution to a conference that hasn't been
    published in the proceedings of a conference)
-   **dataset** (a dataset, e.g. used for experiments)    
-   **electronic** (electronic publications, e.g. eBooks or
    blog entries)
-   **inbook** (a part of a book, which may be a chapter and/or a range
    of pages)
-   **incollection** (a part of a book with its own title)
-   **inproceedings** (an article in the proceedings of a conference)
-   **manual** (technical documentation, manual)
-   **mastersthesis** (a bachelor or master's thesis, a more precise specification is possible with the "type" field)
-   **misc** (an entry type used when nothing else seems appropriate)
-   **patent** (a patent)
-   **periodical** (regularly published work, e.g. a magazine)
-   **phdthesis** (a PhD thesis)
-   **preamble** (an introductory statement)
-   **preprint** (a preprint for a publication that will be published)
-   **presentation** (presentation, talk on an event)
-   **proceedings** (the proceedings of a conference)
-   **standard** (a technical standard, e.g. ISO/IETF)
-   **techreport** (a report published by a school or other institution,
    usually numbered within a series)
-   **unpublished** (a document with an author and title, but not
    formally published)
-   **preprint** (a selected piece of work printed and distributed before the 
    official publication date of the complete work, e.g. for a conference)

**For all entrytypes these fields are required:** title, author/editor and year.
Some entrytypes have additional fields, that **can** be entered by the Users:

- **article:** journal, volume, number, pages, month, language, DOI, ISBN, ISSN, note
- **book:** publisher, volume, number, series, address, edition, month, language, DOI, ISBN, note
- **booklet:** howpublished, address, month, language, DOI, ISN, note
- **inproceedings:** publisher, booktitle, volume, number, series, pages, address, month, organization,
language, DOI, ISBN, ISSN, eventdate, eventtitle, venue, note
- **dataset:** DOI, url
- **electronic:** language, DOI, note
- **inbook:** chapter, pages, publisher, volume, number, series, type, address, edition, month, language,
DOI, ISBN, note  
- **incollection:** publisher, booktitle, volume, number, series, type, chapter, pages, address, edition,
month, language, DOI, ISBN, note
- **manual:** organization, address, edition, month, language, DOI, ISBN, note
- **masterthesis:** school, type, address, month, language, DOI, ISBN, note
- **misc:** howpublished, month, language, DOI, note
- **periodical:** language, DOI, ISSN, note
- **presentation:** language, eventdate, eventtitle, venue, note
- **proceedings:** publisher, volume, number, series, address, month, language, DOI, ISBN, eventdate,
eventtitle, venue, organization, note
- **techreport:** institution, number, type, address, month, language, DOI, note
- **unpublished:** language, DOI, ISBN, ISSN, eventdate, eventtitle, venue, note

-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").
