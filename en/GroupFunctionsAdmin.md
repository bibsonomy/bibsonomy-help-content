<!-- de: GruppenfunktionenAdmin -->


## Admin Functions
--------------------------------------------------------------

As the administrator of a group, you have access to several functions
that other group members can't see. Click on **"groups"** in the left main
menu, hover the mouse over your group's name and then, click on **"settings"**. There, you can take following actions.

------------------------------------------------------------------------

### Change settings

Only the admin of a group is able to change the group's settings. You have access to [these operations](GroupFunctionsAdmin/GroupAdminSettings "wikilink").

------------------------------------------------------------------------

### Manage member list

If you would like to manage the member list of your group and make changes, then [these](GroupFunctionsAdmin/GroupAdminMembers "wikilink") are your possibilities.
    
------------------------------------------------------------------------

### Group invites

For some help with group invitations, visit [this page](GroupFunctionsAdmin/GroupAdminInvites "wikilink").

------------------------------------------------------------------------

### Change Curriculum Vitae

You are able to edit your group's CV. This is done the same way as you
can edit [your own CV](ChangeCV "wikilink").

------------------------------------------------------------------------

### Disband group

To disband a group, visit [this helppage](GroupFunctionsAdmin/GroupAdminDisband "wikilink"). 

-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").