<!-- en: FAQ -->

## Frequently Asked Questions (FAQ)

---

${project.theme.help == "wueresearch"}{

### Quick Links
- [Selecting a publication type](FAQ/#selecting-a-publication-type)
- [Automatically adding publications](FAQ/#automatically-adding-publications)
- [The WueResearch browser add-on](FAQ/#the-wueresearch-browser-add-on)
- [Selecting a citation style](FAQ/#selecting-a-citation-style)

---

## Selecting a Publication Type

**1. How do I know to which Publication Type my publication belongs?**

> To select a publication type for your publication, an overview of all selectable entry types is available in the “Add publication” menu. You can also find more information on adding publications and selecting the correct publication type in the following FAQs.

**2. To which Entry Type do Encyclopedia Articles belong?**

> Depending on the type of publication, encyclopedia articles belong to the entry types “Contribution to an Edited Collection” or “Book Chapter”.

**3. To which Entry Type do Legal Commentaries, Contributions to Legal Commentaries and Notes of/to Decisions belong?**

> Depending on the type of publication, they are listed as “Contribution to an Edited Collection” or as “Book Chapter”.

**4. To which Entry Type do Textbooks and Contributions to a Textbook belong?**

> Textbooks are assigned to the publication type “Monograph (Book)”, and contributions to a textbook belong to “Book Chapter”.

**5. To which Entry Type do Editorials and Letters to the Editor belong?**

> “Editorials” and “Letters to the Editor” are not independent publication types in WueResearch. They are assigned to the entry type “Article”.

**6. How can I record the editorship of a book or journal in WueResearch?**

> If you are the editor of a Monograph (Book), an Edited Collection or Conference Proceedings, or a Special Issue, then enter your name in the general information section in the field “Editor(s)” (as well as the names of possible other editors). Please leave the field “Author(s)” blank. Fill out all other fields as usual.

**7. What is the difference between a Review and a Book Review?**

> A “Review” is a systematic review in the form of an overview of the literature on a specific topic. An attempt is made to collect, summarize and critically evaluate all available knowledge on this topic. The already published specialist literature serves as its basis. In contrast, a “Book Review” is a critical appraisal of a book or article, or a summary of a work that is not written by the author.

**8. Why doesn’t the Publication Type “Journal” exist?**

> Currently, WueResearch only includes editorships of journals if the editorship refers to a single work (“special issue of a journal”). Permanent membership in editorial committees of journals is currently not recorded by WueResearch. Articles that have been published in scientific journals are assigned to the Publication Type “Article”.

---

## Automatically Adding Publications

**9. Why is my publication not found when searching for its ISSN or ISBN?**

> When searching for an ISSN or ISBN, only publications already added to WueResearch are searched. If the publication you are looking for is not yet stored in the systems database, an error message appears. If possible, please use the DOI of the publication you want to add when searching. Alternatively, please add the publication manually.

---

## The WueResearch Browser Add-On

**10. Why can’t I install the WueResearch browser add-on?**

> The WueResearch browser add-on is currently only supported by Firefox and Chrome. If you use a different browser, please use the automatic import or add the publication manually.

**11. Why doesn’t the import of my publication work with the WueResearch browser- add-on?**

> The WueResearch browser add-on only works on the homepages of certain publishers (currently around 150 supported publishers). You can find an overview of all supported publishers [here](../scraperinfo).

---

## Selecting a citation style

**12. How can I set preferred citation styles for my profile?**

> You can select your favorite citation styles which you would like to use regularly and that should be displayed when viewing a publication in the “Settings” menu of your profile. To do this, go to the “Settings” tab in the settings and navigate to the section “favourite export formats”. In the search field (“add export format”) type in the name of the citation style you would like to add
}