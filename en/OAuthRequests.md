<!-- de: OAuthAnfragen -->

## OAuth requests
--------------------------

After you have successfully [implemented OAuth's authorization process](OAuth "wikilink"), you can
now make requests to ${project.name}'s API.

You can now use ${project.name}'s rest logic interface to perform API
operations. 

    RestLogicFactory rlf = new RestLogicFactory("<HOME_URL>/api", RenderingFormat.XML);
    LogicInterface rl = rlf.getLogicAccess(accessor);
    
    Post<Bookmark> testPost = generatePost(accessor.getRemoteUserId());
    	
    //
    // publish first test post
    //
    List<Post<? extends Resource>> uploadPosts = new LinkedList<Post<? extends Resource>>();
    uploadPosts.add(testPost);
    testPost.getResource().recalculateHashes();
    String firstHash = testPost.getResource().getIntraHash();
    try {
        rl.createPosts(uploadPosts);
    } catch (BadRequestOrResponseException e) {
        System.err.println(e.getMessage());
    }

------------------------------------------------------------------------

Please replace `<HOME_URL>` with ${project.home}.

A **running example** can be found [here](https://bitbucket.org/bibsonomy/bibsonomy-help-content/src/b8d20eb1bde387f1a8772d91943452f64d5edda2/code-samples/oauth-rest-demo/src/main/java/org/bibsonomy/OAuthRestApiDemo.java?at=default&fileviewer=file-view-default). If you have further questions,
please feel free to write a mail to ${project.apiemail}.  

-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").
