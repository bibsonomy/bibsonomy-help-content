<!-- de: Einstellungen/JabRefLayouts -->

---------------------------

[`Back to your profile settings.`](Settings "wikilink")

---------------------

## Manage your own JabRef files
--------------------

*Warning: JabRef ist out-of-date and therefore only supported for legacy reasons. It is recommended to use files in CSL (Citation Style Language) instead. You can find help on the internal [page for CSL layouts](https://www.bibsonomy.org/help_en/settings/CSLLayouts).*

If you would like to create your own layouts with JabRef, you can find further information on the
[official JabRef webpage](https://help.jabref.org/en/CustomExports). You can then upload those files on 
${project.name} to customize the layout of any publication list and to export it in your desired style.
For this, you need at least one file with the extension `.layout`, which should describe your custom layout.

----------------------------

### Saving your own JabRef layouts on ${project.name}

It is possible to import your JabRef files to ${project.name}to customize the layout of your 
publication list. Follow the steps below:

- First, open the **settings page** of your ${project.name} account. You can find an instruction in the [help section]
  (https://www.bibsonomy.org/help_en/Settings).
  
- To visit the overview of your available layouts, you have to click on **"layout files"**.
![ <File:layout.png>](JabRefLayouts/layout.png "fig: File:layout.png") 

- Afterwards, you have to open the tab **layout files** and click  on the button **"Durchsuchen"** next to **item.layout**.
  Choose your JabRef file from your personal computer to upload it to ${project.name}.
   - If you want to display something at the beginning or the end of your publication list, you can upload additional files 
     with the extensions `begin.layout` or `end.layout`.

- Then, use the button **"upload"** to confirm your choice.
![ <File:upload.png>](JabRefLayouts/upload.png "fig: File:upload.png")

- Your publication list will now feature your own style.

----------------------------

### Deleting your own JabRef layout from ${project.name}

To delete uploaded layout files, you have to follow two steps:

- First, visit the menu **layout files** in your **settings** page as described above.

- There, click on the red button **"delete"**.                                                
![ <File:delete.png>](JabRefLayouts/delete.png "fig: File:delete.png")

