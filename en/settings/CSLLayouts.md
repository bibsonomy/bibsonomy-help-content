<!-- de: Einstellungen/CSLLayouts -->

---------------------------

[`Back to your profile settings.`](Settings "wikilink")

---------------------

## Manage your own CSL-files
-------------------------

${project.name} generally imports all CSL files from the official Github-Repository for [CSL files](https://github.com/citation-style-language/styles).
If you can not find a suitable style, it is possible to create your own CSL files. You can upload these on ${project.name} to display and export your publication list in your own style.
First, you need a file with the extension `.csl` that describes your desired layout. 
You can create your own CSL files by using for example the [visual editor](https://editor.citationstyles.org/visualEditor/).
For further information feel free to visit the [official webpage of the CSL project](https://citationstyles.org).

-----------------------

### Saving your own CSL layouts on ${project.name}

You can import your CSL files in ${project.name} to customize the layout of your citations. Follow the steps below:

- First, open the **settings page** of your ${project.name} account. You can find an instruction in the [help section]
  (https://www.bibsonomy.org/help_en/Settings).

- To visit the overview of your available layouts, you have to click on **"layout files"**.
![ <File:layout.png>](CSLLayouts/layout.png "fig: File:layout.png") 

- Afterwards, click on the button **"Durchsuchen"** and choose a file from your personal computer to upload it.

- Then, use the button **"upload"** to confirm your choice.
![ <File:upload.png>](CSLLayouts/upload.png "fig: File:upload.png")

- If you wish to cite a publication, you can now select your custom layout from the list of citation styles.
![ <File:cite.png>](CSLLayouts/cite.png "fig: File:cite.png")

-----------------------

### Deleting your own CSL layout from ${project.name}

To delete uploaded layout files, you have to follow two steps:

- First, visit the menu **layout files** in your **settings** page as described above.

- There, click on the red button **"delete"**.                                                
![ <File:delete.png>](CSLLayouts/delete.png "fig: File:delete.png")
