<!-- de: Genealogie -->

## ${project.name} Genealogy
---------------------------

The ${project.name} [ Genealogy](${project.home}persons "wikilink") is a
dissertation-based genealogy of research at German universities.  
It is based on the dissertation catalogue of the [Deutsche
Nationalbibliothek (DNB)](http://www.dnb.de/EN/Home/home_node.html). Users enter linkages between existing
dissertations and provide information about missing dissertations, so
that the reconstruction of an interdisciplinary genealogy of research in
Germany can be realised.  
Researchers of all fields and disciplines can help. Dissertation authors
can link their work to their advisor (their “Doktorvater”) and search
for their roots in academia. Advisors can document the dissertations of
their former students.  

To use the ${project.name} Genealogy, it is enough to have a
${project.name} useraccount (how to create a useraccount is described [
here](CreateNewAccount "wikilink")). Data is exclusively used for
non-commercial science and higher education research.  

As you can see, you can search for an author's scientific work:  

![ <File:1.png>](genealogie/1.png  "fig: File:1.png")  

And then, you can view and add information about his work:  

![ <File:2.png>](genealogie/2.png  "fig: File:2.png")  

![ <File:3.png>](genealogie/3.png  "fig: File:3.png")  

By clicking the arrow icon in the upper right corner you can sort the displayed publications by year, title or author.  

To use the Genealogy and to get more information, follow [ this link](${project.home}persons "wikilink").

-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").


