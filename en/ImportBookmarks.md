<!-- de: LesezeichenImportieren -->

## Import Bookmarks from other services or tools
---------------------------------------------

You can import a batch of bookmarks at once (instead of adding each
bookmark individually). This might be relevant to include bookmarks from
existing collections, e.g. from another bookmarking service or from your
browser bookmarks. In the following, we list services with short
instructions how to import bookmarks from them.

------------

### Browser

You can export your bookmarks in your webbrowser into an HTML file and then import this HTML file into ${project.name}.
For instructions, visit [this page](ImportBookmarks/ImportBookmarksBrowser "wikilink").

-------------

### Delicious

You can import your bookmarks at the [imports](${project.home}import/bookmarks "wikilink") page. In order to do this, follow [these instructions](ImportBookmarks/ImportBookmarksDelicious "wikilink").

-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").


