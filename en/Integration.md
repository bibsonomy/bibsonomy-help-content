<!-- de: Integration -->

## Integration with other Websites and Services
--------------------------------------------

${project.name} integrates well with other websites, content management
systems, and reference managers. Furthermore, you can connect ${project.name} 
to your own software using one of our API (application
programming interface) clients. The following list describes ways to
integrate ${project.name} with various systems and technologies -
ranging from code snippets to complete and powerful, easy-to-install
plugins or API clients.

------------------------------------------------------------------------

### Browser

Bookmarking web pages and publications is very easy with our
${project.name} browser plugins, adding three useful buttons to your
browser's navigation bar. At the moment there are plugins available for
Chrome/Chromium, for Firefox and for Safari. If you do not wish to install a
plugin or if you use another browser, you might like the JavaScript [Bookmarklets](${project.home}buttons#buttons.Bookmarklets "wikilink") with similar functionality.

For help regarding integration with your browser, visit [this page](Integration/IntegrationBrowser "wikilink").

------------------------------------------------------------------------

### Programs

[This helppage](Integration/IntegrationPrograms "wikilink") regards the integration of ${project.name} in programs (Citavi, Emacs, JabRef, KBibTeX, Sublime Text und TeXlipse).

------------------------------------------------------------------------

### Websites

Follow [this page](Integration/IntegrationWebsites "wikilink") for help regarding the integration of ${project.name} in websites (Confluence, GoogleDocs, GoogleScholar und Moodle).

------------------------------------------------------------------------

### Your own website

If you would like to know, how to invorporate ${project.name} content or links to ${project.name} into your own website, follow [this link](Integration/IntegrationOwnWebsite "wikilink").

------------------------------------------------------------------------

### Libraries

#### Digital Libraries

With ${project.name}, you are able to extract and store information
from digital, public online libraries (OPAC) easily and fast. [ Click
here](DigitalLibraries "wikilink") to find out which libraries are linked to ${project.name}.

#### Discovery Service

Many libraries in Germany use *Resource Discovery Services (RDS)* for
inventory management. ${project.name} provides a direct RDS interface.
You can learn how to use this interface [ here](DiscoveryService "wikilink").

#### OpenURL resolver

You can you connect your ${project.name} user profile with your
library's URL resolver. This way all URLs, also in publications, can be
resolved via your library. You can find instructions for this [ here](OpenURL "wikilink").

------------------------------------------------------------------------

### Applications

#### Bookmark Exporter

With the [Python](http://www.python.org/) based [export function](https://bitbucket.org/bibsonomy/bibsonomy-python/src/tip/onefile.py?fileviewer=file-view-default) you can download your ${project.name} bookmarks and publications and store them in a file.
Read more in the [Instructions](https://bitbucket.org/bibsonomy/bibsonomy-python).

#### Link checker

This [link status checker](http://www.christianschenk.org/projects/linkstatuschecker/) checks whether your ${project.name} bookmarks are still downloadable.

#### RSS

[RSS](http://en.wikipedia.org/wiki/RSS) is a family of formats to create
feeds of frequently updated information. Follow our [
tutorial](RSS-FeedsSubscribe "wikilink") to learn how to subscribe to a
particular RSS feed from ${project.name}. You can stay informed about
recent post of particular tags, users, or groups.

------------------------------------------------------------------------

### REST API

${project.name} provides a webservice using Representational State
Transfer (REST), a software architectural style for distributed
hypermedia systems. The REST API is intended for developers who want to
develop applications which interact with ${project.name}. Follow 
[this link](Integration/IntegrationAPI) for help on that matter.

------------------------------------------------------------------------

### Others

#### Android

Gunnar Aastrand Grimnes created the Android application [Bibsonomy
Poster](https://play.google.com/store/apps/details?id=net.gromgull.android.bibsonomyposter) that allows posting with a single click from an Android device. The code
repository can be found [here](https://github.com/gromgull/bibsonomy-poster).

#### OAuth

[OAuth](http://oauth.net/) is an established protocol for secure API
authorization which allows users to grant third party applications
access to their data without being obliged to enter credentials outside
of ${project.name}. Learn [ here](OAuth "wikilink") how to establish a
connection between your application and ${project.name} via OAuth.

-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").