<!-- de: DiscoveryService -->

## Discovery Service
-----------------

Many libraries in Germany use *Resource Discovery Services (RDS)* for
inventory management. ${project.name} provides a direct RDS interface
and is supported by mostly all university libraries in Hessen (HeBIS-Verbund).

Here is a list with the libraries that certainly support ${project.name}:

-   [Suchportal der ULB Darmstadt](http://hds.hebis.de/ulbda)
-   [Suchportal Frankfurt am Main](http://hds.hebis.de/ubffm)
-   [JustFind UB Gießen](http://hds.hebis.de/ubgi)
-   [KARLA II - Das Rechercheportal der Universität Kassel](http://hds.hebis.de/ubks)
-   [Rechercheportal der Universitätsbibliothek Mainz](http://hds.hebis.de/ubmz)
-   [Katalog Plus - Das Rechercheportal der Universität Marburg](http://hds.hebis.de/ubmr)
-   [FILIP - Suchportal der Hochschul- und Landesbibliothek Fulda](http://hds.hebis.de/hlbfu/)
-   [Katalog Plus der Hochschul- und Landesbibliothek Rhein-Main](http://hds.hebis.de/hsrm/)
-   [THMfind - Suchportal der Technischen Hochschule Mittelhessen](http://hds.hebis.de/thm/)


You may ask your library to find out if ${project.name} is supported.
The following instructions describe how you can store publications from
the library catalogue directly in your bibliography in ${project.name}.

1.  Use the library's HeBIS interface to find a publication. In this
    example publications about "textmining" are searched.
2.  Click on **"remind"** or **"send to"**. If a menu appears, choose
    ${project.name} (some libraries don't provide a menu but proceed
    directly to the next step).
3.  Fill out the form and click on **"save"**. The entry will be saved
    in ${project.name} under *publications*.


![ <File:1.png>](discoveryservice/1.png  "fig: File:1.png")


-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").
