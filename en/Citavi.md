<!-- de: Citavi -->

## Exchange data with Citavi
-------------------------

You can exchange articles between the reference manager software
[Citavi](http://www.citavi.de/de/index.html) and ${project.name}. To do so, you need a ${project.name} useraccount
as well as the current version of Citavi.


------------------------------------------------------------------------

### Import data from ${project.name} to Citavi

You can import publications from ${project.name} (your own or others)
to your Citavi projects.

-   In the Citavi menu bar (on the left upper side), click on **"Online
    search"** (Ctrl + L).
    
-   Click on **"Add database or catalog"** and enter "BibSonomy"
    as the name. After a few seconds, the result will be displayed
    under "Found". Select the checkbox and click on **"Add"**, then on **"Close"**.
    
    ![ <File:suche.png>](citavi/suche.png  " File:suche.png")
    
    ![ <File:suche2.png>](citavi/suche2.png  " File:suche2.png")

-   In the menu **"Online search"**, choose "BibSonomy" as
    the database. Now you can enter the desired search terms (All
    fields, Author, Title, Year range). To start the search in the
    ${project.name} database, click on **"Search"**.
-   In the result list, select the titles that should be imported to
    your Citavi project and click on **"Add to project"**. In the
    following dialogue, you will be asked if you want to import only the
    selected titles or all results. Confirm your choice with **"OK"**.
    The entries will now be imported to your Citavi project.
    
    ![ <File:ergebnisse.png>](citavi/ergebnisse.png  "fig: File:ergebnisse.png")  

**Advice:** You also get to the menu **"Online search"** by clicking on
**"File"** in the Citavi menu, then on **"Import..."** and finally on **"New online search"**.

------------------------------------------------------------------------

### Export data from Citavi to ${project.name}

It is possible to export entries from your Citavi literature database to
your literature collection in ${project.name}.

**Important:** For this function, you have to activate the
**TeX support** in Citavi. To do so, click on **"Tools"** in the Citavi
menu, then on **"Options..."** and finally, select the checkbox for
**"TeX support"**. Confirm your change by clicking on **"OK"**.

![<File:texunterstuetzung.png>](citavi/texunterstuetzung.png  "fig: File:texunterstuetzung.png")


-   In the Citavi menu, click on **"File"** and then on **"Export"**.
    Choose if you want to export only the selected title or all titles
    from your Citavi project and click on **"Next"**.
    
-   Now you have to choose the export format. Click on **"Add export
    filter"** and enter "BibSonomy" as the name. After a few
    seconds, the result will be displayed under "Found". Select the
    checkbox and click on **"Add"**, then on **"Close"**.
    
    ![ <File:export.png>](citavi/export.png  " File:export.png")

    ![ <File:export2.png>](citavi/export2.png  " File:export2.png")

-   Select **"Bibsonomy (Online)"** as export format and click on **"Next"**.

-   Enter your user name from ${project.name} (not the number of your
    library card) and for the password, enter your **API key**. You can
    find your API key in the ${project.name} user menu under
    **"settings"** in the tab **"settings"**. Also, you can select the
    visibility of the entry (public, private, friends). Click on **"Next"**.
    
-   Finally, you can decide if you want to save the export preset
    or not. By clicking on **"Next"**, the selected titles will be
    exported to ${project.name} and you can view them in your
    publication collection.

-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").


