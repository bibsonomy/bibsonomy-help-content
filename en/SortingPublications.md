<!-- de: PublikationenSortieren -->

## Sorting publications
--------------------

In ${project.name}, there are several pages where bookmarks and
publications are displayed in two columns (e.g. on the [
homepage](Bib: "wikilink") or on [ a user's page](${project.home}user/hotho "wikilink")). On these pages, you are able to sort
the bookmarks and publications.

------------------------------------------------------------------------

### Simple sorting

For simple sorting, just click on the **sorting symbol**, an arrow next to four bars, in the respective
column. Now the options for **sorting** are listed.  

![ <File:menu.png>](sortingpublications/menu.png  "fig: File:menu.png")  
  
- **criterion:** You can sort entries by **date** or by **title**
(alphabetically).

-   **order:** Choose between **ascending** order (oldest date - recent
    date or A - Z, respectively) or **descending** order (recent date -
    oldest date or Z - A, respectively).

------------------------------------------------------------------------

### Advanced sorting

**Note:** Advanced sorting is **only available for publications**.

Advanced sorting is taking place by adding one or more
parameters to the URL (see [ URL syntax](URL-Syntax "wikilink")). The following parameters are available:  
  
-  **sortPage** - How is the list sorted?
	-   **values (can be linked with a | ):**
        -   **author** - the author's name
        -   **editor** - the editor's name
        -   **year** - year of publication
        -   **entrytype** - type of publication
        -   **title** - title
        -   **booktitle** - the book's title (especially in collections)
        -   **journal** - the journal's name
        -   **school** - the university's name

-   **sortPageOrder** - order of sorting
    -   **values:**
        -   **asc** - ascending
        -   **desc** - descending

-   **duplicates**
    -   **values:**
        -   **yes** - duplicates allowed
        -   **no** - no duplicates in result list

  
**Example:**  
**?sortPage=year&sortPageOrder=asc&duplicates=no**  
Sort by year (sortPage=year) ascending (sortPageOrder=asc) and remove
all duplicates (duplicates=no).  
  
-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").