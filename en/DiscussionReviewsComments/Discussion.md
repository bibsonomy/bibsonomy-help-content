<!-- de: KommentarRezensionBewertung/Diskussion -->

---------------------------

[`Back to the overview on discussion, review and comments.`](DiscussionReviewsComments "wikilink")

---------------------

## Discussions in ${project.name}
------------------------------------

Every user who is logged in can discuss **every resource** (bookmark or
publication), even if the resource isn't in their collection. For every
resource, there can be several entries in the system (for example
entries from different users). The discussions always belongs to the
resource itself, not to any of the entries. Because of this, the links
to the discussion of an entry always direct to the same discussion page,
the discussion page of the resource.  

**Entries with limited visibility**: Private entries or entries with
limited visibility (for groups or friends) can be discussed, too. But in
this case, a public copy of the entry is created when the first
comment/review is written. This copy neither belongs to the owner of the
original entry nor to the user that starts the discussion. It simply
belongs to the system. Before a copy of an entry with limited visibility
is created, a warning will appear before a user starts the discussion.
Private notes won't be copied.  

**Important:** There's a difference between reviews (with
rating) and comments (without rating). Every user can write only **one
review** for an article, but **any number of comments**. Reviews and
comments can be edited or deleted by the user at anytime.
