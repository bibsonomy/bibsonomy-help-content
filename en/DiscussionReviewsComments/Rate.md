<!-- de: KommentarRezensionBewertung/Bewerten -->

---------------------------

[`Back to the overview on discussion, review and comments.`](DiscussionReviewsComments "wikilink")

---------------------

## Rate publications/bookmarks
-----------------------------------------


**Goal:** You learn how to rate an existing entry (bookmark/publication).  

${project.theme == "puma"}{

**Prerequisite:** You have to be registered for PUMA ([How to unlock
library card for PUMA](How-toUnlockLibraryCardFor_PUMA "wikilink")).

}

${project.theme == "bibsonomy"}{

**Prerequisite:** You have to have a BibSonomy user account ([How to
create a new user account](CreateNewAccount "wikilink")) and you have to
be logged in ([Sign in with BibSonomy user account](SignInWithUseraccount "wikilink")).

}

1.  Click on the stars that are located below an entry
    (bookmark/publication).  
    
    ![ <File:post1.png>](Rate/post1.png  "fig: File:post1.png")
    
2.  In the detailed view, you can see the details of a
    bookmark/publication as well as **comments and reviews** further below.  
    
    In the following, you get short explanations of the sections A, B
    and C:

    -   **A (rating distribution):** These two diagrams show all
        given ratings. The left diagram shows the rating distribution.  
        In the right diagramm the user ratings are shown once more in
        form of stars (0 stars = very bad, 5 stars = very good).
        
    -   **B (create comment):** Here you can create your own
        comment/your own review for this article.
    -   **C (earlier comments):** In this section, you can see all
        existing comments/reviews for this article that are visible
        for you. Furthermore, you can see who wrote the comment/review
        and when. Below each entry, you have the possibility to answer
        to it (**the black arrow**). With this feature, you can react to
        existing reviews/comments.  
        
        ![<File:overview_neu1.png>](Rate/overview_neu1.png  "fig: File:overview_neu1.png")  

3.  To write a review or comment, fill in the following fields in
    section **B (create comment)**. See picture below.
    
    1.  **review or comment:** Choose if you want to write a review or
        a comment. When writing a review, you can rate an entry (on a
        scale of 0 to 5). The more stars you give, the better you rate
        the entry.
        
    2.  **write your review:** Here you can enter the text of
        your review.
        
    3.  **anonymous:** If you check this checkbox, your review will be
        visible publicly, but your user name won't be published with it.
        
    4.  **visibility:** Set who can see your review.  

	    -   *public*: Every user can see the review.  
    	-   *private*: Only you can see the review. This option is suitable
            for your own notes.  
    	-   *friends*: The review will be visible only for your friends.  
    	-   *groups*: Choose the group that should be able to see your
            review.  

4.  Finally, click on **Review** to save and publish your review.

     ![ <File:review2.png>](Rate/review2.png  "fig: File:review2.png")  

**Advice:** The average rating of an entry is calculated
by using **all given reviews**, also the private ones. That means, if
you write a private review, the text of the review is only visible for
yourself, but the rating contributes to the average rating calculation.
