<!-- de: DieAblage -->

## The clipboard
-------------

The clipboard allows you to collect publications easily. You can collect
your own and third-party-entries.  
  
![ <File:1.png>](clipboard/1.png  " File:1.png")
  
How to use the clipboard:

1.  Every publication has a toolbar next to it. Click on the symbol with
    the black folder **(“add this publication to your
    clipboard”)**.
    
2.  The publication is now stored in your clipboard. To view the
    clipboard, click on the person icon in the right [ main
    menu](User_Interface "wikilink") and in the following sub-menu,
    click on **“clipboard”**. In addition, you can see a red circle with
    the number of publications in your clipboard next to the
    person icon.

-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").
  

