<!-- de: Moodle -->

## ${project.name} on the E-Learning platform Moodle
--------------------------------------------------

[Moodle](https://moodle.org) is a popular **e-learning environment**. 
PBM is the ${project.name} plugin for **Moodle**. It enables you 
to publish literature lists from ${project.name} within a Moodle course.. 

--------------------------------------------------

### Functions

The PBM plugin enables you to publish lists of literature within a 
moodle course that are fetched from ${project.name}.
The lists are configured through specifying a user name, a user group, and/or a set of tags.

You can choose from different CSL files [here](http://citationstyles.org/) to
change the styles in which the bibliography is displayed on the moodle course page..

--------------------------------------------------

### For Admins

#### Installation

The following steps are necessary to add the PBM Plugin to a running Moodle installation.

  1. Download the plugin from the [BitBucket repository](https://bitbucket.org/bibsonomy/pbm)
     or directly from Moodles [plugin repository](https://moodle.org/plugins/view.php?plugin=mod_pbm).
  2. Unzip the file within your mod folder of the moodle installation. (e.g. /var/www/moodle/mod/pbm)
  3. Go to the Moodle website, log in as an administrator and navigate to "Site administration -> Plugins -> Plugins overview".
  4. Click the button "Check for available updates" at the top of the plugins overview page. 

    (Most of the time Moodle will inform you automatically about database updates before the previous step.)
  5. Moodle should now inform you that database updates have to be made. You have to confirm those. 
  6. You will be asked for a default server address, OAuth consumer credentials and a choice of citation styles. 
     For more details on those topics, see the next step.


#### Configurations

You can find the options for configurations under *Site administration/Plugins/PUMA/BibSonomy*.

**Default server for OAuth**: If you want to provide OAuth authentication, the selected server will be used. 
Choose the URL of ${project.name} (http://www.bibsonomy.org/).

**Consumer-Key/Consumer-Secret(Optional)**: Before your application can access BibSonomy's API, both applications must 
establish a secured communication channel. This is done by initially exchanging credentials, a so called consumer key 
which identifies your application and a corresponding consumer secret which is used for signing and verifying your requests.
If you want to obtain a consumer key and consumer secret for your application, please write an email to ${project.apiemail}
to demand OAuth consumer credentials. 
This is optional as it's also possible to use API Key authentication (BasicAuth).

**Available CSL files**: Here you can choose between several citation styles 
for adding/editing PBM instances within courses.

--------------------------------------------------

### For Users

#### Create a publication list in a moodle course

The following steps explain how the plugin can be used by a teacher/trainer to generate literature lists on a Moodle course page.
**Note**: The teacher/trainer who creates a new PBM instance needs 
valid authentication credentials deposited in their user profile settings.
A user profile can be edited by clicking the user name and choosing "Profile". 
The PBM specific fields can be found under the ${project.name} API Key and OAuth category. 
(For further details, see **User Configuration**)

  1. Turn on the editing mode and click the "Add an activity or resource" link.
  2. A list with activity modules will pop up. Mark PBM and click add.
  3. Now you can add information in order to specify the list of literature.

**Get content from user or group?**: Lists of literature can be fetched from a specific user or a group.

**Insert user or group id**: The identifier for a source: e.g. a user name or group id.

**Tags**: A white space separated list of tags.

**Provide associated documents?**: When checked, documents attached to literature will be displayed and downloadable.

**Select Authentication Method**: Determines which authentication method should be used when the publications being 
fetched from the ${project.name} server. For more details see **User Configuration**.

**Server: API Key | OAuth**: Here you can see the currently chosen server for API Key authentication and OAuth authentication. 
The server for API Key authentication can be selected within the profile settings. 
The OAuth server has to be selected by an admin in the plugin settings.

**Citation style**: The citation style determines how literature will be formatted. 
PBM comes with a couple of [csl files](http://citationstyles.org/). 

**Citation style language**: Some citation styles may differ between certain languages.


#### Add to my ${project.name} Button 

 When a user has provided authentication credentials (See **User Configuration** for further information) they can add 
 publications from a list to his personal list on ${project.name}.
 Just click the button at the lower right of each publication. When a publication already is in the personal list a green tick will be displayed.


#### User Configuration


![ <File:1.png>](moodle/1.png  "fig: File:1.png")  


**API Server**: This server will be used for API Key authentication (BasicAuth).

**API User**: The user id of a ${project.name} account.

**API Key**: The API key can be found at [Settings](https://www.bibsonomy.org/settings) -> "Settings-Tab" in the API area when logged in at a ${project.name} system.

**OAuth Link**: When this field is checked and changes of the profile are saved the user will be redirected to the default OAuth server to allow this user using OAuth.

**Note** that the user has to be logged in at the ${project.name} system **before** linking his/her account. Otherwise the redirection will not work properly and he/she needs to do the linkage again.

----------------

More information on **installation and usage** you can find [here](https://moodle.org/plugins/mod_pbm).

-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").