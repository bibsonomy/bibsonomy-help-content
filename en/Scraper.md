<!-- de: Scraper -->

## Scrapers in ${project.name}
----------------------------

A scraper is a useful function in ${project.name} that helps you saving
publications from external websites to ${project.name}.  

**Usage:** On an external literature catalogue (e.g. [ACM Digital
Library](http://dl.acm.org/)), you find a publication that you want to
copy to your collection in ${project.name}. The easiest way is by using
the ${project.name} [browser extensions](http://www.bibsonomy.org/buttons). Click on the
**${project.name} symbol** in the browser and choose **"Save publication to ${project.name}"**.  

![<File:Browsererweiterung.png>](scraper/Browsererweiterung.png  "fig: File:Browsererweiterung.png")  

You will automatically be redirected to ${project.name} and the scraper
extracts all relevant information about this publication and displays it
in the input fields. You can make changes if you want to and finally
save the publication by clicking on **"save"**.  

![ <File:ScraperBib.png>](scraper/ScraperBib.png  "fig: File:ScraperBib.png")  

An overview on all supported external websites where information can
automatically be extracted by scrapers can be found [here](${project.home}scraperinfo "wikilink").  
You can also use the [scraping service](http://scraper.bibsonomy.org/)
to extract bibliographical metadata from a website by entering a URL.

-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").