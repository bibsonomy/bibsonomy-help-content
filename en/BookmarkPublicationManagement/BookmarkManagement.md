<!-- de: LesezeichenPublikationenManagen/LesezeichenManagen -->

---------------------------

[`Back to management of publications and bookmarks.`](BookmarkPublicationManagement "wikilink")

---------------------

## Manage bookmarks
---------------------------------------------

**Introduction:** Bookmarks make it possible to use the WorldWideWeb
like a book. A bookmark allows you to remember the exact address of an
online document. ${project.name} gives you the ability to store/manage
bookmarks centrally.

------------------------------------------------

### Add bookmarks

${project.theme == "bibsonomy"}{

**Goal:** This tutorial shows you how to add a web page as a bookmark.  

![ <File:bookmark1.png>](BookmarkManagement/bookmark1.png  "fig: File:bookmark1.png")  

1.  Click on the menu item *"add post"* in the ([main menu](User_Interface "wikilink")). A sub-menu appears.
    
2.  Click on **"add bookmark"** with the left mouse button.

![ <File:bookmark_add2.png>](BookmarkManagement/bookmark_add2.png  "fig: File:bookmark_add2.png")

}

${project.theme == "puma"}{

**Goal:** This tutorial shows you how to add a web page as a bookmark.  

![ <File:bookmark1.png>](BookmarkManagement/bookmark1.png  "fig: File:bookmark1.png")  

1.  Click on the menu item *"add post"* in the ([main menu](User_Interface "wikilink")). A sub-menu appears.
    
2.  Click on **"add bookmark"** with the left mouse button.

![ <File:bookmark_add2.png>](BookmarkManagement/bookmark_add2.png  "fig: File:bookmark_add2.png")

}

${project.theme == "wueresearch"}{

**Goal:** This tutorial shows you how to add a web page as a bookmark.  

![ <File:bookmarkwueen.png>](wueresearch/BookmarkPublicationManagement/BookmarkManagement/bookmarkwueen.png  "fig: File:bookmarkwueen.png")  

1.  Click on the menu item *"add post"* in the ([main menu](User_Interface "wikilink")). A sub-menu appears.
    
2.  Click on **"add bookmark"** with the left mouse button.

}

3.  In the field *' URL*', enter the address (URL) of the web page that
    you want to add as a bookmark. Then click on **"Continue".**
    
4.  In the following step, you will be asked to enter some
    additional data. These are:

    -   **URL**: (Will automatically be transferred from the
        previous step)
        
    -   **title:** Enter the title/name of the page.  
        **Tip**: Under the textbox, you can see the web page's original
        title in small blue letters as a suggestion. By clicking on it,
        you can add it as title.
        
    -   **description:** You can enter your own comment or you can
        also store a little abstract.
        
    -   **tags:** tags help you to organize your
        bookmarks/publications in ${project.name} better (for further
        information about tags, [ click here](Tags "wikilink")). You can
        add as many tags as you like. Tags are separated from each other
        by spaces.  
        **Tip 1:** If you want to use a tag consisting of several
        words (such as **computer science department**), then use
        [PascalCase](http://c2.com/cgi/wiki?PascalCase) - such as **ComputerScienceDepartment.**  
        **Tip 2:** ${project.name} makes suggestions for tags based on
        the website's URL and your description (see
        **"recommendation"**). By clicking on the recommended tags, you
        can add them to the bookmark.
    -   **visibility settings:** You can choose to whom your entry will be visible.
        
    -   **group options:** If your post is interesting for one or more groups, you can mark your 			post "relevant" for these groups.
    
5.  Click on **"Save"** to store the bookmark.

6.  The bookmark is now stored. Please note that it may take some time
    (one second to several minutes) until a newly stored bookmark can be
    found by the search function.

------------------------------------------------------------------------

### Tips for searching and archiving

-   ${project.name} only stores the web address of a web document (HTML
    page, video, image, etc.), but not the document itself. It can
    therefore happen that you saved an address of a web document but
    later the document is no longer available (e.g. because the document
    was deleted, the provider/website no longer exists or the document
    has been moved, etc.). We therefore recommend to store important
    documents on your computer (make a backup).
    
-   In addition to this, please remember that a web document can be
    changed at any time. In this case it is also recommended to make
    backups on your computer. Also it is common practice in scientific
    work to ALWAYS add the date of visiting an internet document when it
    is cited (you can enter this information into the field "description").
    
-   ${project.name} supports the [RFC 7089](http://tools.ietf.org/html/rfc7089) specification. This makes
    it possible to show bookmarks in ${project.name} like they were
    stored, even if the page has changed. To use this feature, you have
    to install the Memento plugin in your browser. The plugin is
    available for [Mozilla Firefox](https://addons.mozilla.org/de/firefox/addon/mementofox/) and [Google Chrome](http://bit.ly/memento-for-chrome).

------------------------------------------------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").
