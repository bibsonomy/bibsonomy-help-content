<!-- de: LesezeichenPublikationenManagen/LiteraturlistenErstellen -->

---------------------------

[`Back to management of publications and bookmarks.`](BookmarkPublicationManagement "wikilink")

---------------------

## Create publication lists
----------------

**Goal:** This tutorial shows you how to create publication lists with
your publications or with the publications of another user. You can use
these publication lists on external websites (see also: [URL syntax](URL-Syntax "wikilink")).  
To create publication lists, just enter the following URLs into your browser.

**Note: The lists will show 20 items only. You can display more items by adding `?items=variousnumber` at the end of the URL (replace `variousnumber` with the number of posts you want to be displayed).** 

-   **General list:**  
    `http://www.bibsonomy.org/publ/user/<username>`
    Replace &lt;username> by your own username.  
    **Example:**
    [<http://www.bibsonomy.org/publ/user/hotho>](http://www.bibsonomy.org/publ/user/hotho).  
![<File:Publicationlist.png>](PublicationlistCreate/Publicationlist.png  "fig: File:Publicationlist.png")  
  
- **General list without tags:**  
`http://www.bibsonomy.org/publ/user/<username>?notags=1`  
Replace &lt;username> by your own username.  
**Example:**
[<http://www.bibsonomy.org/publ/user/hotho?notags=1>](http://www.bibsonomy.org/publ/user/hotho?notags=1).  
  
- **General list with tag limitation:**  
`http://www.bibsonomy.org/publ/user/<username>/<tagname>  `
Replace &lt;username> by your own username and &lt;tagname> by the tag that
the publications should be tagged with.  
**Example:**
[<http://www.bibsonomy.org/publ/user/hotho/myown>](http://www.bibsonomy.org/publ/user/hotho/myown).  
  
- **BibTeX list:**  
`http://www.bibsonomy.org/bib/user/<username>`
Replace &lt;username> by your own username. A list of BibTeX entries for
the publications of a user is created.

*Note: The list will show 20 items only. You can display more items by adding `?items=variousnumber` at the end of the URL. Replace `variousnumber` with the number of posts you want to be displayed (see also [URL syntax](http://www.bibsonomy.org/help_en/URL-Syntax#section-URL-Syntax-HTMLFormatting).)*

**Example:**
[<http://www.bibsonomy.org/bib/user/hotho>](http://www.bibsonomy.org/bib/user/hotho).  
  ![ <File:Biblist.png>](PublicationlistCreate/Biblist.png  "fig: File:Biblist.png")  

------------------------------------------------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").
