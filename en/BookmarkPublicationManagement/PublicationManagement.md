<!-- de: LesezeichenPublikationenManagen/PublikationenManagen -->

---------------------------

[`Back to management of publications and bookmarks.`](BookmarkPublicationManagement "wikilink")

---------------------

## Manage publications
---------------------------------------------

**Introduction:** In ${project.name}, 'publications' means
all kinds of documents (articles, monographs, etc.). You can create and
manage your own or third-party publications (for example as the basis
for a scientific research).

------------------------------------------------------------------------

### Add publications

[This tutorial](BookmarkPublicationManagement/PublicationAdd "wikilink") shows you how to **add** a new publication to ${project.name}.

------------------------------------------------------------------------

${project.theme == "wueresearch"}{

### Advanced Search for publications

[This tutorial](BookmarkPublicationManagement/PublicationSearch "wikilink") explains how to use the **advanced search** in ${project.name}. 

------------------------------------------------------------------------

}

### Create publication lists

Follow [these steps](BookmarkPublicationManagement/PublicationlistCreate "wikilink") to create lists of publications, which can later be used on external webpages.

------------------------------------------------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").
