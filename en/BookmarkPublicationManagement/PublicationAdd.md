<!-- de: LesezeichenPublikationenManagen/PublikationenHinzufügen -->

---------------------------

[`Back to management of publications and bookmarks.`](BookmarkPublicationManagement "wikilink")

---------------------

## Add Publications
---------------------------------------------

**Goal:** This tutorial shows you how to add a new publication in
${project.name}, e.g. to create a publication list later.

1.  Move the pointer over the menu item **"add post"** in the
    [Main Menu](User_Interface "wikilink"). A sub-menu appears.
    
2.  Click on **"add publication"** in the menu by pressing the left
    mouse button.
    
3.  There are five different possibilities to add new publications:

    -   **manual:** You can add all entries manually.
	    1.  Enter all required data into the appropriate fields.
    	2.  Click on **"post"**.
    	3.  The next dialog allows you to add additional information. You
            can also skip this step by clicking directly on **"Save"** to
            store the publication.

    -   **BibTeX/EndNote snippet:** This option allows you to import the
        data from almost any literature management program (for
        example Citavi). You can find the [instructions here.](AddPublications/ImportData "wikilink")
        
    -   **upload file:** This option allows you to upload EndNote and
        BibTeX files to ${project.name} to import them automatically.
        You can find the [instructions here.](AddPublications/ImportData "wikilink")
        
    -   **ISBN/DOI - the fastest way:** Allows you to enter publications
        by ISBN, ISSN, or DOI.
	    1.  Fill in the boxes with the ISBN, ISSN or DOI.  
        2.  Click on **"post"**.  
        3.  The data is automatically collected from various catalogs.  
        4.  If you agree with the displayed data, click on **"save"**.  

${project.theme == "bibsonomy"}{

    -   **scan code:** This option allows you to add a publication by
       scanning its ISBN code with your webcam. A detailed description
       how to do this can be found [here](AddPublications/AddScan "wikilink").

![ <File:add_en.jpg>](PublicationAdd/add_en.jpg  "fig: File: add_en.jpg")  

}

${project.theme == "puma"}{

    -   **scan code:** This option allows you to add a publication by
       scanning its ISBN code with your webcam. A detailed description
       how to do this can be found [here](AddPublications/AddScan "wikilink").

![ <File:add_en.jpg>](PublicationAdd/add_en.jpg  "fig: File: add_en.jpg")  

}

${project.theme == "wueresearch"}{

![ <File:addpublwueen.png>](wueresearch/BookmarkPublicationManagement/PublicationAdd/addpublwueen.png  "fig: File: addpublwueen.png")  

}

------------------------------------------------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").
