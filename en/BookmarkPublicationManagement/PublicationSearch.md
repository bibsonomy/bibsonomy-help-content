<!-- de: LesezeichenPublikationenManagen/PublikationenSuchen -->

---------------------------

[`Back to management of publications and bookmarks.`](BookmarkPublicationManagement "wikilink")

---------------------

## Advanced Search for publications
---------------------------------------------

**Goal:** This tutorial explains how to use the **advanced search** to find publications in ${project.name}.

1. Select the **"publications"** tab in the main menu above (see [User Interface](User_Interface "wikilink")). A search bar and a list of recent releases will appear.
2. To use the advanced search, press the blue lettered **"Advanced Search"**. This is accompanied by blue arrows pointing down.
3. This opens a new search mask that offers you new options.

![ <File:searchmask.png>](PublicationSearch/searchmask.png  "fig: File:searchmask.png")  

Here, you can use the following options to search for publications in ${project.name}.  
    - **"Field"**: By clicking on the button, you can select the desired search category (e.g. title, author, year, ...).  
    - **"Entrytypes"**: By clicking on the button, you can filter, of which entry type the search results should be (e.g. article, review, monograph, ...).  
    - **"Year"**: Here, you can filter the search results by years. If you would like to filter by a period, then click on **"Select year range"** on the right. You can now enter two years to describe the desired span.  
    - **"AND/OR"**: You can decide whether all search parameters should be fulfilled in the result (AND) or at least one (OR).  

Finally, you can confirm your advanced search by pressing the blue button **"Add"**. This will append the specified search parameters to you search term.

Would you like more information about the search options, which you can select automatically thanks to the advanced search? Visit [this help page](SearchPageHelp "wikilink").

------------------------------------------------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").
