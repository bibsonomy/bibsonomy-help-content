<!-- de: LesezeichenPublikationenManagen/PublikationenLesezeichen -->

---------------------------

[`Back to management of publications and bookmarks.`](BookmarkPublicationManagement "wikilink")

---------------------

## Publications and Bookmarks
------------------------------------

This section deals with functions in ${project.name} which you can apply
to bookmarks as well as to publications.

------------------------------------------------------------------------

### View your own bookmarks and publications


**Goal:** This tutorial shows you how to view your bookmarks and
publications that were entered by you.  

${project.theme == "bibsonomy"}{

1.  If not already done, sign in to ${project.name} with your
    user account. [This guide](SignInWithUseraccount "wikilink") shows you how to sign in.
    
2.  Move the mouse pointer to the [main
    menu](User_Interface "wikilink") item **"myBibSonomy"** - a sub-menu appears.
    
3.  Click on the subitem **"my posts"**.

4.  ${project.name} now shows you all of your saved bookmarks (on
    the left) and publications (on the right).
    **Tip:** Depending on how many bookmarks and publications you
       have stored, it may be possible that ${project.name} cannot
       display all of them on one website. In this case,
       ${project.name} distributes your publications/bookmarks on
       several pages. Therefore, please scroll to the end of the
       website, there you will find the option to navigate to the next
       pages.  
          
       **Example:** ![<File:pageing.PNG>](PublicationBookmark/pageing.PNG  "fig: File:pageing.PNG")  

5.  To see detailed information about a publication, just click on the
    publication's title. You will be redirected to the detailed view.

------------------------------------------------------------------------

### Search for all available bookmarks and publications in ${project.name}

**Goal:** This tutorial shows you how to search for your own or other
bookmarks and publications in ${project.name}. 

1.  Above the blue bar on the right hand side you can find the searchbar.

2.  Press the left mouse button on the blue arrow next to *"search"*.

3.  A menu appears, select the record that you want to search. For
    example, select *"author"* to search for a specific author.  
    
    ![ <File:search.png>](PublicationBookmark/search.png  "fig: File:search.png")  

4. Then, enter your search term into the white textbox. Finally, click on the magnifying glass symbol or press the "enter" key on your keyboard. In a few seconds, ${project.name} shows you all available results.

**Tip:** To get the best search results, read the [ overview on the
different search options](SearchPageHelp "wikilink") that are available in ${project.name}.

------------------------------------------------------------------------

### Access to OpenAccess Publication Services

**Goal:** You learn how to search for a digital edition of a publication
in an OpenAccess database with the help of ${project.name}. 

**Prerequisite:** To search OpenAccess databases, you have to be in the
detailed view of a publication. To achieve that, just click on the
publication's title. You will be redirected to the detailed view.  

![ <File:fulltext.png>](PublicationBookmark/fulltext.png  "fig: File:fulltext.png")  

1.  Click on the drop down menu next to **"search on"**. A sub-menu
    appears.

2.  Choose the OpenAccess database in which you want to search for
    the publication.

}

${project.theme == "puma"}{

1.  If not already done, sign in to ${project.name} with your
    user account. [This guide](SignInWithUseraccount "wikilink") shows you how to sign in.
    
2.  Move the mouse pointer to the [main
    menu](User_Interface "wikilink") item **"myBibSonomy"** - a sub-menu appears.
    
3.  Click on the subitem **"my posts"**.

4.  ${project.name} now shows you all of your saved bookmarks (on
    the left) and publications (on the right).
    **Tip:** Depending on how many bookmarks and publications you
       have stored, it may be possible that ${project.name} cannot
       display all of them on one website. In this case,
       ${project.name} distributes your publications/bookmarks on
       several pages. Therefore, please scroll to the end of the
       website, there you will find the option to navigate to the next
       pages.  
          
       **Example:** ![<File:pageing.PNG>](PublicationBookmark/pageing.PNG  "fig: File:pageing.PNG")  

5.  To see detailed information about a publication, just click on the
    publication's title. You will be redirected to the detailed view.

------------------------------------------------------------------------

### Search for all available bookmarks and publications in ${project.name}

**Goal:** This tutorial shows you how to search for your own or other
bookmarks and publications in ${project.name}. 

1.  Above the blue bar on the right hand side you can find the searchbar.

2.  Press the left mouse button on the blue arrow next to *"search"*.

3.  A menu appears, select the record that you want to search. For
    example, select *"author"* to search for a specific author.  
    
    ![ <File:search.png>](PublicationBookmark/search.png  "fig: File:search.png")  

4. Then, enter your search term into the white textbox. Finally, click on the magnifying glass symbol or press the "enter" key on your keyboard. In a few seconds, ${project.name} shows you all available results.

**Tip:** To get the best search results, read the [ overview on the
different search options](SearchPageHelp "wikilink") that are available in ${project.name}.

------------------------------------------------------------------------

### Access to OpenAccess Publication Services

**Goal:** You learn how to search for a digital edition of a publication
in an OpenAccess database with the help of ${project.name}. 

**Prerequisite:** To search OpenAccess databases, you have to be in the
detailed view of a publication. To achieve that, just click on the
publication's title. You will be redirected to the detailed view.  

![ <File:fulltext.png>](PublicationBookmark/fulltext.png  "fig: File:fulltext.png")  

1.  Click on the drop down menu next to **"search on"**. A sub-menu
    appears.

2.  Choose the OpenAccess database in which you want to search for
    the publication.

}

${project.theme == "wueresearch"}{

1.  If not already done, sign in to ${project.name} with your
    user account. [This guide](SignInWithUseraccount "wikilink") shows you how to sign in.
    
2.  Move the mouse pointer to the [main
    menu](User_Interface "wikilink") item **"myWueResearch"** - a sub-menu appears.
    
3.  Click on the subitem **"my posts"**.

4.  ${project.name} now shows you all of your saved bookmarks (on
    the left) and publications (on the right).
    **Tip:** Depending on how many bookmarks and publications you
       have stored, it may be possible that ${project.name} cannot
       display all of them on one website. In this case,
       ${project.name} distributes your publications/bookmarks on
       several pages. Therefore, please scroll to the end of the
       website, there you will find the option to navigate to the next
       pages.  
          
       **Example:** ![<File:pageing.PNG>](PublicationBookmark/pageing.PNG  "fig: File:pageing.PNG")  

5.  To see detailed information about a publication, just click on the
    publication's title. You will be redirected to the detailed view.

------------------------------------------------------------------------

### Search for all available bookmarks and publications in ${project.name}

**Goal:** This tutorial shows you how to search for your own or other
bookmarks and publications in ${project.name}. 

1.  Above the blue bar on the right hand side you can find the searchbar.

2.  Press the left mouse button on the blue arrow next to *"search"*.

3.  A menu appears, select the record that you want to search. For
    example, select *"author"* to search for a specific author.  
    
    ![ <File:searchwueen.png>](wueresearch/BookmarkPublicationManagement/PublicationBookmark/searchwueen.png  "fig: File:searchwueen.png")  

4. Then, enter your search term into the white textbox. Finally, click on the magnifying glass symbol or press the "enter" key on your keyboard. In a few seconds, ${project.name} shows you all available results.

**Tip:** To get the best search results, read the [ overview on the
different search options](SearchPageHelp "wikilink") that are available in ${project.name}.

------------------------------------------------------------------------

### Access to OpenAccess Publication Services

**Goal:** You learn how to search for a digital edition of a publication
in an OpenAccess database with the help of ${project.name}. 

**Prerequisite:** To search OpenAccess databases, you have to be in the
detailed view of a publication. To achieve that, just click on the
publication's title. You will be redirected to the **detailed view**.  

![ <File:fulltextwueen.png>](wueresearch/BookmarkPublicationManagement/PublicationBookmark/fulltextwueen.png  "fig: File:fulltextwueen.png")  

To access an OpenAcess Publication service, just click on one of the OpenAccess databases on the right.

}

-----------------------------------

[ Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").
