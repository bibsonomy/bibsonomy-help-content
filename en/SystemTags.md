<!-- de: SystemTags -->

## System tags
-----------

System tags are special [tags](Tags "wikilink") that come with a predefined meaning. At
present, ${project.name} offers three types of system tags:

------------------

### Executable system tags

Executable system tags are added to a post to execute some special
action with that post.

-   `for:<groupname>`: Tagging a post with `for:xygroup` will copy that
    post to the group's collection with the tag replaced by
    `from:YourUserName`. Should you change or even delete your post, the
    copied post of the group will remain unaltered. You must be a member
    of the group you forward the post to.
    
-   `send:<username>`: This tag allows sending a post to the inbox of
    another ${project.name} user. To use this tag, the receiver must
    have added you to his [ friends list](AddFriends "wikilink") or you must be a member of the same [ group](GroupFunctions "wikilink").

---------------

### Markup system tags

Markup system tags are used to label your posts. Currently
${project.name} supports the following markup system tags:

-   `myown`: A post tagged with `myown` will appear on your CV page.

-   `sys:relevantFor:<groupname>`: A post with the tag
    `sys:relevantFor:xygroup` will appear on the "relevantFor"-page of
    the `xygroup`. Thus, this tag has the same effect as selecting
    `xygroup` in the "relevant for"-box while editing the post.
-   `sys:hidden:<tag>`: Use this tag with `sys:hidden:xyz` to "hide" your post. Then it is invisible to every user besides yourself. 

-------------------

### Search system tags

Search system tags are not for tagging a post but rather to be used as a
filter in search queries. They all have the same syntax:
`sys:<fieldname><fieldvalue>`. For example, searching with
`sys:author:xyz` will only return posts where the author is `xyz`.
${project.name} supports the following filters:

-   `sys:author:<authorname>` filters the search range by author.

-   `sys:bibtexkey:<bibtexkey>` filters the search range by bibtexkey.

-   `sys:entrytype:<entrytype>` filters the search range by [ entry type](Entrytypes "wikilink").
-   `sys:group:<group>` filters the search range by group.

-   `sys:not:<tag>` excludes entries with the tag &lt;tag>. Here you can
    also use wildcards, e.g. `sys:not:news_*` excludes all posts with
    tags that start with `news_`.
    
-   `sys:title:<title>` filters the search range by title.

-   `sys:user:<user>` filters the search range by user.

-   `sys:year:<year>` filters the search by year. Several arguments are
    possible, e.g.:
    -   `2000`: all posts from the year 2000
    -   `2000-`: all posts from the year 2000 or from one of the following
        years
    -   `-2000`: all posts from the year 2000 or from an earlier year
    -   `1990-2000`: all posts from the years 1990 to 2000

-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").