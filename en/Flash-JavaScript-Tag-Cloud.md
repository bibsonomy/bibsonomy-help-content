<!-- de: Flash-JavaScript-Tag-Cloud -->

## Flash and JavaScript Tagcloud
-----------------------------

With the following Snippets and two little files, you can cheer up your
website with a nice little tag cloud widget. I hope you enjoy!

*Björn-Elmar Macek*

--------------------------------

### Files

##### Optional (no-flash.jpg)

Download the file from the attachment section and put it into the folder
`$web-app-root/resources/image`.

##### swfobject.js und tagcloud.swf
Get both files here (<http://www.schoenwandt.info/download/wpcumulus.zip>) and put them into the
`$web-app-root/resources/javascript` or
`$web-app-root/resources/flash` folder respectively.

Put the files into the Javascript section of the webpage where you want to display your Tagcloud.

    <SCRIPT>
    ...
    if(flashTest())
    	calcFlashTags();
    else
    	calcTextTags();
    ...
    </SCRIPT>

Put the following code into the HTML code of the webpage where you want
to display your Tagcloud.

    <!-- COMPLETE TAG CLOUD IN A DIV -->
    <div>
    <a id="changeCloud" style="float:right" href="javascript:calcTextTags()" title="Flashfree Version"> <img style="height:35px" src="resources/image/no-flash.jpg"/></a>
        <div style="font-weight:bold;  margin-left:10%" id="tagCloudFrame">
    	<h2 style="color:#006699; ">bibSonomy tagcloud</h2>
          <p id="textcloud"><!-- --></p>
     		 <div  id="tagCloud"><!--  --></div></div>
          <p style="font-size:8pt; margin-left:2em">Get the same BibSonomy Tagcloud for your page. Have a look <a href="http://www.bibsonomy.org/help_en/Flash-JavaScript-Tag-Cloud">here./a></p>
    </div>


Import the Javascript file [ tagcloud.js](TagCloud.js "wikilink") into
the HTML code of the webpage where you want to display your Tagcloud.


    function renderTagCloud(items, name, id)
    {
        var tags = new Array();
     	         var nrOfItems = items.length;
       	         var taglist = "";
             	   items.sort(function(a,b) { return parseFloat(b.count) - parseFloat(a.count); } );
                	var color=new Array("0x339966", "0xFFD042", "0x809FFF","0xDF80FF");
           	     var size=new Array("9", "14", "18");
           	     for(var i=0; i < 36; i++)
           	     {
                      	  var tag = items[i].label;
                      	  var aTagLink="<a href='http://www.bibsonomy.org/user/"+name+"/"+tag +"' hicolor='0x006699' color='"+color[3-(Math.floor(i/9))]+"' style='font-size:"+size[2-(Math.floor(i/12))]+"pt;'>"+tag+"</a>";
                    	    taglist=taglist+aTagLink;
              	  }
    
                var so = new SWFObject("resources/flash/tagcloud.swf", "tagcloud", "250", "250", "7", "#7FFF00");
                // uncomment next line to enable transparency
                so.addParam("wmode", "transparent");
                so.addVariable("tcolor", "0=C3=97333333");
                so.addVariable("mode", "tags");
                so.addVariable("distr", "true");
                so.addVariable("tspeed", "100");
                so.addVariable("tagcloud", "<tags>"+taglist+"</tags>");
                so.write(id);
        }
    
    function renderFlashlessTagCloud(items, name, id)
    {
        var tags = new Array();
                var nrOfItems = items.length;
                var taglist = "";
                items.sort(function(a,b) { return parseFloat(b.count) - parseFloat(a.count); } );
                var size=new Array("9", "12", "14");
                var color=new Array("#339966", "#FFD042", "#809FFF","#DF80FF");
                for(var i=0; i < 36; i++)
                {
                        var tag = items[i].label;
                        var aTagLink="<a href='http://www.bibsonomy.org/user/"+name+"/"+tag +"' style='text-decoration : none; font-size:"+size[2-(Math.floor(i/12))]+"pt; color:"+color[3-(Math.floor(i/9))]+"'>"+tag+"</a>";
                        items[i]=aTagLink;
                        //taglist=taglist+" "+aTagLink;
                }
                items.sort();
                for(var i=0; i < 36; i++)
                {
                        taglist=taglist+" "+items[i];
                }
                $("#changeCloud").empty();
                $("#tagCloud").empty();
                $(taglist).appendTo("#"+id);
    }


	function calcFlashTags()
	    {
	            var name = $("#account_data_username").attr("value");
	            $.ajax( {
	                type : "GET",
	                    url : "http://www.bibsonomy.org/json/tags/user/"+name,
	                    dataType : "jsonp",
	                    success : function(json) {
	                            renderTagCloud(json.items, name, "tagCloud");
	                    },
	                    error : function(req, status, e) {
	                                      window.alert("error accessing bibsonomy api");
	                    }
	              });
	    }
	
	function calcTextTags()
	{
	    var name = $("#account_data_username").attr("value");
	    $.ajax( {
	        type : "GET",
	            url : "http://www.bibsonomy.org/json/tags/user/"+name,
	            dataType : "jsonp",
	            success : function(json) {
	                    renderFlashlessTagCloud(json.items, name, "textcloud");
	            },
	            error : function(req, status, e) {
	                              window.alert("error accessing bibsonomy api");
	            }
	      });
	}
	
	function flashTest()
	{
	    flash = false;
	    if ((navigator.plugins) && (navigator.plugins.length > 0))
	    {
	       for (a = 0; a < navigator.plugins.length; a++)=
	          if ((navigator.plugins[a].name.indexOf('Flash') != -1) &&
	              (parseInt(navigator.plugins[a].name) >= 4))=
	          {
	             flash = true;
	             break;
	          }
	    }
	    // alternativ, testet nur auf beliebige Version (>2?):
	    if ((navigator.mimeTypes) && (navigator.mimeTypes.length > 0))
	       for (a = 0; a < navigator.mimeTypes.length; a++)
	          if (navigator.mimeTypes[a].type.indexOf("application/x-shockwave-flash")>=0)
	             flash = true;
	    return flash;
	}

  
-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").




