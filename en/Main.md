<!-- de: Main -->

## Welcome to the help pages of ${project.name}!
--------------------------------------------

![ <File:header.png>](main/header.png  " File:header.png")

${project.name} supports you purposefully and easily at your daily
business with publications, lists of publications, and bookmarks.

![ <File:Student-Read-04.png>](main/Student-Read-04.png  " File:Student-Read-04.png") | ![ <File:Student-Laptop.png>](main/Student-Laptop.png  " File:Student-Laptop.png") | ![ <File:Binary-Code.png>](main/Binary-Code.png  " File:Binary-Code.png")
--- | --- | ---
**Beginners** | **Advanced Users** | **Software developers**
Here you will find answers to the basic questions that every beginner has. For example: How do I sign up? Where do I find a publication? How can I manage bookmarks and publications? | You want to get more out of ${project.name}? Find out how to use ${project.name} as a social bookmark system or how to search for publications with more precision (advanced search). | You are a software developer and need information to integrate ${project.name} into your software or into your website? ${project.name} offers you interfaces and additional software to do so.
[Go to the help page for beginners.](MainBeginner "wikilink") | [Go to the help page for advanced users.](MainAdvanced "wikilink") | [Go to the help page for software developers.](MainDeveloper "wikilink")
