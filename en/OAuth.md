<!-- de: OAuth -->

## ${project.name} via OAuth
--------------------------

**Note:** You can download example code for the access to
${project.name} via OAuth [here](https://bitbucket.org/bibsonomy/bibsonomy-help-content/src/b8d20eb1bde387f1a8772d91943452f64d5edda2/code-samples/oauth-rest-demo/src/main/java/org/bibsonomy/OAuthRestApiDemo.java?at=default&fileviewer=file-view-default).

${project.name}'s [API](https://bitbucket.org/bibsonomy/bibsonomy/wiki/documentation/api/REST%20API) allows you to access all of your posts systematically.
The API's help page describes how you can authorize your requests using your API key
and secret. If you want to access ${project.name} within your
application in behalf of a user, this approach is not feasible, as users
would have to store their API key and secret within your application.

[OAuth](https://oauth.net/) is an established protocol for secure API
authorization which allows users to grant third party applications
access to their data without being obliged to enter credentials outside
of ${project.name}.

On this page you will learn how to acess ${project.name} from your application using OAuth
and making [OAuth requests](OAuthRequests "wikilink") to ${project.name}'s API.

------------------------------------------------------------------------

### How to access ${project.name} from your application using OAuth

#### 1) Request an OAuth Consumer Key and Consumer Secret

Before your application can access ${project.name}'s API, both
applications must establish a secured communication channel. This is
done by initially exchanging credentials, a so called *consumer key*
which identifies your application and a corresponding *consumer secret*
which is used for signing and verifying your requests. Both symmetric
([HMAC](https://en.wikipedia.org/wiki/HMAC)) and public key ([RSA](https://en.wikipedia.org/wiki/RSA_%28algorithm%29)) encryption is supported.

If you want to obtain a consumer key and consumer secret for your
application, please write an email to ${project.apiemail}, with the following information:

- Name of your application
- Short description of your application
- The type of your consumer secret (HMAC or RSA)
- The callback url of your service (if applicable)
  
  

**Note: To ensure a safe communication via e-mail, the use of a encryption standard (for example [PGP](https://en.wikipedia.org/wiki/Pretty_Good_Privacy))
is highly recommended. It protects the data from unauthorized access and helps to verify
the identity of the sender.** 

------------------------------------------------------------------------

#### 2) Implement OAuth's authorization process

If a user grants your application access to his data in
${project.name}, the user is redirected back and forth between your
application and ${project.name} for eventually passing a so called
*access token* to your application which can then be used to authorize
your requests to the API. This process is explained in detail in the
[OAuth user guide](https://hueniverse.com/protocol-workflow-266b6a975cbf).

Essentially, your application needs to redirect the user to
${project.name}'s OAuth authorization page with previously obtained
temporary credentials given as request parameters (e.g.
[${project.home}oauth/authorize?oauth_token=xxxxxxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx](${project.home}oauth/authorize?oauth_token=xxxxxxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx])):

![ <File:access.png>](oauth/access.png  "fig: File:access.png")  

If the user authorizes your temporary credentials, they will either be
redirected to your site (if you provided a callback URL) or the user has
to manually switch to your application. These authorized credentials can
then be used to obtain the access token which authorizes requests.  

![ <File:success.png>](oauth/success.png  "fig: File:success.png")  

${project.name}'s OAuth Rest-API client for Java facilitates this
process. If you use [maven](https://maven.apache.org/), just add the
following code to your *pom.xml*:

${project.theme == "puma"}{
(Note: PUMA is based on the BibSonomy system, so libraries for BibSonomy can be used for PUMA as well.)
}

    <project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
    	<modelVersion>4.0.0</modelVersion>
    
    	<groupId>org.bibsonomy</groupId>
    	<artifactId>oauth-rest-demo</artifactId>
    	<version>1.0-SNAPSHOT</version>
    
    	<repositories>
    		<repository>
    			<id>bibsonomy-repo</id>
    			<name>Releases von BibSonomy-Modulen</name>
    			<url>http://dev.bibsonomy.org/maven2/</url>
    		</repository>
    	</repositories>
    
    	<build>
    		<plugins>
    			<plugin>
    				<groupId>org.apache.maven.plugins</groupId>
    				<artifactId>maven-compiler-plugin</artifactId>
    				<version>2.0</version>
    				<configuration>
    					<source>1.7</source>
    					<target>1.7</target>
    					<compilerArguments>
    						<encoding>UTF-8</encoding>
    					</compilerArguments>
    				</configuration>
    			</plugin>
    		</plugins>
    	</build>
    
    	<dependencies>
    		<dependency>
    			<groupId>org.bibsonomy</groupId>
    			<artifactId>bibsonomy-rest-client-oauth</artifactId>
    			<version>3.5.0</version>
    		</dependency>
    	</dependencies>
    
    </project>



Alternatively, you can download the jar files [directly](https://dev.bibsonomy.org/maven2/).

Obtaining a temporarily credential is as easy as:

    BibSonomyOAuthAccesssor accessor = new BibSonomyOAuthAccesssor("<YOUR CONSUMER KEY>", "<YOUR CONSUMER SECRET>", "<YOUR CALLBACK URL>");
    String redirectURL = accessor.getAuthorizationUrl();

You now have to redirect the user to `redirectURL`. Afterwards, the
previously obtained temporarily credential is transformed to an access
token: 

    accessor.obtainAccessToken();

------------------------------------------------------------------------

Now that your authorization request is completed, you can make OAuth requests to ${project.name}'s API.
Learn more about this [here](OAuthRequests "wikilink").

-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").
