<!-- de: PasswortVergessen -->

## How to recover your password
----------------------------

${project.theme == "puma"}{

With PUMA, your username and password are identical with the data that
you got with the registration at [university library Kassel (Universitätsbibliothek Kassel)](http://www.bibliothek.uni-kassel.de).
If you forgot this data, please contact the [Leser- und Mahnverwaltung](http://www.ub.uni-kassel.de/lmv.html).

}

${project.theme == "bibsonomy"}{

Go to the [ password reminder page](${project.home}reminder "wikilink") and enter
your username and your e-mail address that you provided at registration.
Now you need to be verified via "Captcha" (just check the empty checkbox in the "Captcha" section). 
We will send you an **e-mail** with a temporary password which you can use to sign in. After
that, you have to change your password again.  

![ <File:1.png>](passwordrecovery/1.png  "fig: File:1.png")  
}

------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").


