<!-- de: LesezeichenExportieren -->

## Export Bookmarks
-------------------------------------

With ${project.name} you are able to export your lists of bookmarks in different formats
in order to use them with other programs. Instead of exporting each bookmark individually, 
you can also export a batch of bookmarks at once.

To export your bookmarks, go to your **user page** to view the bookmarks you have already added to your collection.
([Click here](BookmarkPublicationManagement "wikilink") to learn how to add bookmarks.)
Click on the **arrow symbol** on the very right next to the column with the bookmarks. 
Now you can choose your preferred export format for exporting your bookmarks.

${project.theme == "bibsonomy"}{

![ <File:1.png>](exportbookmarks/1.png  "fig: File:1.png")

}

${project.theme == "puma"}{

![ <File:1.png>](exportbookmarks/1.png  "fig: File:1.png")

}

${project.theme == "wueresearch"}{

![ <File:exportbmswueen.png>](wueresearch/ExportBookmarks/exportbmswueen.png  "fig: File:exportbmswueen.png")

}

After you clicked on an export format, your bookmarks will be displayed in the corresponding format.
You can now import the data into your requested program.

Read more about how to open your stored bookmark list with commonly used software [here](ExportDataSpecific "wikilink").

---------------------------------------

#### RSS

In ${project.name}, you can use RSS feeds to subscribe to your own/other bookmarks and publication lists.
You will be informed about all news/updates immediatly. Learn more about RSS feeds [here](RSS-FeedsSubscribe "wikilink").


#### BibTeX

The internationally standardized data format that is supported by all major bibliographic management programs is called BibTeX.
After you chose this format, press the right mouse button and select the option **"save as"** to store your bookmark list.


#### XML

After you choose the format *"XML"*, press the right mouse button and select the option **"save as"** to store your bookmark list in XML format.


#### Bookpubl

In the *bookpubl* format,the titles of the bookmarks in your list as well as the corresponding links to the bookmarked websites
will be displayed.
To display your bookmark list in the bookpubl format, add *"bookpubl/"* to the URL right after *"bibsonomy/org/"*, like in this example:

`https://www.bibsonomy.org/bookpubl/`

`https://www.bibsonomy.org/bookpubl/user/yourusername` 

To store the list, press the right mouse button and select the option **"save as"**. 

-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").

