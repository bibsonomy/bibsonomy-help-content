<!-- de: Wordpress -->

## WordPress Plugin
--------------------------

The `bibsonomy_csl` WordPress plugin allows you to create publication
lists based on the [Citation Style Language (CSL)](http://citationstyles.org).

----------

### Features

-   [Insert publication lists] (integration/wordpress/AddPublicationLists "wikilink") by using the meta box integration in
    articles
-   Alternatively you can use WordPress shortcodes
-   Select your publications from different content types (e.g.
    user/group/viewable)
-   Filter the results with tags or fulltext search
-   Select your favourite citation style from a list of pre-installed
    styles
-   Alternatively use your own citation style by inserting the url
-   Manipulate the layout of your list with CSS
-   Save your API settings (e.g. API user, API key) in an separate admin
    options page
-   [Add Tag Clouds](integration/wordpress/Tagclouds "wikilink") from BibSonomy to your blog as WordPress Widget and
    choose from three different layouts

For further information on how to use the features, look [here](integration/wordpress/AddPublicationLists "wikilink").

-------------

### Download

You can find the WordPress Plugin at the WordPress Plugin Repository:
[BibSonomy CSL WordPress Plugin](http://wordpress.org/extend/plugins/bibsonomy-csl/).

You can find further instructions on the installation [here](integration/wordpress/Install "wikilink").  

-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").