<!-- de: LesezeichenPublikationenManagen -->

## Manage literature, publications and bookmarks
---------------------------------------------

${project.name} offers you a variety of ways to manage your
publications and bookmarks and also to perform literature search. On
this page you will find everything you need to use ${project.name} as a
standard literature database (e.g. Citavi, EndNote, etc.).

------------------------------------------------------------------------

### Bookmarks - The Basics

**Introduction:** Bookmarks make it possible to use the WorldWideWeb
like a book. A bookmark allows you to remember the exact address of an
online document. ${project.name} gives you the ability to store/manage
bookmarks centrally. Follow [this instruction](BookmarkPublicationManagement/BookmarkManagement "wikilink")
to access further information.

------------------------------------------------------------------------

### Publications - The Basics

**Introduction:** In ${project.name}, 'publications' means
all kinds of documents (articles, monographs, etc.). You can create and
manage your own or third-party publications (for example as the basis
for a scientific research). [Here](BookmarkPublicationManagement/PublicationManagement "wikilink") 
is more information regarding management of publications with ${project.name}.

------------------------------------------------------------------------

### Publications and Bookmarks

[This section](BookmarkPublicationManagement/PublicationBookmark "wikilink") deals with functions in ${project.name} which you can apply
to bookmarks as well as to publications.

------------------------------------------------------------------------


[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").

  


