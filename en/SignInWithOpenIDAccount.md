<!-- de: MitOpenIDAnmelden -->

## Sign in with OpenID account
---------------------------

**Goal:** This tutorial shows you how to sign in to ${project.name} by
using an existing OpenID account.

**Requirements:** For this tutorial, you need an OpenID account. To
create such an account follow [these instructions](http://openid.net/get-an-openid/).

**Alternative:** If you don't have an OpenID account, we recommend you
to [ create your own user account](CreateNewAccount "wikilink") for ${project.name}.

-------------------

### What is OpenID?

[OpenID](http://openid.net/) is an open, decentralized standard which
allows users to log onto many different services on the web using the
same identity identification (single sign-on). This kind of
authentication is provided by a growing number of websites, including
large ones like AOL, Google, Microsoft, MySpace, Yahoo and many others.

----------------

### Instructions:

1.  Click on the **"sign-in"**-button on the homepage. A new window will pop up.
2.  At the bottom, you can choose your OpenID Provider (e.g. Yahoo).

	![ <File:1.png>](signinwithopenidaccount/1.png  "fig: File:1.png") 

3.  You will be redirected to your OpenID provider. Please follow the
    instructions on the screen (these steps will vary depending on your
    provider).  
    **Advice:** Your OpenID provider will ask you if you want to grand
    ${project.home} access to your data - you must confirm
    this question to continue.

----------------
  
[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").
