<!-- de: Konzepte -->

## Concepts
--------

### What are concepts?

With concepts, you can organize your tags in groups. For example, you
can choose the tag *"Europe"* as the supertag (the name of the concept),
and then you can add the tags *"Germany"*, *"France"* and *"Italy"* as
subtags. This way, these tags are grouped together. That means, if you
search with the concept *"Europe"*, you get all bookmarks/publications
that are tagged with one of the subtags (meaning *"Germany"*, *"France"*
or *"Italy"*) as a result.  
This is useful if you often search for bookmarks/publications belonging
to a specific topic. 

-----------------------

###View concepts
To view your concepts, click on **my ${project.name}** in the [ left main menu](User_Interface "wikilink"),
then on **concepts**. You now get an overview on all your concepts and their subtags.  

![ <File:view.png>](concepts/view.png  "fig: File:view.png")  
To view concepts that are currently **popular** in ${project.name}, click on **popular** in the [ left main menu](User_Interface "wikilink"), then on **concepts**. You can now see the most popular concepts and their subtags.  

----------------------

### Add/edit your concepts

To add a new concept or to edit an existing one, click on the **person icon** in the [ right main menu](User_Interface "wikilink"), then on **edit tags**.  

With **add subtags to concepts**, you can add a new concept or add new tags to an existing concept. To add a new concept, type the desired tag that should be the concept's name into the field 'supertag' and the tag that should be added to the concept into the field 'subtag'. Repeat this step for all tags that should be added to a concept.  

With **delete subtags from concepts**, you can delete subtags from existing concepts. To do so, type the concept's name into the field 'supertag' and the tag that should be deleted into the field 'subtag'.  

![ <File:edit.png>](concepts/edit.png  "fig: File:edit.png")  

------------------------

### Navigation via concepts

To search with concepts, just use the search
bar in the right upper corner. Click on the **blue arrow** next to
'search' and in the dropdown menu, choose **concepts**. Type the concept
you want to search for into the search field and click on the
**magnifying glass symbol** or press the **enter key**.  

![ <File:search.png>](concepts/search.png  "fig: File:search.png")  
The bookmarks/publications tagged with the concept's subtags will be displayed.  
In the sidebar on the right, you can see all subtags that are mapped to
the concept's supertag (name).

-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").


