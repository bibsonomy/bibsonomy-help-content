<!-- de: JabrefInstallieren -->

## JabRef Plugin - installation
-------------------------------

**Caution**: Since JabRef version 2.12, plugins are no
longer supported in JabRef. So in newer versions the ${project.name}
plugin is no longer working.

[JabRef](http://www.jabref.org/) is an open source bibliography reference manager based on BibTeX format.
JabRef can be connencted with ${project.name} by using a plugin that allows you
to access your publications from ${project.name} directly in JabRef.  

In this tutorial, we show you how to download and configure JabRef and
the ${project.name} plugin. You can find information on data exchange
between JabRef and ${project.name} on [JabRef- data exchange](Jabref "wikilink").
If you have questions regarding the plugin, visit our [contact page](Contact "wikilink").

------------------------------------------------------------------------

### General information

For general information regarding the JabRef plugin, visit [this page](JabrefInstall/JabrefGeneral "wikilink").

------------------------------------------------------------------------

### Installation

The installation of the JabRef plugin is explained [here](JabrefInstall/JabrefInstallation "wikilink").

------------------------------------------------------------------------

### Configuration

Before you can start working with the plugin, you have to [configure it](JabrefInstall/JabrefConfig "wikilink").

-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").