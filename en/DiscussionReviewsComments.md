<!-- de: KommentarRezensionBewertung -->

## Discussion: Reviews and Comments
--------------------------------

In ${project.name}, you can comment and rate publications and bookmarks
very easily. This way, you are able to discuss publications with other
users, write reviews or give your opinion on a publication in form of "stars".

------------------------------------------------------------------------

### Discussions in ${project.name}

In ${project.name} every user can discuss ressources (bookmarks and publications).
Further information on this topic can be found [here](DiscussionReviewsComments/Discussion).

------------------------------------------------------------------------

### Rate publications/bookmarks

If you desire to rate an existing entry (bookmark or publication), follow [these instructions](DiscussionReviewsComments/Rate).

------------------------------------------------------------------------

### Comment reviews

Reviews written by other users for a publication/bookmark can be
commented any number of times. To comment a review, click on the **black
arrow** below the review.  
Writing a comment is the **same as writing a review**, except the fact that
in comments you can't rate an article.  

![<File:comment_neu.png>](DiscussionReviewsComments/comment_neu.png  "fig: File:comment_neu.png") 

-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").
