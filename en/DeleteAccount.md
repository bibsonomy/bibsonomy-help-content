<!-- de: BenutzerkontoEndgültigLöschen -->

## How can I delete my ${project.name} account?
---------------------------------------------

To delete your account, follow these three steps:

**Caution:** If you delete your account, this action cannot be undone.

-   Log in to ${project.name}.

-   Go to the [ settings page](${project.home}settings "wikilink") and choose the
    tab "settings".
-   At the bottom of the page, you can delete your account. If you
    really are sure to delete your account, type **"yes"** into the text
    box and click on the "delete account" button.

![ <File:deleteaccount.png>](deleteaccount/deleteaccount.png  "fig: deleteaccount.png")

-----------------------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").
