<!-- de: Tags -->

## Use the tag system
------------------

Tags allow you to organize your bookmarks and publications easier. In
${project.name} you also can use tags as a keyword for searching and
comparing items.

------------------------------------------------------------------------

### How does the tag system work?

From your computer or in real life, you probably know the opportunity to
store files/documents in folders and subfolders. A file is placed in a
specific folder. The keyword system works in a completely different way.  

You can add as many tags as you want to a file/publication. That way you can find all
files about a specific topic very fast and easily, for you only have to
search for the corresponding tag. The advantage of the tag system is the
fact that you can combine different tags when searching to achieve
precise and specific results. For example, if you're looking for
literature about the topic *"politics in France"*, you can search with
both tags *"politics"* AND *"France"*. As a result, you get all articles
that deal with both topics.  
With this function, ${project.name} makes it easy for you to store,
manage and share your literature with others.


**Tip:** Good tags are short and concise. In addition, we recommend to
always use singular words (e.g. use *"tree"* instead of *"trees"* - **or
even better**: use two keywords: *"tree"* AND *"forest"*), thus avoiding
unnecessary word forms. If you want to use a tag that consists of two
words (e.g. *"Computer Science"*) you should use [PascalCase](http://c2.com/cgi/wiki?PascalCase) (e.g. *"ComputerScience"*).

------------------------------------------------------------------------

### Add tags to bookmarks/publications

You can add tags to a bookmark/publication to organize them better and
keep the overview.  
You can add tags everytime you [ add a bookmark/publication](BookmarkPublicationManagement "wikilink") to your
collection (whether you post a new one or you add an existing one). When
adding, the system displays **recommendations** for tags based on your
frequently used tags. Also, the system displays the **tags of the copied
posts**, in case you are adding an existing one.  

${project.theme == "bibsonomy"}{

![ <File:addTags.png>](tags/addTags.png  "fig: File:addTags.png")  

------------------------------------------------------------------------

### Edit tags on bookmarks/publications

If you already added a bookmark/publication to your collection, it is
still possible to edit the entered tags.  
There are several ways to edit the tags.

1.  Edit tags via **"edit post"**
2.  Edit tags via **"fast edit"**
3.  Edit tags via **"edit tags"**


![ <File:editPost2.png>](tags/editPost2.png  "fig: File:editPost2.png")  


#### Edit tags via "edit post"
Click on the **black pencil button** (edit this bookmark/publication) on the
right side of a post. You are directed to a page where you can edit the
information about this post, including the tags. When you are done with editing,
don't forget to click on the **save** button on the bottom of the page.  


#### Edit tags via "fast edit"

Click on the **blue pencil button** (edit tags) on the left side of a
post. A pop-up window appears where you can edit a post's tags directly.
You can delete tags by clicking on the X-symbol and add tags by writing
them into the textfield, separated by a space character. Click on
**save** to save your changes or on **close** to discard them.  


#### Edit tags via "edit tags"

In ${project.name}, you aren't only able to edit the tags of a single
post, but also you can edit all your tags that you have used at once. To
do so, click on the **person symbol** in the [ right main menu](User_Interface "wikilink") and then on
**edit tags**. On the following page, you can edit your tags and [
concepts](Concepts "wikilink").  

- **rename/replace tags:** Here you can replace a specific tag by
another tag. This is useful, if you have used two similar tags that you
want to merge into one, for example you can replace *augmentedreality*
by *augmentedReality*.  

- **add subtags to concepts:** To add a subtag to a [
concept](Concepts "wikilink"), just write the concept's name into the
field 'supertag' and the tag you want to add into 'subtag'.  
- **delete subtags from concepts:** To delete a subtag from a [
concept](Concepts "wikilink"), just write the concept's name into the
field 'supertag' and the tag you want to delete into 'subtag'.  

------------------------------------------------------------------------

### Navigation via tags

You can browse through publications and bookmarks in ${project.name} by
using tags.  

- **Option 1:** In order to search for **bookmarks/publications with a
    specific tag**, just use the search bar in the right upper corner. Click
    on the **blue arrow** next to 'search' and in the dropdown menu, choose
    **tag**. Type the tag you want to search for into the search field and
    click on the **magnifying glass symbol** or press the **enter key**.  
    
    ![ <File:search.png>](tags/search.png  "fig: File:search.png")  

}

${project.theme == "puma"}{

![ <File:addTags.png>](tags/addTags.png  "fig: File:addTags.png")  

------------------------------------------------------------------------

### Edit tags on bookmarks/publications

If you already added a bookmark/publication to your collection, it is
still possible to edit the entered tags.  
There are several ways to edit the tags.

1.  Edit tags via **"edit post"**
2.  Edit tags via **"fast edit"**
3.  Edit tags via **"edit tags"**


![ <File:editPost2.png>](tags/editPost2.png  "fig: File:editPost2.png")  


#### Edit tags via "edit post"
Click on the **black pencil button** (edit this bookmark/publication) on the
right side of a post. You are directed to a page where you can edit the
information about this post, including the tags. When you are done with editing,
don't forget to click on the **save** button on the bottom of the page.  


#### Edit tags via "fast edit"

Click on the **blue pencil button** (edit tags) on the left side of a
post. A pop-up window appears where you can edit a post's tags directly.
You can delete tags by clicking on the X-symbol and add tags by writing
them into the textfield, separated by a space character. Click on
**save** to save your changes or on **close** to discard them.  


#### Edit tags via "edit tags"

In ${project.name}, you aren't only able to edit the tags of a single
post, but also you can edit all your tags that you have used at once. To
do so, click on the **person symbol** in the [ right main menu](User_Interface "wikilink") and then on
**edit tags**. On the following page, you can edit your tags and [
concepts](Concepts "wikilink").  

- **rename/replace tags:** Here you can replace a specific tag by
another tag. This is useful, if you have used two similar tags that you
want to merge into one, for example you can replace *augmentedreality*
by *augmentedReality*.  

- **add subtags to concepts:** To add a subtag to a [
concept](Concepts "wikilink"), just write the concept's name into the
field 'supertag' and the tag you want to add into 'subtag'.  
- **delete subtags from concepts:** To delete a subtag from a [
concept](Concepts "wikilink"), just write the concept's name into the
field 'supertag' and the tag you want to delete into 'subtag'.  

------------------------------------------------------------------------

### Navigation via tags

You can browse through publications and bookmarks in ${project.name} by
using tags.  

- **Option 1:** In order to search for **bookmarks/publications with a
    specific tag**, just use the search bar in the right upper corner. Click
    on the **blue arrow** next to 'search' and in the dropdown menu, choose
    **tag**. Type the tag you want to search for into the search field and
    click on the **magnifying glass symbol** or press the **enter key**.  
    
    ![ <File:search.png>](tags/search.png  "fig: File:search.png")  

}

${project.theme == "wueresearch"}{

![ <File:addTagswueen.png>](wueresearch/Tags/addTagswueen.png  "fig: File:addTagswueen.png")  

------------------------------------------------------------------------

### Edit tags on bookmarks/publications

If you already added a bookmark/publication to your collection, it is
still possible to edit the entered tags.  
There are several ways to edit the tags.

1.  Edit tags via **"edit post"**
2.  Edit tags via **"fast edit"**
3.  Edit tags via **"edit tags"**


![ <File:editTagswueen.png>](wueresearch/Tags/editTagswueen.png  "fig: File:editTagswueen.png")  


#### Edit tags via "edit post"
Click on the **black pencil button** (edit this bookmark/publication) on the
right side of a post. You are directed to a page where you can edit the
information about this post, including the tags. When you are done with editing,
don't forget to click on the **save** button on the bottom of the page.  


#### Edit tags via "fast edit"

Click on the **blue pencil button** (edit tags) on the left side of a
post. A pop-up window appears where you can edit a post's tags directly.
You can delete tags by clicking on the X-symbol and add tags by writing
them into the textfield, separated by a space character. Click on
**save** to save your changes or on **close** to discard them.  


#### Edit tags via "edit tags"

In ${project.name}, you aren't only able to edit the tags of a single
post, but also you can edit all your tags that you have used at once. To
do so, click on the **person symbol** in the [ right main menu](User_Interface "wikilink") and then on
**edit tags**. On the following page, you can edit your tags and [
concepts](Concepts "wikilink").  

- **rename/replace tags:** Here you can replace a specific tag by
another tag. This is useful, if you have used two similar tags that you
want to merge into one, for example you can replace *augmentedreality*
by *augmentedReality*.  

- **add subtags to concepts:** To add a subtag to a [
concept](Concepts "wikilink"), just write the concept's name into the
field 'supertag' and the tag you want to add into 'subtag'.  
- **delete subtags from concepts:** To delete a subtag from a [
concept](Concepts "wikilink"), just write the concept's name into the
field 'supertag' and the tag you want to delete into 'subtag'.  

------------------------------------------------------------------------

### Navigation via tags

You can browse through publications and bookmarks in ${project.name} by
using tags.  

- **Option 1:** In order to search for **bookmarks/publications with a
    specific tag**, just use the search bar in the right upper corner. Click
    on the **blue arrow** next to 'search' and in the dropdown menu, choose
    **tag**. Type the tag you want to search for into the search field and
    click on the **magnifying glass symbol** or press the **enter key**.  
    
    ![ <File:searchTagswueen.png>](wueresearch/Tags/searchTagswueen.png  "fig: File:searchTagswueen.png")  

}

    The bookmarks/publications tagged with the specific tag will be
    displayed.  
    In the sidebar on the right, you can see additional information about
    this tag: [ concepts](Concepts "wikilink") with this tag, about [related tags](Tags#RelatedTags "wikilink") and [ similar tags](Tags#SimilarTags "wikilink").  
  
- **Option 2:** When you click on a tag that is displayed with a post
    (for example on the [ home page](Bib:\ "wikilink")), you can see **all
    posts of a user** with this specific tag. In the sidebar on the right,
    you can see information about

	-   this tag as a tag from all users
	-   this tag as a concept from this specific user
    -   this tag as a concept from all users
    -   related tags
    -   the user's concepts
    -   the user's tags.

------------------------------------------------------------------------

### Related tags

Related tags are those tags which were assigned together to a post. If
e.g. a user has tagged a post with *"java"* and *"programming"*, then those
two tags are related.

------------------------------------------------------------------------

### Similar tags

Similar tags on the other side are computed by a more complex similarity
measure coming from the research on information retrieval (namely cosine
similarity in the vector of the popular tags). Similar tags are in many
cases synonym tags.

### System tags
-----------

System tags are special tags that come with a predefined meaning. At
present, ${project.name} offers three types of system tags:

-   Executable system tags
-   Markup system tags
-   Search system tags

Click [here](SystemTags "wikilink") to learn more about system tags and their
functions.

-----------------------------------

[Click here to go back to beginner's area and learn more about the
basic functions](MainBeginner "wikilink").

  

