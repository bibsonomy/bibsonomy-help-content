<!-- en: OAuthRequests -->

## OAuth Anfragen
------------------------------

Nachdem Sie erfolgreich den [Autorisierungsprozess von OAuth implementiert](OAuth "wikilink") haben, können 
Sie Anfragen an die API von ${project.name} stellen.

Jetzt können Sie das ${project.name}- *rest logic interface* nutzen, um
auf die API zuzugreifen.

    RestLogicFactory rlf = new RestLogicFactory("<HOME_URL>/api", RenderingFormat.XML);
    LogicInterface rl = rlf.getLogicAccess(accessor);
    
    Post<Bookmark> testPost = generatePost(accessor.getRemoteUserId());
    	
    //
    // publish first test post
    //
    List<Post<? extends Resource>> uploadPosts = new LinkedList<Post<? extends Resource>>();
    uploadPosts.add(testPost);
    testPost.getResource().recalculateHashes();
    String firstHash = testPost.getResource().getIntraHash();
    try {
        rl.createPosts(uploadPosts);
    } catch (BadRequestOrResponseException e) {
        System.err.println(e.getMessage());
    }

------------------------------------------------------------------------

Bitte ersetzen Sie `<HOME_URL>` mit ${project.home}.

Ein **funktionierendes Beispiel** finden Sie [hier](https://bitbucket.org/bibsonomy/bibsonomy-help-content/src/b8d20eb1bde387f1a8772d91943452f64d5edda2/code-samples/oauth-rest-demo/src/main/java/org/bibsonomy/OAuthRestApiDemo.java?at=default&fileviewer=file-view-default). Falls Sie weitere Fragen haben,
schreiben Sie bitte eine E-Mail an ${project.apiemail}.  

------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink") 