<!-- en: UnlockAdvancedMode -->

## Erweiterte Ansicht/Funktionen freischalten
------------------------------------------

Um eine hohe Benutzbarkeit von ${project.name} zu erreichen, haben wir
uns dafür entschieden, nicht alle Funktionen des Systems direkt
anzuzeigen. Diese Funktionen richten sich insbesondere an
fortgeschrittene Nutzer.  

**Ziel:** Diese Anleitung zeigt Ihnen, wie Sie die erweiterten
Funktionen mit wenigen Klicks freischalten.  

![ <File:1.png>](erweitertefunktionenfreischalten/1.png  "fig: File:1.png")  


Anleitung:

1.  Klicken Sie auf das Benutzermenü.  

2.  Klicken Sie in diesem Menü auf **"Einstellungen"**.  
   ![ <File:2.png>](erweitertefunktionenfreischalten/2.png  "fig: File:2.png") 

3.  Klicken Sie auf der Seite den Reiter **"Einstellungen"** an.  

4.  Wählen Sie unter **"Erscheinungsbild"** die Option
    **"Erweitert"** aus.
    
5.  Speichern Sie Ihre Auswahl mit einem Klick auf **"Layout speichern"**.


Das erweiterte Layout ist nun aktiv.  

------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink")
