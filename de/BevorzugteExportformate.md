<!-- en: FavouriteExportFormats -->

## Bevorzugte Exportformate auswählen
----------------------------------

![ <File:1.png>](bevorzugteexportformate/1.png  "fig: File:1.png")

Um Ihre bevorzugten Exportformate festzulegen, klicken Sie im Personenmenü im [
rechten Hauptmenü](Programmoberfläche "wikilink") auf [Einstellungen](Einstellungen "wikilink") 
und dann auf den Reiter **"Einstellungen"**. 

1.	Unter **"Layouts Ihrer Tagbox und Ihrer
	Eintragslisten"** haben Sie die Möglichkeit, Ihre bevorzugten Exportformate hinzuzufügen, 
	indem Sie sie in das Feld **“Exportformat hinzufügen“** tippen.

2.	Wenn Sie ein Exportformat wieder entfernen wollen, klicken Sie auf das rote Feld **“Löschen“** 		hinter dessen Namen.

3.	Wenn Sie Ihre bevorzugten Exportformate gewählt haben, bestätigen Sie Ihre Auswahl durch das 		Klicken auf **“Layout speichern“** am Ende des Layoutabschnitts.

![ <File:2.png>](bevorzugteexportformate/2.png  "fig: File:2.png")

Sie können sich nun Publikationen in den von Ihnen gewählten Formaten anzeigen lassen.
Klicken Sie hierfür auf den nach unten zeigenden Pfeil im Menü in der unteren 
rechten Ecke eines jeden Publikationseintrags. 
Die von Ihnen gewählten Exportformate werden nun am Ende des erscheinenden Menüs angezeigt.

------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink")