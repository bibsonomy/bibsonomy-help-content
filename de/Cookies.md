<!-- en: Cookies -->

## Cookies
------------


${project.name} verwendet sogenannte *Cookies*. Diese dienen dazu, unser Angebot nutzerfreundlicher, effektiver und sicherer zu machen. Cookies sind kleine Textdateien, die auf Ihrem Rechner abgelegt werden und die Ihr Browser speichert. Die meisten der von uns verwendeten Cookies sind sogenannte *Session-Cookies*. Sie werden nach Ende Ihres Besuchs automatisch gelöscht, es sei denn Sie haben bei der Anmeldung die Option "Eingeloggt bleiben" gewählt. In diesem Fall bleibt der Cookie für ein Jahr bestehen, sofern dieser nicht manuell gelöscht wird. ${project.name} nutzt möglicherweise zu Zwecken der eigenen internen Statistik ihrer Webseitenbesuche *Piwik*, eine Software zur Auswertung
von Webserver-Zugriffen.

Piwik verwendet ebenfalls sogenannte *Cookies*, die auf Ihrem Computer gespeichert werden und die eine Analyse der Benutzung unseres Webangebots ermöglichen. Die durch einen Cookie erzeugten Informationen über Ihre Benutzung werden nur auf Servern von ${project.name} gespeichert. Die so erfassten Daten werden nur zum Zweck der eigenen internen Statistik genutzt und im Anschluss an die Auswertung gelöscht.
Wenn Ihr Webbrowser die *Do-Not-Track*-Technik unterstützt und Sie diese aktiviert haben, wird ihr Besuch von Piwik ignoriert.

Sie können die Installation der Cookies durch eine entsprechende Einstellung Ihrer Browser-Software verhindern; wir weisen Sie jedoch darauf hin, dass Sie in diesem Fall möglicherweise nicht sämtliche Funktionen unseres Webangebots vollumfänglich nutzen können.

Für Rückfragen stehen wir Ihnen unter der E-Mail-Adresse
${project.email} gerne zur Verfügung.

------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink")