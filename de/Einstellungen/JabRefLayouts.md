<!-- en: settings/JabRefLayouts -->

-----------------

[`Zurück zu den Einstellungen Ihres Profils geht es hier.`](Einstellungen "wikilink")

---------------------

## Eigene JabRef Layout-Dateien verwalten
---------------------------

*Achtung: Da es sich bei JabRef um ein veraltetes System handelt, wird dieses durch ${project.name} nur noch geringfügig  unterstützt. Als Alternative bietet es sich an, Layout-Dateien im CSL-Format (Citation Style Language) zu nutzen. Weitere Informationen dazu finden Sie auf der internen [Hilfeseite für CSL-Layouts](https://www.bibsonomy.org/help_de/Einstellungen/CSLLayouts).		*

Möchten Sie eigene Layouts in Form einer JabRef Layout-Datei erstellen, finden Sie nähere Informationen auf der 
[offiziellen JabRef Seite](https://help.jabref.org/en/CustomExports). Diese können Sie dann auf ${project.name} hochladen, 
um Ihre Publikationsliste bei ${project.name} darzustellen und im entsprechenden Format zu exportieren. Dazu benötigen 
Sie mindestens eine Datei mit der Endung `.layout`, die Ihr gewünschtes Layout beschreibt.

---------------------------------

### Speichern von eigenen JabRef Layouts in ${project.name} 

Es ist möglich JabRef Dateien in ${project.name} zu importieren, um das Layout der Publikationsliste Ihrem Stil anzupassen.

- Gehen Sie dazu zuerst zu den **Einstellungen** Ihres ${project.name}-Accounts. Eine Anleitung dazu finden Sie auf 
  der entsprechenden [Hilfe-Seite](https://www.bibsonomy.org/help_de/Einstellungen).

-   Klicken Sie dort auf den Reiter **layout files**, um zur Übersicht der möglichen Layout-Dateien zu kommen.
![ <File:layout.png>](JabRefLayouts/layout.png  "fig: File:layout.png") 

-   Anschließend klicken Sie unter **JabRef Layout-Datei** auf den Knopf **"Durchsuchen"** neben **item.layout** und 
    wählen Sie eine Datei von Ihrem persönlichen Computer aus, um diese auf ${project.name} hochzuladen.
    - Möchten Sie an dieser Stelle zusätzlich noch den Beginn bzw. das Ende der Publikationsliste besonders visualisieren, so 
      können Sie darüber hinaus noch Dateien mit der Endung `begin.layout` bzw. `end.layout` hochladen.
    
-   Haben Sie dies getan, dann klicken Sie abschließend auf **"Abschicken"**, um Ihre Auswahl zu bestätigen. 
![ <File:abschicken.png>](JabRefLayouts/abschicken.png  "fig: File:abschicken.png") 

- Betrachten Sie nun eine Publikationsliste, so erscheint Sie in Ihrem gewünschten Layout.

---------------------------------

### Löschen von eigenen JabRef Layouts von ${project.name}

Um hochgeladene Layout-Dateien zu löschen, müssen Sie folgende Schritte befolgen:

-   Gehen Sie (wie oben beschrieben) zum Reiter **layout files** in den **Einstellungen**.

-   Klicken Sie dort neben der zu löschenden Datei auf den Knopf mit der Aufschrift **"Löschen"**.
![ <File:loeschen.png>](JabRefLayouts/loeschen.png  "fig: File:loeschen.png")

