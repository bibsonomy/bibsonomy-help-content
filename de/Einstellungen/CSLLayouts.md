<!-- en: settings/CSLLayouts -->

-----------------

[`Zurück zu den Einstellungen Ihres Profils geht es hier.`](Einstellungen "wikilink")

---------------------

## Eigene CSL-Dateien verwalten
--------------------------------

${project.name} importiert alle CSL Dateien aus dem offiziellen Github-Repository für [CSL-Dateien](https://github.com/citation-style-language/styles).
Sollte für Sie bei diesen Styles dennoch nicht das richtige dabei sein, können Sie eigene CSL-Dateien erstellen und auf ${project.name} hochladen, um Ihre Publikationsliste bei ${project.name} darzustellen und in diesem Format zu exportieren. 
Dazu benötigen Sie zuerst eine Datei mit der Endung `.csl`, die Ihr gewünschtes Layout beschreibt.
Eigene CSL-Dateien können Sie beispielsweise mit dem [visuellen Editor](https://editor.citationstyles.org/visualEditor/) erstellen.
Für weitere Informationen können Sie die [offizielle Seite des CSL-Projekts](https://citationstyles.org) besuchen.

---------------------------------

### Speichern von eigenen CSL Layouts in ${project.name} 

Es ist möglich CSL-Dateien in ${project.name} zu importieren, um das Layout der Zitationen Ihrem Stil anzupassen.

-   Gehen Sie dazu zuerst zu den **Einstellungen** Ihres ${project.name}-Accounts. Eine Anleitung dazu finden Sie auf 
    der entsprechenden [Hilfe-Seite](https://www.bibsonomy.org/help_de/Einstellungen).
    
-   Klicken Sie dort auf den Reiter **layout files**, um zur Übersicht der möglichen Layout-Dateien zu kommen.
![ <File:layout.png>](CSLLayouts/layout.png  "fig: File:layout.png") 

-   Anschließend klicken Sie auf den Knopf **"Durchsuchen"** und wählen Sie eine Datei von Ihrem
    persönlichen Computer aus, um diese auf ${project.name} hochzuladen.
    
-   Haben Sie dies getan, dann klicken Sie abschließend auf **"Abschicken"**, um Ihre Auswahl zu bestätigen. 
![ <File:abschicken.png>](CSLLayouts/abschicken.png  "fig: File:abschicken.png") 

- Möchten Sie nun ein Publikation zitieren, so erscheint das hochgeladene Layout in der Liste der Zitierstile.
![ <File:zitieren.png>](CSLLayouts/zitieren.png  "fig: File:zitieren.png")

---------------------------------

### Löschen von eigenen CSL Layouts von ${project.name}

Um hochgeladene Layout-Dateien zu löschen, müssen Sie folgende Schritte befolgen:

-   Gehen Sie (wie oben beschrieben) zum Reiter **layout files** in den **Einstellungen**.

-   Klicken Sie dort neben der zu löschenden Datei auf den roten Knopf mit der Aufschrift **"Löschen"**.
![ <File:loeschen.png>](CSLLayouts/loeschen.png  "fig: File:loeschen.png")
