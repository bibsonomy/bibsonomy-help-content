<!-- en: BookmarkPublicationManagement/BookmarkManagement -->

-----------------

[`Zurück zur Verwaltung von Publikationen und Lesezeichen geht es hier.`](LesezeichenPublikationenManagen "wikilink")

---------------------

## Lesezeichen verwalten
----------------------------------

**Einführung:** Lesezeichen (engl. bookmarks) ermöglichen es, das
Internet wie ein Buch zu verwenden. Mit einem Lesezeichen merken Sie
sich die genaue Adresse eines Internet-Dokuments. ${project.name} gibt
Ihnen die Möglichkeit, Lesezeichen zentral zu speichern und zu
verwalten.

------------------------------------------------------------------------

### Hinzufügen von Lesezeichen

${project.theme == "bibsonomy"}{

**Ziel:** Diese Anleitung zeigt Ihnen, wie Sie eine Webseite als
Lesezeichen hinzufügen.  

![ <File:bookmark1.png>](LesezeichenManagen/bookmark1.png  "fig: File:bookmark1.png")  

1.  Klicken Sie auf den Menüpunkt **"Eintragen"** (im [
    Hauptmenü](Programmoberfläche "wikilink")). Ein Untermenü klappt auf.
    
2.  Klicken Sie im Untermenü mit der linken Maustaste auf **"Lesezeichen
    hinzufügen"**.
    

![ <File:bookmark_add2.png>](LesezeichenManagen/bookmark_add2.png  "fig: File:bookmark_add2.png")
    
}

${project.theme == "puma"}{

**Ziel:** Diese Anleitung zeigt Ihnen, wie Sie eine Webseite als
Lesezeichen hinzufügen.  

![ <File:bookmark1.png>](LesezeichenManagen/bookmark1.png  "fig: File:bookmark1.png")  

1.  Klicken Sie auf den Menüpunkt **"Eintragen"** (im [
    Hauptmenü](Programmoberfläche "wikilink")). Ein Untermenü klappt auf.
    
2.  Klicken Sie im Untermenü mit der linken Maustaste auf **"Lesezeichen
    hinzufügen"**.
    

![ <File:bookmark_add2.png>](LesezeichenManagen/bookmark_add2.png  "fig: File:bookmark_add2.png")
    
}

${project.theme == "wueresearch"}{

**Ziel:** Diese Anleitung zeigt Ihnen, wie Sie eine Webseite als
Lesezeichen hinzufügen.  

![ <File:bookmarkwue.png>](wueresearch/LesezeichenPublikationenManagen/LesezeichenManagen/bookmarkwue.png  "fig: File:bookmarkwue.png")  

1.  Klicken Sie auf den Menüpunkt **"Eintragen"** (im [
    Hauptmenü](Programmoberfläche "wikilink")). Ein Untermenü klappt auf.
    
2.  Klicken Sie im Untermenü mit der linken Maustaste auf **"Lesezeichen
    hinzufügen"**.
    
}

3.  Tragen Sie unter **URL** die Adresse (URL) der Webseite ein, die Sie
    als Lesezeichen hinzufügen möchten. Klicken Sie danach auf **"Weiter"**.
    
4.  Im folgenden Schritt werden Sie aufgefordert, einige
    Zusatzdaten einzutragen. Diese sind:
    
    -   **URL:** (Wird automatisch aus dem vorherigen
        Schritt übernommen)
    -   **Titel:** Tragen Sie den Titel/Namen der Seite ein.  
        **Tipp:** Unterhalb des Eingabefeldes befindet sich in kleiner
        blauer Schrift der Originaltitel der Seite. Diesen können Sie
        durch Anklicken mit der linken Maustaste übernehmen.
    -   **Beschreibung, Kommentar:** Hier können Sie einen eigenen
        Kommentar zum Lesezeichen hinterlegen.  
        **Tipp:** In diesem Feld können Sie z. B. auch ein kleines
        Abstract hinterlegen.
    -   **tags:** Tags (dt. Schlagworte) ermöglichen das vereinfachte
        Wiederfinden und Organisieren von Lesezeichen/Publikationen in
        ${project.name} (weitere Informationen zu Tags finden sie [
        hier](Tags "wikilink")). Es können beliebig viele Tags vergeben werden.
        Tags werden untereinander durch Leerzeichen getrennt.  
        **Tipp 1:** Wenn Sie einen Tag verwenden möchten, der aus
        mehreren Wörtern besteht (z. B. Fachbereich Informatik), dann
        verwenden Sie [PascalCase](http://c2.com/cgi/wiki?PascalCase) - z. B. FachbereichInformatik.  
        **Tipp 2:** ${project.name} versucht, aus der URL und Ihrem
        Kommentar einige Empfehlungen (siehe "Empfehlungen") für
        mögliche Tags zu generieren. Diese können Sie per
        Mausklick übernehmen.
    -   **Sichtbarkeit:** Hier können Sie bestimmen, für wen Ihr Eintrag sichtbar sein soll.
    -   **Gruppenoptionen:** Wählen Sie eine oder mehrere Gruppen aus für die Ihr Eintrag interessant sein könnte. 
    
5.  Klicken Sie auf **"Speichern"**, um das Lesezeichen einzutragen.

6.  Das Lesezeichen ist nun gespeichert. Bitte beachten Sie, dass ein
    neu eingetragenes Lesezeichen einige Zeit benötigt, bis es über die
    Suche gefunden werden kann (Zeitspanne von einer Sekunde bis
    wenige Minuten).

------------------------------------------------------------------------

### Tipps für die Recherche und Archivierung

-   ${project.name} speichert nur die Adresse des Internet-Dokuments
    (HTML-Seite, Video, Bild, etc.), jedoch nicht das Dokument selbst.
    Es kann somit passieren, dass das von Ihnen gespeicherte
    Internet-Dokument zu einem späteren Zeitpunkt nicht mehr verfügbar
    ist (z. B. weil das Dokument gelöscht wurde, der Anbieter/die
    Webseite nicht mehr existiert oder aber das Dokument umgezogen ist
    (neue Adresse usw.). Wir empfehlen Ihnen daher, wichtige Dokumente
    auf Ihrem Computer zu speichern (Sicherungskopie).
    
-   Neben dem oben genannten Punkt bedenken Sie bitte, dass ein
    Internet-Dokument jederzeit geändert werden kann. Auch hier hilft
    die oben genannte Sicherungskopie. Zudem sollten Sie, dies
    entspricht den allgemeinen Richtlinien wissenschaftlichen Arbeitens,
    in der Literaturangabe zu einem Internetdokument IMMER das Datum und
    die Uhrzeit des letzten Abrufs mit angeben (diese Angaben können Sie
    in das Feld „Beschreibung, Kommentar“ eintragen).
    
-   ${project.name} unterstützt die [RFC 7089](http://tools.ietf.org/html/rfc7089) Spezifikation. Damit wird
    es möglich, Lesezeichen so zu betrachten, wie sie in
    ${project.name} gespeichert wurden, selbst wenn sich die Seite in
    der Zwischenzeit geändert hat. Um diese Funktion zu nutzen, müssen
    Sie das Memento-Plugin in Ihrem Browser installieren. Das Plugin
    existiert für [Mozilla Firefox](https://addons.mozilla.org/de/firefox/addon/mementofox/) und [Google Chrome](http://bit.ly/memento-for-chrome).

------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink")