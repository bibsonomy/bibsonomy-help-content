<!-- en: BookmarkPublicationManagement/PublicationlistCreate -->

-----------------

[`Zurück zur Verwaltung von Publikationen und Lesezeichen geht es hier.`](LesezeichenPublikationenManagen "wikilink")

---------------------

## Literaturlisten erstellen
----------------

${project.theme == "bibsonomy"}{

**Ziel:** Diese Anleitung zeigt Ihnen, wie Sie aus Ihren Publikationen
(oder denen eines anderen Nutzers) Listen erstellen können, die später
auf externen Webseiten verwendet werden können (siehe auch:
[URL-Syntax](http://www.bibsonomy.org/help_de/URL-Syntax#section-URL-Syntax-HTMLFormatierung)).  
Geben Sie dafür einfach die folgenden URLs in Ihren Browser ein.

**Hinweis: Auf den Listen werden standardmäßig höchstens 20 Einträge angezeigt. Sie können sich mehr Einträge anzeigen lassen, indem Sie `?items=beliebigeZahl` an das Ende der URL anhängen (ersetzen Sie `beliebige Zahl` mit der Anzahl an Einträgen, die angezeigt werden sollen.)**

-   **Allgemeine Liste:**  
    `http://www.bibsonomy.org/publ/user/<username>`  
    Ersetzen Sie &lt;username&gt; durch Ihren Benutzernamen.  
    
    **Beispiel:**
    [http://www.bibsonomy.org/publ/user/hotho](http://www.bibsonomy.org/publ/user/hotho)
![<File:Publicationlist.png>](LiteraturlistenErstellen/Publicationlist.png  "fig: File:Publicationlist.png")  
  
- **Allgemeine Liste ohne Tags:**  
`http://www.bibsonomy.org/publ/user/<username>?notags=1`  
Ersetzen Sie &lt;username&gt; durch Ihren Benutzernamen.  
**Beispiel:**
[<http://www.bibsonomy.org/publ/user/hotho?notags=1>](http://www.bibsonomy.org/publ/user/hotho?notags=1).  
  
- **Allgemeine Liste mit Tag-Einschränkung:**  
`http://www.bibsonomy.org/publ/user/<username>/<tagname>` 
Ersetzen Sie &lt;username> durch Ihren Benutzernamen und &lt;tagname> durch
den Tag, der in den Publikationen enthalten sein soll.  
**Beispiel:**
[<http://www.bibsonomy.org/publ/user/hotho/myown>](http://www.bibsonomy.org/publ/user/hotho/myown).  
  
- **BibTeX-Liste:**  
`http://www.bibsonomy.org/bib/user/<username> `  
Ersetzen Sie &lt;username> durch Ihren Benutzernamen. Eine Liste aus
BibTeX-Einträgen für Publikationen des Nutzers wird erstellt.

*Hinweis: Auf der Liste werden standardmäßig höchstens 20 Einträge angezeigt. Sie können sich mehr Einträge anzeigen lassen, indem Sie `?items=beliebigeZahl` an das Ende der URL anhängen. Ersetzen Sie `beliebige Zahl` mit der Anzahl an Einträgen, die angezeigt werden sollen (siehe auch [URL-Syntax](http://www.bibsonomy.org/help_de/URL-Syntax#section-URL-Syntax-HTMLFormatierung).)*

**Beispiel:**
[<http://www.bibsonomy.org/bib/user/hotho>](http://www.bibsonomy.org/bib/user/hotho).  
  ![ <File:Biblist.png>](LiteraturlistenErstellen/Biblist.png  "fig: File:Biblist.png")

}

${project.theme == "puma"}{

**Ziel:** Diese Anleitung zeigt Ihnen, wie Sie aus Ihren Publikationen
(oder denen eines anderen Nutzers) Listen erstellen können, die später
auf externen Webseiten verwendet werden können (siehe auch:
[URL-Syntax](URL-Syntax "wikilink)).  
Geben Sie dafür einfach die folgenden URLs in Ihren Browser ein.

**Hinweis: Auf den Listen werden standardmäßig höchstens 20 Einträge angezeigt. Sie können sich mehr Einträge anzeigen lassen, indem Sie `?items=beliebigeZahl` an das Ende der URL anhängen (ersetzen Sie `beliebige Zahl` mit der Anzahl an Einträgen, die angezeigt werden sollen.)**

-   **Allgemeine Liste:**  
    `http://www.bibsonomy.org/publ/user/<username>`  
    Ersetzen Sie &lt;username&gt; durch Ihren Benutzernamen.  
    
    **Beispiel:**
    [http://www.bibsonomy.org/publ/user/hotho](http://www.bibsonomy.org/publ/user/hotho)
![<File:Publicationlist.png>](LiteraturlistenErstellen/Publicationlist.png  "fig: File:Publicationlist.png")  
  
- **Allgemeine Liste ohne Tags:**  
`http://www.bibsonomy.org/publ/user/<username>?notags=1`  
Ersetzen Sie &lt;username&gt; durch Ihren Benutzernamen.  
**Beispiel:**
[<http://www.bibsonomy.org/publ/user/hotho?notags=1>](http://www.bibsonomy.org/publ/user/hotho?notags=1).  
  
- **Allgemeine Liste mit Tag-Einschränkung:**  
`http://www.bibsonomy.org/publ/user/<username>/<tagname>` 
Ersetzen Sie &lt;username> durch Ihren Benutzernamen und &lt;tagname> durch
den Tag, der in den Publikationen enthalten sein soll.  
**Beispiel:**
[<http://www.bibsonomy.org/publ/user/hotho/myown>](http://www.bibsonomy.org/publ/user/hotho/myown).  
  
- **BibTeX-Liste:**  
`http://www.bibsonomy.org/bib/user/<username> `  
Ersetzen Sie &lt;username> durch Ihren Benutzernamen. Eine Liste aus
BibTeX-Einträgen für Publikationen des Nutzers wird erstellt.

*Hinweis: Auf der Liste werden standardmäßig höchstens 20 Einträge angezeigt. Sie können sich mehr Einträge anzeigen lassen, indem Sie `?items=beliebigeZahl` an das Ende der URL anhängen. Ersetzen Sie `beliebige Zahl` mit der Anzahl an Einträgen, die angezeigt werden sollen (siehe auch [URL-Syntax](http://www.bibsonomy.org/help_de/URL-Syntax#section-URL-Syntax-HTMLFormatierung).)*

**Beispiel:**
[<http://www.bibsonomy.org/bib/user/hotho>](http://www.bibsonomy.org/bib/user/hotho).  
  ![ <File:Biblist.png>](LiteraturlistenErstellen/Biblist.png  "fig: File:Biblist.png")

}

${project.theme == "wueresearch"}{

**Ziel:** Diese Anleitung zeigt Ihnen, wie Sie aus Ihren Publikationen
(oder denen eines anderen Nutzers) Listen erstellen können, die später
auf externen Webseiten verwendet werden können (siehe auch:
[URL-Syntax](URL-Syntax "wikilink")).  
Geben Sie dafür einfach die folgenden URLs in Ihren Browser ein.

**Hinweis: Auf den Listen werden standardmäßig höchstens 20 Einträge angezeigt. Sie können sich mehr Einträge anzeigen lassen, indem Sie `?items=beliebigeZahl` an das Ende der URL anhängen (ersetzen Sie `beliebige Zahl` mit der Anzahl an Einträgen, die angezeigt werden sollen.)**

-   **Allgemeine Liste:**  
    `http://www.bibsonomy.org/publ/user/<username>`  
    Ersetzen Sie &lt;username&gt; durch Ihren Benutzernamen.  
    
    **Beispiel:**
    [http://www.bibsonomy.org/publ/user/hotho](http://www.bibsonomy.org/publ/user/hotho)
![<File:Publicationlist.png>](LiteraturlistenErstellen/Publicationlist.png  "fig: File:Publicationlist.png")  
  
- **Allgemeine Liste ohne Tags:**  
`http://www.bibsonomy.org/publ/user/<username>?notags=1`  
Ersetzen Sie &lt;username&gt; durch Ihren Benutzernamen.  
**Beispiel:**
[<http://www.bibsonomy.org/publ/user/hotho?notags=1>](http://www.bibsonomy.org/publ/user/hotho?notags=1).  
  
- **Allgemeine Liste mit Tag-Einschränkung:**  
`http://www.bibsonomy.org/publ/user/<username>/<tagname>` 
Ersetzen Sie &lt;username> durch Ihren Benutzernamen und &lt;tagname> durch
den Tag, der in den Publikationen enthalten sein soll.  
**Beispiel:**
[<http://www.bibsonomy.org/publ/user/hotho/myown>](http://www.bibsonomy.org/publ/user/hotho/myown).  
  
- **BibTeX-Liste:**  
`http://www.bibsonomy.org/bib/user/<username> `  
Ersetzen Sie &lt;username> durch Ihren Benutzernamen. Eine Liste aus
BibTeX-Einträgen für Publikationen des Nutzers wird erstellt.

*Hinweis: Auf der Liste werden standardmäßig höchstens 20 Einträge angezeigt. Sie können sich mehr Einträge anzeigen lassen, indem Sie `?items=beliebigeZahl` an das Ende der URL anhängen. Ersetzen Sie `beliebige Zahl` mit der Anzahl an Einträgen, die angezeigt werden sollen (siehe auch [URL-Syntax](http://www.bibsonomy.org/help_de/URL-Syntax#section-URL-Syntax-HTMLFormatierung).)*

**Beispiel:**
[<http://www.bibsonomy.org/bib/user/hotho>](http://www.bibsonomy.org/bib/user/hotho).  
  ![ <File:Biblist.png>](LiteraturlistenErstellen/Biblist.png  "fig: File:Biblist.png")

}

------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink")