<!-- en: BookmarkPublicationManagement/PublicationManagement -->

-----------------

[`Zurück zur Verwaltung von Publikationen und Lesezeichen geht es hier.`](LesezeichenPublikationenManagen "wikilink")

---------------------

## Publikationen - Grundlagen 
----------------------------------

**Einführung:** Unter Publikationen versteht man in ${project.name}
alle Arten von Dokumenten (egal ob Artikel, Monografie, usw.). Sie
können eigene oder fremde Publikationen (z. B. als Basis für eine
Literaturrecherche) mit ${project.name} erstellen/verwalten/recherchieren.

------------------------------------------------------

### Hinzufügen von Publikationen

[Diese Anleitung](LesezeichenPublikationenManagen/PublikationenHinzufügen "wikilink") zeigt Ihnen, 
wie Sie eine neue Publikation zu ${project.name} **hinzufügen**.

---------------------------------------

${project.theme == "wueresearch"}{

### Erweiterte Suche nach Publikationen

[Diese Anleitung](LesezeichenPublikationenManagen/PublikationenSuchen "wikilink") erklärt, wie 
sich die **erweiterte Suche** von ${project.name} benutzen lässt.

--------------------------------

}

### Literaturlisten erstellen

Folgen Sie [dieser Anleitung](LesezeichenPublikationenManagen/LiteraturlistenErstellen), wenn Sie erfahren möchten, wie Sie aus Ihren Publikationen
(oder denen eines anderen Nutzers) Listen erstellen können, die später
auf externen Webseiten verwendet werden können.

------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink")