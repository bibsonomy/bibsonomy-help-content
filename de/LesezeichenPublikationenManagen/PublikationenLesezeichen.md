<!-- en: BookmarkPublicationManagement/PublicationBookmark -->

-----------------

[`Zurück zur Verwaltung von Publikationen und Lesezeichen geht es hier.`](LesezeichenPublikationenManagen "wikilink")

---------------------

## Publikationen und Lesezeichen

---------------------------------------------------

Diese Seite befasst sich mit Funktionen in ${project.name}, die
Sie sowohl auf Lesezeichen als auch auf Publikationen anwenden können.

------------------------------------------------------------------------

### Anzeigen eigener Lesezeichen und Publikationen

**Ziel:** Diese Anleitung zeigt Ihnen, wie Sie Ihre Lesezeichen &
Publikationen, die sie in ${project.name} eingetragen haben, abrufen
und anzeigen können.  

1.  Falls noch nicht geschehen, melden Sie sich bei ${project.name} mit
    Ihrem Nutzeraccount an. Wie das geht, erfahren Sie [ in dieser Anleitung](MitNutzeraccountAnmelden "wikilink").
2.  Im [ Hauptmenü](Programmoberfläche "wikilink") gehen Sie mit dem Mauszeiger auf den Punkt **mein ${project.name}** - ein
    Untermenü erscheint.
3.  Klicken Sie im Untermenü auf **"meine Einträge"**.
4.  ${project.name} gibt Ihnen nun alle von Ihnen gespeicherten
    Lesezeichen (links) & Publikationen (rechts) aus.

      **Tipp:** Je nachdem, wie viele Lesezeichen und Publikationen Sie gespeichert haben, kann es sein, dass nicht alle auf einer Seite angezeigt werden können. ${project.name} teilt dann Ihre gespeicherten Einträge auf mehrere Seiten auf. Daher scrollen Sie bitte bis zum Ende der Webseite, dort finden Sie die Möglichkeit, durch die einzelnen Seiten zu blättern.  
          
      **Beispiel:** ![<File:pageing.PNG>](PublikationenLesezeichen/pageing.PNG  "fig: File:pageing.PNG")  

5.  Um Details einer Publikation zu sehen, klicken Sie einfach auf den
    Titel der Publikation. Sie werden auf die Detailansicht weitergeleitet.

------------------------------------------------------------------------

### Suche nach allen verfügbaren Lesezeichen & Publikationen in ${project.name} 

**Ziel:** Diese Anleitung zeigt Ihnen, wie Sie auf ${project.name} nach
eigenen und nach fremden Lesezeichen und Publikationen suchen können.  

1.  Oberhalb des blauen Balkens auf der rechten Seite finden Sie die Suche.

2.  Klicken Sie mit der linken Maustaste auf den blauen Pfeil hinter *"Suche"*.

${project.theme == "bibsonomy"}{

3.  Es erscheint ein Menü. Wählen Sie den Datensatz aus (durch Klick mit
    der linken Maustaste), den Sie durchsuchen möchten - z. B. wählen
    Sie *"Autor"*, um nach einem bestimmten Autor zu suchen.  
    ![ <File:search.png>](PublikationenLesezeichen/search.png  "fig: File:search.png") 
    
4.  Geben Sie dann in das weiße Feld Ihren Suchbegriff ein.

Klicken Sie abschließend auf das Lupensymbol oder drücken Sie "Enter" auf Ihrer Tastatur.
${project.name} gibt Ihnen in wenigen Sekunden alle verfügbaren Ergebnisse aus.`

------------------------------------------------------------------------

### Zugriff auf OpenAccess-Publikationsdienste

**Ziel:** Sie lernen, wie Sie über die Detailansicht einer Publikation
in ${project.name} nach der digitalen Ausgabe in einer
OpenAccess-Datenbank suchen können.  

**Voraussetzung:** Sie müssen die gewünschte Publikation in der
Detailansicht aufgerufen haben. Dazu klicken Sie einfach auf den Namen
der Publikation, dann werden Sie auf die Detailansicht weitergeleitet.  

![ <File:fulltext.png>](PublikationenLesezeichen/fulltext.png  "fig: File:fulltext.png")  

1.  Klicken Sie auf das Auswahlmenü bei **"Suchen auf"**. Ein Untermenü
erscheint.

2.  Wählen Sie aus der angezeigten Liste die OpenAccess-Datenbank, die
    Sie nach diesem Artikel durchsuchen möchten.

}

${project.theme == "puma"}{

3.  Es erscheint ein Menü. Wählen Sie den Datensatz aus (durch Klick mit
    der linken Maustaste), den Sie durchsuchen möchten - z. B. wählen
    Sie *"Autor"*, um nach einem bestimmten Autor zu suchen.  
    ![ <File:search.png>](PublikationenLesezeichen/search.png  "fig: File:search.png") 
    
4.  Geben Sie dann in das weiße Feld Ihren Suchbegriff ein.

Klicken Sie abschließend auf das Lupensymbol oder drücken Sie "Enter" auf Ihrer Tastatur.
${project.name} gibt Ihnen in wenigen Sekunden alle verfügbaren Ergebnisse aus.`

------------------------------------------------------------------------

### Zugriff auf OpenAccess-Publikationsdienste

**Ziel:** Sie lernen, wie Sie über die Detailansicht einer Publikation
in ${project.name} nach der digitalen Ausgabe in einer
OpenAccess-Datenbank suchen können.  

**Voraussetzung:** Sie müssen die gewünschte Publikation in der
Detailansicht aufgerufen haben. Dazu klicken Sie einfach auf den Namen
der Publikation, dann werden Sie auf die Detailansicht weitergeleitet.  

![ <File:fulltext.png>](PublikationenLesezeichen/fulltext.png  "fig: File:fulltext.png")  

1.  Klicken Sie auf das Auswahlmenü bei **"Suchen auf"**. Ein Untermenü
erscheint.

2.  Wählen Sie aus der angezeigten Liste die OpenAccess-Datenbank, die
    Sie nach diesem Artikel durchsuchen möchten.

}

${project.theme == "wueresearch"}{

3.  Es erscheint ein Menü. Wählen Sie den Datensatz aus (durch Klick mit
    der linken Maustaste), den Sie durchsuchen möchten - z. B. wählen
    Sie *"Autor"*, um nach einem bestimmten Autor zu suchen.  
    ![ <File:searchwue.png>](wueresearch/PublikationenLesezeichenManagen/PublikationenLesezeichen/search.png  "fig: File:searchwue.png") 
    
4.  Geben Sie dann in das weiße Feld Ihren Suchbegriff ein.

Klicken Sie abschließend auf das Lupensymbol oder drücken Sie "Enter" auf Ihrer Tastatur.
${project.name} gibt Ihnen in wenigen Sekunden alle verfügbaren Ergebnisse aus.`

------------------------------------------------------------------------

### Zugriff auf OpenAccess-Publikationsdienste

**Ziel:** Sie lernen, wie Sie über die Detailansicht einer Publikation
in ${project.name} nach der digitalen Ausgabe in einer
OpenAccess-Datenbank suchen können.  

**Voraussetzung:** Sie müssen die gewünschte Publikation in der
Detailansicht aufgerufen haben. Dazu klicken Sie einfach auf den Namen
der Publikation, dann werden Sie auf die Detailansicht weitergeleitet.  

![ <File:fulltextwue.png>](wueresearch/PublikationenLesezeichenManagen/PublikationenLesezeichen/fulltextwue.png  "fig: File:fulltextwue.png")  

1.  Klicken Sie auf das Auswahlmenü bei **"Suchen auf"**. Ein Untermenü
erscheint.

2.  Wählen Sie aus der angezeigten Liste die OpenAccess-Datenbank, die
    Sie nach diesem Artikel durchsuchen möchten.

}

------------------------------------

[ Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink")
    

