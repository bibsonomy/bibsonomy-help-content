<!-- en: BookmarkPublicationManagement/PublicationSearch -->

-----------------

[`Zurück zur Verwaltung von Publikationen und Lesezeichen geht es hier.`](LesezeichenPublikationenManagen "wikilink")

---------------------

## Erweiterte Suche nach Publikationen
----------------

**Ziel:** Diese Anleitung zeigt Ihnen, wie Sie die **erweiterte Suche** von ${project.name} benutzen können.

1. Wählen Sie im obigen Hauptmenü (siehe [Benutzeroberfläche](Programmoberfläche "wikilink")) den Reiter **"Publikationen"** aus. Nun sehen Sie eine Suchleiste sowie eine Liste der neuesten Veröffentlichungen. 
2. Um die erweiterte Suche zu nutzen, drücken Sie auf den blauen Schriftzug **"Erweiterte Suche"**. Dieser wird begleitet von einem blauen Pfeil, der nach unten zeigt. 
3. Dadurch klappt eine neue Suchmaske auf, die Ihnen neue Möglichkeiten bietet.

![ <File:suchmaske.png>](PublikationenSuchen/suchmaske.png  "fig: File:suchmaske.png")  

Hier können Sie folgende Möglichkeiten nutzen, um zielgerichtet nach Publikationen bei ${project.name} zu suchen.  
    - **"Suchfeld"**: Per Klick auf den Knopf können Sie die gewünschte Suchkategorie auswählen (bspw. Titel, Autor, Jahr, ... ).  
    - **"Eintragstyp"**: Per Klick auf den Knopf können Sie filtern, welchen Eintragstyp die Suchergebnisse haben sollen (bspw. Artikel, Review, Monographie, ...).  
    - **"Jahr"**: Hier können Sie die Sucheergebnisse nach einem Jahr filtern. Möchten Sie nach einem Zeitraum filtern, dann klicken Sie rechts auf **"Zeitraum wählen"**. Nun können Sie zwei Jahreszahlen angeben, um die gewünschte Zeitspanne zu beschreiben.  
    - **"UND/ODER"**: Hier können Sie entscheiden, ob alle Suchparameter im Ergebnis erfüllt sein sollen (UND) oder nur mindestens eines (ODER).  

Schließlich können Sie per Drücken des blauen Knopfes **"Hinzufügen"** ihre erweiterte Suche bestätigen. Dadurch werden die spezifizierten Suchparameter an Ihren Suchbegriff angehängt.
  
Möchten Sie weitere Informationen zu den Suchparametern, die Sie dank dieser 
Suchmaske automatisch auswählen können, dann besuchen Sie [diese Hilfsseite](SuchenHilfe "wikilink").

------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink")