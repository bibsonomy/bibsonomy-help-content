<!-- en: BookmarkPublicationManagement/PublicationAdd -->

-----------------

[`Zurück zur Verwaltung von Publikationen und Lesezeichen geht es hier.`](LesezeichenPublikationenManagen "wikilink")

---------------------

## Hinzufügen von Publikationen
----------------

**Ziel:** Diese Anleitung zeigt Ihnen, wie Sie eine neue Publikation in
${project.name} hinzufügen, um z. B. später daraus eine Literaturliste
zu erstellen.

1.  Bewegen Sie Ihren Mauszeiger auf den Menüpunkt **"Eintragen"** (im [
    Hauptmenü](Programmoberfläche "wikilink")). Ein Untermenü klappt auf.
   
2.  Klicken Sie im Untermenü mit der linken Maustaste auf **"Publikation
    hinzufügen"**.
    
3.  Sie haben fünf verschiedene Möglichkeiten,
    Publikationen/Literaturverweise einzutragen:

       -   **Per Hand:** Sie können alle Eintragungen manuell vornehmen.
		1.  Geben Sie alle geforderten Daten in die entsprechenden Felder ein und klicken Sie anschließend auf **"Weiter"**.
    	2.  Im folgenden Dialog erhalten Sie die Möglichkeit, weiterführende Informationen einzugeben. Sie können aber auch direkt auf  **"Speichern"** klicken, um die Publikation einzutragen.

    -   **BibTeX/EndNote-Schnipsel:** Diese Option ermöglicht es Ihnen,
        die Daten aus fast jedem Programm zur Literaturverwaltung zu
        importieren (z. B. auch Citavi). Ein Tutorial dazu finden Sie [ hier](EintragPublikationen/LiteraturlisteImportieren "wikilink").

    -   **Datei hochladen:** Mit dieser Option können Sie EndNote- und
        BibTeX-Dateien direkt in ${project.name} hochladen und
        automatisch importieren lassen. Ein Tutorial dazu finden Sie [ hier](EintragPublikationen/LiteraturlisteImportieren "wikilink").

       -   **ISBN/DOI - die schnellste Variante:** Ermöglicht es Ihnen, Publikationen anhand von ISBN, ISSN oder DOI einzutragen.  
        1.  Tragen Sie in das entsprechende Feld entweder ISBN, ISSN oder DOI ein.  
     	2.  Klicken Sie auf **"Weiter"**.  
     	3.  Die Daten werden automatisch aus verschiedenen Verbundkatalogen abgerufen.  
     	4.  Wenn Sie mit allen angezeigten Daten einverstanden sind, dann klicken Sie auf **"Speichern"**.  

${project.theme == "bibsonomy"}{

    -   **Code scannen:** Diese Option ermöglicht es Ihnen, eine
        Publikation durch Scannen ihres ISBN-Codes hinzuzufügen. Eine
        detaillierte Anleitung dazu finden Sie [hier](EintragPublikationen/EintragScan "wikilink").

![ <File:add_de.jpg>](PublikationenHinzufügen/add_de.jpg  "fig: File: add_de.jpg")  

}

${project.theme == "puma"}{

    -   **Code scannen:** Diese Option ermöglicht es Ihnen, eine
        Publikation durch Scannen ihres ISBN-Codes hinzuzufügen. Eine
        detaillierte Anleitung dazu finden Sie [hier](EintragPublikationen/EintragScan "wikilink").

![ <File:add_de.jpg>](PublikationenHinzufügen/add_de.jpg  "fig: File: add_de.jpg")  

}

${project.theme == "wueresearch"}{

![ <File:addpublwue.png>](wueresearch/LesezeichenPublikationenManagen/PublikationenHinzufügen/addpublwue.png  "fig: File: addpublwue.png")  

}

------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink")