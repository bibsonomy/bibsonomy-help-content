<!-- en: JabrefInstall -->

## JabRef-Plugin - Installation
----------------------------

**Achtung:** Seit der JabRef-Version 2.12 werden **keine Plugins** mehr in JabRef unterstützt. Das ${project.name}-Plugin funktioniert in den neueren Versionen daher leider nicht mehr.


[JabRef](http://www.jabref.org/) ist ein OpenSource-Literaturverwaltungsprogramm, das BibTeX als Standardformat
benutzt. JabRef kann durch ein Plugin mit ${project.name} verknüpft
werden, das es ermöglicht, in JabRef direkt auf Publikationen aus
${project.name} zuzugreifen.  

In diesem Tutorial zeigen wir Ihnen, wie Sie JabRef und das ${project.name}-Plugin herunterladen und konfigurieren. Informationen zum Austausch von Daten zwischen JabRef und ${project.name} finden Sie
unter [JabRef - Datenaustausch](Jabref "wikilink"). Wenn Sie Fragen zu dem Plugin haben, besuchen Sie unsere [Kontaktseite](Contact "wikilink").


------------------------------------------------------------------------

### Allgemeine Informationen

Für allgemeine Informationen zum JabRef-Plugin, besuchen Sie [diese Seite](JabrefInstallieren/JabrefAllgemein "wikilink").

------------------------------------------------------------------------

### Installation

Eine Anleitung zur Installation gibt es [hier](JabrefInstallieren/JabrefInstallation "wikilink").

------------------

### Konfiguration

Bevor Sie mit dem Plugin arbeiten können, müssen Sie es [konfigurieren](JabrefInstallieren/JabrefKonfiguration "wikilink").

------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink") 

  


