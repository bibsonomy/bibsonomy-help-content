<!-- en: CreateNewAccount -->

## Einen neuen Benutzeraccount anlegen
-----------------------------------

**Ziel:** Die folgende Anleitung zeigt Ihnen, wie Sie einen neuen
${project.name}-Benutzeraccount erstellen. 
Dieser ist nötig, um ${project.name} aktiv nutzen zu können.

1.  Klicken Sie hier, um einen [neuen Benutzeraccount zu
    erstellen](${project.home}register "wikilink").
    
2.  Füllen Sie alle notwendigen Felder zu **„Persönlichen
    Informationen“** aus.  
    
    ![ <File:1.png>](neuennutzeraccountanlegen/1.png  "fig: File:1.png")  
    
3.  **„Captcha“**: Klicken Sie in das leere weiße Kästchen um zu bestätigen,
	dass Sie kein Roboter sind: ein grünes Häkchen erscheint.
    
4.  Um ${project.name} nutzen zu können, müssen Sie die **Datenschutz-
    und Nutzungsbedingungen** von ${project.name} akzeptieren. Klicken
    Sie dazu auf das graue Kästchen neben "Ich akzeptiere die
    Datenschutzbestimmungen und die Nutzungsbedingungen
    von ${project.name}".
    
5.  Klicken Sie unten auf **„Registrieren“**.  
    
    ![ <File:2.png>](neuennutzeraccountanlegen/2.png  "fig: File:2.png")  
    
6.  Sollten Sie die Eingabe wichtiger Daten vergessen haben oder sind
    bestimmte Daten nicht korrekt, dann erhalten
    Sie eine Fehlermeldung. Sie müssen diesen Fehler korrigieren
    (Tipps/Hilfe dazu finden Sie in der Fehlermeldung). Sobald der
    Fehler beseitigt wurde, klicken Sie erneut unten auf **„Registrieren“**.
    
7.  Wenn alle Daten korrekt sind, dann begrüßt Sie ${project.name} als
    neuen Benutzer.
    
8.  **Wichtig!** – Bevor es richtig losgehen kann, schauen Sie bitte in
    Ihrem E-Mail-Postfach nach, dort finden Sie
    eine **Bestätigungs-E-Mail**. Diese E-Mail enthält einen Link, den Sie
    anklicken müssen. Dadurch stellen wir sicher, dass Ihre
    E-Mail-Adresse korrekt ist. Nachdem Ihre E-Mail-Adresse überprüft
    wurde, können Sie ${project.name} nutzen.

Mit Ihrem neuen Benutzer-Account können Sie sich am System anmelden und
alle Funktionen kostenlos nutzen.

----------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink")

