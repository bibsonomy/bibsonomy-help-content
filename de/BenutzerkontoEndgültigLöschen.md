<!-- en: DeleteAccount -->

## Wie kann ich mein ${project.name}-Konto löschen?
-------------------------------------------------

**Achtung:** Wenn Sie Ihren Account gelöscht haben, kann das
nicht mehr rückgängig gemacht werden.

Um Ihr Konto (Account) zu löschen, befolgen Sie diese Schritte:  

-   Melden Sie sich bei ${project.name} an.

-   Gehen Sie zur [ Einstellungsseite](${project.home}settings "wikilink") und
    wählen Sie den Reiter "Einstellungen".

-   Im untersten Absatz können Sie Ihren Account löschen. Geben Sie dazu
    auf die Frage "Sind Sie sicher?" die Antwort **"yes"** in das
    Textfeld ein und klicken Sie auf "Konto löschen".

![ <File:kontoloeschen.png>](benutzerkontoendgültiglöschen/kontoloeschen.png  "fig: kontoloeschen.png")

------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink")
  
  
