<!-- en: Integration -->

## Integration mit anderen Webseiten und Systemen
----------------------------------------------

${project.name} lässt sich in andere Webseiten, Content Management
Systeme und Referenzverwaltungssysteme integrieren. Außerdem können Sie
${project.name} mit Ihrer eigenen Software verbinden, indem Sie einen
unserer API (application programming interface)-Clients benutzen. Die
folgende Liste beschreibt Möglichkeiten, ${project.name} mit
verschiedenen Systemen und Technologien zu verbinden. Diese umfassen
Codeschnipsel, aber auch komplette, einfach zu installierende Plugins
oder API-Clients.

------------------------------------------------------------------------

### Browser

Mit unseren ${project.name} Browser Plugins ist es sehr einfach,
Lesezeichen bei Webseiten und Publikationen zu setzen. Der
Navigationsleiste des Browser werden hierbei drei nützliche Buttons
hinzugefügt. Im Moment gibt es Plugins für Chrome/Chromium, für Firefox und für Safari.
Falls Sie kein Plugin installieren möchten oder einen anderen Browser benutzen, könnten
JavaScript [Bookmarklets](${project.home}buttons#buttons.Bookmarklets "wikilink") für Sie eine Alternative sein.

Für eine Hilfestellung zur Integration in Browsern, folgen Sie [diesem Link](Integration/IntegrationBrowser "wikilink").

------------------------------------------------------------------------

### Programme

Zur Integration von ${project.name} in Programmen 
(Citavi, Emacs, JabRef, KBibTeX, Sublime Text und TeXlipse), 
folgen Sie [dieser Anleitung](Integration/IntegrationProgramme "wikilink").

------------------------------------------------------------------------

### Webseiten

Wie ${project.name} in Webseiten (Confluence, GoogleDocs, GoogleScholar und Moodle) integriert werden kann, finden Sie [hier](Integration/IntegrationWebseiten "wikilink").


------------------------------------------------------------------------

### Eigene Webseite

Es gibt mehrere Möglichkeiten, Inhalte aus ${project.name} oder Links
zu ${project.name} auf Ihrer eigenen Webseite zu integrieren. Folgen Sie dazu [diesem Link](Integration/IntegrationEigeneWebseite "wikilink").

------------------------------------------------------------------------

### Bibliotheken

#### Digitale Bibliotheken

Mit ${project.name} können Sie Literatur-Informationen aus digitalen,
öffentlichen Online-Katalogen (OPAC) einfach und schnell extrahieren und
speichern. Welche Bibliotheken mit ${project.name} verknüpft sind,
finden Sie [ hier](DigitaleBibliotheken "wikilink").

#### Discovery Service

Viele Bibliotheken deutschland-/weltweit setzen auf sogenannte *Resource
Discovery Services* (RDS) zur Bestandsverwaltung. ${project.name}
bietet eine direkte RDS-Schnittstelle. Wie Sie diese nutzen können,
erfahren Sie [ hier](DiscoveryService "wikilink").

#### OpenURL-Resolver

Sie können Ihr ${project.name}-Nutzerkonto mit dem URL-Resolver Ihrer
Bibliothek verknüpfen. Damit können alle URLs, auch in
Publikationseinträgen, über Ihre Bibliothek aufgelöst werden. Eine
Einleitung dazu finden Sie [ hier](OpenURL-Resolver "wikilink").

------------------------------------------------------------------------

### Anwendungen

#### Bookmark Exporter

Mit Hilfe der auf [Python](http://www.python.org/) basierenden [Exportfunktion](https://bitbucket.org/bibsonomy/bibsonomy-python/src/tip/onefile.py?fileviewer=file-view-default) können Sie Lesezeichen und Publikationen herunterladen und in einer 
Datei speichern. 
Lesen Sie dazu mehr in der [Benutzungsanleitung](https://bitbucket.org/bibsonomy/bibsonomy-python).

#### Link-Checker

Dieser [Link Status Checker](http://www.christianschenk.org/projects/linkstatuschecker/) zeigt an, ob Ihre ${project.name}-Lesezeichen noch herunterladbar sind.

#### RSS

[RSS](http://de.wikipedia.org/wiki/RSS) ist ein Format, das Änderungen auf einer Seite in Form von Feeds anzeigen kann.
In unserem [Tutorial](RSS-FeedsAbonnieren "wikilink") zeigen wir, wie man in ${project.name} einen RSS-Feed abonniert.
Auf diese Weise bleiben Sie über Änderungen und Neuigkeiten von bestimmten Tags,
Nutzern oder Gruppen informiert.

------------------------------------------------------------------------

### REST-API

${project.name} bietet einen Webservice auf Basis des Representational
State Transfer (REST) an. REST ist ein Architekturstil für verteilte
Softwaresysteme, bei dem eine einheitliche Schnittstelle die Interaktion
erleichtert. Die REST-API ist für Softwareentwickler gedacht, deren
Anwendungen mit ${project.name} interagieren sollen.  
Für eine Anleitung, folgen Sie [diesem Link](Integration/IntegrationAPI "wikilink").

------------------------------------------------------------------------

### Sonstige

#### Android

Die Android-Applikation [Bibsonomy Poster](https://play.google.com/store/apps/details?id=net.gromgull.android.bibsonomyposter)
von Gunnar Aastrand Grimnes macht es möglich, mit einem Klick ein
Lesezeichen auf einem Androidgerät hinzuzufügen. Das Code-Repository
befindet sich [hier](https://github.com/gromgull/bibsonomy-poster).

#### OAuth

[OAuth](http://oauth.net/) ist ein etabliertes Protokoll für sichere
API-Autorisierung, die es Nutzern ermöglicht, einer dritten Anwendung
den Zugriff auf ihre Daten zu erlauben, ohne, dass sie ihre
Anmeldeinformationen außerhalb von ${project.name} angeben müssen. Wie
Sie eine Verbindung zwischen Ihrer Anwendung und ${project.name} über
OAuth aufbauen können, erfahren Sie [ hier](OAuth "wikilink").

------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink") 

