<!-- en: GroupFunctionsAdmin-->

## Administrator-Funktionen
---------------------------

Als Administrator einer Gruppe haben Sie Zugriff auf einige Funktionen,
die für andere Mitglieder der Gruppe nicht sichtbar sind. Klicken Sie
auf **"Gruppen"** im linken Hauptmenü, bewegen Sie den Mauszeiger auf den
Namen Ihrer Gruppe und klicken Sie dann auf **"Einstellungen"**. Dort können Sie
folgende Aktionen durchführen.

------------------------------------------------------------------------

### Einstellungen bearbeiten

Nur der Administrator einer Gruppe kann die Gruppeneinstellungen verändern. Auf [diese Funktionen](GruppenfunktionenAdmin/GruppeAdminEinstellungen "wikilink") können Sie zugreifen.

------------------------------------------------------------------------

### Mitgliederliste verwalten

Möchten Sie die Mitgliederliste verwalten und Änderungen vornehmen, befolgen Sie [diese Anleitung](GruppenfunktionenAdmin/GruppeAdminMitgliederliste "wikilink").

------------------------------------------------------------------------

### Einladungen

Diese [Hilfeseite](GruppenfunktionenAdmin/GruppeAdminEinladungen "wikilink") erklärt Ihnen, wie Sie Einladungen und Beitrittsgesuche verwalten können.

------------------------------------------------------------------------

### Lebenslauf bearbeiten

Als Administrator können Sie den Lebenslauf der Gruppe bearbeiten. Dies
funktioniert genauso wie das [Bearbeiten Ihres eigenen Lebenslaufes](LebenslaufÄndern "wikilink").

------------------------------------------------------------------------

### Gruppe auflösen

Das Auflösen einer Gruppe erfordert die Durchführung [dieser Schritte](GruppenfunktionenAdmin/GruppeAdminAufloesen "wikilink").

------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink") 