<!-- en: TagLayout -->

## Das Layout Ihrer Tagbox
-----------------------

Um das Layout Ihrer Tagbox zu ändern, klicken Sie im Personenmenü im [
rechten Hauptmenü](Programmoberfläche "wikilink") auf [Einstellungen](Einstellungen "wikilink") und dann auf den Reiter
**"Einstellungen"**. Unter **"Layouts Ihrer Tagbox und Ihrer
Eintragslisten"** haben Sie einige Möglichkeiten, das Layout Ihrer
Tagbox zu verändern.  
  
![ <File:formular.png>](taglayout/formular.png  "fig: File:formular.png")

- **Tag-Anzeige:** Tags können entweder als **Wolke** (cloud) oder als
**Liste** angezeigt werden.  
![<File:tagcloud-list.png>](taglayout/tagcloud-list.png  "fig: File:tagcloud-list.png") 

- **Tag-Reihenfolge:** Tags können entweder nach **Alphabet** oder nach
ihrer **Häufigkeit** sortiert werden.  
![<File:tagalpha-freq.png>](taglayout/tagalpha-freq.png  "fig: File:tagalpha-freq.png") 

- **Tag-Tipps:** Bewegt man den Mauszeiger auf ein Tag, so wird ein Tipp
(tooltip) angezeigt. Optionen sind hier **Ja** oder **Nein**.  
  
- **Tag-Auswahl:** Die Menge der dargestellten Tags wird entsprechend
Ihrer Auswahl beschränkt. Es gibt hier zwei Optionen:

    -   **Top X:** Nur die X häufigsten Tags werden angezeigt.
    -   **Min. Häufigkeit:** Nur die Tags, deren Häufigkeit mindestens X
        ist, werden angezeigt.  
        Den Schrankenwert X können Sie unten festlegen.  
          
-   **Schrankenwert:** Je nach Ihrer Option bei der Tag-Auswahl hat
    dieser Wert verschiedene Bedeutungen.
    -   **Top X:** die Anzahl der dargestellten Tags.  
        Beispiel: Bei einem Schrankenwert von X = 5 werden die fünf
        häufigsten Tags angezeigt.  
    -   **Min. Häufigkeit:** die Mindesthäufigkeit eines Tags.  
        Beispiel: Bei einem Schrankenwert von X = 5 werden nur Tags
        angezeigt, deren Häufigkeit größer oder gleich fünf ist.  
          
-   **Einträge pro Seite:** Hier können Sie die maximale Anzahl der Einträge, die angezeigt werden sollen, bestimmen.
  
------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink") 

