<!-- en: ExportData -->

## Literaturliste exportieren
--------------------------

Der Export bereits erstellter Lesezeichen/Publikationslisten aus
${project.name} in andere Programme ist jederzeit problemlos möglich.
Das international einheitliche Datenformat, welches von allen gängigen
Programmen zur Literaturverwaltung unterstützt wird, heißt BibTeX. Der
Export erfolgt in zwei Schritten. Exportieren Sie zuerst aus
${project.name} die gewünschten Lesezeichen/Publikationen, dann
importieren Sie die BibTeX-Daten in das gewünschte Programm. Diese Seite
gibt Ihnen dazu hilfreiche Tipps und zeigt am Beispiel häufig
verwendeter Programme, wie der Export aus ${project.name} funktioniert.

-------------------------

### Literaturverzeichnis exportieren - allgemein

#### Zitationen kopieren

${project.theme == "bibsonomy"}{

Wenn Sie nur eine einzelne Publikation zitieren möchten, haben Sie die Möglichkeit, die 
dazugehörige Zitation zu Ihrer lokalen Zwischenablage hinzuzufügen.

Hierfür gibt es zwei Möglichkeiten:

**A:** Klicken Sie auf den nach unten zeigenden Pfeil im Menü, der sich unten rechts
unter jedem Publikationseintrag befindet. Sie können nun eines Ihrer [bevorzugten Exportformate](BevorzugteExportformate "wikilink") wählen. 
Anschließend erscheint ein Fenster. Klicken Sie auf **Zitation zur lokalen Zwischenablage hinzufügen**, 
um die Zitation zu kopieren und in Ihr gewünschtes Programm zu importieren.

![ <File:cit1.png>](literaturlisteexportieren/cit1.png  "fig: File:cit1.png")

**B:** Klicken Sie auf den Namen der Publikation, die Sie exportieren möchten.
Am unteren Ende der Seite mit den Publikationsdetails finden Sie die Möglichkeit, 
**diese Publikation zu zitieren**, indem Sie auf die Schaltfläche am unteren Ende der Seite klicken.

-----------------------------

### Literaturverzeichnis aus Publikationen zusammenstellen

**Ziel:** Diese Anleitung zeigt Ihnen, wie Sie Literaturverzeichnisse
aus Publikationen zusammenstellen. Außerdem erfahren Sie, wie Sie das
zusammengestellte Literaturverzeichnis in ${project.name} speichern
und/oder exportieren können.

**Voraussetzung:** Wenn Sie noch nicht damit vertraut sind, wie man in
${project.name} Lesezeichen und Publikationen sucht und anzeigt, dann
lesen Sie sich bitte vorher folgende [Anleitung zu Lesezeichen und Publikationen](LesezeichenPublikationenManagen "wikilink").  

Um Publikationen zu Ihrer Literaturliste hinzuzufügen und somit die Daten mehrerer
Publikationen zu exportieren, gehen Sie
folgendermaßen vor:

1.  Wenn Sie in der Suchleiste rechts oben einen Begriff eingegeben
    haben, werden Ihnen als Ergebnisse auf der linken Seite Lesezeichen
    und auf der rechten Seite Publikationen angezeigt. Bei jeder
    Publikation befindet sich eine Symbolleiste.
    
2.  Klicken Sie auf das Ordnersymbol
    (wenn Sie die Maus über dieses Symbol bewegen, erscheint die Anzeige
    **"Diese Publikation zur Ablage hinzufügen“**).  
    
    ![ <File:1_neu.png>](literaturlisteexportieren/1_neu.png  "fig: File:1_neu.png") 
    
3.  Die Publikation ist nun in Ihrer Ablage gespeichert. Um die Ablage
    aufzurufen, klicken Sie im rechten [Hauptmenü](Programmoberfläche "wikilink") auf das Personensymbol und
    im daraufhin erscheinenden Untermenü auf den Punkt **"Ablage“**.
    Neben dem Personensymbol wird außerdem die aktuelle Anzahl der
    Publikationen in der Ablage angezeigt (die Zahl im roten Kreis).  
    
    ![ <File:2_neu.png>](literaturlisteexportieren/2_neu.png  "fig: File:2_neu.png")  

Befolgen Sie die Schritte 1 bis 3 für alle Publikationen, die Sie zu
Ihrer Literaturliste hinzufügen möchten. Um Ihre Literaturliste zu
exportieren, rufen Sie wie oben unter Schritt 3 beschrieben die Ablage auf.

--------------------

### Literaturverzeichnis exportieren 

  ![ <File:3_neu.png>](literaturlisteexportieren/3_neu.png  "fig: File:3_neu.png")

1.  Klicken Sie in der Ablage auf das Pfeilsymbol ganz rechts - es werden verschiedene 			Exportoptionen angezeigt.
    
2.  Wählen Sie unter den Exportoptionen das gewünschte Format aus,
    das Sie zum Exportieren Ihrer Literaturliste nutzen möchten.
    ${project.name} zeigt Ihnen die häufigsten Exportformate direkt an
    (RSS, BibTeX, RDF). Mit einem Klick auf **"mehr..."** haben Sie die
    Möglichkeit, Ihre Literaturliste in weitere Export-Formate zu
    exportieren (z. B. HTML).
    
    **A:** Wählen Sie ein Exportformat aus der Box oder aus Ihren Favorite layouts.
    
    **B:** Wählen Sie die Anzahl der Posts, die Sie exportieren wollen. Sie können entweder eine 		Anzahl an Posts aus den angezeigten Boxen wählen **bevor** Sie ein Exportformat auswählen, oder 	Sie fügen `?items=variousnumber` an das Ende der URL an, **nachdem** Sie ein Exportformat 			wählen.
    
    (**Beispiel:** Wenn Sie 10 Einträge exportieren wollen, fügen Sie `?items=10` an das Ende der URL an, wollen Sie 20 Einträge exportieren `?items=20` usw.)
    
     ![ <File:exportformats.png>](literaturlisteexportieren/exportformats.png  "fig: File:exportformats.png")
    
    
    **Hinweis**: Wir empfehlen Ihnen als Export-Format
    das BibTeX-Format. Dieses Format ist so weit verbreitet, dass man
    hier von einem De-Facto-Standard in Sachen Literaturverwaltung
    sprechen kann.  
    
3.  Nachdem Sie ein Exportformat angeklickt haben, wird je nach
    gewähltem Format sofort ein Fenster angezeigt, wo Sie die Datei
    speichern können. Manchmal wird jedoch Ihre Ablage in dem gewählten
    Format angezeigt. Klicken Sie mit der rechten Maustaste auf die
    Seite und wählen Sie **„Speichern unter…“**, um die Datei zu
    speichern.  
    
    ![ <File:export1.png>](literaturlisteexportieren/export1.png  "fig: File:export1.png")
    
4.  Um die Ablage für die nächste Textrecherche zu bereinigen, klicken
    Sie auf das Ordnersymbol in der Ablage und wählen Sie die
    Option **„Ablage leeren“**. Bitte denken Sie daran, dass diese
    Aktion nicht rückgängig gemacht werden kann, daher sollten Sie alle
    Daten vorher sichern.

-------------------------------

}

${project.theme == "puma"}{

Wenn Sie nur eine einzelne Publikation zitieren möchten, haben Sie die Möglichkeit, die 
dazugehörige Zitation zu Ihrer lokalen Zwischenablage hinzuzufügen.

Hierfür gibt es zwei Möglichkeiten:

**A:** Klicken Sie auf den nach unten zeigenden Pfeil im Menü, der sich unten rechts
unter jedem Publikationseintrag befindet. Sie können nun eines Ihrer [bevorzugten Exportformate](BevorzugteExportformate "wikilink") wählen. 
Anschließend erscheint ein Fenster. Klicken Sie auf **Zitation zur lokalen Zwischenablage hinzufügen**, 
um die Zitation zu kopieren und in Ihr gewünschtes Programm zu importieren.

![ <File:cit1.png>](literaturlisteexportieren/cit1.png  "fig: File:cit1.png")

**B:** Klicken Sie auf den Namen der Publikation, die Sie exportieren möchten.
Am unteren Ende der Seite mit den Publikationsdetails finden Sie die Möglichkeit, 
**diese Publikation zu zitieren**, indem Sie auf die Schaltfläche am unteren Ende der Seite klicken.

-----------------------------

### Literaturverzeichnis aus Publikationen zusammenstellen

**Ziel:** Diese Anleitung zeigt Ihnen, wie Sie Literaturverzeichnisse
aus Publikationen zusammenstellen. Außerdem erfahren Sie, wie Sie das
zusammengestellte Literaturverzeichnis in ${project.name} speichern
und/oder exportieren können.

**Voraussetzung:** Wenn Sie noch nicht damit vertraut sind, wie man in
${project.name} Lesezeichen und Publikationen sucht und anzeigt, dann
lesen Sie sich bitte vorher folgende [Anleitung zu Lesezeichen und Publikationen](LesezeichenPublikationenManagen "wikilink").  

Um Publikationen zu Ihrer Literaturliste hinzuzufügen und somit die Daten mehrerer
Publikationen zu exportieren, gehen Sie
folgendermaßen vor:

1.  Wenn Sie in der Suchleiste rechts oben einen Begriff eingegeben
    haben, werden Ihnen als Ergebnisse auf der linken Seite Lesezeichen
    und auf der rechten Seite Publikationen angezeigt. Bei jeder
    Publikation befindet sich eine Symbolleiste.
    
2.  Klicken Sie auf das Ordnersymbol
    (wenn Sie die Maus über dieses Symbol bewegen, erscheint die Anzeige
    **"Diese Publikation zur Ablage hinzufügen“**).  
    
    ![ <File:1_neu.png>](literaturlisteexportieren/1_neu.png  "fig: File:1_neu.png") 
    
3.  Die Publikation ist nun in Ihrer Ablage gespeichert. Um die Ablage
    aufzurufen, klicken Sie im rechten [Hauptmenü](Programmoberfläche "wikilink") auf das Personensymbol und
    im daraufhin erscheinenden Untermenü auf den Punkt **"Ablage“**.
    Neben dem Personensymbol wird außerdem die aktuelle Anzahl der
    Publikationen in der Ablage angezeigt (die Zahl im roten Kreis).  
    
    ![ <File:2_neu.png>](literaturlisteexportieren/2_neu.png  "fig: File:2_neu.png")  

Befolgen Sie die Schritte 1 bis 3 für alle Publikationen, die Sie zu
Ihrer Literaturliste hinzufügen möchten. Um Ihre Literaturliste zu
exportieren, rufen Sie wie oben unter Schritt 3 beschrieben die Ablage auf.

--------------------

### Literaturverzeichnis exportieren 

  ![ <File:3_neu.png>](literaturlisteexportieren/3_neu.png  "fig: File:3_neu.png")

1.  Klicken Sie in der Ablage auf das Pfeilsymbol ganz rechts - es werden verschiedene 			Exportoptionen angezeigt.
    
2.  Wählen Sie unter den Exportoptionen das gewünschte Format aus,
    das Sie zum Exportieren Ihrer Literaturliste nutzen möchten.
    ${project.name} zeigt Ihnen die häufigsten Exportformate direkt an
    (RSS, BibTeX, RDF). Mit einem Klick auf **"mehr..."** haben Sie die
    Möglichkeit, Ihre Literaturliste in weitere Export-Formate zu
    exportieren (z. B. HTML).
    
    **A:** Wählen Sie ein Exportformat aus der Box oder aus Ihren Favorite layouts.
    
    **B:** Wählen Sie die Anzahl der Posts, die Sie exportieren wollen. Sie können entweder eine 		Anzahl an Posts aus den angezeigten Boxen wählen **bevor** Sie ein Exportformat auswählen, oder 	Sie fügen `?items=variousnumber` an das Ende der URL an, **nachdem** Sie ein Exportformat 			wählen.
    
    (**Beispiel:** Wenn Sie 10 Einträge exportieren wollen, fügen Sie `?items=10` an das Ende der URL an, wollen Sie 20 Einträge exportieren `?items=20` usw.)
    
     ![ <File:exportformats.png>](literaturlisteexportieren/exportformats.png  "fig: File:exportformats.png")
    
    
    **Hinweis**: Wir empfehlen Ihnen als Export-Format
    das BibTeX-Format. Dieses Format ist so weit verbreitet, dass man
    hier von einem De-Facto-Standard in Sachen Literaturverwaltung
    sprechen kann.  
    
3.  Nachdem Sie ein Exportformat angeklickt haben, wird je nach
    gewähltem Format sofort ein Fenster angezeigt, wo Sie die Datei
    speichern können. Manchmal wird jedoch Ihre Ablage in dem gewählten
    Format angezeigt. Klicken Sie mit der rechten Maustaste auf die
    Seite und wählen Sie **„Speichern unter…“**, um die Datei zu
    speichern.  
    
    ![ <File:export1.png>](literaturlisteexportieren/export1.png  "fig: File:export1.png")
    
4.  Um die Ablage für die nächste Textrecherche zu bereinigen, klicken
    Sie auf das Ordnersymbol in der Ablage und wählen Sie die
    Option **„Ablage leeren“**. Bitte denken Sie daran, dass diese
    Aktion nicht rückgängig gemacht werden kann, daher sollten Sie alle
    Daten vorher sichern.

-------------------------------

}

${project.theme == "wueresearch"}{

Wenn Sie nur eine einzelne Publikation zitieren möchten, haben Sie die Möglichkeit, die 
dazugehörige Zitation zu Ihrer lokalen Zwischenablage hinzuzufügen.

Hierfür gibt es drei Möglichkeiten:

**A:** Klicken Sie auf den nach unten zeigenden Pfeil im Menü, der sich unten rechts
unter jedem Publikationseintrag befindet. Sie können nun eines Ihrer [bevorzugten Exportformate](BevorzugteExportformate "wikilink") wählen. 
Anschließend erscheint ein Fenster. Klicken Sie auf **Zitation zur lokalen Zwischenablage hinzufügen**, 
um die Zitation zu kopieren und in Ihr gewünschtes Programm zu importieren.

![ <File:maincitwue.png>](wueresearch/LiteraturlisteExportieren/maincitwue.png  "fig: File:maincitwue.png")

**B:** Klicken Sie auf den Namen der Publikation, die Sie exportieren möchten.
Am unteren Ende der Seite mit den Publikationsdetails finden Sie die Möglichkeit, 
**diese Publikation zu zitieren**, indem Sie auf die Schaltfläche am unteren Ende der Seite klicken.

**C:** Befindet sich die Publikation in Ihrer Ablage oder Literaturliste, dann können sie alternativ auf den nach unten zeigenden Pfeil klicken, welcher sich rechts unter jedem Publikationseintrag befindet. Hier haben Sie jedoch nur Zugriff auf eine kleinere Auswahl der gängigsten Zitationsstile.

-----------------------------

### Literaturverzeichnis aus Publikationen zusammenstellen

**Ziel:** Diese Anleitung zeigt Ihnen, wie Sie Literaturverzeichnisse
aus Publikationen zusammenstellen. Außerdem erfahren Sie, wie Sie das
zusammengestellte Literaturverzeichnis in ${project.name} speichern
und/oder exportieren können.

**Voraussetzung:** Wenn Sie noch nicht damit vertraut sind, wie man in
${project.name} Lesezeichen und Publikationen sucht und anzeigt, dann
lesen Sie sich bitte vorher folgende [Anleitung zu Lesezeichen und Publikationen](LesezeichenPublikationenManagen "wikilink").  

Um Publikationen zu Ihrer Literaturliste hinzuzufügen und somit die Daten mehrerer
Publikationen zu exportieren, gehen Sie
folgendermaßen vor:

1.  Wenn Sie in der Suchleiste rechts oben einen Begriff eingegeben
    haben, werden Ihnen als Ergebnisse auf der linken Seite Lesezeichen
    und auf der rechten Seite Publikationen angezeigt. Bei jeder
    Publikation befindet sich eine Symbolleiste.
    
2.  Klicken Sie auf das Ordnersymbol
    (wenn Sie die Maus über dieses Symbol bewegen, erscheint die Anzeige
    **"Diese Publikation zur Ablage hinzufügen“**).  
    
    ![ <File:addclipwue.png>](wueresearch/LiteraturlisteExportieren/addclipwue.png  "fig: File:addclipwue.png") 
    
3.  Die Publikation ist nun in Ihrer Ablage gespeichert. Um die Ablage
    aufzurufen, klicken Sie im rechten [Hauptmenü](Programmoberfläche "wikilink") auf das Personensymbol und
    im daraufhin erscheinenden Untermenü auf den Punkt **"Ablage“**.
    Neben dem Personensymbol wird außerdem die aktuelle Anzahl der
    Publikationen in der Ablage angezeigt (die Zahl im roten Kreis).  
    
    ![ <File:clipboardwue.png>](wueresearch/LiteraturlisteExportieren/clipboardwue.png  "fig: File:clipboardwue.png")  

Befolgen Sie die Schritte 1 bis 3 für alle Publikationen, die Sie zu
Ihrer Literaturliste hinzufügen möchten. Um Ihre Literaturliste zu
exportieren, rufen Sie wie oben unter Schritt 3 beschrieben die Ablage auf.

--------------------

### Literaturverzeichnis exportieren 

  ![ <File:exportclipwue.png>](wueresearch/LiteraturlisteExportieren/exportclipwue.png  "fig: File:exportclipwue.png")

1.  Klicken Sie in der Ablage auf das Pfeilsymbol ganz rechts - es werden verschiedene 			Exportoptionen angezeigt.
    
2.  Wählen Sie unter den Exportoptionen das gewünschte Format aus,
    das Sie zum Exportieren Ihrer Literaturliste nutzen möchten.
    ${project.name} zeigt Ihnen die häufigsten Exportformate direkt an
    (RSS, BibTeX, RDF). Mit einem Klick auf **"mehr..."** haben Sie die
    Möglichkeit, Ihre Literaturliste in weitere Export-Formate zu
    exportieren (z. B. HTML).
    
    **A:** Wählen Sie ein Exportformat aus der Box oder aus Ihren Favorite layouts.
    
    **B:** Wählen Sie die Anzahl der Posts, die Sie exportieren wollen. Sie können entweder eine 		Anzahl an Posts aus den angezeigten Boxen wählen **bevor** Sie ein Exportformat auswählen, oder 	Sie fügen `?items=variousnumber` an das Ende der URL an, **nachdem** Sie ein Exportformat 			wählen.
    
    (**Beispiel:** Wenn Sie 10 Einträge exportieren wollen, fügen Sie `?items=10` an das Ende der URL an, wollen Sie 20 Einträge exportieren `?items=20` usw.)
    
     ![ <File:exportformats.png>](literaturlisteexportieren/exportformats.png  "fig: File:exportformats.png")
    
    
    **Hinweis**: Wir empfehlen Ihnen als Export-Format
    das BibTeX-Format. Dieses Format ist so weit verbreitet, dass man
    hier von einem De-Facto-Standard in Sachen Literaturverwaltung
    sprechen kann.  
    
3.  Nachdem Sie ein Exportformat angeklickt haben, wird je nach
    gewähltem Format sofort ein Fenster angezeigt, wo Sie die Datei
    speichern können. Manchmal wird jedoch Ihre Ablage in dem gewählten
    Format angezeigt. Klicken Sie mit der rechten Maustaste auf die
    Seite und wählen Sie **„Speichern unter…“**, um die Datei zu
    speichern.  
    
4.  Um die Ablage für die nächste Textrecherche zu bereinigen, klicken
    Sie auf das Ordnersymbol in der Ablage und wählen Sie die
    Option **„Ablage leeren“**. Bitte denken Sie daran, dass diese
    Aktion nicht rückgängig gemacht werden kann, daher sollten Sie alle
    Daten vorher sichern.

-------------------------------

}

Wenn Sie mehr darüber erfahren wollen, wie Sie Ihre ${project.name} Daten in ein häufig verwendetes Programm wie Microsoft Word
oder Bibliographix exportieren können, klicken Sie [hier](LiteraturlisteExportierenSpezifisch "wikilink"). 

------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink")

