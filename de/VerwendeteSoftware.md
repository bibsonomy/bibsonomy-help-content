<!-- en:UsedSoftware -->

## Verwendete Software
-----------------------------------------

  -   [Apache HTTPD](http://httpd.apache.org/) - kostenloser Open-Source Webserver

  -   [Apache Tomcat](https://tomcat.apache.org/) - - Ausführungsumgebung für Java-Code auf dem Server (Servlets) und Java Server Pages (JSP)
  
  -   [CSL](http://citationstyles.org/),  [CSL Style Repository](https://github.com/citation-style-language/styles) - XML-Sprache zur Beschreibung von Formaten für bibliografische Angaben und Zitierstile
  
  -   [Elasticsearch](https://www.elastic.co/guide/en/elasticsearch/reference/current/index.html) - Open-Source REST-basierte Such- und Analytik Engine auf Basis von Apache Lucene
  
  -   [HAProxy](http://www.haproxy.org/) - Open-Source Software, die einen hochverfügbaren Load Balancer und Proxy Server für TCP und HTTP- basierte Anwendungen bereitstellt und  Anfragen auf mehrere Webserver verteilt, um die Leistung einer Serverumgebung zu verbessern
  
  -   [Java](https://www.java.com/en/download/faq/whatis_java.xml) - objektorientierte Programmiersprache und Laufzeitumgebung zur Entwicklung von plattformunabhängigen Anwendungen
  
  -   [MySQL](https://dev.mysql.com/doc/) - relationales Datenbankverwaltungssystem, führende Datenbank für internetbasierte Anwendungen
  
  -   [Ubuntu](https://www.ubuntu.com/) - kostenlose Linux-Distribution, leicht zu bedienendes Betriebssystem mit aufeinander abgestimmter Software


------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink") 