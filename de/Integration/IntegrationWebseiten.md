<!-- en: Integration/IntegrationWebsites -->

-----------------

[`Zurück zur Übersicht der Integration geht es hier.`](Integration "wikilink")

---------------------

## Integration in Webseiten
----------------------------------------------

### Confluence

Ein interessantes Werkzeug von Christian Schenk ist das [Confluence
Plugin](http://www.christianschenk.org/projects/confluence-bibsonomy-plugin/). Das Plugin macht es möglich, eine Tagliste/Tagcloud und die neuesten
BibTeX-Einträge jeder beliebigen Seite in [Confluence](http://www.atlassian.com/software/confluence) anzuzeigen.

---------------------------------

### GoogleDocs

Das ${project.name}-Addon für Google Docs ermöglicht es Ihnen, in
Google Docs direkt aus ${project.name} zu zitieren. Wie es
funktioniert, erfahren Sie in [ dieser Anleitung](AddonGoogleDocs "wikilink").

---------------------------------

### GoogleScholar

Mit dem ${project.name} Scholar Plugin für Chrome lassen sich
${project.name} und GoogleScholar miteinander verknüpfen. Es ist dann
möglich, direkt im GoogleScholar-Webinterface Publikationen zu
verwalten. Die Erweiterung kann im [Chrome Web Store](https://chrome.google.com/webstore/detail/bibsonomy-scholar/nfncjdnkilenkgnhchaapiiboaimpoon)
direkt hinzugefügt werden. Weitere Informationen zur
GoogleScholar-Erweiterung gibt es im ${project.name}-[Blog](http://blog.bibsonomy.org/2015/12/feature-of-week-bibsonomy-scholar.html).

---------------------------------

### Moodle

Moodle ist eine beliebte E-Learning-Plattform. ${project.name} kann
Kursbeschreibungen und E-Learning-Projekte aufwerten, indem es
einschlägige Literatur anbietet, z.B. über RSS-Feed. Eine Beschreibung
des Moduls sowie eine Anleitung für Kursadministratoren und Nutzer
finden Sie auf der [ Moodle-Hilfeseite](Moodle "wikilink") und [hier](https://moodle.org/plugins/mod_pbm).