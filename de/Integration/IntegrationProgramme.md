<!-- en: Integration/IntegrationPrograms -->

-----------------

[`Zurück zur Übersicht der Integration geht es hier.`](Integration "wikilink")

---------------------

## Integration in Programmen
----------------------------------------------

${project.name} lässt sich in verschiedene Programme integrieren. 
Folgende Links führen zu Anleitungen:

* [BibLaTeX](Integration/IntegrationProgramme/IntegrationBibLaTeX)

* [Citavi](Integration/IntegrationProgramme#Citavi)

* [Emacs](Integration/IntegrationProgramme#Emacs)

* [JabRef](Integration/IntegrationProgramme/IntegrationJabRef)

* [KBibTeX](Integration/IntegrationProgramme#KBibTeX)

* [Sublime Text](Integration/IntegrationProgramme#SublimeText)

* [TeXlipse](Integration/IntegrationProgramme#TeXlipse)

<br/>
<br/>

---------------------------------

### Citavi

Sie können Artikel zwischen dem Literaturverwaltungsprogramm
[Citavi](http://www.citavi.de/de/index.html) und ${project.name}
austauschen. Eine Anleitung dazu finden Sie [hier](Citavi "wikilink").

---------------------------------

### Emacs

[Emacs](http://www.gnu.org/software/emacs/) ist ein mächtiger, anpassbarer Texteditor. Das Package
[AUCTeX](http://www.gnu.org/software/auctex/) macht Emacs zu einer komfortablen LaTeX-Umgebung. In unserem
[Blogpost](http://blog.bibsonomy.org/2011/09/feature-of-week-access-your-posts-in.html) beschreiben wir, wie Emacs aufgesetzt werden muss, damit es automatisch
BibTeX-Referenzen aus ${project.name} verwenden kann.

---------------------------------

### KBibTeX

[KBibTeX](http://home.gna.org/kbibtex/) ist eine KDE-Anwendung für die
Offline-Verwaltung Ihrer Publikationen, die ähnlich wie
[JabRef](http://jabref.sourceforge.net/) funktioniert. Diese Anwendung
integriert mehrere Online-Services und ermöglicht es Ihnen, die
gefundenen Dateien zu speichern oder in ${project.name} zu suchen.

---------------------------------

### SublimeText

Für den Texteditor [Sublime Text](http://www.sublimetext.com/) gibt es
ein LaTeX Plugin namens [Latexing](http://www.latexing.com/). Dieses
Plugin ermöglicht es, ${project.name} einzubinden (wie das geht,
erfahren Sie in diesem [Tutorial)](http://docs.latexing.com/stable/tutorials/setup-bibsonomy-with-latexing.html).

---------------------------------

### TeXlipse

[TeXlipse](http://texlipse.sourceforge.net/) ist ein LaTeX-Entwicklungs-Plugin für [Eclipse](https://www.eclipse.org/). Durch
die TeXlipse ${project.name}-Erweiterung können Sie Ihre Referenzen aus
${project.name} verwalten. Informationen zur Installation und zur
Benutzung finden Sie in diesem [Tutorial](TeXlipseBibSonomyExtension "wikilink").

