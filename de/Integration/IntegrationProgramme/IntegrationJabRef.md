<!-- en: Integration/IntegrationPrograms/IntegrationJabRef -->

-----------------

[`Zurück zur Übersicht der Integration in Programmen geht es hier.`](Integration/IntegrationProgramme "wikilink")

---------------------

## Integration in JabRef
----------------------------------------------

[JabRef](http://www.jabref.org/) ist ein OpenSource-Literaturverwaltungsprogramm, das mit dem BibTeX-Format als
Standardformat arbeitet.  

**${project.name}-Plugin**  
*Achtung: Seit der JabRef-Version 2.12 werden keine Plugins mehr in
JabRef unterstützt!*  
Um Literaturdaten zwischen ${project.name} und JabRef einfacher
austauschen zu können, gibt es das ${project.name}-Plugin für JabRef.
Hier finden Sie weitere Informationen zur [Installation](JabrefInstallieren "wikilink") und zur [Anwendung](Jabref "wikilink").

**JabRef-Layout aus Jar-Files extrahieren**  
In JabRef haben Sie die Möglichkeit, Ihre Literaturdaten in
benutzerdefinierten Formaten zu exportieren. Diese Formate heißen
Layouts und sind in den Layout-Dateien definiert. Auf unserer
[Entwicklerseite](http://dev.bibsonomy.org/maven2/org/bibsonomy/bibsonomy-layout/) können Sie die zurzeit verfügbaren Layouts herunterladen. Die Layouts
sind in einem Jar-File komprimiert. Sie können diese Datei durch ein
Zip-Werkzeug wie WinZip entpacken und dann öffnen. Die Layouts finden
Sie unter dem Pfad `\\org\\bibsonomy\\layout\\jabref`.

Um ein Layout zu benutzen, müssen Sie seine `'.begin'`- und
`'.end'`-Files zusammen mit dem entsprechenden Layout-File extrahieren.
Wenn Sie beispielsweise ein Harvard-HTML-JabRef-Layout benutzen möchten,
müssen Sie die `'harvard.layout'`-, '`harvard.begin.layout'`- und
`'harvard.end.layout'`-Files extrahieren.