<!-- en: Integration/IntegrationPrograms/IntegrationBibLaTeX -->

-----------------

[`Zurück zur Übersicht der Integration in Programmen geht es hier.`](Integration/IntegrationProgramme "wikilink")

---------------------

## Integration in BibLaTeX
----------------------------------------------

[BibLaTeX](http://mirrors.rit.edu/CTAN/help/Catalogue/entries/biblatex.html) ist eine Erweiterung zu BibTex.
Es bietet viele Möglichkeiten, Bibliographien und Literaturverzeichnisse zu verwalten. 
So ist es beispielsweile möglich, durch das Hinzufügen folgender Zeilen in 
den LaTex Projekt Header inkonsistente Bibliographien zu vermeiden:

        \usepackage[backend=biber]{biblatex}
    	\addbibresource[location=remote]
    	{http://www.bibsonomy.org/bib/user/thoni/myown}

Jedes Mal, wenn die PDF. Datei kompiliert wird, wird nun eine Liste der Publikationen mit dem Tag „myown“ vom User „thoni“ heruntergeladen.

Um sich eine Bibliographie ausgeben zu lassen, tippen Sie

        \printbibliography

Ein detaillierteres Tutorial finden Sie [hier](https://www.sharelatex.com/blog/2013/07/31/getting-started-with-biblatex.html).