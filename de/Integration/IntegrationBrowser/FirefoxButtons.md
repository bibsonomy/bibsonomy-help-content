<!-- en: Integration/IntegrationBrowser/FirefoxButtons -->


-----------------

[`Zurück zur Übersicht der Integration in Browsern geht es hier.`](Integration/IntegrationBrowser "wikilink")

---------------------

## Integration in Firefox mit ${project.name} Buttons
----------------------------------

${project.name} besitzt ein eigenes Add-on, das sie kostenlos für Firefox herunterladen können.
Gehen Sie dazu auf Ihre Add-on-Schaltfläche (about:addons oder Strg+Umschalt+A) und suchen Sie dort nach
${project.name} **"Buttons"**. Alternativ folgen Sie [diesem Link](https://addons.mozilla.org/de/firefox/addon/bibsonomy-buttons/).

${project.theme == "puma"}{

Möchten Sie das Add-on für das System **"PUMA"** für Firefox herunterladen, dann folgen Sie [diesem Link](https://www.bibsonomy.org/puma_files/firefox/puma_buttons-latest.xpi). Leider kann es derzeit nicht im offiziellen Store von Firefox angeboten werden.

}

Nachdem Sie das Add-on hinzugefügt haben, ist die Oberfläche von Firefox um einen
Button erweitert worden.

-------------------

![ <File:MainButton.png>](FirefoxButtons/MainButton.png  "fig: File:MainButton.png")

Durch diesen Button gelangen Sie zu einem kleinen Auswahlmenü mit vier weiteren Buttons, die Ihnen folgende Funktionen bieten:

1. **Navigation zu ${project.name}**:
Nutzen Sie diesen Button, um direkt zu Ihrer eigenen ${project.name}-Seite geleitet zu werden.

2. **Bookmark in ${project.name} speichern**:
Nutzen Sie diesen Button, um schnell ein Lesezeichen für ${project.name} zu erstellen. Hierbei werden von Ihrer momentan geöffneten Webseite ihre URL, ihr Titel und ihre Beschreibung automatisch übernommen.

3. **Publikation in ${project.name} speichern**:
Nutzen Sie diesen Button, um schnell eine Publikation in ${project.name} zu erstellen. Es werden automatisch möglichst
viele Metadaten von ${project.name} übertragen. 

4. **Optionen**: 
	Hier können zwei Einstellungen vorgenommen werden:
	- Erstens können Sie einstellen, ob die Aktionen des Add-ons in einem neuen Tab ausgeführt werden sollen. Setzen Sie hier einen Haken, wenn Sie Ihren aktuellen Tab weiterhin benötigen. Dann öffnet sich ein neuer Tab, der Sie zur entsprechenden ${project.name}-Seite führt, auf der Sie Ihre gewünschte Aktion fortführen können. 
	- Zweitens ist es möglich, zu entscheiden, ob Posts sichtbar oder unsichtbar sein sollen. Kreuzen Sie **Öffentlich** an, sofern Sie Ihre neuen Lesezeichen und Publikationen mit anderen Nutzern von ${project.name} teilen wollen. Möchten Sie dies nicht, so kreuzen Sie **Privat** an.
	- ![ <File:ButtonSettingsDE.png>](FirefoxButtons/ButtonSettingsDE.png  "fig: File:ButtonSettingsDE.png")



