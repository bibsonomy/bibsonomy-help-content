<!-- en: Integration/IntegrationBrowser -->

-----------------

[`Zurück zur Übersicht der Integration geht es hier.`](Integration "wikilink")

---------------------

## Integration im Browser
----------------------------------------------

Mit unseren ${project.name} Browser Plugins ist es sehr einfach,
Lesezeichen bei Webseiten und Publikationen zu setzen. Der
Navigationsleiste des Browser werden hierbei drei nützliche Buttons
hinzugefügt. Im Moment gibt es Plugins für Chrome/Chromium, für 
[Firefox](Integration/IntegrationBrowser#firefox "wikilink") und für [Safari](Integration/IntegrationBrowser#Safari "wikilink").

------------------------------------------------------------------------

### Chrome / Chromium

Wenn Sie dieses Add-On installieren, werden Ihrem Chrome/Chromium
Browser drei ${project.name}-Buttons hinzugefügt. Ein Button führt Sie
direkt zu Ihrer mein ${project.name}-Seite. Die anderen beiden Buttons
sind dafür da, um Lesezeichen und Publikationen mit nur einem Klick
(oder Tastaturkürzel) in ${project.name} zu speichern. Die Beschreibung
und den Download finden Sie auf der [Browser Add-ons & Bookmarklets-Seite](http://www.bibsonomy.org/buttons).
Für eine Anleitung zur Benutzung des Add-ons, besuchen Sie [diese Seite](Integration/IntegrationBrowser/ChromeButtons "wikilink").

------------------------------------------

### Firefox

**Add-On**  
Wenn Sie dieses Add-On installieren, werden Ihrem Firefox Browser drei
${project.name}-Buttons hinzugefügt. Ein Button führt Sie direkt zu
Ihrer mein ${project.name}-Seite. Die anderen beiden Buttons sind dafür
da, um Lesezeichen und Publikationen mit nur einem Klick (oder
Tastaturkürzel) in ${project.name} zu speichern. Die Beschreibung und
den Download finden Sie auf der [Browser Add-ons & Bookmarklets-Seite](http://www.bibsonomy.org/buttons).
Für eine Anleitung zur Benutzung des Add-ons, besuchen Sie [diese Seite](Integration/IntegrationBrowser/FirefoxButtons "wikilink").

**Suche**  
Sie können eine Suche nach ${project.name}-Inhalten auch direkt in
Ihrer Firefox Adressleiste durchführen.  

-  Dazu erstellen Sie zunächst ein neues Lesezeichen. Klicken Sie auf
    das **"Lesezeichen verwalten-Symbol"** (rechts neben dem
    Lesezeichen-Stern) und anschließend auf **"Lesezeichen verwalten"**.
    Klicken Sie links auf **"Lesezeichen-Symbolleiste"** (oder eine der
    anderen Optionen), dann in der oberen Menüleiste auf **"Verwalten"** und
    schließlich auf **"Neues Lesezeichen..."**.

-   **Suche in Ihrem ${project.name}-Account**  
    Wenn Sie in Ihrem eigenen ${project.name}-Account suchen möchten,
    geben Sie folgendes in die Felder ein:  
    Name: `Suche My BibSonomy`  
    Adresse: `www.bibsonomy.org/user/IHR_NUTZERNAME/%s`   
    Schlüsselwort: `bs`  
    
    Nun können Sie oben in der Adressleiste beispielsweise folgende
    Suchanfrage eingeben:  
    **bs myown 2015**, um alle Ihre Einträge mit den Tags *myown* und
    *2015* zu finden.
    
-   **Globale Suche in ${project.name}**  
    Wenn Sie im gesamten ${project.name}-System nach Einträgen suchen
    möchten, geben Sie folgendes in die Felder ein:  
    Name: `Globale Suche BibSonomy`  
    Adresse: `www.bibsonomy.org/search/%s`  
    Schlüsselwort: `as`  
    Nun können Sie oben in der Adressleiste beispielsweise folgende
    Suchanfrage eingeben:  
    **as java**, um alle Einträge mit dem Tag *java* zu finden, die in
    ${project.name} eingetragen sind.

    ![ <File:lesezeichen.png>](IntegrationBrowser/lesezeichen.png  " File:lesezeichen.png")

**Zotero**  
[Zotero](http://www.zotero.org/) ist eine Erweiterung für Firefox und
sammelt, verwaltet und zitiert Publikationen. Wir unterstützen den
Export von Literaturangaben aus ${project.name} zu Zotero sowie den
Import von Einträgen in der Zotero-Bibliothek nach ${project.name}.
Eine Anleitung dazu finden Sie [hier](Zotero "wikilink").

-----------------------------------------------

### Safari

Wenn Sie dieses Add-On installieren, werden Ihrem Safari Browser drei
${project.name}-Buttons hinzugefügt. Ein Button führt Sie direkt zu
Ihrer mein ${project.name}-Seite. Die anderen beiden Buttons sind dafür
da, um Lesezeichen und Publikationen mit nur einem Klick (oder
Tastaturkürzel) in ${project.name} zu speichern. Die Beschreibung und
den Download finden Sie auf der [Browser Add-ons & Bookmarklets-Seite](http://www.bibsonomy.org/buttons).

------------------------------------------------------------------------