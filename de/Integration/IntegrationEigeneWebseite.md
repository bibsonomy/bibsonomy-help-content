<!-- en: Integration/IntegrationOwnWebsite -->

-----------------

[`Zurück zur Übersicht der Integration geht es hier.`](Integration "wikilink")

---------------------

## Integration auf eigener Webseite
----------------------------------------------

### Bookmarklinks

Einige Webseiten bieten einen Bookmarklink auf ihrer Seite an, damit der
Benutzer ganz einfach Artikel der Seite in sozialen Netzwerken teilen
oder in einem Lesezeichensystem speichern kann. Sie können auf Ihrer
eigenen Webseite oder Blog einen Bookmarklink für ${project.name}
hinzufügen, indem Sie einen kurzen JavaScript-Code einfügen (dieser
befindet sich auf der [ Browser Add-ons & Bookmarklets Seite](Bib:/buttons "wikilink") und [ auf der
JavaScript-Codeschnipsel-Seite](JS-Codeschnipsel "wikilink")).  

-----------------------------------------------

### Tagwolken

${project.name} ermöglicht die Einbindung seiner Tagwolken, z.B. für
Ihre Publikationen, in Ihre private Webseite. Hierfür bietet
${project.name} einen JSON-Feed für Tags, der von JavaScript-Snippets
verarbeitet wird. Momentan ist dieses Feature nur für die BibTeX-Seite
erhältlich, in naher Zukunft wird es jedoch auf alle Seiten ausgedehnt,
die Tagwolken anbieten.  
Bitte werfen Sie einen Blick auf das Beispiel und probieren Sie die
verschiedenen Argumente im Formular aus. Die Tagwolke gehört zur
folgenden Publikation:

*Andreas Hotho and Robert Jäschke and Christoph Schmitz and Gerd Stumme.
[${project.name}: A Social Bookmark and Publication Sharing System](Bib:/bibtex/1d28c9f535d0f24eadb9d342168836199 "wikilink").
Proceedings of the Conceptual Structures Tool Interoperability Workshop, 2006.*

Um eine Tagwolke in Ihre Seite einzubinden, gehen Sie folgendermaßen
vor:

-   Geben Sie diese Zeilen an der Stelle in Ihren HTML-Code ein, an der
    die Tagwolke angezeigt werden soll:

        <div id="tags">
        <div class="title">${project.name} Tags</div>
        <ul id="tagbox" class="tagcloud"></ul>
        </div>

-   Binden Sie einen Verweis auf die verarbeitende JavaScript-Datei ein
    (die JavaScript-Datei finden Sie [ hier](TagCloud.js "wikilink")):

        <script type="text/javascript" src="tagCloud.js"> 
        </script>

-   Möchten Sie Ihre Tagwolke wie im obigen Beispiel formatieren,
    verwenden Sie diese [ CSS-Datei](TagCloud.css "wikilink") und binden
    Sie sie wie folgt ein. Sollte Ihnen diese Art der Ausführung nicht
    zusagen oder nicht zum aktuellen Aufbau Ihrer Webseite passen,
    können Sie natürlich Ihre eigenen Layoutdateien verwenden.
    
        <link rel="stylesheet" type="text/css" href="tagCloud.css" />
    
-   Schließlich müssen Sie die JavaScript-Funktion `getTags()` aufrufen. Dazu müssen drei Argumente übergeben werden.

    -   Erstens die URL, die den JSON-Feed enthält. Zum Beispiel: `/json/tags/bibtex/1d28c9f535d0f24eadb9d342168836199`  für alle Schlagwörter, die sich auf diese Publikation beziehen.

	-    Das zweite Argument ist der Grenzwert der
        abzurufenden Schlagwörter.
        
    -   Schließlich die Reihenfolge der Schlagwörter. Sie können wählen
        zwischen einer alphabetischen Reihenfolge, indem Sie das
        Stichwort `alpha` wählen oder die Sortierung nach gezählten
        Schlagwörtern mit dem Stichwort `frequency`.  
        
        Das folgende Beispiel ruft 25 sich auf diese Publikation
        beziehende Schlagwörter in alphabetischer Reihenfolge ab. Die
        Tagwolke wird dynamisch mit den abgerufenen Schlagwörtern gefüllt.  
        `getTags(/json/tags/bibtex/1d28c9f535d0f24eadb9d342168836199, 25, "alpha")`

Eine weitere Anleitung, wie Sie Tagwolken auf Ihrer Webseite darstellen
können, finden Sie [ auf dieser Seite](Flash-JavaScript-Tag-Cloud "wikilink").  

-----------------------------------------------

### Publikationslisten

Integrieren Sie Publikationslisten (z.B. Ihre eigenen Publikationen),
die das gleiche Format haben wie in ${project.name}, auf Ihrer
Webseite. Dazu müssen Sie ein iframe in Ihren HTML-Code einfügen, das
folgendermaßen aussieht: 

    <iframe name='my publications' 
        src='<URL>' 
        height='400' 
        width='100%' 
        style='border: none;'>
    <p>
      Unfortunately, your browser is not capable of showing embedded frames (iframes).
    </p>
    </iframe>

Die URL kann jede beliebige Seite aus ${project.name} sein, z.B. Ihre Benutzerseite oder die Seite einer
Ihrer Gruppen. Es ist wichtig, dass am Ende der URL der Parameter
`format=embed` steht. Beispielsweise zeigt die URL
<http://www.bibsonomy.org/user/jaeschke/myown?items=1000&resourcetype=publication&sortPage=year&sortPageOrder=desc&format=embed>
alle (bis zu 1000) Publikationen des Nutzers *jaeschke* an, denen das
Schlagwort "myown" zugeordnet wurde, absteigend nach Jahr sortiert.

-----------------------------------------------

### JSON-Feed

Sie können für jede ${project.name}-Seite einen
[JSON-Feed](http://www.json.org/) generieren, indem Sie `json/` vor den
Pfadteil der URL stellen. Um beispielsweise den JSON-Feed für `/tag/json` zu bekommen, geben Sie `/json/tag/json` ein.

Sie erhalten einen JSON-Feed, der mit [Exhibit](http://www.simile-widgets.org/exhibit/) kompatibel ist und
alle Lesezeichen und Publikationen der entsprechenden Seite enthält. Um
den JSON-Feed in Ihr Exhibit einzugeben, fügen Sie einen Link dazu in
den Header Ihres Exhibit HTML Codes.

${project.theme == "bibsonomy"}{

    <link href="http://www.bibsonomy.org/tag/json?callback=cb" type="application/jsonp" rel="exhibit/data" ex:jsonp-callback="cb" />

}

${project.theme == "puma"}{

    <link href="http://puma.uni-kassel.de/tag/json?callback=cb" type="application/jsonp" rel="exhibit/data" ex:jsonp-callback="cb" />

}

Schauen Sie sich diese [Liste von Publikationen](http://www.kde.cs.uni-kassel.de/hotho/publication_json.html) an, um zu sehen, welche Möglichkeiten Sie mit JSON und Exhibit haben.

Benutzer unseres bisherigen JSON Feeds sollten darauf achten, dass sich
das Format inzwischen etwas geändert hat:

-   Die Tags stehen jetzt in der Liste mit dem Key `items` und nicht `tags`.
-   Der Name der Tags wird durch den Key `label` und nicht `name` ausgedrückt.
-   Es gibt zwei weitere Listen (`types` and `properties`), um eine
    einfachere Exhibit-Integration zu ermöglichen.

-----------------------------------------------

### TYPO3

Mit Hilfe der TYPO3-Erweiterung [BibSonomy CSL](http://typo3.org/extensions/repository/view/ext_bibsonomy_csl)
können Sie Publikationslisten anlegen, die die in ${project.name}
gespeicherten bibliographischen Referenzen beinhalten, aber auch auf den
Publikationsdateien basierende Tagwolken erstellen. Weiterhin ermöglicht
die Erweiterung eine einfache Zusammensetzung von Publikationslisten für
die persönliche Benutzung oder um Publikationslisten einer
Forschungsgruppe/eines Projekts anzuzeigen. Für die Darstellung der
Publikationslisten bzw. Tagwolken können Sie zwischen vielen verfügbaren
Stylesheets der Citation Style Language (CSL) wählen, oder aber eigene
Styles erstellen. Außerdem können Sie Dokumente, die an
${project.name}-Einträge angehängt worden sind, zum Download
bereitstellen. Um die TYPO3-Erweiterung zu installieren, besuchen Sie
die [offizielle BibSonomy CSL-Seite](http://typo3.org/extensions/repository/view/ext_bibsonomy_csl) im TYPO3-Erweiterungs-Repository. Die TYPO3-Erweiterung ist ein [OpenSource-Projekt](https://bitbucket.org/bibsonomy/bibsonomy-typo3-csl). Weitere Informationen finden Sie im [ Tutorial zur Integration in TYPO3](Typo3 "wikilink").

${project.theme == "puma"}{

-----------------------------------------------

### VuFind

Das OpenSource-Projekt [VuFind](http://vufind-org.github.io/vufind/)
beinhaltet Katalogisierung, Suche und Ablagemöglichkeiten für
Bibliotheken. PUMA kann einem laufenden VuFind-System hinzugefügt
werden. Um PUMA an Ihrer Institution zu installieren oder um es mit
Ihrem VuFind zu verbinden, kontaktieren Sie bitte das [PUMA-Team](http://www.academic-puma.de/uber-uns/kontakt/).

}

-----------------------------------------------

### Wordpress

Mit dem Wordpress-Plugin haben sie die Möglichkeit,
${project.name}-Features (z.B. die Tagwolke oder auch
Publikationslisten) in Ihren Wordpress-Blog einzufügen. Zur Darstellung
der Features können Sie zwischen den verschiedenen verfügbaren
bibliografischen Stylesheets der Citation Style Language (CSL) wählen,
aber auch eigene Styles erstellen. Außerdem können Sie Dokumente, die an
Publikationen angehängt worden sind, zum Download bereitstellen oder
Vorschaubilder anzeigen.  
Für weitere Informationen und zum Download lesen Sie unser [Tutorial](Wordpress "wikilink").  

-----------------------------------------------

### XWiki

Zur Einbindung von ${project.name} in eine XWiki-Seite gehen Sie
folgendermaßen vor:

1.  Vergewissern Sie sich, dass das XWiki RSS Feed Plugin in Ihrem XWiki
    Instance installiert ist, indem Sie Ihre `xwiki.cfg`-Datei editieren
    (sie ist voreingestellt).
2.  Editieren Sie die Wiki-Seite dort, wo Sie ${project.name}-Daten
    anzeigen lassen wollen.
3.  Geben Sie folgenden Befehl ein, der das XWiki RSS-Makro  
    `rss:feed=/publrssN/user/username?items=1000`  
    aufruft, und zwar dort, wo der Feed-Parameter mit den RSS-Daten
    verknüpft ist, die Sie auf dieser Anzeige angezeigt bekommen möchten.
4.  Speichern Sie die Seite.

Die ${project.name}-Daten werden nun auf Ihrer Seite angezeigt.

Wenn Sie Sie die Anzeigeform jedes Feed-Elements individuell anpassen
möchten, so können Sie hierfür XWiki Velocity oder Groovy Scripting
Features benutzen wie in dem folgenden Groovy-Beispiel:

    feed =}} 
    {{xwiki.feed.getFeeds("http://www.bibsonomy.org/publrssN/user/username?items=1000")}} 
    {{for (entry in feed.entries) {}} \\ {{desc = entry.getDescription().value}} 
    {{println("{pre}")}} 
    {{println(desc)}} 
    {{println("{/pre}")}} 
    {{ } 

Beispiel: [Nepomuk-Publikationen](http://nepomuk.semanticdesktop.org/xwiki/bin/view/Main1/Publications)

-----------------------------------------------

### Zope

Sie können Inhalte aus ${project.name} dem Content Management System von [Zope](http://www.zope.org/) hinzufügen.  

**Publikationen**  
Publikationslisten können mit Hilfe des ${project.name}-RSS-Feeds auf Ihrer Zope-Seite dargestellt
werden. Eine detaillierte Beschreibung des [RDF Summary](http://www.zope.org/Members/EIONET/RDFSummary/) Produkts erhalten Sie bei Zope.

**Tagwolken**  
Wie Sie Tagwolken auf Zope-Seiten erstellen, erfahren Sie in [ diesem Tutorial](TagwolkenAufZope-Seiten "wikilink").
