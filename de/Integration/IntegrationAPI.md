<!-- en: Integration/IntegrationAPI -->

-----------------

[`Zurück zur Übersicht der Integration geht es hier.`](Integration "wikilink")

---------------------

## Integration mit REST-API
----------------------------------------------

${project.name} bietet einen Webservice auf Basis des Representational
State Transfer (REST) an. REST ist ein Architekturstil für verteilte
Softwaresysteme, bei dem eine einheitliche Schnittstelle die Interaktion
erleichtert. Die REST-API ist für Softwareentwickler gedacht, deren
Anwendungen mit ${project.name} interagieren sollen.  
Um auf die API zuzugreifen, können Sie die angebotene Client Library in
der Programmiersprache Java nutzen, oder Sie können direkt mit dem
Webservice interagieren, falls Sie einen Client in einer anderen
Programmiersprache schreiben möchten.  
Um auf die API zugreifen zu können, benötigen Sie Ihren API-Schlüssel
(API-Key). Diesen finden Sie auf der [Einstellungsseite](Bib:/settings "wikilink") unter dem Reiter "Einstellungen".  

-   [REST-API-Repository](http://dev.bibsonomy.org/maven2/org/bibsonomy/bibsonomy-rest-client/)
-   Anleitungen: [Dokumentation auf Bitbucket](https://bitbucket.org/bibsonomy/bibsonomy/wiki/documentation/api/REST%20API), [Web-Präsentation](http://www.academic-puma.de/rest-training/#/)
-   [XML-Schema](https://bitbucket.org/bibsonomy/bibsonomy/wiki/documentation/api/XML%20Schema)
-   [Javadoc-Dokumentation](http://www.bibsonomy.org/help/doc/rest-client-javadoc/index.html)  

---------------------------------

### Java

Sie können mit Hilfe der Programmiersprache Java auf die REST-API zugreifen.  

-   [Nutzungsbeispiele](https://bitbucket.org/bibsonomy/bibsonomy/wiki/documentation/api/Java%20API%20Examples)
-   [Quelldateien](https://bitbucket.org/bibsonomy/bibsonomy/src/tip/bibsonomy-rest-client/?at=default)  

---------------------------------

### PHP

Restclient-PHP ist ein Paket aus PHP-Skripten, das einen REST-Client
sowie enthält einige Utilities, die hilfreich sind für die Entwicklung
einer PHP-Applikation, die mit der ${project.name}-REST-API
interagieren soll. Der REST-Client verwaltet Funktionen, die von der
${project.name}-REST-API angeboten werden.  

-   [BitBucket-Repository](https://bitbucket.org/bibsonomy/restclient-php)
-   [Demo-Dateien und Beispiele](http://www.academic-puma.de/rest-training/restclient_training.zip)

---------------------------------

### Python

Es gibt einen API-Client, um mit Hilfe der Programmiersprache
[Python](http://www.python.org/) Einträge aus ${project.name} abzurufen.  
Mit der bereitgestellten [Exportfunktion](https://bitbucket.org/bibsonomy/bibsonomy-python/src/tip/onefile.py?fileviewer=file-view-default) lassen 
sich Posts herunterladen und speichern. Mehr dazu in der [Benutzungsanleitung](https://bitbucket.org/bibsonomy/bibsonomy-python). 

-   [Download](https://pypi.python.org/pypi/bibsonomy)
-   [Benutzungsanleitung](https://bitbucket.org/bibsonomy/bibsonomy-python)
