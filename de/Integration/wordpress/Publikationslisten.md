<!-- en: Integration/wordpress/AddPublicationLists-->

## Publikationslisten zu Ihrem WordPress Blog hinzufügen
---------------------------------

Wenn Sie wie üblich einen neuen Blogeintrag verfassen oder einen statische Seite erstellen, finden Sie unten auf
der Seite eine neue Meta Box mit dem Namen **"Add BibSonomy Publications"**.
Sie können Publikationen aus den Inhaltstypen **user**, **group** und **viewable** wählen. 


Hier ein Beispiel: Angenommen, Sie wollen Ihre eigenen Publikationen in einem Blogeintrag veröffentlichen
(in ${project.name} sind alle eigenen Publikationen bereits mit dem Tag [myown](http://blog.bibsonomy.org/search/label/myown) gekennzeichnet.)
Wählen Sie **"user"** als Inhaltstyp und geben Sie Ihren ${project.name}nutzernamen  als **content type value** ein.
Jetzt müssen Sie Ihre Auswahl filtern, indem Sie den Tag **"myown"** verwenden: 
Hierfür müssen Sie "myown" in das Feld für Tags eintragen.
Falls Sie mehr als einen Tag auswählen möchten, trennen Sie sie durch ein Leerzeichen.
Sie können wahlweise auch die Anzahl der Publikationen begrenzen.

Nun können Sie einen der vorinstallieren Zitierstile benutzen, die Sie als Layout für Ihre 
Publikationsliste verwenden wollen.
Falls Ihr gewünschter CSL-Stil bei der Standardauswahl nicht dabei ist, können Sie einen eigenen Stil auswählen,
indem Sie die URL des Stylesheets eingeben.
Die Citation Style Language (CSL) ist eine XML-Sprache zur Beschreibung von Formaten für bibliografische Angaben und Zitierstile.
Eine umfangreiche Liste mit kostenlosen verfügbaren Stilen finden Sie [hier](http://www.zotero.org/styles/).


![ <File:Publikationslisten1.png>](Publikationslisten/Publikationslisten1.png "fig: File:Publikationslisten1.png")

  

Um das Design zu verändern, können Sie das CSS anpassen. Ein Beispiel ist bereits voreingestellt. Falls es Ihnen nicht gefällt, können Sie es 
einfach löschen.


![ <File:Publikationslisten2.png>](Publikationslisten/Publikationslisten2.png "fig: File:Publikationslisten2.png")


Speichern Sie Ihren Eintrag und wechseln Sie zur Vorschau. Falls Sie möchten, können Sie Ihre Einstellungen und CSS-Definitionen Ihren 
Wünschen anpassen.

*Achtung: Sie benutzen Ihren eigenen Account, um Einträge aus ${project.name} abzurufen. Das bedeutet, dass alle Einträge, die für Sie auf ${project.name} sichtbar sind (auch Ihre privaten Einträge), auch auf Ihrem Blog sichtbar gemacht werden, wenn Sie der Beschreibung in der Meta Box entsprechen. (In unserem Beispiel: alle Einträge, die Sie mit dem Tag **"myown"** gekennzeichnet haben.)*

​	
