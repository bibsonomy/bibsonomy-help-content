<!-- en: Integration/wordpress/Tagclouds -->

## Tagwolken zu Ihrem WordPress Blog hinzufügen
---------------------------------

**${project.name} Tagwolken** lassen sich sehr einfach zu Ihrem Blog hinzufügen.
Wechseln Sie zunächst mithilfe des Designmenüs zu der Widget Seite. 

![ <File:Tagwolken.png>](Tagwolken/Tagwolken.png  "fig: File:Tagwolken.png")
  
  
Ziehen Sie das ${project.name} Tag Cloud Widget an die gewünschte Stelle, füllen Sie die Felder aus, wählen Sie
einen Layoutstyle und speichern Sie zuletzt die Einstellungen.