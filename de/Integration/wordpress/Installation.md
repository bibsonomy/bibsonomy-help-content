<!-- en: Integration/wordpress/Install -->

## BibSonomy CSL Installation
---------------------------------

Um **Bibsonomy CSL** zu installieren, melden Sie sich als Administrator bei WordPress
an und suchen Sie im Plugin Installationsmenü nach BibSonomy CSL.
Installieren Sie im Installationsmenü das Plugin "BibSonomy CSL and BibSonomy Tag Cloud Widget". 
(Als Alternative können Sie das Plugin auch [hier](http://wordpress.org/extend/plugins/bibsonomy-csl/) herunterladen, 
es entpacken und mithilfe eines FTP Client hochladen.)

Nachdem die Installation abgeschlossen ist, finden Sie das neue Element **BibSonomy CSL** im Einstellungsmenü. 
Klicken Sie es an und tragen Sie Ihren ${project.name} Nutzernamen und den [BibSonomy API-Schlüssel](https://www.bibsonomy.org/settings?selTab=1&lang=de) ein. Speichern Sie nun die Einstellungen.

![ <File:Installation1.png>](Installation/Installation1.png  "fig: File:Installation1.png")

Nun können Sie Ihren neuen Blogpost wie üblich verfassen.
