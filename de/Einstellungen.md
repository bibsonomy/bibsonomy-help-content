<!-- en: Settings -->

## Einstellungen
-----------------

![ <File:0.png>](Einstellungen/0.png  "fig: File:0.png")

Um die [ Einstellungsseite](${project.home}settings "wikilink") aufzurufen, klicken Sie auf das Personensymbol
im rechten [ Hauptmenü](Programmoberfläche "wikilink") und dann auf "Einstellungen".
Jetzt haben Sie die Auswahl zwischen mehreren Reitern, um Ihre
persönlichen Nutzereinstellungen einzusehen und zu ändern.  
![ <File:1.png>](Einstellungen/1.png  "fig: File:1.png")  

-   **Mein Profil:** Sie können allgemeine persönliche Informationen
    hinzufügen, wie beispielsweise Ihren Namen, Geburtsdatum,
    Kontaktdaten, Interessen, und Sie können ein Profilbild hochladen.
    Die hier eingetragenen Informationen werden auch auf Ihrer [
    Lebenslaufseite](LebenslaufÄndern "wikilink") angezeigt.  
    Außerdem können Sie auch die Sichtbarkeit Ihres Profils einstellen
    (public, private, friends).  
    
-   **Einstellungen:** Hier können Sie das [ Layout Ihrer Tagbox und
    Ihrer Eintragslisten](TagLayout "wikilink") ändern, Ihren API-Schlüssel einsehen, 	  Logging- und Löschen-Optionen ändern,
    Ihr Passwort ändern und [Ihren Account löschen](BenutzerkontoEndgültigLöschen "wikilink"). Außerdem können
    Sie die eingestellte Sprache ändern, Ihre [bevorzugten Exportformate] (BevorzugteExportformate "wikilink") festlegen und die [erweiterten Funktionen
    freischalten](ErweiterteFunktionenFreischalten "wikilink").  
    
-   **Layout-Datei:** Hier können Sie Ihre Layout-Dateien
    hochladen, um Ihre Publikationsliste nach Ihren
    Wünschen darzustellen.  Weitere Informationen dazu finden Sie auf den internen Hilfeseiten von Bibsonomy zu [CSL-Layouts](https://www.bibsonomy.org/help_de/Einstellungen/CSLLayouts), bzw. [JabRef-Layouts](https://www.bibsonomy.org/help_de/Einstellungen/JabRefLayouts) oder auf den offiziellen Seiten von [JabRef](http://jabref.sourceforge.net/help/CustomExports.php) und [CSL](http://docs.citationstyles.org/en/stable/primer.html).
    
-   **Lebenslauf:** Sie können die Information, die auf Ihrer
    Lebenslaufseite angezeigt werden, einsehen und bearbeiten. Außerdem
    können Sie das Layout editieren (eine Anleitung dazu [ finden Sie hier](LebenslaufÄndern "wikilink")).  
    
-   **OAuth-Consumers:** Eine Auflistung aller
    [OAuth](http://oauth.net/)-Consumer, die eine Autorisierung auf
    Ihren ${project.name}-Account von Ihnen bekommen haben. Wie man von
    Ihrer Anwendung aus mittels OAuth auf ${project.name} zugreift, [
    lesen Sie hier](OAuth "wikilink").  
    
-   **Gruppen:** Hier können Sie neue [Gruppen](Gruppenfunktionen "wikilink") erstellen und sehen, in
    welchen Gruppen Sie Mitglied sind.  
    
-   **Synchronisation:** Um Ihre Lesezeichen und Publikationen zwischen
    zwei Systemen synchron zu halten, können Sie hier einen [ neuen
    Synchronisationsclient](Synchronisation "wikilink") hinzufügen.  

------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink")