<!-- en: Synchronisation -->

## Synchronisation zwischen BibSonomy und PUMA
-------------------------------------------

Typischer Anwendungsfall: Sie haben Ihre Daten bislang bei [
PUMA](Projekte#section-Projekte-AkademischesPublikationsmanagementPUMA "wikilink") gepflegt, möchten sich aber bei PUMA abmelden und mit BibSonomy
weiterarbeiten. Oder Sie wollen die Zusatzfunktionen von PUMA nutzen und
Ihre Daten von BibSonomy zu PUMA überspielen, um auf beiden Systemen
immer mit den gleichen Daten zu arbeiten. Zum automatischen Abgleich
Ihrer Daten zwischen den beiden Systemen können Sie die
Synchronisationsfunktion nutzen. Voraussetzung ist, dass Sie auf
**beiden Systemen als Nutzer registriert sind**. Gehen Sie wie folgt
vor:  

-  Loggen Sie sich mit Ihren Benutzerdaten bei BibSonomy ein.

-   Gehen Sie ins Menü **"Einstellungen"** und klicken Sie auf die
    Registerkarte **"Synchronisation"**.
    
-   Geben Sie die Adresse des **Synchronisationsclients** ein, z. B.
    <http://puma.uni-kassel.de/>.
    
-   Wählen Sie eine **Synchronisationsrichtung** oder nutzen Sie die
    Möglichkeit, in **beide Richtungen** zu synchronisieren.
    
-   Wählen Sie aus, ob Sie **Publikationen und Lesezeichen** oder nur
    eines von beiden synchronisieren wollen.
    
-   Speichern Sie die Einstellungen, indem Sie auf die Schaltfläche
    **"Speichern"** klicken.
    
-   Klicken Sie zum Abschluss auf den Link **"Synchronisations-Seite"**
    und fordern Sie einen **Synchronisationsplan** an, um eine Vorschau
    auf die Synchronisation zu erhalten. Schließen Sie die
    Synchronisation mit einem Klick auf den **"OK"**-Button ab, wenn die
    Vorschau Ihren Vorstellungen entspricht. Andernfalls kommen Sie über
    den Link **"Einstellungen"** zurück zur Synchronisationsstartseite,
    um die Synchronisation anzupassen.
    

![ <File:sync.png>](synchronisation/sync.png  "fig: File:sync.png")

------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink") 


