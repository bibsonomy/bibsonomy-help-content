<!-- en: InterIntraHash -->

## Inter-Hashes und Intra-Hashes
-----------------------------

In ${project.name} werden alle Einträge (Lesezeichen und Publikationen)
zusammen mit einem *Hash* (einer eindeutigen Ziffern- und
Buchstabenfolge) abgespeichert, um den Eintrag eindeutig identifizieren
zu können. Außerdem werden Publikationen sogar mit zwei Hashes
abgespeichert (Inter-Hash und Intra-Hash). Auf dieser Seite finden Sie
Informationen zu den verschieden Hash-Typen.


------------------------------------------------------------------------

### Warum werden Hashes benutzt?

Bei Literaturreferenzen ist es oft problematisch, Duplikate zu erkennen,
da es von Nutzer zu Nutzer Unterschiede gibt, wie Felder zu einer
Publikation (z.B. Name des Journals oder Autor) ausgefüllt werden.
Einerseits sollte es Nutzern erlaubt sein, mehrere Publikationen
gespeichert zu haben, die sich nur minimal unterscheiden. Andererseits
möchte man Publikationen anderer Nutzer finden, die sich alle auf
dasselbe Paper oder dasselbe Buch beziehen, auch wenn sie nicht komplett
identisch sind.

Um beide Ziele zu erfüllen, gibt es in ${project.name} zwei Hashes, um
Publikationen zu vergleichen. Einer ist dazu da, um die Publikationen
eines einzelnen Nutzers zu vergleichen **(Intra-Hash)**, der andere
vergleicht Publikationen verschiedener Nutzer **(Inter-Hash)**. Der
Vergleich erfolgt durch Normalisierung und Verknüpfung von
BibTeX-Feldern, dann wird das Ergebnis mit Hilfe des [MD5 message
digest-Algorithmus](ftp://ftp.rfc-editor.org/in-notes/rfc1321.txt) verarbeitet und anschließend die resultierenden Hashes verglichen. Die
MD5-Verarbeitung wird ausschließlich aus Effizienzgründen durchgeführt,
da diese Art der Verarbeitung die Speicherung in der Datenbank mit einer
festen Länge ermöglicht. Dadurch, dass die Hashes zusammen mit den
Einträgen in derselben Datenbanktabelle gespeichert werden, können
Einträge sehr schnell gesucht und miteinander verglichen werden.

Der **Intra-Hash** ist relativ streng und beinhaltet die Felder *Titel,
Autor, Editor, Jahr, Eintragstyp, Journal, Buchtitel, Volume* und
*Nummer*. Auf diese Weise wird es einem Nutzer möglich gemacht, Einträge
mit denselben Autoren im selben Jahr, aber mit unterschiedlicher Volume
(z.B. ein technischer Report und den dazugehörigen Journal-Artikel)
gespeichert zu haben.

Dagegen ist der **Inter-Hash** weniger spezifisch und beinhaltet
lediglich die Felder *Titel, Jahr* und *Autor* bzw. *Editor* (je
nachdem, was der Nutzer eingetragen hat).

In beiden Hashes werden die beinhalteten Felder normalisiert, das heißt,
spezifische Zeichen werden entfernt, Leerzeichen und
Autoren-/Editornamen werden normalisiert. Letzteres wird dadurch
erreicht, dass der erste Buchstabe des Vornamens mit einem Punkt mit dem
Nachnamen verknüpft wird (beides in Kleinbuchstaben). Personen werden
danach mit diesem String alphabetisch sortiert und mit einem Strichpunkt
verknüpft.

------------------------------------------------------------------------

### Demo

Auf der [Hash-Beispiel-Seite](${project.home}hashExample) wird das Generieren von Inter-Hash und Intra-Hash demonstriert.
Füllen Sie dort einfach die Felder des Formulars aus und lassen Sie
${project.name} beide Hashes berechnen.

------------------------------------------------------------------------

### Quellcode

Die Berechnung der Hashes wird in der Klasse
`org.bibsonomy.model.util.SimHash` ausgeführt.  
Der folgende Codeabschnitt berechnet den *Intra-Hash*:

	public static String getSimHash2(final BibTex bibtex) {
    		return StringUtils.getMD5Hash(StringUtils.removeNonNumbersOrLettersOrDotsOrSpace(bibtex.getTitle())     + " " + 
    				StringUtils.removeNonNumbersOrLettersOrDotsOrSpace(PersonNameUtils.serializePersonNames(bibtex.getAuthor(), false))    + " " + 
    				StringUtils.removeNonNumbersOrLettersOrDotsOrSpace(PersonNameUtils.serializePersonNames(bibtex.getEditor(), false))    + " " + 
    				StringUtils.removeNonNumbersOrLettersOrDotsOrSpace(bibtex.getYear())      + " " + 
    				StringUtils.removeNonNumbersOrLettersOrDotsOrSpace(bibtex.getEntrytype()) + " " + 
    				StringUtils.removeNonNumbersOrLettersOrDotsOrSpace(bibtex.getJournal())   + " " + 
    				StringUtils.removeNonNumbersOrLettersOrDotsOrSpace(bibtex.getBooktitle()) + " " +
    				StringUtils.removeNonNumbersOrLetters(bibtex.getVolume())                 + " " +
    				StringUtils.removeNonNumbersOrLetters(bibtex.getNumber())
    		);
    	}


Der folgende Codeabschnitt berechnet den *Inter-Hash*:


	public static String getSimHash1(final BibTex publication) {
		if (!present(StringUtils.removeNonNumbersOrLetters(PersonNameUtils.serializePersonNames(publication.getAuthor())))) {
			// no author set --> take editor
			return StringUtils.getMD5Hash(getNormalizedTitle(publication.getTitle()) + " " +
					PersonNameUtils.getNormalizedPersons(publication.getEditor())            + " " +
					getNormalizedYear(publication.getYear()));
		}
		// author set
		return StringUtils.getMD5Hash(getNormalizedTitle(publication.getTitle()) + " " + 
				PersonNameUtils.getNormalizedPersons(publication.getAuthor())            + " " + 
				getNormalizedYear(publication.getYear()));
	}

Um zu sehen, wie weitere Hilfsfunktionen funktionieren, besuchen Sie das
[Bitbucket-Repository](https://bitbucket.org/bibsonomy/bibsonomy/src/stable/bibsonomy-model/src/main/java/org/bibsonomy/model/util/).

------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink") 
  
  
  

