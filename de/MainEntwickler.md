<!-- en: MainDeveloper -->

Hilfe für Softwareentwickler
----------------------------

**Hinweis:** Auf dieser Seite finden Sie die **Hilfe für Softwareentwickler**.
Für weitere Informationen besuchen Sie die [ Hilfe für Einsteiger](MainEinsteiger "wikilink") und die
[ Hilfe für Fortgeschrittene](MainFortgeschrittene "wikilink").


${project.name} ist ein kostenloser Service, der Recherche,
Literaturverwaltung und Publikationsmanagement vereint. Darüberhinaus
ermöglicht ${project.name} das gemeinsame Arbeiten im Team (z. B. durch
den Austausch von Internet-Lesezeichen oder gerade gelesener Literatur).
Diese Seite fasst für Sie alle Funktionen für Softwareentwickler
zusammen.

----

![ <File:Binary-Code.png>](mainentwickler/Binary-Code.png  " File:Binary-Code.png")

### ${project.name} - Informationen für (Web-)Entwickler


Sie möchten ${project.name} in ihre Webseite/Webapp integrieren? - Kein
Problem. Wir stellen Ihnen kostenlos folgende Schnittstellen zur Verfügung.  

-  [BibSonomy/PUMA-Entwicklerdokumentation auf Bitbucket](https://bibsonomy.bitbucket.io/)

-   [REST-API](https://bitbucket.org/bibsonomy/bibsonomy/wiki/documentation/api/REST%20API)

<!-- -->

-   [ Integration in andere Systeme/Software - Übersicht](Integration "wikilink")

<!-- -->

-   [ JavaScript - Erleichtern Sie ihren Webseitenbesuchern Vermerke](JS-Codeschnipsel "wikilink")

<!-- -->

-   [Anonymisierte Datensätze zu eigenen Forschungszwecken beantragen](https://www.kde.cs.uni-kassel.de/bibsonomy/dumps/)


----

![ <File:News.png>](mainentwickler/News.png  " File:News.png")

### ${project.name} - Informationen für Journalisten/Presse


Informationen für Pressevertreter und Interessierte zu ${project.name}
und den Schwester-Projekten finden Sie hier:  

-   [ Projekte/Kooperation - Eine Übersicht](Projekte "wikilink")

<!-- -->

-   [ Presse zu BibSonomy/PUMA](Presse "wikilink")


----

${project.theme == "bibsonomy"}{

![<File:Messages-Information-01.png>](mainentwickler/Messages-Information-01.png  " File:Messages-Information-01.png")

### Direkter Kontakt zu den Entwicklern von ${project.name}


Haben Sie Fragen? Brauchen Sie Hilfe? Oder haben Sie vielleicht
Vorschläge, was wir noch besser machen könnten? Hier finden Sie unsere
Kontaktdaten. Wir freuen uns auf Ihr Feedback.  

------------------

![<File:Message-Mail.png>](mainentwickler/Message-Mail.png  "fig: File:Message-Mail.png")
<webmaster@bibsonomy.org>

-   Direkte Hilfe bei Fragen und Problemen zu ${project.name}.  

-------------------

![<File:WordPress.png>](mainentwickler/WordPress.png  "fig: File:WordPress.png")
[blog.bibsonomy.org](http://blog.bibsonomy.org)

-   "Feature of the week"
-   Meldungen zu Releases und aktuellen Entwicklungen
-   Kommentare, die beantwortet werden

------------------------

![<File:Twitter-Bird.png>](mainentwickler/Twitter+Bird.png  "fig: File:Twitter-Bird.png")
[Join "BibSonomyCrew" on Twitter](https://twitter.com/BibSonomyCrew)

-   Updates zu Entwicklungen
-   Direkter Kontakt

------------------

![<File:Conference-Call.png>](mainentwickler/Conference-Call.png  "fig: File:Conference-Call.png")
Für Freunde der guten alten Mailinglisten:
<bibsonomy-discuss-de@cs.uni-kassel.de>

-   Diskussionen rund um BibSonomy
-   Diskussionen sind öffentlich und alle Nutzer sehen die Fragen und Antworten

}

-----------------------


Auf dieser Seite finden Sie die **Hilfe für Softwareentwickler**.
Für weitere Informationen besuchen Sie die [ Hilfe für Einsteiger](MainEinsteiger "wikilink") und die
[ Hilfe für Fortgeschrittene](MainFortgeschrittene "wikilink").  


