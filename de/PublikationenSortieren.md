<!-- en: SortingPublications -->

## Publikationen sortieren
-----------------------

Auf ${project.name} gibt es mehrere Seiten, auf denen Ihnen Lesezeichen
und Publikationen in zwei Spalten angezeigt werden (z.B. [ Home](${project.home}"wikilink") oder [ die Seite eines
Nutzers](${project.home}user/hotho "wikilink")). Auf diesen Seiten haben Sie die
Möglichkeit, die Lesezeichen und Publikationen zu sortieren.


------------------------------------------------------------------------

### Einfache Sortierung

Für die einfache Sortierung klicken Sie in der jeweiligen Spalte auf das
**Sortierungssymbol**, dem Symbol mit dem Pfeil neben vier Querbalken. 
Nun werden die Optionen zur **Sortierung** angezeigt.
![ <File:menu.png>](publikationensortieren/menu.png  "fig: File:menu.png")  
  
- **Kriterium:** Sie können die Einträge nach **Datum** oder nach
**Titel** (alphabetisch) sortiert anzeigen lassen.

-   **Reihenfolge:** Wählen Sie zwischen **aufsteigend** (ältestes
    Datum - jüngstes Datum bzw. A - Z) und **absteigend** (jüngstes
    Datum - ältestes Datum bzw. Z - A).

------------------------------------------------------------------------

### Erweiterte Sortierung

**Info:** Die erweiterte Sortierung kann **nur für Publikationen** verwendet werden.

Die erweiterte Sortierung erfolgt durch Parameter, die an die jeweilige URL angehängt werden (siehe auch [
URL-Syntax](URL-Syntax "wikilink")). Folgende Parameter stehen Ihnen zur Verfügung:  
  
- **sortPage** - Wonach wird sortiert?

	-   **Werte (können durch | verknüpft werden):**
        -   **author** - Autorenname
        -   **editor** - Herausgebername
        -   **year** - Erscheinungsjahr
        -   **entrytype** - Publikationstyp
        -   **title** - Titel
        -   **booktitle** - Buchtitel (insb. bei Artikel in Sammelbänden)
        -   **journal** - Journalname
        -   **school** - Universitätsname
        
-   **sortPageOrder** - Reihenfolge der Sortierung
    -   **Werte:**
        -   **asc** - aufsteigend
        -   **desc** - absteigend
        
-   **duplicates**
    -   **Werte:**
        -   **yes** - Erlaube Duplikate
        -   **no** - Entferne Duplikate aus der Ergebnisansicht

  
**Beispiel:**  
**?sortPage=year&sortPageOrder=asc&duplicates=no**  
Sortiere nach Erscheinungsjahr (sortPage=year) aufsteigend
(sortPageOrder=asc) und entferne alle Duplikate (duplicates=no).  
  
  
------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink") 

