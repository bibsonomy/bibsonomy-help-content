<!-- en: DiscoveryService -->

## Discovery Service
-----------------

Viele Bibliotheken deutschland-/weltweit setzen auf sogenannte *Resource
Discovery Services* (RDS) zur Bestandsverwaltung. ${project.name}
bietet eine direkte RDS-Schnittstelle und wird von fast allen hessischen
Universitätsbibliotheken unterstützt (HeBIS-Verbund).

Eine Liste der uns bekannten Bibliotheken, die ${project.name}
unterstützen:

-   [Suchportal der ULB Darmstadt](http://hds.hebis.de/ulbda)
-   [Suchportal Frankfurt am Main](http://hds.hebis.de/ubffm)
-   [JustFind UB Gießen](http://hds.hebis.de/ubgi)
-   [KARLA II - Das Rechercheportal der Universität Kassel](http://hds.hebis.de/ubks)
-   [Rechercheportal der Universitätsbibliothek Mainz](http://hds.hebis.de/ubmz)
-   [Katalog Plus - Das Rechercheportal der Universität Marburg](http://hds.hebis.de/ubmr)
-   [FILIP - Suchportal der Hochschul- und Landesbibliothek Fulda](http://hds.hebis.de/hlbfu/)
-   [Katalog Plus der Hochschul- und Landesbibliothek Rhein-Main](http://hds.hebis.de/hsrm/)
-   [THMfind - Suchportal der Technischen Hochschule Mittelhessen](http://hds.hebis.de/thm/)

  
Fragen Sie ggf. bei Ihrer Bibliothek nach, ob ${project.name}
unterstützt wird. Im Folgenden zeigt diese Anleitung, wie Sie über den
Bibliothekskatalog Publikationen in Ihre Literaturliste übernehmen
können.

1.  Suchen Sie über das HeBIS-System der Bibliothek nach einer
    gewünschten Publikation. Im Beispiel wird nach "Textmining" gesucht.
2.  Klicken Sie auf **"Merkliste"** bzw. **"Senden an"** - Wenn ein
    Auswahlmenü erscheint, dann wählen Sie ${project.name} (manche
    Bibliotheken zeigen kein Auswahlmenü, sondern springen gleich zum
    nächsten Punkt).
3.  Füllen Sie alle Felder aus und klicken Sie abschließend auf
    **"Speichern"**. Die Daten werden für Sie direkt in ${project.name}
    unter Publikationen gespeichert.

  
![ <File:1.png>](discoveryservice/1.png  "fig: File:1.png")

------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink")
    
  

