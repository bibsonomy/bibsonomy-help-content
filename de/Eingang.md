<!-- en: Inbox -->

## Eingang
-----------------

Im [ Eingang](${project.home}inbox "wikilink") werden Einträge gespeichert, die
Ihre Freunde Ihnen geschickt haben.

![ <File:inbox.png>](eingang/inbox.png  "fig: File:inbox.png") 

---------------------------------

### Verschicken von Einträgen

Um ein Lesezeichen oder eine Publikation an einen anderen
${project.name}-Nutzer (z. B. *xyz*) zu schicken, können Sie das [
Systemtag](SystemTags "wikilink") `send:xyz` benutzen. Der Eintrag wird
dann getaggt mit `from:<IhrNutzername>` und in den Eingang von *xyz*
kopiert. Um den Missbrauch des Eingangs zu verhindern, muss der
Empfänger des Eintrags

-   entweder Sie als Freund hinzugefügt haben
-   oder ein Mitglied einer Gruppe sein, in der auch Sie ein
    Mitglied sind.

Nachdem der Eintrag erfolgreich gesendet wurde, wird er mit dem Tag
`sent:xyz` versehen.

![ <File:sendtag.png>](eingang/sendtag.png  "fig: File:sendtag.png")

-----------------------------

### Erhalten und Verwenden von Einträgen

Im Eingang liegen alle Einträge, die Ihnen Ihre Freunde geschickt haben.
Benutzen Sie **"Diese Publikation/dieses Lesezeichen in die eigene
Sammlung kopieren"**, um den Eintrag in Ihre Sammlung zu übertragen.

![ <File:copypublication.png>](eingang/copypublication.png  "fig: File:copypublication.png")

  

Mit **"Diese Publikation/dieses Lesezeichen aus Ihrem Eingang entfernen"**
können Sie den Eintrag aus dem Eingang löschen und mit dem **"Eingang
leeren"**-Knopf entfernen Sie sämtliche Einträge aus Ihrem Eingang.

![ <File:removepublication.png>](eingang/removepublication.png  "fig: File:removepublication.png")  

------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink")


