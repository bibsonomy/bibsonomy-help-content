<!-- en: CommunityPosts -->

## Community Posts
---------------

### Was sind Community Posts?

${project.theme == "bibsonomy"}{

In ${project.name} werden Lesezeichen und Publikationen automatisch auf
zwei verschiedene Repräsentationsweisen gespeichert. Wenn Sie ein Lesezeichen/eine Publikation zu Ihrer Sammlung hinzufügen,
wird ein **privater Eintrag** erstellt, der zu Ihrer Sammlung gehört und
der nur von Ihnen selbst bearbeitet werden kann.  
Außerdem wird eine **öffentliche Repräsentation** für dieses
Lesezeichen/diese Publikation erstellt, ein sogenannter **Community
Post** (Gemeinschaftseintrag). Diese Art von Eintrag gibt einen
Überblick über alle Informationen, die von verschiedenen Nutzern über
diesen Eintrag verfügbar sind. Die Metadaten dieses Eintrages können von
allen Nutzern bearbeitet werden.  
Idealerweise kann die Community von ${project.name} alle relevanten Informationen zu der jeweiligen Veröffentlichung bereitstellen.

------------------------------------------------------------------------

### Wie kann ich einen Community Post anschauen?

Um zur **Community Post-Ansicht** eines Eintrages zu kommen, klicken Sie
einfach auf einen Eintrag, z.B. in Ihrer Sammlung oder im
${project.name}-Feed. Klicken Sie dann auf den **schwarzen Pfeil**
rechts neben dem Eintrag und schließlich auf **Community Post**.  

![ <File:arrow.png>](communityposts/arrow.png  "fig: File:arrow.png")  

------------------------------------------------------------------------

### Community Post - Seitenübersicht

In diesem Abschnitt werden die dargestellten Informationen auf der
Community Post-Seite erklärt.  

-   **Aktions-Buttons:**  
    Ganz oben auf der Community Post-Seite befinden sich Buttons.  
    ![ <File:buttons.png>](communityposts/buttons.png  "fig: File:buttons.png")  
    - Mit dem **Stift** können Sie den Community Post bearbeiten (allgemeine und erweiterte Informationen angeben, Kommentare und Notizen hinzufügen).
    -   Der Button mit den **drei Linien** zeigt Ihnen den **Verlauf des
        Eintrages** (nur sichtbar, wenn der Eintrag einen Verlauf hat).
    -   Falls Sie den Eintrag noch nicht in Ihrer Sammlung haben, können
        Sie ihn durch Klicken auf den **grünen Button in Ihre Sammlung
        kopieren**.  
        Falls Sie den Eintrag bereits in Ihrer Sammlung haben, wird ein
        anderer Button gezeigt, durch den Sie zur **Ansicht des
        Eintrages in Ihrer Sammlung** gelangen (nicht der Community
        Post).  
        ![
        <File:buttonsCollection.png>](communityposts/buttonsCollection.png  "fig: File:buttonsCollection.png")  

<!-- -->

-   **Allgemeine Informationen:**  
    Die wichtigsten Informationen zu einem Eintrag, z.B. **Titel**,
    **Autoren**, **Journal** und **Jahr**.

<!-- -->

-   **Zusammenfassung:**  
    Eine kurze Zusammenfassung des Eintrags. Klicken Sie auf **(mehr)**,
    um die ganze Zusammenfassung zu lesen.

<!-- -->

-   **Links und Ressourcen:**  
    Links, die zu diesem Eintrag angegeben wurden, z.B. **URL**, **DOI**
    oder ein **Dokument**.

<!-- -->

-   **Tags:**  
    [ Tags](Tags "wikilink"), mit denen dieser Eintrag versehen worden
    ist (von allen Nutzern, die diesen Eintrag in ihrer Sammlung
    haben).  
    ![ <File:tags.png>](communityposts/tags.png  "fig: File:tags.png")  

<!-- -->

-   **Nutzer:**  
    Nutzer, die diesen Eintrag in ihrer Sammlung haben. Klicken Sie auf
    das **Foto** eines Nutzers, um zu dessen Nutzerseite zu gelangen.
    Klicken Sie auf das **Buchsymbol** unter dem Foto, um den Eintrag in
    der Sammlung des Nutzers anzusehen.  
    ![ <File:users.png>](communityposts/users.png  "fig: File:users.png")  

<!-- -->

-   **Kommentare und Rezensionen:**  
    Kommentare, Rezensionen und Bewertungen für diesen Eintrag
    (falls vorhanden). Außerdem können Sie [ Ihren eigenen
    Kommentar/Ihre eigene Rezension
    verfassen](KommentarRezensionBewertung "wikilink").  
    ![ <File:comments.png>](communityposts/comments.png  "fig: File:comments.png")  

<!-- -->

-   **Zitieren Sie diese Publikation:**  
    Verschiedene **Zitierformate** für diesen Eintrag, z.B. Harvard,
    APA, BibTeX und EndNote.

}

${project.theme == "puma"}{

In ${project.name} werden Lesezeichen und Publikationen automatisch auf
zwei verschiedene Repräsentationsweisen gespeichert. Wenn Sie ein Lesezeichen/eine Publikation zu Ihrer Sammlung hinzufügen,
wird ein **privater Eintrag** erstellt, der zu Ihrer Sammlung gehört und
der nur von Ihnen selbst bearbeitet werden kann.  
Außerdem wird eine **öffentliche Repräsentation** für dieses
Lesezeichen/diese Publikation erstellt, ein sogenannter **Community
Post** (Gemeinschaftseintrag). Diese Art von Eintrag gibt einen
Überblick über alle Informationen, die von verschiedenen Nutzern über
diesen Eintrag verfügbar sind. Die Metadaten dieses Eintrages können von
allen Nutzern bearbeitet werden.  
Idealerweise kann die Community von ${project.name} alle relevanten Informationen zu der jeweiligen Veröffentlichung bereitstellen.

------------------------------------------------------------------------

### Wie kann ich einen Community Post anschauen?

Um zur **Community Post-Ansicht** eines Eintrages zu kommen, klicken Sie
einfach auf einen Eintrag, z.B. in Ihrer Sammlung oder im
${project.name}-Feed. Klicken Sie dann auf den **schwarzen Pfeil**
rechts neben dem Eintrag und schließlich auf **Community Post**.  

![ <File:arrow.png>](communityposts/arrow.png  "fig: File:arrow.png")  

------------------------------------------------------------------------

### Community Post - Seitenübersicht

In diesem Abschnitt werden die dargestellten Informationen auf der
Community Post-Seite erklärt.  

-   **Aktions-Buttons:**  
    Ganz oben auf der Community Post-Seite befinden sich Buttons.  
    ![ <File:buttons.png>](communityposts/buttons.png  "fig: File:buttons.png")  
    - Mit dem **Stift** können Sie den Community Post bearbeiten (allgemeine und erweiterte Informationen angeben, Kommentare und Notizen hinzufügen).
    -   Der Button mit den **drei Linien** zeigt Ihnen den **Verlauf des
        Eintrages** (nur sichtbar, wenn der Eintrag einen Verlauf hat).
    -   Falls Sie den Eintrag noch nicht in Ihrer Sammlung haben, können
        Sie ihn durch Klicken auf den **grünen Button in Ihre Sammlung
        kopieren**.  
        Falls Sie den Eintrag bereits in Ihrer Sammlung haben, wird ein
        anderer Button gezeigt, durch den Sie zur **Ansicht des
        Eintrages in Ihrer Sammlung** gelangen (nicht der Community
        Post).  
        ![
        <File:buttonsCollection.png>](communityposts/buttonsCollection.png  "fig: File:buttonsCollection.png")  

<!-- -->

-   **Allgemeine Informationen:**  
    Die wichtigsten Informationen zu einem Eintrag, z.B. **Titel**,
    **Autoren**, **Journal** und **Jahr**.

<!-- -->

-   **Zusammenfassung:**  
    Eine kurze Zusammenfassung des Eintrags. Klicken Sie auf **(mehr)**,
    um die ganze Zusammenfassung zu lesen.

<!-- -->

-   **Links und Ressourcen:**  
    Links, die zu diesem Eintrag angegeben wurden, z.B. **URL**, **DOI**
    oder ein **Dokument**.

<!-- -->

-   **Tags:**  
    [ Tags](Tags "wikilink"), mit denen dieser Eintrag versehen worden
    ist (von allen Nutzern, die diesen Eintrag in ihrer Sammlung
    haben).  
    ![ <File:tags.png>](communityposts/tags.png  "fig: File:tags.png")  

<!-- -->

-   **Nutzer:**  
    Nutzer, die diesen Eintrag in ihrer Sammlung haben. Klicken Sie auf
    das **Foto** eines Nutzers, um zu dessen Nutzerseite zu gelangen.
    Klicken Sie auf das **Buchsymbol** unter dem Foto, um den Eintrag in
    der Sammlung des Nutzers anzusehen.  
    ![ <File:users.png>](communityposts/users.png  "fig: File:users.png")  

<!-- -->

-   **Kommentare und Rezensionen:**  
    Kommentare, Rezensionen und Bewertungen für diesen Eintrag
    (falls vorhanden). Außerdem können Sie [ Ihren eigenen
    Kommentar/Ihre eigene Rezension
    verfassen](KommentarRezensionBewertung "wikilink").  
    ![ <File:comments.png>](communityposts/comments.png  "fig: File:comments.png")  

<!-- -->

-   **Zitieren Sie diese Publikation:**  
    Verschiedene **Zitierformate** für diesen Eintrag, z.B. Harvard,
    APA, BibTeX und EndNote.

}

${project.theme == "wueresearch"}{

In ${project.name} werden Lesezeichen und Publikationen automatisch auf
zwei verschiedene Repräsentationsweisen gespeichert. Wenn Sie ein Lesezeichen/eine Publikation zu Ihrer Sammlung hinzufügen,
wird ein **privater Eintrag** erstellt, der zu Ihrer Sammlung gehört und
der nur von Ihnen selbst bearbeitet werden kann.  
Außerdem wird eine **öffentliche Repräsentation** für dieses
Lesezeichen/diese Publikation erstellt, ein sogenannter **Community
Post** (Gemeinschaftseintrag). Diese Art von Eintrag gibt einen
Überblick über alle Informationen, die von verschiedenen Nutzern über
diesen Eintrag verfügbar sind. Die Metadaten dieses Eintrages können von
allen Nutzern bearbeitet werden.  
Idealerweise kann die Community von ${project.name} alle relevanten Informationen zu der jeweiligen Veröffentlichung bereitstellen.

------------------------------------------------------------------------

### Wie kann ich einen Community Post anschauen?

Um zur **Community Post-Ansicht** eines Eintrages zu kommen, klicken Sie
einfach auf einen Eintrag, z.B. in Ihrer Sammlung oder im
${project.name}-Feed. Klicken Sie dann auf den **schwarzen Pfeil**
rechts neben dem Eintrag und schließlich auf **Community Post**.  

![ <File:arrow.png>](communityposts/arrow.png  "fig: File:arrow.png")  

------------------------------------------------------------------------

### Community Post - Seitenübersicht

In diesem Abschnitt werden die dargestellten Informationen auf der
Community Post-Seite erklärt.  

-   **Aktions-Buttons:**  
    Ganz oben auf der Community Post-Seite befinden sich Buttons.  
    ![ <File:buttonswue.png>](communityposts/buttonswue.png  "fig: File:buttonswue.png")  
    - Mit dem **Stift** können Sie den Community Post bearbeiten (allgemeine und erweiterte Informationen angeben, Kommentare und Notizen hinzufügen).
    -   Der Button mit den **drei Linien** zeigt Ihnen den **Verlauf des
        Eintrages** (nur sichtbar, wenn der Eintrag einen Versionsverlauf hat).
    -   Falls Sie den Eintrag noch nicht in Ihrer Sammlung haben, können
        Sie ihn durch Klicken auf den **Kopieren**-Button in Ihre Sammlung
        kopieren.  
        Falls Sie den Eintrag bereits in Ihrer Sammlung haben, wird ein
        anderer Button angezeigt, durch den Sie zur **Ansicht des
        Eintrages in Ihrer Sammlung** gelangen (nicht der Community
        Post).  
        ![
        <File:buttonsCollectionwue.png>](communityposts/buttonsCollectionwue.png  "fig: File:buttonsCollectionwue.png")  

<!-- -->

-   **Allgemeine Informationen:**  
    Die wichtigsten Informationen zu einem Eintrag, z.B. **Titel**,
    **Autoren**, **Journal** und **Jahr**.

<!-- -->

-   **Zusammenfassung:**  
    Eine kurze Zusammenfassung des Eintrags. Klicken Sie auf **(mehr)**,
    um die ganze Zusammenfassung zu lesen.

<!-- -->

-   **Links und Ressourcen:**  
    Links, die zu diesem Eintrag angegeben wurden, z.B. **URL**, **DOI**
    oder ein **Dokument**.

<!-- -->

-   **Tags:**  
    [ Tags](Tags "wikilink"), mit denen dieser Eintrag versehen worden
    ist (von allen Nutzern, die diesen Eintrag in ihrer Sammlung
    haben).  
    ![ <File:tagswue.png>](communityposts/tagswue.png  "fig: File:tagswue.png")  

<!-- -->

-   **Nutzer:**  
    Nutzer, die diesen Eintrag in ihrer Sammlung haben. Klicken Sie auf
    das **Foto** eines Nutzers, um zu dessen Nutzerseite zu gelangen.
    Klicken Sie auf das **Buchsymbol** unter dem Foto, um den Eintrag in
    der Sammlung des Nutzers anzusehen.  
    ![ <File:userswue.png>](communityposts/userswue.png  "fig: File:userswue.png")  

<!-- -->

-   **Kommentare und Rezensionen:**  
    Kommentare, Rezensionen und Bewertungen für diesen Eintrag
    (falls vorhanden). Außerdem können Sie [ Ihren eigenen
    Kommentar/Ihre eigene Rezension
    verfassen](KommentarRezensionBewertung "wikilink").  
    ![ <File:commentswue.png>](communityposts/commentswue.png  "fig: File:commentswue.png")  

<!-- -->

-   **Zitieren Sie diese Publikation:**  
    Verschiedene **Zitierformate** für diesen Eintrag, z.B. Harvard,
    APA, BibTeX und EndNote.

}

------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink")


