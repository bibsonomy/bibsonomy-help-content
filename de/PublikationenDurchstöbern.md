<!-- en: BrowsePublications -->

## Publikationen/Lesezeichen durchstöbern
--------------------------------------

Es wurde bereits gezeigt, wie man nach [ Lesezeichen/Publikationen
suchen kann](LesezeichenPublikationenManagen "wikilink"). Die folgende Anleitung zeigt Ihnen, wie Sie Ihre eigenen Lesezeichen/Publikation
durchstöbern können. Sie erhalten so einen schnellen Überblick des
eigenen Literaturbestands.  
  
**Voraussetzung:** Sie müssen, um diese Anleitung durchführen zu können,
die [ erweiterte Ansicht freischalten](ErweiterteFunktionenFreischalten "wikilink").  
  
![ <File:1.png>](publikationendurchstöbern/1.png  "fig: File:1.png")  
![ <File:2.png>](publikationendurchstöbern/2.png  "fig: File:2.png")  
  
So durchstöbern Sie Ihre Lesezeichen/Publikationen:  

1.  Klicken Sie mit der Maus auf **"mein ${project.name}"**. Ein Menü
    klappt auf. Klicken Sie dann auf **"Publikationen durchstöbern"**
    **(Hinweis**: Dieser Menüpunkt ist nur zu sehen, wenn Sie zuvor die [erweiterte Ansicht freigeschaltet haben](ErweiterteFunktionenFreischalten "wikilink")).

2.  Unter **"Suchoptionen"** können Sie verschiedene
    **Tags** (Schlagworte) und/oder **Autoren** auswählen, zu denen Sie
    alle Einträge anzeigen möchten. Um mehr als nur einen Listenpunkt zu
    wählen, halten Sie die STRG- bzw. CTRL-Taste während des
    Mausklicks gedrückt.
    
3.  Die Buttons **"und / oder"** können Sie dazu nutzen, um die
    Listenauswahl unterschiedlich zu verknüpfen.
    
4.  In der Liste **"Suchergebnisse"** sehen Sie alle Ergebnisse, die auf
    die Auswahl von 2. und 3. passen.
   
5.  Das Textfeld **"Filter"** ermöglicht es Ihnen, die Suchergebnisse
    von 4. noch weiter zu filtern.

------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink") 
  

