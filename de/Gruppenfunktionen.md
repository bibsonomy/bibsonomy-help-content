<!-- en: GroupFunctions -->

## Gruppenfunktionen
-----------------

Gruppen erleichtern in ${project.name} die gemeinsame
Literaturrecherche. Gruppen können sowohl das gemeinsame Arbeiten an
Artikeln/Projekten erleichtern, als auch innerhalb einer
Institution/Arbeitsgruppe die Kommunikationen über neue und interessante
fremde/eigene Artikel erleichtern. 

Auf dieser Seite finden Sie Verweise zu kurzen
Anleitungen zu den verschiedenen Gruppenfunktionen.  

Einige Gruppenfunktionen sind Administratoren der Gruppe vorbehalten. 
[Hier](GruppenfunktionenAdmin "wikilink") können Sie mehr über diese Funktionen erfahren.

------------------------------------------------------------------------

### Gruppen suchen und beitreten

Möchten Sie eine Gruppe suchen und ihr beitreten, befolgen Sie die Schritte dieser [Anleitung](Gruppenfunktionen/GruppeSuchen "wikilink").

------------------------------------------------------------------------

### Eine neue Gruppe gründen

[Hier](Gruppenfunktionen/GruppeGruenden "wikilink") wird Ihnen erklärt, wie Sie eine neue Gruppe
erstellen können.  

------------------------------------------------------------------------

### Die Gruppenseite

Die Gruppenseite bietet Ihnen mehrere Funktionen. Vergleichen Sie dazu diese [Anleitung](Gruppenfunktionen/GruppenSeite "wikilink").

------------------------------------------------------------------------

### Lesezeichen/Publikationen mit der Gruppe teilen

Bei Bibsonomy ist es möglich, mit einer Gruppe Lesezeichen und Publikationen zu teilen. [Diese Schritte](Gruppenfunktionen/GruppeLesezeichen "wikilink") sind dazu erforderlich.

------------------------------------------------------------------------

### Gruppendiskussionen

[Diese Hilfeseite](Gruppenfunktionen/GruppenDiskussionen "wikilink") zeigt Ihnen, wie Sie innerhalb einer Gruppe mit Kollegen diskutieren können.

------------------------------------------------------------------------

### Aus einer Gruppe austreten

Möchten Sie aus einer Gruppe wieder austreten,  befolgen Sie die Schritte dieser [Anleitung](Gruppenfunktionen/GruppeAustreten "wikilink"). 

------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink") 