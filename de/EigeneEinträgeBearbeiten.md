<!-- en: EditOwnEntries -->

## Eigene Einträge bearbeiten
--------------------------

**Ziel:** Diese Anleitung zeigt Ihnen, wie Sie mehrere
Lesezeichen/Publikationen gleichzeitig bearbeiten können.  
**Hinweis:** Sie können nur Ihre **eigenen Einträge**
bearbeiten (das heißt, Einträge, die Sie in Ihre Sammlung kopiert
haben). Einträge, die sich nicht in Ihrer Sammlung befinden, können
**nicht bearbeitet werden**.  

------------------------------------------------------------------------

### Übersicht

Wenn Sie nicht nur ein Lesezeichen/eine Publikation, sondern mehrere auf
einmal bearbeiten möchten, gehen Sie auf Ihre **Nutzerseite** (z.B.
durch Klicken auf Ihren Nutzernamen rechts auf der blauen Leiste) und
klicken Sie auf das **Stiftsymbol** rechts neben der Lesezeichen- bzw.
Publikationsspalte.   

${project.theme == "bibsonomy"}{

![ <File:menu.png>](eigeneeinträgebearbeiten/menu.png  "fig: File:menu.png")  

Sie werden auf eine Seite weitergeleitet, die folgendermaßen aufgebaut ist:  

![ <File:page_neu.png>](eigeneeinträgebearbeiten/page_neu.png  "fig: File:page_neu.png")  

}

${project.theme == "puma"}{

![ <File:menu.png>](eigeneeinträgebearbeiten/menu.png  "fig: File:menu.png")  

Sie werden auf eine Seite weitergeleitet, die folgendermaßen aufgebaut ist:  

![ <File:page_neu.png>](eigeneeinträgebearbeiten/page_neu.png  "fig: File:page_neu.png")  

}

${project.theme == "wueresearch"}{

![ <File:menuwue.png>](wueresearch/EigeneEinträgeBearbeiten/menuwue.png  "fig: File:menuwue.png")  

Sie werden auf eine Seite weitergeleitet, die folgendermaßen aufgebaut ist:  

![ <File:page_neuwue.png>](wueresearch/EigeneEinträgeBearbeiten/page_neuwue.png  "fig: File:page_neuwue.png")  

}

1. **Aktion auswählen:** Hier können Sie wählen, was Sie an Ihren
	Einträgen ändern möchten.
2.  **Tags hinzufügen:** Hier können Sie die Tags eintragen, die Ihren
    Einträgen hinzugefügt werden sollen.
3.  **Sichtbarkeit setzen:** Legen Sie fest, für wen Ihre Einträge
    sichtbar sein sollen.
4.  **Ihre Einträge:** Hier sehen Sie Ihre Einträge aufgelistet, rechts
    daneben stehen die jeweiligen Tags der Einträge. Durch Klicken auf
    das Quadrat ganz links neben einem Eintrag können Sie Einträge aus-
    oder abwählen.
5.  **Nächste Seite:** Wenn Ihre Einträge nicht auf eine Seite passen,
    können Sie hier auf die nächste Seite mit Einträgen wechseln. Sie
    können die maximale Anzahl der Einträge, die auf einer Seite
    angezeigt werden, in den [Einstellungen](${project.home}settings "wikilink") unter dem Reiter
    "Einstellungen" festlegen.  

**Anmerkung:** Einige Felder auf der Seite sind zu Beginn **inaktiv**
(das heißt, sie können noch nicht benutzt werden und werden **grau
dargestellt**). Erst nachdem Sie eine **Aktion ausgewählt** haben,
werden die jeweiligen Felder aktiv.

------------------------------------------------------------------------

### Aktionen

Unter **"Bitte Aktion auswählen"** stehen **fünf Aktionen** zur Auswahl,
um Einträge zu bearbeiten. Setzen Sie dort **Häkchen** neben den
Einträgen, für die die Aktion gelten soll. Nach der Aktion bestätigen
Sie die Änderungen durch Klicken auf **"Absenden"**.  
![ <File:Actions.png>](eigeneeinträgebearbeiten/Actions.png  "fig: File:Actions.png") 

#### Tags zu allen ausgewählten Posts hinzufügen
Bei dieser Option können Sie in das Feld **"Tags hinzufügen"** die [Tags](Tags "wikinlink") eintragen, die den ausgewählten Einträgen hinzugefügt werden sollen.  


#### Die Tags aller ausgewählten Posts separat bearbeiten
Bei dieser Option können Sie die [Tags](Tags "wikilink") Ihrer Einträge **einzeln bearbeiten**, indem Sie die Änderungen in das Feld neben einem Eintrag ("Ihre Tags") schreiben. 


#### BibTeX-Schlüssel normalisieren
Hier können Sie die BibTeX-Schlüssel der ausgewählten Einträge normalisieren, das heißt, der Schlüssel der Einträge wird auf das Format ***AutorJahrTitel*** gesetzt.  


#### Einträge löschen
Hier können Sie die Einträge auswählen, die aus Ihrer Sammlung **gelöscht** werden sollen.


#### Sichtbarkeit einstellen
Hier können Sie die Sichtbarkeit der ausgewählten Einträge ändern. Wählen Sie im Feld **"Sichtbarkeit
setzen"** zwischen den Optionen **öffentlich**, **privat** und **andere** (z.B. Freunde oder Gruppen).  

------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink")

