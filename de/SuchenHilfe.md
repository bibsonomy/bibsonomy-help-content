<!-- en: SearchPageHelp -->

## Suchoptionen
------------

Auf dieser Seite finden Sie einen Überblick über die verschiedenen
Optionen, die Sie bei der Suche in ${project.name} verwenden können.
Weitere Details dazu finden Sie bei [Apache Lucene - Query Parser Syntax](https://lucene.apache.org/core/5_2_1/queryparser/org/apache/lucene/queryparser/classic/package-summary.html#package_description).  
Geben Sie zum Suchen Ihre Suchanfragen einfach in die Suchleiste **oben
rechts** ein.

![ <File:searchbar.png>](suchenhilfe/searchbar.png  "fig: File:searchbar.png")  

-----------------------------

### Übersichtstabelle

Ausdruck | Bedeutung | Beispiel 
-- | -- | -- | --
term | ein einfaches Wort finden | `hello` 
"..." | eine Folge von Wörtern finden | `"hello dolly"`
 ? | ein beliebiges einzelnes Zeichen (das Fragezeichen darf nicht das erste Zeichen einer Suchanfrage sein) | `te?t` 
* | beliebige Zeichenketten (der Stern darf nicht das erste Zeichen einer Suchanfrage sein) | `test*` 
OR | Seite wird gefunden, wenn einer mehrerer Terme enthalten ist | `"hello dolly" hello` `"hello dolly" OR hello` 
AND | Seite wird gefunden, wenn alle Terme enthalten sind | `"hello dolly" AND "dolly lucy"`
+ | Term nach dem '+' muss enthalten sein | `+hello dolly` 
-/NOT/! | Term nach dem '-' darf nicht enthalten sein.  Alternativen: NOT oder ! | `"hello dolly" -"dolly lucy"` / `"hello dolly" NOT "dolly lucy"` 
(...) | Schachtelung von Anfragen | `(hello OR dolly) AND website` 
\~ | *unscharfe* Suchanfragen. Finden von Wörtern, die ähnlich geschrieben werden. | `roam~`
\~n | *Umgebungssuche* innerhalb einer Umgebung einer festlegbaren Anzahl von Wörtern | `"hello dolly"~10`
\^n | *Verstärkungsfaktor* um Wichtigkeiten in einer Suchanfrage abzustufen | `"hello dolly"^4 "dolly lucy"`  
\\ | *Escapen* von Zeichen mit Sonderbedeutung: + - && &#124;&#124; ! ( ) { } [ ] \^ " \* ? : \\ | `\(1\+1\)\:2` für (1+1):2

------------------------------------------------------------------------

### Beispiele

#### Mit einem Wort suchen

Um nach einem einzelnen Wort zu suchen, geben Sie dieses Wort einfach in
die Suchleiste ein. **Die Groß-/Kleinschreibung wird dabei nicht beachtet.** 

**Beispiel:** `java` findet alle Einträge zum Thema "java".  

------------------------------------------------------------------------

#### Mit mehreren Wörtern suchen

Um mit mehreren Wörtern zu suchen, setzen Sie **Anführungszeichen** vor
und nach die Wörter.  

**Beispiel:** `"programming language"` findet alle Einträge mit der
Phrase "programming language".  

------------------------------------------------------------------------

#### Platzhalter (? und \*)

Wenn Sie sich der Schreibweise eines Wortes nicht sicher sind oder nach
mehreren Versionen eines Wortes suchen wollen, können Sie Platzhalter
benutzen. **Der Platzhalter darf dabei nie am Anfang einer Suchanfrage
stehen.**  

**?:** Ein Fragezeichen steht für ein beliebiges einzelnes Zeichen.  
**Beispiel:** Mit dem Ausdruck `te?t` finden Sie sowohl Einträge mit dem
Wort "test" als auch "text".  

**\*:** Ein Stern steht für eine beliebig lange Zeichenkette.  
**Beispiel:** Mit dem Ausdruck `test*` finden Sie Einträge mit den
Wörtern "test", "tests", "tester" und "testing".  

**Kombination:** Dies ist auch mit mehreren Wörtern möglich. 
**Beispiel:**
Mit dem Ausdruck `te?t foo?` werden alle Einträge mit einer 
beliebigen Kombination der Buchstaben angezeigt. Also "text food",
"text foot", aber auch "test food" und "test foot".

------------------------------------------------------------------------

#### Reguläre Ausdrücke

In ${project.name} können Sie auch mit [Regulären Ausdrücken](https://de.wikipedia.org/wiki/Regul%C3%A4rer_Ausdruck)
suchen. Der Suchausdruck muss dazu zwischen zwei **Schrägstrichen** stehen.  

**Beispiel:** Mit `/[bl]ook/` suchen Sie nach "book" und "look".  

------------------------------------------------------------------------

#### AND, OR, NOT, + und -

Die Operatoren AND, OR und NOT gehören zu den [Boolschen Operatoren](https://de.wikipedia.org/wiki/Boolescher_Operator). Mit
ihnen können mehrere Suchwörter miteinander verknüpft werden.  

**AND:** Wenn Sie zwei Suchwörter durch AND miteinander verknüpfen,
werden nur Einträge gefunden, in denen **beide** Suchwörter vorkommen.  
**Beispiel:** `honey AND bee` zeigt Einträge an, die sowohl "honey"
als auch "bee" enthalten.  

**OR:** Wenn Sie zwei Suchwörter durch OR miteinander verknüpfen, werden
Einträge gefunden, in denen **eines der beiden** Suchwörter vorkommen.  
**Beispiel:** `"computer science" OR computer` findet Einträge mit
den Wörtern "computer science" oder "computer".  

**NOT:** Mit der Verknüpfung NOT können Sie Wörter, nach denen nicht
gesucht werden soll, ausschließen.  
**Beispiel:** `"computer science" NOT computer` sucht nur nach
Einträgen, in denen "computer science" vorkommt, und schließt Einträge
mit dem Wort "computer" aus.  

**'+' und '-':** Ein vorangestelltes '+' bedeutet, dass ein Wort
**vorkommen muss**, ein '-' dagegen, dass das Wort **nicht vorkommen
darf**. Enthält eine Seite sowohl mit '-' als auch mit '+' markierte
Suchwörter, so taucht sie nicht in der Ergebnisliste auf.  
**Beispiel:** `+java -script` findet Seiten, in denen das Wort "java"
vorkommen muss, das Wort "script" aber nicht vorkommen darf.  

**Kombination:** Diese Operatoren können auch mit Platzhaltern und Regulären
Ausdrücken kombiniert werden.
**Beispiel**: Suchen Sie nach `global AND foo?`, dann werden
Sie Einträge finden, in denen die **beiden** Suchwörter "global" und
"food" bzw. "foot" vorkommen.

------------------------------------------------------------------------

#### Schachtelung

Mit **runden Klammern** können Sie Ihre Suchanfragen gruppieren und
schachteln. Dadurch bleibt es immer klar, welcher Operator zuerst
ausgeführt wird.  

**Beispiel:** Der Ausdruck `(user OR person) AND test` sucht
nach Seiten, in denen entweder die Wörter "user test" oder "person test"
vorkommen.  

Der Ausdruck `user:hotho (tags:web OR tags:data)` zeigt alle Einträge des 
Users "hotho" an, die entweder den Tag "web" oder "data" haben.

------------------------------------------------------------------------

#### Unscharfe Suchanfragen

Unter unscharfen Suchanfragen versteht man Suchanfragen, bei denen ein
oder mehrere Buchstaben des Suchterms **ausgetauscht** werden dürfen. Es
wird also nach Wörtern gesucht, die ähnlich geschrieben werden.  
Eine Zahl nach dem '~'-Symbol legt fest, wie viele Buchstaben
geändert werden dürfen (Zahlen **zwischen 0 und 2** sind erlaubt). Wenn
keine Zahl nach dem '~'-Symbol steht, werden automatisch 2 Änderungen
angenommen.  

**Beispiel:** Die Suche mit dem Term `site~` findet auch Einträge mit
den Wörtern "side" oder "kate". Die Suchanfrage `site~1` findet Einträge
mit "kate" jedoch nicht mehr.  

------------------------------------------------------------------------

#### Umgebungssuche

Bei der Suche ist es möglich, zu bestimmen, in welchem Abstand zwei
Wörter maximal voneinander entfernt sein dürfen. Dazu werden nach einer
Suchphrase ein '~'-Symbol und eine Zahl für die Anzahl der Wörter
angefügt.  

**Beispiel:** Die Suchanfrage `"java training"~10` findet nur Einträge,
in denen die Wörter "java" und "training" nicht mehr als 10 Wörter voneinander 
entfernt vorkommen.  

---------------------------------

#### Verstärkungsfaktor

Wenn ein Suchterm relevanter ist als ein anderer, können Sie diesen
verstärken. Dazu fügen Sie ein '^'-Symbol und eine positive Zahl an den
zu verstärkenden Suchterm an. Je höher die Zahl, desto mehr wird der
Term verstärkt.  

**Beispiel:** Im Suchterm `java^4 script` wird verstärkt nach dem Term
"java" gesucht.  



------------------------------------------------------------------------

#### Escapen (maskieren)

Maskieren bedeutet, die Funktion der Sonderzeichen zu unterdrücken und
es als normales Zeichen darzustellen. Um Sonderzeichen **( + - && &#124;&#124; ! ( ) { } [ ] \^ " \* ? : \\ )** zu maskieren,
schreiben Sie ein **'\\**' vor das Zeichen.  

**Beispiel:** Der Term `\(1\+1\)\:2` steht für die Suchanfrage "(1+1):2".  

------------------------------------------------------------------------

#### Suche in Kategorien

Um Ihre Suchanfrage präziser zu gestalten, können Sie in einzelnen
Kategorien suchen (Tag, Benutzer, Gruppe, Autor, Konzept,
BibTeX-Schlüssel). Dazu klicken Sie entweder auf den **blauen Pfeil**
neben dem Feld **Suche** und wählen die passende Kategorie aus
(empfohlen), oder Sie schreiben die Kategorie direkt in das Suchfeld
(siehe Tabelle). **Achtung:** Dafür müssen Sie die englische Bezeichnung
der Kategorien benutzen.  

Kategorie | Beschreibung | Beispiel
-- | -- | -- | --
tags:*term* | sucht nur in der Liste der Tags | `tags:java` 
user:*term* | sucht nur in der Liste der Nutzer | `user:hotho` 
group:*term* | sucht nur in der Liste der Gruppen | `group:kde` 
author:*term* | sucht nur in der Liste der Autoren | `author:gabbard`

**Beispiel:** Suchen Sie nach `user:hotho`, dann finden Sie Einträge des Nutzers "hotho".

**Kombination:** Sie können die verschiedenen Kategorien auch in einer Suchanfrage kombinieren.
**Beispiel:** Wenn Sie im Suchfeld als Volltextsuche 
den Ausdruck 
`user:hotho (tags:web OR tags:data)` eingeben, 
erhalten Sie als Ergebnis der Suchanfrage Einträge des Nutzers "hotho", die den Tag
"web" oder "data" haben.

------------------------------------------------------------------------

#### Intervallsuche

Sie haben die Möglichkeit, Einträge in einem bestimmten Intervall zu
suchen. Dazu müssen Sie in der Suchanfrage angeben, in welcher
**Kategorie** gesucht werden soll (z.B. title, author,...) und
anschließend das Intervall festlegen.
Bei **eckigen Klammern** werden die Intervallgrenzen mit einbezogen,
bei **geschweiften Klammern** werden sie nicht dazu gezählt. 

**Beispiel:**
Mit der Suchanfrage `title: [java TO perl]` werden alle Einträge gefunden, deren
Titel alphabetisch gesehen zwischen "java" und "perl" liegt ("java" und
"perl" werden dazugezählt).  

Mit der Suchanfrage `author: {gabbard TO grasset}` werden alle
Einträge der Autoren aufgezählt, die alphabetisch gesehen zwischen
"gabbard" und "grasset" liegen ("gabbard" und "grasset" werden nicht
dazugezählt).  

------------------------------------------------------------------------

#### Fortgeschrittene Suche

Zusätzlich zu den verschiedenen Kategorien (Tag, Benutzer, Gruppe, Autor, Konzept, 
BibTeX-Schlüssel), welche Sie durch das Klicken auf den blauen Pfeil neben dem **Suchfeld**
auswählen können, haben Sie die Möglichkeit, Ihre Suchanfrage noch präziser auszuführen, indem
Sie in einer der folgenden Kategorien suchen.
Tippen Sie hierzu die gewünschte Kategorie, gefolgt von einem Doppelpunkt und Ihrem Suchbegriff,
in die Suchleiste ein.

**Beispiel:** `journal: European Economic Review`

**Kombination:** Durch die Nutzung von Kategorien und anderen Suchmethoden können Sie Ihre 
Suchergebnisse beliebig verfeinern.
**Beispiel:** Die Suche `journal: Historische Zeitschrift (rezension OR imperial)` findet
Publikationen in der "Historischen Zeitschrift", die **entweder** das Wort "rezension" **oder** 
das Wort "imperial" beinhalten.


Kategorie | Beschreibung | 
-- | -- | -- | -- 
doi: | DOI  einer Publikation
isbn: | ISBN  einer Publikation
issn: | ISSN einer Publikation
journal: | voll ausgeschriebener Name der Zeitschrift
volume: | Band einer Zeitschrift oder eines Buches einer mehrbändigen Reihe
year: | Erscheinungsjahr (aus vier Ziffern bestehend)
publisher: | Name des Verlags
adress: | Adresse des Verlags oder der Institution
edition: | Auflage eines Buches, als Zahlwort geschrieben (z.B. "Zweite")
institution: | Name der finanzierenden Institution eines Berichts
organization: | Name der finanzierenden Organisation einer Konferenz / eines Handbuchs
series: | Name einer Reihe von Büchern
school: | Name der Schule, an der eine Abhandlung geschrieben wurde
language: | Sprache eines Eintrags
misc: | Informationen aus zusätzlichen Feldern für Informationen, die nicht in die vordefinierten Kategorien passen

------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink") 