<!-- en: AddonGoogleDocs -->

## Addon für Google Docs
---------------------

**Hinweis:** Diese Funktion befindet sich momentan noch
in der Entwicklungsphase. Wir arbeiten momentan noch an einer
nutzerfreundlicheren Lösung. Technisch versierten Nutzern wollen wir
aber diese neue Funktion nicht vorenthalten. Wir freuen uns über Ihr
Feedback.

Das ${project.name}-Addon für Google Docs ermöglicht es Ihnen, in
Google Docs direkt aus ${project.name} zu zitieren.

**Wichtig:** Bevor Sie die Anleitung ausführen, lesen Sie diese bitte
einmal komplett durch, so müssen Sie nicht wieder zu dieser Seite
zurückspringen.

**Voraussetzungen:** Sie benötigen einen Google-Account.

1.  Klicken Sie auf [diesen
    Link](https://docs.google.com/document/d/1O9h5zqcd8UAOnJszDnilr4-_0uBfCk2Pw-qdDtphtPE/edit?usp=sharing),
    Sie gelangen dadurch zu Google Docs. Gegebenenfalls müssen Sie sich
    bei Google Docs anmelden.
    
2.  Sie sehen das von uns vorbereitete Dokument. Dieses enthält
    zusätzliche Skripte, die es erlauben, auf
    ${project.name} zuzugreifen. Das Dokument wird automatisch Ihrer
    Bibliothek hinzugefügt.
3.  Um mit dem Dokument arbeiten zu können, müssen Sie es kopieren. Nur
    mit der Kopie können Sie dann arbeiten. Klicken Sie dazu auf
    **"Datei"** - **"Eine Kopie erstellen..."**.  
    
    ![ <File:1.png>](addongoogledocs/1.png  "fig: File:1.png")  
    
4.  Um eine Publikation mit ${project.name} zu zitieren, klicken Sie in
    Google Docs auf **"Addons"** - **"BibSonomy"** - **"cite from BibSonomy"**.  
    
    ![ <File:2.png>](addongoogledocs/2.png  "fig: File:2.png")  
    
5.  Bei der ersten Anmeldung müssen Sie sich authentifizieren. Klicken
    Sie dazu auf **"Zulassen"**.

_Weitere Informationen:_
<https://bitbucket.org/fwhkoenig/bibsonomy-googledocs-add-on/wiki/Home>
(auf Englisch).  

------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink")
