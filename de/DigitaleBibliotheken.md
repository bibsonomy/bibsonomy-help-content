<!-- en: DigitalLibraries -->

## Digitale Bibliotheken
---------------------

Mit ${project.name} haben Nutzer von Bibliothekskatalogen (Online
Public Access Catalogues, OPAC) die Möglichkeit, die aus dem Katalog
gezogenen Informationen direkt in ${project.name} zu kopieren und zu
verwalten.

----------------

### Wozu Social Bookmarking mit digitalen Bibliotheken verknüpfen?

Social Bookmarking verbessert Suche, Erfassung und Verwaltung von
digitalen Informationen. Es ermöglicht den Teilnehmern Informationen 
beizutragen und auszutauschen. Benutzer können die bibliografischen 
Informationen, die sie aus dem OPAC-Bibliothekskatalog bezogen haben,
speichern und mit ihren persönlichen Kommentaren versehen. Zudem ist es
möglich, auch in den Ressourcen anderer Nutzer zu forschen und damit die eigene
Sammlung um interessante Objekte zu vergrößern. Sie können so zahlreiche
Informationsquellen übersichtlich verwalten und sich durch verschiedene
Themenbereiche leiten lassen.

Nachfolgend einige Beispiele für die Integration von ${project.name} in
OPAC-Bibliothekskataloge.

------------------------------------------------------------------------

### ${project.name} in der Bibliothek der Universität Köln

Die [digitale Bibliothek der Universität Köln](http://kug.ub.uni-koeln.de/portal/kug/home.html?l=de) hat einen
Link auf ${project.name} eingebunden, um es ihren Nutzern zu
ermöglichen, die Suchergebnisse aus dem Katalog direkt in
${project.name} zu speichern. Wenn Sie auf das **grüne
${project.name}-Symbol (rechts neben einem Eintrag)** klicken, werden
Sie zu ${project.name} weitergeleitet. Sie können die Literaturdaten
dann nochmals bearbeiten und Informationen hinzufügen.

![ <File:koeln1.png>](digitalebibliotheken/koeln1.png  "fig: File:koeln1.png")  

------------------------------------------------------------------------

### ${project.name} in der Bibliothek der Universität Heidelberg

Die [digitale Bibliothek der Universität Heidelberg](http://katalog.ub.uni-heidelberg.de/cgi-bin/search.cgi)
bietet das Speichern der Metadaten von Büchern und anderen Publikationen
in ${project.name} ebenfalls an. Um Literaturdaten nach
${project.name} zu extrahieren, klicken Sie auf einen Eintrag in der
Ergebnisliste. Rechts oben in der grauen Leiste klicken Sie auf
**"Exportieren/Zitieren"**. Dort wählen Sie anschließend
**${project.name}**. Sie werden zu ${project.name} weitergeleitet
und können auch hier die Daten vor dem Speichern nochmals bearbeiten.

![ <File:heidelberg.png>](digitalebibliotheken/heidelberg.png  "fig: File:heidelberg.png")  

------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink")

