<!-- en: DiscussedPosts -->

## Diskutierte Einträge
--------------------

Ein wichtiger Aspekt bei der Arbeit mit Literatur ist der Austausch von
Ideen und Gedanken darüber mit Kollegen. Die [Diskussionsmöglichkeiten](KommentarRezensionBewertung "wikilink") in
${project.name} unterstützen Sie dabei, indem sie ein
Kommunikationsforum für Gedanken zu Publikationen und Webseiten bieten,
in dem man sich mit Freunden, Kollegen oder einfach allen Benutzern
unterhalten kann.

Die "Diskutierte-Einträge"-Seiten geben einen Überblick über Ressourcen,
die vor kurzem diskutiert wurden. Dabei kann zwischen Seiten mit
Einträgen von einem bestimmten Benutzer, Einträgen einer bestimmten
Gruppe ([mehr zu Diskussion in Gruppen](Gruppenfunktionen "wikilink")) oder einer Seite mit Einträgen von beliebigen Benutzern des Systems gewählt werden.

--------------

### Beliebte Diskussionen

Um die Einträge, die vor kurzem diskutiert worden sind, anzuzeigen,
klicken Sie im [ linken Hauptmenü](Programmoberfläche "wikilink") auf
**Beliebt** und anschließend auf **Diskussionen**. Durch Klicken auf die
Sternbewertung eines Eintrags gelangen Sie zur Detailansicht. Dort
können Sie Rezensionen und Kommentare ansehen und verfassen.

-------------

### Diskussionen eines Benutzers

Um Ressourcen zu finden, die von einem bestimmten Benutzer diskutiert
wurden, rufen Sie zunächst dessen Benutzerseite auf (durch Klicken auf
den Nutzernamen **oder** durch Nutzen der Suchfunktion). In der Sidebar
auf der rechten Seite klicken Sie auf den Link **"Zeige von -Nutzername-
diskutierte Einträge"**.  
![ <File:user.png>](diskutierteeinträge/user.png  "fig: File:user.png")  
Um die **eigenen diskutierten Einträge** aufzurufen, klicken Sie im [
linken Hauptmenü](Programmoberfläche "wikilink") auf **mein ${project.name}** und anschließend auf **diskutierte Einträge**.

------------

### Diskussionen einer Gruppe

Ähnlich wie die Benutzer-fokussierte Version zeigen diese Seiten
Ressourcen, die von Mitgliedern einer bestimmten Gruppe diskutiert
wurden. Um die diskutierten Einträge einer Gruppe aufzurufen, rufen Sie
zunächst die [ Gruppenseite](Gruppenfunktionen "wikilink") auf (durch
Klicken auf den Gruppennamen **oder** durch Nutzen der Suchfunktion). In
der Sidebar auf der rechten Seite klicken Sie auf den Link **"Zeige
kürzlich diskutierte Einträge von -Gruppenname-"**.  
![ <File:group.png>](diskutierteeinträge/group.png  "fig: File:group.png")  

**Hinweis:** Auf jeder der oben genannten Übersichtsseiten
werden selbstverständlich die **Sichtbarkeitsbeschränkungen** der
Diskussionsbeiträge berücksichtigt. Zum Beispiel werden auf der
Diskutierte-Einträge-Seite eines Benutzers keine Ressourcen aufgelistet,
die dieser Benutzer nur anonym kommentiert oder rezensiert hat.

-------------

### Diskussionsstatistik

Auf jeder Seite, wo diskutierte Einträge eines Benutzers, einer Gruppe
oder von ${project.name} insgesamt aufgelistet sind, befindet sich
rechts in der Sidebar eine **Statistik-Box**. Dort wird die
Bewertungsverteilung aller Rezensionen des Benutzers/der Gruppe/des
Systems dargestellt. Auf diese Weise werden Bewertungen besser
vergleichbar.  
  
![ <File:statistic.png>](diskutierteeinträge/statistic.png  "fig: File:statistic.png")

------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink")