<!-- en: TagCloud.css -->

tagCloud.css
------------

    #tags {
      float: right;
      width: 40%;
      margin: 0px;
      padding: 5px;
      background-color: #EEEEEE;
      border:1px dotted #BBBBBB;
      color:white;
      font-family:Verdana,Geneva,Arial,Helvetica,sans-serif;
      font-size:100.01%;
    }
    
    .title {
	    margin:15px 15px 15pt 10px;
	    padding: 0px;
	    color: #006699;
	    text-decoration: none;
	    font-size: 18px;
	    font-weight: bold;
    }

