<!-- en: DiscussionReviewsComments -->

## Diskutieren, Kommentieren und Bewerten
------------------------------------------------------------------------

${project.name} verfügt über ein praktisches Kommentar- und
Bewertungssystem. Dieses System ermöglicht es gemeinsam mit anderen über
Publikationen zu diskutieren, Rezensionen zu schreiben oder ganz schnell
das eigene Votum in "Sternen" auszudrücken.

------------------------------------------------------------------------

### Diskussion in ${project.name}

In ${project.name} kann über jede Ressource (Lesezeichen oder Publikation) diskutiert werden. 
Die Funktionsweise und alle Vorraussetzungen finden Sie [hier](KommentarRezensionBewertung/Diskussion "wikilink").

------------------------------------------------------------------------

### Publikation/Lesezeichen bewerten

Möchten Sie Einträge, also Lesezeichen oder Publikationen, bewerten, dann folgen Sie [dieser Anleitung](KommentarRezensionBewertung/Bewerten "wikilink").

------------------------------------------------------------------------

### Rezensionen kommentieren

Rezensionen, die andere Nutzer zu Lesezeichen/Publikationen geschrieben
haben, können wiederum beliebig oft kommentiert werden. Um eine
Rezension zu kommentieren, klicken Sie auf den **schwarzen Pfeil** unter
der Rezension.  
Das **Verfassen eines Kommentars** funktioniert genauso wie das Verfassen
einer Rezension, außer, dass die Bewertung (die Vergabe von Sternen) wegfällt.  

![<File:comment_neu.png>](KommentarRezensionBewertung/comment_neu.png  "fig: File:comment_neu.png")  

-----------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink")


