<!-- en: SignInWithOpenIDAccount -->

## Mit OpenID-Account bei ${project.name} anmelden
------------------------------------------------

**Ziel:** Diese Anleitung zeigt Ihnen, wie Sie sich mithilfe eines
OpenID-Accounts bei ${project.name} anmelden.

**Wichtig (Voraussetzung):** Für diese Anleitung benötigen Sie einen
OpenID-Account. Wie Sie einen solchen Account anlegen, erfahren Sie [auf
der folgenden Webseite](http://openid.net/get-an-openid/).

**Alternativ:** Wenn Sie nicht über einen OpenID-Account verfügen, dann
empfehlen wir Ihnen, einen [ eigenen Account](NeuenNutzeraccountAnlegen "wikilink") für ${project.name} anzulegen.

---------------

### Was ist OpenID?

[OpenID](http://openid.net/) ist ein offener, dezentralisierter
Standard, der es dem Benutzer ermöglicht, sich bei vielen verschiedenen
Diensten im Internet mit denselben Anmeldedaten anzumelden. Sie müssen
sich dafür nur einmal bei Ihrem OpenID-Diensteanbieter anmelden und
benötigen daher nur ein Kennwort. Diese Art der Authentifizierung wird
bereits von einer wachsenden Anzahl großer Websites benutzt, wie AOL,
Google, Microsoft, MySpace, Yahoo und vielen anderen.

--------------------

### Anleitung:

1.  Klicken Sie auf der Startseite auf **"Anmelden"**. Ein neues Fenster öffnet sich.
2.  Wählen Sie nun Ihren gewünschten OpenID-Provider, z.B. Yahoo.

    ![ <File:1.png>](mitopenidanmelden/1.png  "fig: File:1.png")  
    
3.  ${project.name} leitet Sie nun zu Ihrem OpenID-Provider weiter.
    Bitte folgen Sie einfach den Anweisungen auf dem Bildschirm (das
    weitere Vorgehen ist je nach Provider unterschiedlich).  
    
    **Hinweis:** Ihr OpenID-Provider wird Sie fragen, ob Sie der
    Webseite ${project.home} Zugriff auf Ihre Daten geben
    möchten. Diese Frage müssen Sie bestätigen, wenn Sie
    ${project.name} nutzen möchten.
    
4.  Sobald Sie sich einmal mit OpenID bei ${project.name} erfolgreich
    angemeldet haben, können Sie jederzeit den oben genannten Weg
    nutzen, um sich erneut anzumelden.

------------------------------------------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink")
