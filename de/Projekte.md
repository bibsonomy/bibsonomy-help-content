<!-- en: Projects -->

## Unsere Projekte
---------------

Die folgenden Projekte sind an ${project.name} beteiligt:

${project.theme == "bibsonomy"}{

-----------------------------

### Akademisches Publikationsmanagement (PUMA)

![<File:puma-logo_schrift-web.png>](projekte/puma-logo_schrift-web.png  "fig: File:puma-logo_schrift-web.png")

[PUMA](http://puma.uni-kassel.de/) kann wie BibSonomy als
Social-Bookmarking-System und als Online-Literaturverwaltung genutzt
werden, es unterstützt jedoch zusätzliche Funktionen, die für den
Einsatz an Hochschulen bzw. Hochschulbibliotheken sowie
wissenschaftlichen Institutionen optimiert ist.

PUMA wurde von der Deutschen Forschungsgemeinschaft (DFG) von 2009-2011
gefördert. Eine zweite Förderphase läuft seit 2013. Die Abkürzung PUMA
steht für "Akademisches Publikationsmanagement".

Weitere Informationen zu PUMA erhalten Sie unter <http://www.academic-puma.de/> 

-----------------------------------

}

### Nepomuk

Wir beteiligen uns am Europäischen Forschungsprojekt Nepomuk. Das
Projekt vereinigt Forscher, Entwickler industrieller Software und
repräsentative Nutzer aus der Industrie, um eine umfassende Lösung dafür
zu entwickeln, wie man die persönliche Arbeitsoberfläche zu einer
Umgebung der Zusammenarbeit erweitert, welche sowohl das persönliche
Informationsmanagement als auch das gemeinsame Zugreifen und den
Austausch in sozialen und organisatorischen Beziehungen unterstützt.
Diese Lösung wird Sozialer Semantischer Desktop genannt.

![ <File:bubbles2.png>](projekte/bubbles2.png  " File:bubbles2.png")

[Nepomuk](http://nepomuk.semanticdesktop.org/) hat die Entwicklung und
den Einsatz einer umfassenden Lösung als Ziel, wie zum Beispiel
Methoden, Datenstrukturen und eine Reihe von Werkzeugen, um den PC zu
einer kollaborativen Umgebung zu erweitern. Dadurch wird die aktuelle
Technik der Online-Zusammenarbeit und der persönlichen Datenverwaltung
verbessert, indem die von Einzelpersonen oder Gruppen gesammelten
Informationen zur Verfügung gestellt und organisiert werden.

------------------------

### TAGora

Das TAGora-Projekt, ein von der Europäischen Kommission finanziertes
[STREP](http://cordis.europa.eu/fp6/instr_strp.htm)-Projekt, befasst sich mit der *Simulation emergenter Eigenschaften in komplexen
Systemen*.

![<File:tagora_artwork1.png>](projekte/tagora_artwork1.png  " File:tagora_artwork1.png")

Der kollaborative Charakter, der vielen Web 2.0-Anwendungen zugrunde
liegt, rückt sie in den Blickpunkt der Wissenschaft komplexer Systeme,
da das Problem der Verbindung des Benutzerverhaltens auf niedriger Ebene
mit den globalen Anwendungszielen auf höchster Ebene ein von der
Wissenschaft der Komplexität typischerweise angegangenes Problem ist:
der Versuch zu verstehen, wie eine beobachtete Struktur durch die
Aktivität und die Wechselwirkung vieler global unkoordinierter
Handelnder entsteht. Die große Zahl beteiligter Nutzer in Verbindung mit
der Tatsache, dass deren Aktivität im Netz stattfindet, bietet erstmalig
die einzigartige Möglichkeit, das "mikroskopische" Verhalten der Nutzer
zu beobachten und es mit den Anwendungseigenschaften auf höchster Ebene
(z.B. die globalen Eigenschaften einer Folksonomie) zu verbinden, indem
man formale Tools und Konzepte der Wissenschaft der Komplexität nutzt.
Das TAGora-Projekt zielt darauf ab, diese einzigartigen Möglichkeiten,
die sich durch die wachsende Popularität netzbasierter sozialer
Wechselbeziehungen in einer Vielzahl von Kontexten ergeben, zu
verwerten.

-------------------------

### Microsoft Accelerating Search

Wir haben den [Microsoft Accelerating Search in Academic Research 2006
RFP Award](http://research.microsoft.com/ur/us/fundingopps/RFPs/Search_2006_RFP_Awards.aspx) gewonnen. Das Ziel dieses RFP ist es, die Zusammenarbeit von Live Labs
mit der akademischen Forschungsgemeinschaft zu unterstützen, und es
setzt den Akzent auf das Forschungsgebiet der Internetsuche. Unser
Projekt befasste sich mit der Weiterentwicklung link-basierter Suche
mittels der sozialen Suche, um eine verbesserte Funktionalität und
multiple Suchparadigmen für das Netz anbieten zu können.

![ <File:ms_logo.png>](projekte/ms_logo.png  " File:ms_logo.png")

------------------------------

### Informationelle Selbstbestimmung im Web 2.0

Die neue Generation des Internets („Web 2.0“ oder „soziales Internet“)
zeichnet sich durch eine sehr freizügige Informationsbereitstellung
durch die Nutzer aus. Vor diesem Hintergrund ist es das Ziel dieses
DFG-Projektes, in enger Interaktion von Informatikern und Juristen die
Chancen und Risiken der neuen Web2.0-Technologien in einem ausgewählten
Szenario zu erkunden und zu gestalten.

Nach Bestandsaufnahme und mittelfristiger Szenarienbildung werden im
Projekt die technischen und rechtlichen Chancen und Risiken, bezogen auf
typisierte Rollen, analysiert. Generische Konzepte für die
datenschutzgerechte Gestaltung der Anwendungen (Identitätsmanagement,
Vermeidung von Personenbezug und Profilbildung, Verantwortlichkeiten)
werden erarbeitet. Parallel dazu werden Algorithmen und Verfahren für
zwei spezifische Aufgaben entwickelt, die diese Konzepte respektieren:
Recommender-Systeme für kooperative Verschlagwortungssysteme sowie
Spam-Entdeckungsverfahren für solche Systeme. Sie werden anhand realer
Daten evaluiert. Die erfolgreichsten Ansätze werden in das kooperative
Publikationsverwaltungssystem ${project.name} implementiert und dort im
laufenden Betrieb evaluiert. Schließlich wird analysiert, inwieweit
Dogmatik und Auslegung des Datenschutzrechts wegen der neuen
Problemlagen des Web 2.0 verändert werden muss und eventuell
gesetzgeberische Aktivitäten erforderlich oder ratsam sind.

------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink") 