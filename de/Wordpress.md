<!-- en: wordpress -->

## WordPress Plugin
--------------------------

Mit dem `bibsonomy_csl` WordPress-Plugin können Sie Publikationslisten
basierend auf der [Citation Style Language (CSL)](http://citationstyles.org) erstellen.

-----------------------------

${project.theme == "bibsonomy"}{

### Features

-   [Fügen Sie Publikationslisten in Artikel ein] (Integration/wordpress/Publikationslisten "wikilink"), indem Sie die
    Meta-Box-Integration nutzen
-   Alternativ können Sie WordPress-Shortcodes nutzen
-   Wählen Sie Ihre Publikationen aus verschiedenen Inhaltstypen
    aus (z.B. user/group/viewable)
-   Filtern Sie die Ergebnisse mit Tags (Schlagwörtern) oder einer
    Volltextsuche
-   Wählen Sie Ihren bevorzugten Zitierstil aus einer Liste aus
    vorinstallierten Stilen aus
-   Alternativ können Sie Ihren eigenen Zitierstil nutzen
-   Bearbeiten Sie das Layout Ihrer Liste mit CSS
-   Speichern Sie Ihre API-Einstellungen (z.B. API-Nutzer, API-Key) auf
    einer separaten Optionsseite für Administratoren
-   Fügen Sie Ihrem Blog [Tagwolken] (Integration/wordpress/Tagwolken "wikilink") aus BibSonomy hinzu und wählen Sie
    zwischen drei verschiedenen Layouts

Für weitere Informationen zu den Features und deren Nutzung, klicken Sie [hier](Integration/wordpress/Publikationslisten "wikilink").

-----------------------------

### Download

Das WordPress Plugin finden Sie im Wordpress-Plugin-Repository:
[BibSonomy CSL WordPress Plugin](http://wordpress.org/extend/plugins/bibsonomy-csl/).  
Eine detallierte Anleitung zur Installation finden Sie [hier](Integration/wordpress/Installation "wikilink").  

}

${project.theme == "puma"}{

### Features

-   [Fügen Sie Publikationslisten in Artikel ein] (Integration/wordpress/Publikationslisten "wikilink"), indem Sie die
    Meta-Box-Integration nutzen
-   Alternativ können Sie WordPress-Shortcodes nutzen
-   Wählen Sie Ihre Publikationen aus verschiedenen Inhaltstypen
    aus (z.B. user/group/viewable)
-   Filtern Sie die Ergebnisse mit Tags (Schlagwörtern) oder einer
    Volltextsuche
-   Wählen Sie Ihren bevorzugten Zitierstil aus einer Liste aus
    vorinstallierten Stilen aus
-   Alternativ können Sie Ihren eigenen Zitierstil nutzen
-   Bearbeiten Sie das Layout Ihrer Liste mit CSS
-   Speichern Sie Ihre API-Einstellungen (z.B. API-Nutzer, API-Key) auf
    einer separaten Optionsseite für Administratoren
-   Fügen Sie Ihrem Blog [Tagwolken] (Integration/wordpress/Tagwolken "wikilink") aus BibSonomy hinzu und wählen Sie
    zwischen drei verschiedenen Layouts

Für weitere Informationen zu den Features und deren Nutzung, klicken Sie [hier](Integration/wordpress/Publikationslisten "wikilink").

-----------------------------

### Download

Das WordPress Plugin finden Sie im Wordpress-Plugin-Repository:
[BibSonomy CSL WordPress Plugin](http://wordpress.org/extend/plugins/bibsonomy-csl/).  
Eine detallierte Anleitung zur Installation finden Sie [hier](Integration/wordpress/Installation "wikilink").  

}

${project.theme.help == "wueresearch"}{

### Features

-   [Fügen Sie Publikationslisten in Artikel ein] (Integration/wordpress/Publikationslisten "wikilink"), indem Sie die
    Meta-Box-Integration nutzen
-   Alternativ können Sie WordPress-Shortcodes nutzen
-   Wählen Sie Ihre Publikationen aus verschiedenen Inhaltstypen
    aus (z.B. user/group/viewable)
-   Filtern Sie die Ergebnisse mit Tags (Schlagwörtern) oder einer
    Volltextsuche
-   Wählen Sie Ihren bevorzugten Zitierstil aus einer Liste aus
    vorinstallierten Stilen aus
-   Alternativ können Sie Ihren eigenen Zitierstil nutzen
-   Bearbeiten Sie das Layout Ihrer Liste mit CSS
-   Speichern Sie Ihre API-Einstellungen (z.B. API-Nutzer, API-Key) auf
    einer separaten Optionsseite für Administratoren
-   Fügen Sie Ihrem Blog [Tagwolken] (Integration/wordpress/Tagwolken "wikilink") aus BibSonomy hinzu und wählen Sie
    zwischen drei verschiedenen Layouts

Für weitere Informationen zu den Features und deren Nutzung, klicken Sie [hier](Integration/wordpress/Publikationslisten "wikilink").

-----------------------------

### Download

Das WordPress Plugin finden Sie im Wordpress-Plugin-Repository:
[BibSonomy CSL WordPress Plugin](http://wordpress.org/extend/plugins/bibsonomy-csl/).  
Eine detallierte Anleitung zur Installation finden Sie [hier](Integration/wordpress/Installation "wikilink").  

}

------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink") 
