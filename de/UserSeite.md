<!-- en: UserPage -->

## Nutzerseite
-----------

Diese Seite ist Ihre **persönliche Nutzerseite**, auf der ausschließlich
die Lesezeichen und Publikationen aus Ihrer Sammlung angezeigt werden.  
Um zu Ihrer Nutzerseite zu kommen, klicken Sie auf Ihren Nutzernamen im
[ rechten Hauptmenü](Programmoberfläche "wikilink") oder geben Sie die URL
`http://www.bibsonomy.org/user/IhrNutzername` in Ihren Browser ein.  
  
Sie können auch die Nutzerseite **jedes anderen Nutzers** aufrufen,
indem Sie seinen Nutzernamen in die obenstehende URL einfügen oder
seinen Nutzernamen in das Suchfeld rechts oben eingeben.  

![ <File:overview1.png>](userseite/overview1.png  "fig: File:overview1.png")  

1.  **Die Lesezeichen/Publikationen des Nutzers** - Hier können Sie die
    Lesezeichen und Publikationen sehen, die in der Sammlung des Nutzers
    gespeichert sind.  
      
2.  **Als Freund hinzufügen/Folgen/CV** - Klicken Sie auf den Button "als
    Freund hinzufügen", um den Nutzer zu Ihrer [
    Freundesliste](FreundeHinzufügen "wikilink") hinzuzufügen oder "folgen", um ihm zu [folgen](Follower "wikilink"). 
    Klicken Sie auf den Button "CV", um den [
    Lebenslauf](LebenslaufÄndern "wikilink") des Nutzers anzusehen.
    Klicken Sie auf den Pfeil neben dem Button und dann auf "Zeige
    personalisierte Einträge", um eine personalisierte Sicht der
    Einträge des Nutzers zu bekommen.  
      
3.  **Diskussion** - Hier können Sie die Einträge, die kürzlich von dem
    Nutzer [ diskutiert wurden](KommentarRezensionBewertung "wikilink"), sehen.  
      
4.  **Konzepte** - Eine Liste der [ Konzepte](Konzepte "wikilink") des Nutzers.  
      
5.  **Ähnliche Benutzer** - Eine Liste der Nutzer, die diesem Nutzer
    ähnlich sind. Klicken Sie auf "mehr...", um den Algorithmus, der die
    Ähnlichkeit zwischen den Nutzern berechnet, zu ändern ([FolkRank](FolkRank "wikilink") (default),
    [Jaccard](https://de.wikipedia.org/wiki/Jaccard-Koeffizient), [Cosine](https://de.wikipedia.org/wiki/Kosinus-%C3%84hnlichkeit) and [TF-IDF](https://de.wikipedia.org/wiki/Tf-idf-Ma%C3%9F)).  
      
6.  **Tags** - Eine Liste der [ Tags](Tags "wikilink") des Nutzers.  
      
------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink") 