<!-- en: JabrefInstall/JabrefGeneral -->

-----------------

[`Zurück zur Übersicht von JabRef geht es hier.`](JabrefInstallieren "wikilink")

---------------------

##  JabRef - Allgemeine Informationen
---------------------

[JabRef](http://jabref.sourceforge.net/) ist ein Open-Source-Programm, das Literaturangaben basierend auf dem BibTeX Format (wie
${project.name}) verwaltet. Ab Version 2.4b1 kann JabRef durch ein [Plugin System](http://jabref.sourceforge.net/help/Plugin.php) erweitert
werden, das auf dem Java Plugin Framework ([JPF](http://jpf.sourceforge.net/)) basiert. Jetzt haben wir ein ${project.name}-Plugin für JabRef entwickelt.

Dieses Plugin ermöglicht es:

-   Publikationseinträge direkt aus ${project.name} abzurufen,
-   Publikationen zu Ihrem ${project.name} hinzuzufügen,
-   Einträge aus Ihrer Sammlung zu löschen.

Sie können beispielsweise Folgendes tun:

-   Laden Sie Ihre gesamte ${project.name}-Sammlung nach JabRef
    herunter, lassen Sie den eingebauten BibTeX-Key-Erzeuger die
    BibTeX-Keys Ihrer Einträge vereinheitlichen und laden Sie alle
    Einträge wieder hoch.
-   Laden Sie Ihre gesamte Sammlung nach JabRef herunter und überprüfen
    Sie alle Einträge auf Vollständigkeit durch einen Integritätscheck
    und laden Sie alle Einträge wieder hoch.
-   Benutzen Sie JabRefs Internetsuchfunktion, um Einträge zu
    importieren (z.B. von PubMed). Bearbeiten Sie diese, und laden Sie
    alle gemeinsam zu ${project.name} hoch.

Alle erwähnten Funktionen lassen sich einfach innerhalb von JabRef
anwenden, ohne dass Sie die Website von ${project.name} dabei besuchen müssen.

-----------------------------------------