<!-- en: JabrefInstall/JabrefInstallation -->

-----------------

[`Zurück zur Übersicht von JabRef geht es hier.`](JabrefInstallieren "wikilink")

---------------------

## JabRef - Installation
---------------------------

Zunächst benötigen Sie einen ${project.name}-Account, um das Plugin zu benutzen.  
Folgen Sie diesen einfachen Schritten, um die Vorteile auszuschöpfen,
die JabRef in Kombination mit ${project.name} ermöglicht: 

1. **Herunterladen von JabRef**  
Um das ${project.name}-Plugin nutzen zu können, benötigen Sie eine alte
Version von JabRef. Diese finden Sie auf dieser Seite:
<http://www.oldfoss.com/JabRef.html>  
Wählen Sie hier eine Version, die älter ist als 2.12, aus, z.B. 2.10:  

![<File:downloadJabref.png>](JabrefInstallation/downloadJabref.png  "fig: File:downloadJabref.png")  

Laden Sie die zu Ihrem Betriebssystem passende Datei herunter und führen
Sie sie aus. JabRef wird dann installiert.  

2. **Herunterladen des ${project.name}-Plugins**  
  Hier können Sie das Plugin herunterladen:
  <https://gforge.cs.uni-kassel.de/frs/download.php/76/jabref-bibsonomy-plugin-2.5.2-bin.jar> 
  
3. **Verschieben der Plugin-Jar-Datei**  
  Verschieben Sie die Plugin-Jar-Datei in einen Unterordner innerhalb des
  JabRef-Verzeichnisses, der ''Plugins ''heißt.  
  
4. **JabRef starten**  
Starten Sie JabRef und Sie können anfangen das Plugin zu benutzen!

----------------------------