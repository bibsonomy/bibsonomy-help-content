<!-- en: JabrefInstall/JabrefConfig -->

-----------------

[`Zurück zur Übersicht von JabRef geht es hier.`](JabrefInstallieren "wikilink")

---------------------

## JabRef - Konfiguration
----------------

Bevor Sie mit dem Plugin arbeiten können, müssen Sie Ihren
${project.name}-Nutzernamen und Ihren API-Schlüssel (API-Key) angeben.
Gehen Sie folgendermaßen vor:

1.  **Neue Datenbank erstellen**  
    Öffnen Sie JabRef und legen Sie zunächst eine neue Datenbank an.
    Klicken Sie dazu auf *File* und dann auch *New database*.  
    
2.  **Die Plugin-Einstellungen öffnen**  
    Klicken Sie im Menü auf *Plugins*, dann auf *BibSonomy* und
    schließlich auf *Settings*.  
    
    ![ <File:jabrefSettings.png>](JabrefKonfiguration/jabrefSettings.png  "fig: File:jabrefSettings.png")  
    
3.  **Daten eingeben**  
    Geben Sie nun Ihren ${project.name}-Benutzernamen und Ihren
    API-Schlüssel (API key) an. Den API-Schlüssel finden Sie auf der [
    Einstellungsseite](${project.home}settings "wikilink") im Reiter "Einstellungen".
    Um den API-Schlüssel zu speichern, wählen Sie das Feld *Store API key* aus.
    Alle weiteren Felder können Sie nach Wunsch auswählen.
    Speichern Sie Ihre Daten durch Klicken auf *Save*.  
    
    ![ <File:jabrefSettings2.png>](JabrefKonfiguration/jabrefSettings2.png  "fig: File:jabrefSettings2.png")  

Nun haben Sie das ${project.name}-Plugin fertig konfiguriert und können
es benutzen. Weitere Informationen zum Austausch von Daten zwischen
JabRef und ${project.name} finden Sie unter [JabRef - Datenaustausch](Jabref "wikilink").
Wenn Sie Fragen zu dem Plugin haben, besuchen Sie unsere [Kontaktseite](Contact "wikilink").

----------------------