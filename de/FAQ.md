<!-- de: AddonGoogleDocs -->

## Häufig gestellte Fragen (FAQ)

---

${project.theme.help == "wueresearch"}{

### Quicklinks
- [Auswahl von Publikationstypen](FAQ/#auswahl-von-publikationstypen)
- [Automatisches Eintragen von Publikationen](FAQ/#automatisches-eintragen-von-publikationen)
- [Das WueResearch-Browser-Add-On](FAQ/#das-wueresearch-browser-add-on)
- [Auswählen eines Zitationsstils](FAQ/#auswählen-eines-zitationsstils)

---

## Auswahl von Publikationstypen

**1. Woher weiß ich, welcher Publikationstyp (Eintragstyp) für meine Veröffentlichung der Richtige ist?**

> Zur Auswahl eines Publikationstyps für Ihre Veröffentlichung steht Ihnen im Menü „Publikationen hinzufügen“ eine Übersicht über alle auswählbaren Eintragstyp zur Verfügung. Weitere Informationen zum Eintragen von Publikationen und der Auswahl des korrekten Publikationstyps finden Sie außerdem in den folgenden FAQs.    

**2. Zu welchem Eintragstyp gehören Artikel zu Enzyklopädien (Lexikonartikel)**

> Je nach Art der Veröffentlichung werden Enzyklopädie-Artikel als „Beitrag in einem Sammelband“ oder als „Buchbeitrag“ erfasst.

**3. Zu welchem Eintragstyp gehören Gesetzeskommentare, Beiträge in Gesetzeskommentaren und Entscheidungsanmerkungen?**

> Je nach Art der Veröffentlichung werden diese als „Beitrag in einem Sammelband“ oder als „Buchbeitrag“ gelistet.

**4. Zu welchem Eintragstyp gehören Lehrbücher bzw. Beiträge zu Lehrbüchern?**

> Lehrbücher werden dem Publikationstyp „Monographie (Buch)“ zugerechnet oder entsprechend als „Buchbeitrag“ erfasst.

**5. Zu welchem Eintragstyp gehören Editorials und Letters to the Editor?**

> „Editorials“ und „Letters to the Editor“ stellen in WueResearch keine eigenständigen Publikationstypen dar. Sie werden dem Eintragstyp „Artikel“ zugerechnet.

**6. Wie kann ich die Herausgeberschaft eines Buchs oder einer Zeitschrift in WueResearch vermerken?**

> Sind Sie Herausgeber\*in einer Monographie (Buch), eines Sammelbands bzw. Tagungsbands oder eines Sonderhefts, dann tragen Sie bei den allgemeinen Informationen im Feld „Herausgeber“ entsprechend Ihren Namen ein (sowie die Namen möglicher weiterer Herausgeber\*innen). Das Feld „Autor(en)“ lassen Sie bitte frei. Alle weiteren zutreffenden Felder füllen Sie wie gewohnt aus. 

**7. Was ist der Unterschied zwischen einem Review und einer Rezension?**

> Unter einem „Review“ wird eine systematische Übersichtsarbeit in Form einer Literaturübersicht zu einem bestimmten Thema verstanden. Dabei wird versucht, alles verfügbare Wissen zu diesem Thema zu sammeln, zusammenzufassen und kritisch zu bewerten. Als Grundlage dient die bereits publizierte Fachliteratur. Im Gegensatz dazu handelt es sich bei einer „Rezension“ um eine kritische Bewertung eines Buchs oder Artikels bzw. um eine Zusammenfassung eines Werkes, das nicht vom Autor selbst stammt.

**8. Warum gibt es den Publikationstyp „Zeitschrift“ nicht?**

> Derzeit können in WueResearch nur Herausgeberschaften von Zeitschriften aufgenommen werden, wenn sich die Herausgeberschaft auf ein einzelnes Werk bezieht („Sonderheft einer Zeitschrift“). Die ständige Mitgliedschaft in Herausgebergremien von Zeitschriften wird hingegen derzeit von WueResearch nicht erfasst. Artikel, die in wissenschaftlichen Fachzeitschriften veröffentlich wurden, werden dem Publikationstyp „Artikel“ zugeordnet. 

---

## Automatisches Eintragen von Publikationen

**9. Warum wird meine Publikation bei der Suche nach ISSN oder ISBN nicht gefunden?**
> Bei der Suche nach ISSN oder ISBN werden nur bereits in WueResearch hinterlegte Veröffentlichungen durchsucht. Ist die gesuchte Publikation in der Datenbank des Systems noch nicht hinterlegt, erscheint eine Fehlermeldung. Wenn möglich verwenden Sie bei der Suche bitte die DOI der Publikation, die Sie hinzufügen möchten. Alternativ tragen Sie die Publikation bitte per Hand in das System ein. 

---

## Das WueResearch-Browser-Add-On

**10. Warum kann ich das WueResearch-Browser-Add-on nicht installieren?**

> Das WueResearch-Browser-Add-on wird zurzeit nur von Firefox und Chrome unterstützt. Sollten Sie einen anderen Browser verwenden, dann nutzen Sie bitte die Automatische Importfunktion oder tragen Sie die Veröffentlichung per Hand ein.

**11. Warum funktioniert der Import meiner Publikation mit dem WueResearch-Browser-Add-on nicht?**

> Das WueResearch-Browser-Add-on funktioniert nur auf der Homepage bestimmter Verleger (derzeit ungefähr 150 unterstütze Verlage). Eine Übersicht über alle unterstützten Verlage finden Sie [hier](../scraperinfo).
 
---

### Auswählen eines Zitationsstils 

**12. Wie kann ich bevorzugte Zitationsstile für mein Profil einstellen?**

> Bevorzugte Zitationsstile, die Sie regelmäßig verwenden wollen und die Ihnen bei der Detailansicht von Publikationen standardmäßig angezeigt werden sollen, können Sie im Menü „Einstellungen“ Ihres Profils auswählen. Gehen Sie dazu in den Einstellungen auf den Reiter „Einstellungen“ und tippen Sie im Abschnitt „Bevorzugte Exportformate“ im Suche-Feld („Exportformat hinzufügen“) den Namen des Zitationsstils ein, den Sie hinzufügen wollen. 

}