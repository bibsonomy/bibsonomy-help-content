<!-- en: Clipboard -->

## Die Ablage
----------

Die Ablage ermöglicht es Ihnen, Publikationen ganz einfach
zwischenzuspeichern. Sie können sowohl eigene als auch fremde
Publikationen in Ihre Ablage aufnehmen.  

![ <File:1.png>](dieablage/1.png  "fig: File:1.png")  


So verwenden Sie die Ablage:

1.  Bei jeder Publikation befindet sich eine Symbolleiste. Klicken Sie
    mit der Maus auf das Symbol mit einem schwarzen Ordner **("Diese Publikation zur Ablage hinzufügen“)**.
2.  Die Publikation wird direkt der Ablage hinzugefügt. Sie erreichen
    die Ablage, indem Sie im rechten [Hauptmenü](Programmoberfläche "wikilink") auf 	das Personensymbol und
    im daraufhin erscheinenden Untermenü auf den Punkt
    **"Ablage“** klicken. Neben dem Personensymbol wird außerdem die
    aktuelle Anzahl der Publikationen in der Ablage angezeigt (die Zahl
    im roten Kreis).

  
------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink")

