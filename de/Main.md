<!-- en: Main -->

Herzlich willkommen auf der Hilfeseite zu ${project.name}!
-----------------

![ <File:header.png>](main/header.png  " File:header.png")

${project.name} unterstützt Sie gezielt und einfach bei Ihrer Arbeit
mit Publikationen, Literaturlisten und Web-Lesezeichen. Nutzen Sie die
Möglichkeiten und melden Sie sich jetzt an oder stöbern Sie ein wenig
auf unseren Seiten.


![ <File:Student-Read-04.png>](main/Student-Read-04.png  " File:Student-Read-04.png") | ![ <File:Student-Laptop.png>](main/Student-Laptop.png  " File:Student-Laptop.png") | ![ <File:Binary-Code.png>](main/Binary-Code.png  " File:Binary-Code.png")
--- | --- | ---
**Einsteiger** | **Fortgeschrittene** | **Softwareentwickler**
Hier finden Sie Antworten auf die grundlegenden Fragen, die jeder Einsteiger hat. Also: Wie melde ich mich an? Was finde ich wo? Wie kann ich Lesezeichen und Publikationen verwalten? | Sie wollen mehr aus dem System herausholen? Erfahren Sie hier, wie Sie sich mit anderen Nutzern vernetzen können, oder wie Sie Publikationen noch exakter suchen können als bisher. | Sie entwickeln Software oder Webseiten und möchten unser System mit Ihrem Programm verknüpfen? Das System bietet Schnittstellen und zusätzliche Software, die Ihnen dabei hilft.
[Zur Einsteigerhilfe wechseln.](MainEinsteiger "wikilink") | [Zur Hilfe für Fortgeschrittene wechseln.](MainFortgeschrittene "wikilink") | [Zur Hilfe für Softwareentwickler wechseln.](MainEntwickler "wikilink")
