<!-- en: ExportDataSpecific -->

Literaturverzeichnis exportieren – programmspezifisch
-----------------------------

Im diesem Abschnitt wird beschrieben, wie man Daten aus ${project.name} in häufig benutzte Programme exportiert. 

--------------------

### Microsoft Word 

1.  Klicken Sie auf das Pfeilymbol ganz rechts - ein
    Menü erscheint.
    
2.  Wählen Sie unter **"Export"** die Option **"mehr..."**.
3.  Die Übersichtsseite für alle Exportformate wird angezeigt. Wählen
    Sie hier **"MSOffice XML"**. Speichern Sie diese Datei dann auf
    Ihrer Festplatte ab.  
    
    ![ <File:word1.png>](literaturlisteexportieren/word1.png  "fig: File:word1.png")  
    
4.  In Microsoft Word können Sie dann diese Datei laden, indem Sie unter
    **"Verweise"** auf **"Quellen verwalten"** klicken. Im erscheinenden
    Dialog können Sie auf **"Durchsuchen"** klicken und die unter
    Schritt 3 gespeicherte Datei laden.  
    
    ![ <File:word2.png>](literaturlisteexportieren/word2.png  "fig: File:word2.png")  
    
    ![ <File:word3.png>](literaturlisteexportieren/word3.png  "fig: File:word3.png")  


-----------------------------

### Bibliographix

1.  Exportieren Sie die Daten aus ${project.name} zunächst als
    BibTeX-Datei mit der Endung .bib oder .txt
    
2.  Starten Sie Bibliographix. Klicken Sie auf den Reiter *"mehr…“*,
    dann auf *"Daten austauschen“*.  
    
    ![ <File:biblio1.png>](literaturlisteexportieren/biblio1.png  "fig: File:biblio1.png")  
    
3.  Wählen Sie im linken Menü die Option *"Import“* und klicken Sie dann
    auf *"auswählen“*, um Ihre zuvor gespeicherte Datei mit der Endung
    .txt oder .bib hochzuladen.
    
4.  Klicken Sie abschließend auf *"Import!“*.  
    
    ![ <File:biblio2.png>](literaturlisteexportieren/biblio2.png  "fig: File:biblio2.png")  

------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink") 


