# Impressum

## Herausgeber

Julius-Maximilians-Universität Würzburg  
Postanschrift: Sanderring 2, 97070 Würzburg  

Telefon: +49 931 31-0  
Fax: +49 931 31-82600  
<info@uni-wuerzburg.de>  

## Rechtsform und Vertretung

Die Julius-Maximilians-Universität Würzburg ist eine Körperschaft des Öffentlichen Rechts und zugleich staatliche Einrichtung nach Art. 1 Abs. 1 BayHSchG. Sie wird gesetzlich vertreten durch den Präsidenten Prof. Dr. Paul Pauli.

## Zuständige Aufsichtsbehörde

Bayerisches Staatsministerium für Wissenschaft und Kunst
Postanschrift: Salvatorstraße 2, 80327 München 

## Umsatzsteuer-Identifikationsnummer

Die Umsatzsteuer-Identifikationsnummer gemäß § 27a Umsatzsteuergesetz lautet DE 134187690.

## Haftungsausschluss

Die Julius-Maximilians-Universität Würzburg übernimmt keine Haftung für die Richtigkeit, Vollständigkeit und Verfügbarkeit der bereit gestellten Inhalte, insbesondere wird eine Haftung für Schäden der Nutzer des Internetangebotes ausgeschlossen. Dies gilt insbesondere auch für IT-Schäden, die durch den Download von Inhalten entstehen.

Die Julius-Maximilians-Universität Würzburg übernimmt darüber hinaus keine Haftung für die Inhalte externer Links und Kommentare von Dritten, insbesondere auf den Social-Media-Kanälen der Julius-Maximilians-Universität Würzburg. Für den Inhalt der verlinkten Seiten sind ausschließlich deren Betreiberinnen und Betreiber verantwortlich.

## Social Media Kanäle

Dieses Impressum gilt auch für alle zentralen Social-Media-Accounts der Universität Würzburg:

<https://www.facebook.com/uniwue>  
<https://www.twitter.com/uni_wue>  
<https://www.instagram.com/uniwuerzburg>  
<https://www.youtube.com/uniwuerzburg>  
<https://www.linkedin.com/school/julius-maximilians-universitat-wurzburg/>  
<https://www.pinterest.de/uniwue/>  

Ferner gilt dieses Impressum auch für alle dienstlichen Social-Media-Accounts der Universität, darunter Accounts von Einrichtungen, Fakultäten, Fachbereichen, Forschungsprojekten, Lehrstühlen und Lehrbereichen auf folgenden Plattformen:

<https://www.facebook.com/>  
<https://www.twitter.com/>  
<https://www.instagram.com/>  
<https://www.youtube.com/>  
<https://de.linkedin.com/>  
<https://tiktok.com/>  
<https://uninow.de/>  
<https://www.pinterest.de/>
