# Datenschutzerklärung

Datenschutzerklärung und Datenverarbeitungsinformation

Diese Datenschutzerklärung gilt für die Webseiten der Domains der Universität Würzburg.

Und als Ergänzung soweit wir personenbezogene Daten in eigener Verantwortung verarbeiten für:

#### Allgemeines

Datenschutz ist unser Anliegen und unsere gesetzliche Verpflichtung. Um die Sicherheit Ihrer Daten angemessen bei der Übertragung zu schützen, verwenden wir orientiert am aktuellen Stand der Technik entsprechende Verschlüsselungsverfahren (z. B. SSL/TLS) und gesicherte technische Systeme.

Für die Webseiten wird als aktive Komponente Javascript verwendet. Sollten Sie diese Funktion in Ihrem Browser abgestellt haben, erhalten Sie einen entsprechenden Hinweis, die Funktion zu aktivieren.

## Google Custom Search

Zur Unterstützung Informationen auf unseren Seiten aufzufinden, setzen wir auf Google Custom Search. In diesen Suchergebnissen werden keine Werbeanzeigen von Google eingeblendet. Nur wenn die das Suchfelde nutzen, werden Daten an Google übertragen.

Das Suchfeld auf dieser Website ("Suchfeld") wird von Google Inc ("Google") zur Verfügung gestellt. Sie erkennen an und stimmen zu, dass die Datenschutzbestimmungen von Google (unter <http://www.google.de/privacy.html>) für Ihre Verwendung des Suchfelds gelten und dass Sie sich durch die Verwendung des Suchfelds Google gegenüber verpflichten, Ihre persönlichen Daten gemäß der Datenschutzbestimmungen zu verwenden.

## Alternative zu Google Custom Search

Wenn Sie diesen Dienst nicht nutzen möchten, können Sie z.B. über <https://duckduckgo.com/search.html?site=uni-wuerzburg.de> oder <https://www.qwant.com/?q=site:uni-wuerzburg.de> unsere Webseite durchsuchen.

## Verantwortlicher

Julius-Maximilians-Universität Würzburg  
Am Sanderring 2  
97070 Würzburg  

Die Universität wird durch den amtierenden Präsidenten oder die amtierende Präsidentin vertreten.

## Kontaktdaten des bestellten behördlichen Datenschutzbeauftragten

Datenschutzbeauftragter der Julius-Maximilians-Universität Würzburg  
Sanderring 2  
Tel. 0931/31-0  
<datenschutz@uni-wuerzburg.de>  

## Zwecke und Rechtsgrundlagen der Verarbeitung

Wir bieten gemäß Art. 2 Abs. 6 BayHSchG, Art. 4 Abs. 1 S. 1 und 2 BayEGovG auf unseren Webseiten unsere Dienste und Verwaltungsleistungen sowie Informationen für die Öffentlichkeit über unsere Tätigkeit.

Unsere Social Media Auftritte sind Teil unserer Öffentlichkeitsarbeit. Unser Bestreben ist es, zielgruppengerecht zu informieren und sich mit Ihnen auszutauschen, Art. 2 Abs. 6 BayHSchG. Wir ermöglichen Ihnen eine schnelle elektronische Kontaktaufnahme und unmittelbare Kommunikation über die Medien Ihrer Wahl, § 5 Abs. 1 Nr. 2 TMG.

Ferner erhalten wir von unseren Social Media Auftritten Statistiken über Abrufe von Inhalten, Beiträge und Interaktionen. Rechtsgrundlage ist Ihre Einwilligung gemäß Art. 6 Abs. 1 lit. a DSGVO.

Inhalte und Beiträge, Anfragen, die Rechte Dritter verletzen oder die den Tatbestand einer Straftat oder Ordnungswidrigkeit erfüllen, gesetzlichen oder vertraglichen Verhaltenspflicht nicht entsprechen , legen wir durch Übermittlung an die zuständige Behörde bzw. dem Social-Media-Dienst offen und blockieren oder löschen diese.

Cookies, Protokolldateien, Statistiken der Social Media Anbieter, Google Custom Search und das Webanalyse-Tool Matomo verwenden wir zur Erstellung von Geschäftsstatistiken, zur Durchführung von Organisationsuntersuchungen, zur Prüfung oder Wartung unseres Webdienstes und zur Gewährleistung der Netz- und Informationssicherheit gemäß Art. 6 Abs. 1 BayDSG, § 13 Abs. 7 TMG, Art. 11 Abs. 1 BayEGovG. Soweit der Verarbeitungszweck nicht beeinträchtigt wird, anonymisieren oder pseudonymisieren wir personenbezogene Daten.

## Datenkategorien

## Administration und Redaktion

Zur Administration und Redaktion werden Funktionskennungen und persönliche Kennungen mit Zugriffschutzmechanismen angelegt und Änderungen protokolliert, die mit diesen Kennungen vorgenommen werden.

## Protokolldateien

Wenn Sie diese oder andere Internetseiten aufrufen, übermitteln Sie über Ihren Internetbrowser Daten an unseren Webserver. Die folgenden Daten werden während einer laufenden Verbindung zur Kommunikation zwischen Ihrem Internetbrowser und unserem Webserver aufgezeichnet:

- Datum und Uhrzeit der Anforderung
- Name der angeforderten Datei
- Seite, von der aus die Datei angefordert wurde
- Zugriffsstatus (beispielsweise Datei übertragen, Datei nicht gefunden)
- verwendete Webbrowser und Bildschirmauflösung sowie das verwendete Betriebssystem
- vollständige IP-Adresse des anfordernden Rechners
- übertragene Datenmenge

## Cookies

Matomo

Unsere Webseitenanalyse Matomo setzt mehrere Cookies ein, um die Nutzung unsere Webseiten analysieren zu können.

Loadbalancer Cookie

Dieser Cookie wird benötigt, um unseren Dienst bestmöglich verfügbar anbieten zu können, indem wir einen Loadbalancer einsetzen, welcher die Last auf unseren Systemen verteilt.

## Kommunikation

Wenn Sie uns ein Anliegen oder eine Meinung per E-Mail, Kontaktformular, Post, Telefon, Fax oder Social Media mitteilen, werden die gemachten Angaben zum Zwecke der Bearbeitung des Anliegens sowie für mögliche Anschlussfragen und zum Meinungsaustausch verarbeitet. Dafür setzen wir immer den gleichen Kommunikationsweg ein, sofern Sie keinen Wechsel wünschen.

## Empfänger oder Kategorien von Empfängern der personenbezogenen Daten

Soweit Sie unsere Social Media Kanäle und Seiten nutzen, verarbeiten auch deren Anbieter Ihre personenbezogen Daten.

Auch unsere IT-Dienstleister können im Rahmen der von uns abgeschlossenen Verträge zur Auftragsverarbeitung Empfänger Ihrer personenbezogenen Daten sein. Um die Sicherheit unserer Datenverarbeitungsanlagen zu gewährleisten, legen wir unsere Dienstleister jedoch nicht offen.

## Übermittlung von personenbezogenen Daten an ein Drittland oder internationale Organisationen

Unsere Social Media Anbieter bzw. die deren kontrollierenden Eigentümer sind unter dem EU-US Privacy Shield und für jede Person einsehbar zertifiziert, sodass ein rechtlich angemessenes Schutzniveau für personenbezogene Daten besteht:

#### Facebook, Inc.

<https://www.privacyshield.gov/participant?id=a2zt0000000GnywAAC&status=Active>

#### Twitter Inc.

<https://www.privacyshield.gov/participant?id=a2zt0000000TORzAAO&status=Active>

#### Youtube (Youtube LLC) und Google+ unter Google LLC

<https://www.privacyshield.gov/participant?id=a2zt000000001L5AAI&status=Active>

#### TikTok Technology Limited

<https://www.tiktok.com/legal/privacy-policy?lang=de>

Dauer der Speicherung der personenbezogenen Daten

## Administration und Redaktion

Personalisierte Administrations- und Redaktionszugänge zu unserer Webseite werden nach Ausscheiden der betreffenden Person in der Verarbeitung beschränkt und ein Jahr nach Abschluss des Jahres zum Zeitpunkt des Ausscheidens gelöscht.

## Protokolldateien

Werden als personenbezogener Daten im Regelfall höchstens dreißig Tage aufbewahrt.

## Cookies

Das Loadbalancer Cookie wird beim Beenden der Browsersitzung gelöscht.

Die Cookies von Matomo werden nach den in den Cookies definierten Zeiten gelöscht.

## Kommunikation

E-Mails, Angaben aus Kontaktformularen, Fax-Mitteilungen, Notizen zu Telefonaten, Post und Social Media Beiträge und Nachrichten die Sie uns nicht öffentlich übermittelt haben, werden alle zwei Jahre darauf geprüft, ob die Speicherung Ihrer Anfragen für Anschlussfragen noch erforderlich ist. Ihre Daten werden, wenn keine Erforderlichkeit mehr vorliegt, in der Verarbeitung beschränkt und noch gemäß den gesetzlichen Aufbewahrungspflichten Berücksichtigung des Archivrechts aufbewahrt.

Haben Sie unter Wissen der Öffentlichkeit oder Teilen dieser über Social Media oder mittels öffentlicher Kommentarfunktion mit uns kommuniziert, können Sie selbst über Dauer der Veröffentlichung entscheiden oder uns um Löschung bitten. Wir löschen diese Daten unter Beachtung des Archivrechts innerhalb unseres Verantwortungsbereiches. Liegen uns nach der Löschung noch Kopien der Daten vor, werden diese in der Verarbeitung beschränkt und noch gemäß den gesetzlichen Aufbewahrungspflichten Berücksichtigung des Archivrechts aufbewahrt.

## Betroffenenrechte

#### Allgemeines

Nach der Datenschutz-Grundverordnung stehen Ihnen folgende Rechte zu:

Werden Ihre personenbezogenen Daten verarbeitet, so haben Sie das Recht, Auskunft über die zu Ihrer Person gespeicherten Daten zu erhalten (Art. 15 DSGVO).

Sollten unrichtige personenbezogene Daten verarbeitet werden, steht Ihnen ein Recht auf Berichtigung zu (Art. 16 DSGVO).

Liegen die gesetzlichen Voraussetzungen vor, so können Sie die Löschung oder Einschränkung der Verarbeitung verlangen sowie Widerspruch gegen die Verarbeitung einlegen (Art. 17, 18 und 21 DSGVO).

Wenn Sie in die Datenverarbeitung eingewilligt haben oder ein Vertrag zur Datenverarbeitung besteht und die Datenverarbeitung mithilfe automatisierter Verfahren durchgeführt wird, steht Ihnen gegebenenfalls ein Recht auf Datenübertragbarkeit zu (Art. 20 DSGVO).

Sollten Sie von Ihren oben genannten Rechten Gebrauch machen, prüfen wir, ob die gesetzlichen Voraussetzungen hierfür erfüllt sind.

Weiterhin besteht ein Beschwerderecht beim Bayerischen Landesbeauftragten für den Datenschutz.

#### Widerrufsrecht

Sie können, wenn der Verarbeitung Ihrer Daten eine Einwilligung zu Grunde liegt, die Einwilligung jederzeit widerrufen, ohne dass die Rechtmäßigkeit der aufgrund der Einwilligung bis zum Widerruf erfolgten Verarbeitung berührt wird.  

> Widerspruchsrecht
> 
> Sie haben das Recht, aus Gründen, die sich aus Ihrer besonderen Situation ergeben, jederzeit gegen die Verarbeitung Sie betreffender personenbezogener Daten, die u.a. aufgrund von Art. 6 Abs. 1 lit. e DSGVO erfolgt, Widerspruch nach Art. 21 DSGVO einzulegen. Wir werden die Verarbeitung Ihrer personenbezogenen Daten einstellen, es sei denn, wir können zwingende schutzwürdige Gründe für die Verarbeitung nachweisen, die Ihre Interessen, Rechte und Freiheiten überwiegen, oder wenn die Verarbeitung der Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen dient.

## Sonstiges zu unserer Datenschutzerklärung

Wir behalten uns vor, diese Datenschutzerklärung gelegentlich anzupassen, damit diese stets den aktuellen rechtlichen Anforderungen entspricht oder um Änderungen unserer Leistungen in der Datenschutzerklärung umzusetzen, z. B. bei der Einführung neuer Services. Für Ihren erneuten Besuch gilt dann die neue Datenschutzerklärung.

Wenn Sie Fragen haben oder Ihre Rechte gegenüber uns ausüben möchten, können Sie neben den Datenschutzbeauftragen auch eine E-Mail schreiben an: webmaster@uni-wuerzburg.de. 

