<!-- en: BookmarkPublicationManagement -->

## Publikationen und Lesezeichen verwalten
---------------------------------------------------

${project.name} bietet Ihnen vielfältige Möglichkeiten, alle
anfallenden Informationen/Daten während einer Literaturrecherche (und
darüber hinaus) zu organisieren und zu verwalten. Auf dieser Seite
finden Sie alles, was Sie benötigen, um ${project.name} wie eine
normale Literaturdatenbank (z. B. Citavi, EndNote, etc.) zu verwenden.

------------------------------------------------------------------------

### Lesezeichen - Grundlagen

**Einführung:** Lesezeichen (engl. bookmarks) ermöglichen es, das
Internet wie ein Buch zu verwenden. Mit einem Lesezeichen merken Sie
sich die genaue Adresse eines Internet-Dokuments. ${project.name} gibt
Ihnen die Möglichkeit, Lesezeichen zentral zu speichern und zu
verwalten. Möchten Sie mehr darüber erfahren, folgen Sie [dieser Anleitung](LesezeichenPublikationenManagen/LesezeichenManagen "wikilink").

------------------------------------------------------------------------

### Publikationen - Grundlagen 

**Einführung:** Unter Publikationen versteht man in ${project.name}
alle Arten von Dokumenten (egal ob Artikel, Monografie, usw.). Sie
können eigene oder fremde Publikationen (z. B. als Basis für eine
Literaturrecherche) mit ${project.name} erstellen/verwalten/recherchieren.
Für eine Anleitung zu diesen Funktionen, besuchen Sie [diese Seite](LesezeichenPublikationenManagen/PublikationenManagen "wikilink").

------------------------------------------------------------------------

### Publikationen und Lesezeichen

[Diese Seite](LesezeichenPublikationenManagen/PublikationenLesezeichen "wikilink") befasst sich mit Funktionen in ${project.name}, die
Sie sowohl auf Lesezeichen als auch auf Publikationen anwenden können.
Darunter fallen das Anzeigen von eigenen Lesezeichen und Publikationen,
das Suchen nach allen verfügbaren Lesezeichen & Publikationen ${project.name} 
und der Zugriff auf OpenAccess-Publikationsdienste.

------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink")
    