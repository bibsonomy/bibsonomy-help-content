<!-- en: Citavi -->

## Datenaustausch mit Citavi
-------------------------

Sie können Artikel zwischen dem Literaturverwaltungsprogramm
[Citavi](http://www.citavi.de/de/index.html) und ${project.name} austauschen. Dazu benötigen Sie sowohl einen
${project.name}-Nutzeraccount als auch die aktuelle Version von Citavi.

------------------------------------------------------------------------

### Import von Daten aus ${project.name} nach Citavi

Sie können Publikationen aus ${project.name} (Ihre eigenen oder andere)
in Ihr Citavi-Projekt importieren.

-   Klicken Sie mit der Maus in der Citavi-Menüleiste (oben links) auf
    die Schaltfläche **"Recherchieren"** (Strg + L).
-   Wählen Sie **"Datenbank/Katalog hinzufügen"** und geben Sie als
    Namen "BibSonomy" ein. Nach wenigen Augenblicken wird das
    Suchergebnis unter "gefunden" angezeigt. Kreuzen Sie das Auswahlfeld
    an und klicken Sie auf **"hinzufügen"**, danach auf **"Schließen"**.

![ <File:suche.png>](citavi/suche.png  " File:suche.png")

![ <File:suche2.png>](citavi/suche2.png  " File:suche2.png")

-   Wählen Sie im Menü **"Recherchieren"** als Datenbank
    "BibSonomy" aus. Schließlich können Sie die gewünschten
    Suchbegriffe eingeben (Freitext (alle Felder), Autor, Titel,
    Jahr (Vorauswahlen)) und die Suche in der ${project.name}-Datenbank
    durch Klicken auf **"Suchen"** starten.
-   Kreuzen Sie die gewünschten Titel in der Ergebnisliste an, die Sie
    in Ihr Citavi-Projekt importieren möchten und klicken Sie auf
    **"Titel übernehmen"**. Im anschließenden Dialog werden Sie gefragt,
    ob Sie nur die markierten Titel oder alle Ergebnisse
    importieren möchten. Bestätigen Sie Ihre Auswahl mit **"OK"**. Die
    Einträge werden nun in Ihr Citavi-Projekt übernommen.

![ <File:ergebnisse.png>](citavi/ergebnisse.png  "fig: File:ergebnisse.png")  
**Hinweis:** Zum Menü **"Recherchieren"** gelangen Sie auch, indem Sie
im Citavi-Menü **"Datei"** auf **"Importieren"** klicken und **"aus
einem Bibliothekskatalog oder einer Fachdatenbank"** wählen.

------------------------------------------------------------------------

### Export von Daten aus Citavi nach ${project.name}

Es ist möglich, Einträge aus Ihrer Citavi-Literaturdatenbank in Ihre
Literatursammlung in ${project.name} zu exportieren.  

**Wichtig:** Für diese Funktion müssen Sie in Citavi die
**TeX-Unterstützung** aktivieren. Dazu klicken Sie im Citavi-Menü auf
**"Extras"**, dann auf **"Optionen..."** und schließlich setzen Sie
einen Haken beim Punkt **"TeX-Unterstützung"**. Bestätigen Sie Ihre
Änderung mit **"OK"**. 

![ <File:texunterstuetzung.png>](citavi/texunterstuetzung.png  "fig: File:texunterstuetzung.png")


-   Klicken Sie im Citavi-Menü auf **"Datei"**, dann auf
    **"Exportieren"**. Wählen Sie, ob Sie nur den markierten Titel oder
    alle Titel aus Ihrem Citavi-Projekt exportieren möchten und klicken
    Sie auf **"Weiter"**.
-   Nun müssen Sie das Exportformat wählen. Klicken Sie auf
    **"Exportfilter hinzufügen"** und geben Sie anschließend als Name
    "BibSonomy" ein. Nach wenigen Augenblicken wird das
    Suchergebnis unter "gefunden" angezeigt. Kreuzen Sie das Auswahlfeld
    an und klicken Sie auf **"hinzufügen"**, danach auf **"Schließen"**.

![ <File:export.png>](citavi/export.png  " File:export.png")

![ <File:export2.png>](citavi/export2.png  " File:export2.png")

-   Wählen Sie als Exportformat **"Bibsonomy (Online)"** aus und klicken
    Sie auf **"Weiter"**.
-   Geben Sie Ihren **Benutzernamen** von ${project.name} (nicht
    die Bibliotheksausweis-Nummer) und als Passwort Ihren
    **API-Schlüssel** (API-Key) ein. Den API-Schlüssel finden Sie im
    ${project.name}-Benutzermenü unter **"Einstellungen"** in der
    Registerkarte **"Einstellungen"**. Sie können außerdem auswählen,
    für wen der Titel sichtbar sein soll (public, private oder friends).
    Klicken Sie auf **"Weiter"**.
-   Wählen Sie abschließend, ob Sie die Exportvorlage speichern wollen
    oder nicht. Durch Klicken auf **"Weiter"** wird der Export von
    Citavi nach ${project.name} durchgeführt und der exportierte Artikel
    wird Ihren Publikationen hinzugefügt.

  
------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink")

