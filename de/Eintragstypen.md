<!-- en: Entrytypes -->

## Eintragstypen
-------------

Eintragstypen werden verwendet, um BibTeX-Einträge nach deren Typ zu
klassifizieren. Zur Zeit unterstützt ${project.name} die folgenden 24
Typen:  
  
-   **article** (Zeitungs- oder Zeitschriftenartikel)
-   **book** (Buch, Monografie mit angegebenem Verlag)
-   **booklet** (gebundenes Druckwerk, aber ohne Verlag
    oder Sponsororganisation)
-   **collection** (ein Sammelband)
-   **conference** (ein Beitrag zu einer Konferenz, der nicht in einem
    Konferenzband erschienen ist)
-   **dataset** (ein Datenset, z. B. für Experimente genutzt)
-   **electronic** (elektronische Veröffentlichungen, z. B. eBooks
    oder Blogeinträge)
-   **inbook** (Teil eines Buches, z. B. ein Kapitel oder
    ein Seitenbereich)
-   **incollection** (Teil eines Buches mit einem eigenem Titel, z. B.
    Beitrag in einem Sammelband)
-   **inproceedings** (Artikel in einem Tagungsband bzw. Konferenzband)
-   **manual** (technische Dokumentation, Handbuch)
-   **mastersthesis** (Master-, Magister- oder Diplomarbeit)
-   **misc** (diesen Eintragstyp können Sie wählen, wenn nichts anderes
    zu passen scheint)
-   **patent** (Patent)
-   **periodical** (ein regelmäßig erscheinendes Werk, z.B. Zeitschrift)
-   **phdthesis** (Doktor- oder andere Promotionsarbeit)
-   **preamble** (eine Einleitung, ein Vorwort)
-   **preprint** (ein Vordruck für ein Publikation, die noch publiziert wird)
-   **presentation** (Präsentation, Vortrag auf einer Veranstaltung)
-   **proceedings** (Tagungsband einer Konferenz)
-   **standard** (Standard)
-   **techreport** (Bericht einer Hochschule oder einer
    anderen Institution)
-   **unpublished** (nicht formell veröffentlichtes Dokument)
-   **preprint** (ausgewähltes Werk, das vor dem offiziellen Veröffentlichungstermin des Gesamtwerks
    erschienen und gedruckt worden ist, z.B. für eine Konferenz)

**Alle Eintragstypen benötigen diese Felder:** title, author/editor, year.
Zusätzlich haben Benutzer bei manchen Entrytypes die Möglichkeit folgende Felder einzutragen:

- **article:** journal, volume, number, pages, month, language, DOI, ISBN, ISSN, note
- **book:** publisher, volume, number, series, address, edition, month, language, DOI, ISBN, note
- **booklet:** howpublished, address, month, language, DOI, ISN, note
- **inproceedings:** publisher, booktitle, volume, number, series, pages, address, month, organization,
language, DOI, ISBN, ISSN, eventdate, eventtitle, venue, note
- **dataset:** DOI, url
- **electronic:** language, DOI, note
- **inbook:** chapter, pages, publisher, volume, number, series, type, address, edition, month, language,
DOI, ISBN, note  
- **incollection:** publisher, booktitle, volume, number, series, type, chapter, pages, address, edition,
month, language, DOI, ISBN, note
- **manual:** organization, address, edition, month, language, DOI, ISBN, note
- **masterthesis:** school, type, address, month, language, DOI, ISBN, note
- **misc:** howpublished, month, language, DOI, note
- **periodical:** language, DOI, ISSN, note
- **presentation:** language, eventdate, eventtitle, venue, note
- **proceedings:** publisher, volume, number, series, address, month, language, DOI, ISBN, eventdate,
eventtitle, venue, organization, note
- **techreport:** institution, number, type, address, month, language, DOI, note
- **unpublished:** language, DOI, ISBN, ISSN, eventdate, eventtitle, venue, note

------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink")
  

