<!-- en: AddPublications -->

## Publikationen eintragen
-----------------------------

In ${project.name} kann jeder Benutzer Publikationen eintragen. Dadurch wächst die ${project.name}-Datenbank mit ihren Benutzern. Diese Anleitung soll Ihnen mehrere Wege zeigen, wie sie dies bewerkstelligen können.

----------------

### Automatische Eintragung per ${project.name}-Addon


Hierbei kann Ihnen das ${project.name}-Addon behilflich sein, da es automatisch Metainformationen von verschiedenen Verlegern und Systemen extrahieren kann. Die Installation wird Ihnen [hier](Integration/IntegrationBrowser "wikilink") erklärt.

-----------------------------

### Manuelle Eintragung durch Einfügen von Informationen per Hand

Sie können einzelne Publikationen aber auch manuell eintragen. Klicken Sie dazu im Hauptmenü auf den Button **"Eintragen"** und auf **"Publikation hinzufügen"**. Hier können Sie auch die ISBN-, DOI- oder ISSN-Nummer eingeben. Benötigen Sie dabei Hilfe, dann besuchen Sie [diese Anleitung](EintragPublikationen/EintragManuell "wikilink"). 

-----------------------------

### Importieren von Publikationen per BibTeX

Sie können Publikationen von anderen Literaturverwaltungsprogrammen (z. B. Citavi) importieren. Dazu müssen Sie zeurst eine BibTeX-Datei erstellen und diese dann in ${project.name} einführen. [Hier](EintragPublikationen/LiteraturlisteImportieren "wikilink") finden Sie dazu eine Anleitung.

-----------------------------

### Eintragung per Scannen des ISBN-Codes

Wenn Sie Zugang zu einer Webcam besitzen, dann ist dieser Weg möglicherweise am geeignetsten. Sie können den ISBN-Code von Ihrer Publikation mit ihr einscannen und dadurch zu ${project.name} hinzufügen. Benötigen Sie dabei Hilfe, dann besuchen Sie [diese Anleitung](EintragPublikationen/EintragScan "wikilink").

-----------------------------

### Eintragung von mehreren Publikationen

Möchten Sie viele Publikationen in einem Schritt hinzufügen? Dann folgen Sie einfach [dieser Anleitung](EintragPublikationen/EintragMehrerePublikationen "wikilink").

-----------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink")

