<!-- en: ImportBookmarks -->

## Lesezeichen aus anderen Anwendungen importieren
-----------------------------------------------

Sie können mehrere Lesezeichen auf einmal importieren (statt jedes
Lesezeichen einzeln hinzuzufügen). Auf diese Weise können sie eine
bereits bestehende Sammlung an Lesezeichen aus einer anderen Anwendung
(z.B. Ihrem Browser) zu ${project.name} hinzufügen. Im Folgenden finden
Sie kurze Anleitungen, wie Lesezeichen importiert werden können.

-----------------------------

### Browser

 Sie können Ihre Lesezeichen aus Ihrem Browser als HTML-Datei exportieren und dann diese HTML-Datei in ${project.name} importieren.  
 Folgen Sie dazu [dieser Anleitung](LesezeichenImportieren/LesezeichenImportierenBrowser "wikilink").

-----------------------------

### Delicious

Nutzen Sie Delicious, so können Sie Ihre Lesezeichen auch von dort importieren. Folgen Sie dazu [dieser Anleitung](LesezeichenImportieren/LesezeichenImportierenDelicious "wikilink").

-----------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink")
