<!-- en: RSS-FeedsSubscribe -->

## RSS-Feeds abonnieren
--------------------

**RSS** (engl. Really Simple Syndication)-Feeds ermöglichen es Ihnen,
immer auf dem neuesten Stand zu sein und keine Neuigkeiten zu verpassen.
Informationen zur RSS-Technik finden Sie unter anderem auf
[Wikipedia](http://de.wikipedia.org/wiki/RSS) oder auch auf [folgender Seite (techfacts)](http://www.techfacts.de/rss-was-genau-ist-das-eigentlich).

Konkret für ${project.name} bedeutet das, dass Sie eigene/fremde
Publikationslisten oder sogar die Publikationslisten von [
Gruppen](Gruppenfunktionen "wikilink") als RSS-Feed abonnieren können.
Sobald etwas Neues geschieht (Änderungen/Neueinträge), werden Sie informiert.  

![ <File:1.png>](rss-feedsabonnieren/1.png  "fig: File:1.png")  

1.  Klicken Sie auf das **Pfeilsymbol (Exportoptionen)** ganz rechts in der "Publikations/Lesezeichen"-Spalte, die Sie abonnieren möchten. Ein Menü erscheint.

2.  Klicken Sie nun auf den Eintrag **"RSS"**. Der RSS-Feed wird erzeugt und an Ihren RSS-Reader
    weitergeleitet (dies ist meist Ihr Webbrowser - weitere Infos hierzu
    finden Sie [hier (techfacts)](http://www.techfacts.de/rss-was-genau-ist-das-eigentlich).  
    
    **Hinweis:** Das weitere Vorgehen ist exemplarisch,
    es richtet sich sowohl nach Ihrem Webbrowser als auch nach
    Ihrem RSS-Reader. Im gezeigten Fall übernimmt der Browser "Mozilla
    Firefox" sowohl die Aufgabe des Webbrowsers als auch die
    des RSS-Readers. 
    
    Wenn Sie **"RSS"** angeklickt haben, werden
    Ihnen von Mozilla Firefox einige Optionen zum Abonnieren angezeigt.
    Wählen Sie hier **"Dynamische Lesezeichen"** und klicken Sie auf
    **"Jetzt abonnieren"**. Ein PopUp-Fenster erscheint.  
    
    ![ <File:RssFeed2.png>](rss-feedsabonnieren/RssFeed2.png  "fig: File:RssFeed2.png")  
    
3.  Geben Sie dem RSS-Feed einen **Namen**. ${project.name} generiert
    immer einen automatischen Namen, in diesem Fall sind es die
    Publikationen (publications for...) eines bestimmten
    Benutzers (/user/...).
   
4.  Geben Sie einen **Ordner** an, in dem der Feed gespeichert
    werden soll.
    
5.  Klicken Sie abschließend auf **"Abonnieren"**, um den Feed
    zu abonnieren/speichern.
    
6.  Der folgende Screenshot zeigt den exemplarisch abonnierten RSS-Feed.
    Vergleichen Sie die linke (orange markierte) Seite, dies ist die
    RSS-Feed-Reader-Ansicht mit der Ansicht rechts, dies ist die
    aktuelle Ansicht in ${project.name}.


![ <File:RssFeed3.png>](rss-feedsabonnieren/RssFeed3.png  "fig: File:RssFeed3.png")  

------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink") 