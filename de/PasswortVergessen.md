<!-- en: PasswordRecovery -->

## Ich habe mein Passwort vergessen
--------------------------------

${project.theme == "puma"}{

Benutzername und Passwort sind bei PUMA identisch mit den Daten, die Sie
bei der Anmeldung in der [Universitätsbibliothek Kassel](http://www.bibliothek.uni-kassel.de) bekommen haben. Melden Sie
sich, wenn Sie nicht weiterkommen, bei der [Leser- und Mahnverwaltung](http://www.ub.uni-kassel.de/lmv.html).

}

${project.theme == "bibsonomy"}{

Gehen Sie zur [ Passwort-Erinnerungsseite](${project.home}reminder "wikilink") und
geben Sie Ihren Benutzernamen und die E-Mail-Adresse ein, die Sie bei
der Registrierung angegeben haben. Nachdem Sie sich per "Captcha" 
verifiziert haben (klicken Sie hierfür unter "Captcha" in das leere Kästchen), 
senden wir Ihnen **per E-Mail** ein temporäres Passwort zu, mit dem Sie sich einloggen können. 
Danach müssenSie ihr Passwort wieder ändern.  

![ <File:1.png>](passwortvergessen/1.png  "fig: File:1.png")  

}

----------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink")
  

