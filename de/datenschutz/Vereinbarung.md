<!--en: privacy/Agreement-->

${project.theme == "bibsonomy"}{


Vereinbarung für eine gemeinsam verantwortliche Stelle
-----------------------------

Wir bieten ein gemeinsam betriebenen Bookmarking-Dienst für jedermann an.  
Wir, das sind:  

- Die Data Mining und Information Retrieval Gruppe der Universität Würzburg  
- Das Fachgebiet Wissensverarbeitung der Universität Kassel  
- Das Institut für Bibliotheks- und Informationswissenschaft Sachgebiet Information Processing and Analytics der Humboldt-Universität zu Berlin 
  
--------------------------------------

**Julius-Maximilians-Universität Würzburg (Körperschaft des öffentlichen Rechts)**  
Am Hubland  
D-97074  
Würzburg  
Tel: +49 931 / 31 – 86731  
Fax: +49 931 / 31 - 86732  
${project.email}  
Vertretungsberechtigter für die Data Mining und Information Retrieval Gruppe am LS VI  
ist Prof. Dr. Andreas Hotho.

**Universität Kassel (Körperschaft des öffentlichen Rechts)**  
Wilhelmshöher Allee 73  
34121 Kassel  
Tel.: +49 561 804 - 6250  
Fax: +49 561 804 - 6259  
${project.email}  
Vertretungsberechtigter für das Fachgebiet Wissensverarbeitung ist Prof. Dr. Gerd Stumme.  

**Humboldt-Universität zu Berlin (Körperschaft des öffentlichen Rechts)**  
Unter den Linden 6  
10099 Berlin  
Telefon: +49 30 2093–0  
Fax: +49 30 2093–2770  
${project.email}  
Vertretungsberechtigter für das Institut für Bibliotheks- und Informationswissenschaft  
Sachgebiet Information Processing and Analytics ist Prof. Dr. Robert Jäschke.  


Unsere Datenschutzbeauftragten sind:

- Behördlicher Datenschutzbeauftragter der Julius-Maximilians-Universität Würzburg 
- Behördliche Datenschutzbeauftragte der Universität Kassel 
- Behördliche Datenschutzbeauftragte der Humboldt-Universität zu Berlin
  
  -----------------------------------
  

Folgende Zwecke verfolgen wir:  
BibSonomy wird als Forschungsprojekt betrieben, um Empfehlungs-, Spammentdeckungs-, oder Ranking- Funktionen zu entwickeln und zu verbessern und die datenschutzgerechte Gestaltung von Social Bookmarking-Systemen voranzubringen.

Das System wird in der Veröffentlichung

> Benz, D. et al.: The Social Bookmark and Publication Management System BibSonomy. The VLDB Journal. 19, 6, 849--875 (2010).

vorgestellt.
Aus dieser können auch Beispiele der Forschung entnommen werden.  
Zusätzlich berichten wir in unserem Blog über unsere aktuellen Forschungsergebnisse.  
Eine Liste von Forschungsprojekten finden Sie [hier](Projekte).  

Als Mittel zur Datenverarbeitung setzen wir u. a. statistische Analyse und Machine Learning ein.  
Die Konzeption für „Privacy by design“ setzen alle beteiligten Stellen um.  
Datenschutzfreundliche Voreinstellungen setzen alle beteiligten Stellen um.  
Die Umsetzung der Datensicherheit, das Melden von Sicherheitsvorfällen und die Benachrichtigung der Betroffenen verantworten alle beteiligten Stellen.  
Die Informationspflichten sind über die Webseite des Portals und mit Anlegen eines Accounts für die Betroffenen verfügbar.    
Die Ausgestaltung verantworten alle beteiligten Stellen.  
Das Recht auf Datenübertragbarkeit setzen alle beteiligten Stellen um.  
Die Anlaufstelle für datenschutzrechtlich Betroffene setzen alle beteiligten Stellen um.  

Ihre Anliegen werden auch unter ${project.email} beantwortet.   
Diese Vereinbarung wird unter https://www.bibsonomy.de/help_de/datenschutz/Vereinbarung veröffentlicht.
}