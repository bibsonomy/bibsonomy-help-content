<!-- en: Scraper -->

## Scraper in ${project.name}
---------------------------

Ein Scraper ist eine sehr nützliche Funktion in ${project.name}, die es
Ihnen erleichtert, Publikationen von **externen Webseiten** in
${project.name} zu speichern.  
  
**Benutzung:** Sie schauen sich auf einer externen
Literatursammlungs-Webseite (z.B. [ACM Digital
Library](http://dl.acm.org/)) eine Publikation an und möchten diese in
Ihre Sammlung in ${project.name} kopieren. Am einfachsten geht das über
die ${project.name}-[Browsererweiterungen](http://www.bibsonomy.org/buttons).
Klicken Sie auf das **${project.name}-Symbol** im Browser und wählen
Sie **"Publikation in ${project.name} speichern"** (das dritte Symbol von oben). 

![<File:Browsererweiterung.png>](scraper/Browsererweiterung.png  "fig: File:Browsererweiterung.png")  

Sie werden automatisch zu ${project.name} weitergeleitet und durch
einen Scraper wurden alle Informationen über diese Publikationen
extrahiert und bereits in das Formular eingetragen. Sie können diese
jetzt noch anpassen und anschließend durch Klicken auf **"Speichern"**
speichern.  
  
![ <File:ScraperBib.png>](scraper/ScraperBib.png  "fig: File:ScraperBib.png")  
  
Eine Übersicht über alle externen Seiten, bei denen durch Scraper
automatisch Informationen extrahiert werden können, finden Sie [hier](${project.home}scraperinfo "wikilink").  
Sie können auch den [Scraper-Service](http://scraper.bibsonomy.org/)
benutzen, um durch Eingabe einer URL bibliographische Metadaten aus
einer Webseite zu extrahieren.

------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink") 
  

