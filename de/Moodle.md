<!-- en: Moodle -->

## ${project.name} auf der E-Learning-Plattform Moodle
----------------------------------------------------

[Moodle](https://moodle.org) ist eine beliebte **E-Learning Umgebung**. 
PBM ist das ${project.name} Plugin für **Moodle**. Mithilfe des Plugins können Sie
Literaturlisten von ${project.name} innerhalb eines Moodlekurses veröffentlichen. 

--------------------------------------------------

### Funktionen

Das PBM Plugin macht es Ihnen möglich, Literaturlisten innerhalb eines Moodlekurses zu veröffentlichen. 
Die Daten für diese Listen werden von ${project.name} bezogen.
Die Listen werden konfiguriert durch die Angabe eines Nutzernamens, einer Nutzergruppe und/oder Tags.

Sie können [hier](http://citationstyles.org/) verschiedene CSL Styles auswählen,
in denen Ihre Literaturlisten im Moodlekurs angezeigt werden.

--------------------------------------------------

### Für Administratoren

#### Installation

Die folgenden Schritte sind notwendig, um das PBM Plugin zu Ihrer Moodle Installation hinzuzufügen.

  1. Laden Sie das Plugin aus dem [BitBucket Repository](https://bitbucket.org/bibsonomy/pbm)
     oder direkt von Moodles [Plugin Repository](https://moodle.org/plugins/view.php?plugin=mod_pbm) herunter.
  2. Entpacken Sie die Datei innerhalb Ihres "mod" Ordners der Moodle Installation. (e.g. /var/www/moodle/mod/pbm)
  3. Gehen Sie nun auf die Moodle Webseite, melden Sie sich als Administrator an und gehen anschließend auf "Einstellungen -> Website-Administration -> Plugins -> Übersicht".
  4. Klicken Sie den Button "Verfügbare Aktualisierungen" ganz oben auf der Übersichtsseite der Plugins. 
     (Meist informiert Moodle Sie automatisch über Aktualisierungen.) 
  5. Moodle sollte nun anzeigen, dass Updates vorgenommen werden müssen. Genehmigen Sie diese. 
  6. Sie werden nun aufgefrodert, eine Default Server Adresse und OAuth Benutzer-Credentials anzugeben und einen Zitationsstil auszuwählen. 
     Mehr Details zu diesem Thema können Sie auch im nächsten Schritt nachlesen.


#### Konfiguration

Die verschiedenen Optionen für die Konfiguration finden Sie unter *Website-Administration/Plugins/PUMA/Bibsonomy*.

**Default Server for OAuth**: Wenn Sie eine OAuth Authentifizierung nutzen wollen, wird der ausgewählte Server verwendet. 
Wählen Sie hierfür die URL von ${project.name} (http://www.bibsonomy.org/).

**Consumer-Key/Consumer-Secret(Optional)**: Bevor Ihre Anwendung auf die API von BibSonomy zugreifen kann, 
müssen beide Anwendungen einen gesicherten Kommunikationskanal aufbauen. Dies wird durch den Austausch von Anmeldedaten, 
dem sogenannten *consumer key* und dem *consumer secret*, erreicht. Der *consumer key* identifiziert Ihre Anwendung, und durch den *consumer secret* werden Ihre Anfragen verifiziert. 
Um einen *consumer key* und ein *consumer secret* zu beantragen, schreiben Sie bitte eine E-Mail an ${project.apiemail}.
Dies ist allerdings optional, da es auch möglich ist, eine API Key Authentifizierung (BasicAuth) zu verwenden.

**Available CSL files**: Hier können Sie zwischen verschiedenen Zitationsstilen wählen, die Sie beim Bearbeiten von PBM Inhalten innerhalb
eines Moodlekurses verwenden können.

--------------------------------------------------

### Für Nutzer

#### Publikationslisten in einem Moodlekurs erstellen

In den folgenden Schritten wird erklärt, wie das Plugin von einem Lehrenden/Trainer verwendet werden kann, um Literaturlisten auf einer Moodlekursseite zu erstellen.
**Hinweis**: Der Lehrende/Trainer der einen neuen PBM Inhalt erstellt, benötigt gültige
Authentification Credentials, die in den Einstellungen des Nutzerprofils hinterlegt sein müssen.
Ein Nutzerprofil kann bearbeitet werden, indem Sie auf den Nutzernamen klicken und "Profil"
auswählen.
DIe für PBM spezifischen Felder können Sie unter der ${project.name} API Key and OAuth Kategorie finden. 
(Weitere Details finden Sie unter **Nutzer Konfiguration**)

  1. Aktivieren Sie den Bearbeitungsmodus und klicken Sie den "Add an activity or resource" Link.
  2. Eine Liste mit Aktivitätsmodulen erscheint. Wählen Sie PBM und klicken Sie "add".
  3. Nun können Sie Informationen hinzufügen, um eine bestimmte Literaturliste zu bestimmen.

**Get content from user or group?**: Literaturlisten können von einem bestimmten Nutzer oder einer bestimmten
Gruppe importiert werden.

**Insert user or group id**: Identifier für eine Quelle: z.B. ein Nutzername oder eine GruppenID. 

**Tags**: Eine Liste von Tags, durch Leerzeichen getrennt.

**Provide associated documents?**: Wenn dies ausgewählt ist, werden Dokumente, die an Literatur angehängt sind, angezeigt und können 
heruntergeladen werden.

**Select Authentication Method**: Hier wird festgelegt, welche Methode der Authentifizierung verwendet werden soll, wenn Publikationen vom ${project.name} Server importiert werden. Weitere Informationen unter **Nutzer Konfiguration**.

**Server: API Key | OAuth**: Hier können Sie den aktuell ausgewählten Server für die API-Key Authentifizierung 
und OAuth Authentifizierung sehen. Der Server für API Key Authentifizierung kann in den Einstellungen des Nutzerprofils geändert werden. 
Der OAuth Server muss von einem Administrator in den Plugin Einstellungen gewählt werden. 

**Citation style**: Der Zitationsstil bestimmt die Formatierung der Literatur. 
PBM hat bereits verschiedene [Csl Files](http://citationstyles.org/), unter denen Sie wählen können. 

**Citation style language**: Einige Zitationsstile können sich in verschiedenen Sprachen unterscheiden.


#### Zu meinem ${project.name} hinzufügen

 Wenn ein Nutzer Anmeldedaten zur Authentifizierung (mehr Informationen unter **Nutzer Konfiguration**) hinterlegt hat, 
 kann er Publikationen zu seiner persönlichen Liste in ${project.name} hinzufügen.
 Klicken Sie hierzu auf den Button, der sich unten rechts bei jeder Publikation befindet. Wenn eine Publikation
 sich bereits auf der persönlichen Liste beinfdet, wird ein grünes Häkchen angezeigt.


#### Nutzer Konfiguration


![ <File:1.png>](moodle/1.png  "fig: File:1.png")  


**API Server**: Dieser Server wird für die API Key Authentifizierung (BasicAuth) verwendet.

**API User**: Die NutzerID eines ${project.name}accounts.

**API Key**: Den API Key finden Sie in den [Einstellungen](https://www.bibsonomy.org/settings) -> "Einstellungen-Tab" im  API Abschnitt, wenn Sie auf ${project.name} eingeloggt sind.

**OAuth Link**: Wenn dieses Feld ausgewählt ist und Änderungen im Profil gesichert werden, wird der Nutzer zum Default OAuth Server weitergeleitet,
um ihm die Verwendung von OAuth zu erlauben.

**Hinweis**: Der Nutzer muss auf ${project.name} eingeloggt sein, **bevor** er seinen Account verknüpft. Andernfalls funktioniert die 
Weiterleitung nicht und die Verknüpfung muss noch einmal durchgeführt werden.


Weitere Informationen über **Installation und Nutzung** finden Sie [hier](https://moodle.org/plugins/mod_pbm).

------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink") 