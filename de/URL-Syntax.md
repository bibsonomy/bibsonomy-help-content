<!-- en: URL-Syntax -->

## URL-Syntax
----------

**Hinweis:** Diese Seite richtet sich vor allem an Entwickler und technisch versierte Nutzer.

Diese Seite fasst alle Informationen zur ${project.name}-URL-Syntax
zusammen. Alle Links auf dieser Seite können angeklickt werden um eine
Beispielausgabe anzuzeigen (insbesondere hilfreich bei Such-URLs).

--------------------------------------------

### Allgemeine Seiten

-   **[ /](${project.home}"wikilink")**  
    Homepage von ${project.name}, zeigt die aktuellsten 50 öffentlichen
    Einträge.  
    
-   **[ /popular](${project.home}popular "wikilink")**  
    Zeigt die 100 häufigsten Einträge der letzten 100.000 öffentlichen
    Einträge.  
    
-   **[ /help\_de](${project.home}help_de "wikilink")**  
    Die Hilfeseite.  
    
-   **[ /help\_de/FAQ](${project.home}help_de/FAQ "wikilink")**  
    Eine Seite, die die am häufigsten gestellten Fragen und ihre
    Antworten enthält.  
    
-   **[ /user/jaeschke](${project.home}user/jaeschke "wikilink")**  
    Zeigt alle öffentlichen Einträge des Benutzers *jaeschke*.  
    
-   **[ /user/jaeschke/web](${project.home}user/jaeschke/web "wikilink")**  
    Zeigt alle öffentlichen Einträge mit dem Schlagwort (Tag) *web* des
    Benutzers *jaeschke*.  
    
-   **[/user/jaeschke/web%20api](${project.home}user/jaeschke/web%20api "wikilink")**  
    Zeigt alle öffentlichen Einträge mit dem Tag *web* und dem Tag *api*
    des Benutzers *jaeschke*.  
    
-   **[ /myHome](myBibSonomy "wikilink")**  
    Link zu der Liste Ihrer eigenen Lesezeichen und bibliografischen
    Referenzen.  
    
-   **[ /myBibTeX](myBibTeX "wikilink")**  
    Link zur BibTeX-Liste Ihrer eigenen bibliografischen Referenzen.  
    
-   **[ /myRelations](myRelations "wikilink")**  
    Link zur Liste Ihrer eigenen Relationen.  
    
-   **[ /myDocuments](myPDF "wikilink")**  
    Link zu Ihrer Sammlung von Volltexten (PDF, PS, TXT, DJV or DJVU).  
    
-   **[ /myDuplicates](myDuplicates "wikilink")**  
    Link zu Ihren eigenen Duplikaten.  
    
---------------------------------

### Parameter zum Sortieren

Immer wenn Sie in ${project.name} Zugriff auf eine
Lesezeichen/Publikationsliste haben, können Sie diese sortieren, indem
Sie an die URL einen/mehrere der folgenden Parameter anhängen. Folgende
Parameter stehen Ihnen zur Verfügung:  

-  **items** - Wie viele Einträge sollen angezeigt werden?  

	(Beispiel: **?items=30** zeigt 30 Einträge an.)  
	*Hinweis: Wenn Sie keinen **item-Parameter** mit der von Ihnen gewünschten Zahl an die URL anhängen, 
	werden standardmäßig höchstens 20 Einträge angezeigt.*
	
- **sortPage** - Wonach wird sortiert?
	-   **Werte (können durch | verknüpft werden):**
        -   **author** - Autorenname
        -   **editor** - Herausgebername
        -   **year** - Erscheinungsjahr
        -   **entrytype** - Publikationstyp
        -   **title** - Titel
        -   **booktitle** - Buchtitel (insb. bei Artikel
            in Sammelbänden)
        -   **journal** - Journalname
        -   **school** - Universitätsname

-   **sortPageOrder** - Reihenfolge der Sortierung
    -   **Werte:**
        -   **asc** - aufsteigend
        -   **desc** - absteigend

-   **duplicates**
    -   **Werte:**
        -   **yes** - Erlaube Duplikate
        -   **no** - Entferne Duplikate aus der Ergebnisansicht


**Beispiel:**  
**?sortPage=year&sortPageOrder=asc&duplicates=no**  
Sortiere nach Erscheinungsjahr (sortPage=year) aufsteigend
(sortPageOrder=asc) und entferne alle Duplikate (duplicates=no)  

----------------------------------

### Verwaltungsseiten

-   **[ /settings](settings "wikilink")**  
    Auf dieser Seite können Sie:
    -   einen Benutzer zu Ihrer Gruppe hinzufügen,
    -   Ihren API-Schlüssel finden und einen neuen erzeugen,
    -   Ihre del.icio.us-Daten importieren,
    -   Ihr Passwort ändern,
    -   Ihre Kontoeinstellungen ändern (E-Mail, Name der Homepage),
    -   Ihre Daten zwischen [BibSonomy](http://www.bibsonomy.org/) und
        [PUMA](http://puma.uni-kassel.de/) synchronisieren.  
        

<!-- -->

-   **[ /postBookmark](postBookmark "wikilink")**  
    Auf dieser Seite können Sie:
    -   eine URL eingeben, um einen Lesezeicheneintrag zu generieren.  
        

<!-- -->

-   **[ /postPublication](postPublication "wikilink")**  
    Auf dieser Seite können Sie:
    -   Typ, Titel, Autor, Herausgeber und Jahr eingeben, um einen
        Publikationseintrag zu generieren,
    -   einen ausgewählten BibTex-Schnipsel in eine Textbox einfügen, um
        einen oder mehrere BibTex-Einträge zu generieren,
    -   eine BibTex-Datei hochladen, um einen oder mehrere
        BibTex-Einträge zu generieren.  
        
-----------------------------

### Autorenseiten

${project.name} bietet eine Möglichkeit, Publikationen über die Namen
ihrer Autoren zu finden. Es gibt verschiedene Wege, diese Suchergebnisse
zu filtern. Gegenwärtig beinhalten die Filter das Publikationsjahr,
einen besonderen Tag und den Benutzernamen der Person, die den Eintrag
gespeichert hat.  

- **[ /author/hotho](author/hotho "wikilink")**  
Zeigt alle Einträge mit dem Autor Hotho.  
  
- **[/author/stumme+hotho+schmitz](author/hotho+stumme+schmitz "wikilink")**  
Zeigt alle Publikationen, die von diesen Autoren veröffentlicht wurden.  
  
- **[/author/stumme+hotho+!schmitz](author/stumme+hotho+!schmitz "wikilink")**
oder **[
/author/stumme+hotho+%21schmitz](author/stumme+hotho+%21schmitz "wikilink")**  
Gibt den Namen eines Autors an, der nicht Teil der gesuchten
Publikationen sein soll, z.B. zeige alle Publikationen an, die von
Stumme und Hotho geschrieben sind, aber nicht von Schmitz.  
  
- **[/author/hotho/clustering](author/hotho/clustering "wikilink")**  
Zeigt alle Einträge mit dem Tag 'clustering' und dem Autor Hotho.  
  
- **[/author/stumme/sys:user:hotho](author/stumme/sys:user:hotho "wikilink")**  
Zeigt alle Publikationen des Autors Stumme in Hothos Sammlung.  
  
- **[/author/stumme+hotho+!schmitz/sys:year:2002-2007%20sys:user:hotho%20folksonomy](author/stumme+hotho+!schmitz/sys:year:2002-2007%20sys:user:hotho%20folksonomy "wikilink")**  
Diese Kombination von Suchergebnissen zeigt alle Publikationen der
Autoren Stumme und Hotho, aber nicht Schmitz in den Jahren 2002 bis 2007
in der Sammlung des Benutzers Hotho, denen der Tag 'folksonomy'
zugeordnet wurde.  
  
- **[/author/stumme/sys:group:kde](author/stumme/sys:group:kde "wikilink")**  
Zeigt alle Publikationen des Autors Stumme in der Sammlung aller
Gruppenmitglieder der Gruppe 'kde'.  
  
-------------------------

### Autorensuche: Erscheinungsjahr

Ein [ Systemtag](SystemTags "wikilink") (System-Schlagwort) kann das Ergebnis
Ihrer Autorensuche auf ein bestimmtes Erscheinungsjahr oder einen
bestimmten Zeitraum beschränken. Es sind vier Formate möglich:  


-   **[/author/stumme/sys:year:2007](author/stumme/sys:year:2007 "wikilink")**  
    Zeigt alle Publikationen des Autors Stumme des Jahres 2007.  
    
-   **[/author/stumme/sys:year:2003-2007](author/stumme/sys:year:2003-2007 "wikilink")**  
    Zeigt alle Publikationen des Autors Stumme zwischen 2003 und 2007.  
    
-   **[/author/stumme/sys:year:-2005](author/stumme/sys:year:-2005 "wikilink")**  
    Zeigt alle Publikationen des Autors Stumme bis zum Jahr 2005.  
    
-   **[/author/stumme/sys:year:1997-](author/stumme/sys:year:1997- "wikilink")**  
    Zeigt alle Publikationen des Autors Stumme seit 1997.  
    
-----------------------------

### Freundeseiten

Eine Einführung in die Sichtbarkeitseinstellungen finden Sie unter [
Freunde](FreundeHinzufügen "wikilink"). Im folgenden Abschnitt werden
die wichtigsten Freunde-URLs aufgeführt.  


-   **[ /friends](friends "wikilink")**  
    Zeigt alle für Freunde sichtbar gesetzten Einträge aller Benutzer,
    die Sie als Ihren Freund angegeben haben. Zusätzlich können Sie:
    -   einen Freund zu Ihrer Freundesliste hinzufügen,
    -   einen Freund von Ihrer Freundesliste entfernen.  
    
-   **[ /friend/jaeschke](friend/jaeschke "wikilink")**  
    Zeigt alle Beiträge, welche für Freunde des Benutzers *jaeschke*
    sichtbar gesetzt sind. Sie können sie nur dann sehen, wenn
    *jaeschke* Sie als Freund angegeben hat.  
    
-   **[ /friend/jaeschke/web](friend/jaeschke/web "wikilink")**  
    Zeigt alle Beiträge mit dem Tag 'web', welche für Freunde des
    Benutzers *jaeschke* sichtbar gesetzt sind. Sie können sie nur dann
    sehen, wenn *jaeschke* Sie als Freund angegeben hat.  
    
-   **[/friend/jaeschke/web%20api](friend/jaeschke/web%20api "wikilink")**  
    Zeigt alle Beiträge mit dem Tag 'web' und dem Tag 'api', welche für
    Freunde des Benutzers *jaeschke* sichtbar gesetzt sind. Sie können
    sie nur dann sehen, wenn *jaeschke* Sie als Freund angegeben hat.  

----------------------------

### Gruppenseiten

Eine Einführung in die Sichtbarkeitseinstellungen finden Sie unter [
Gruppen](Gruppenfunktionen "wikilink"). Im folgenden Abschnitt werden
die wichtigsten Gruppen-URLs aufgeführt.  


-   **[ /groups](groups "wikilink")**  
    Zeigt alle Gruppen des Systems an.  
    
-   **[ /group/kde](group/kde "wikilink")**  
    Zeigt alle Einträge von Mitgliedern der Gruppe 'kde', wenn Sie
    Gruppenmitglied sind.  
    
-   **[ /group/kde/web](group/kde/web "wikilink")**  
    Zeigt alle Einträge mit dem Tag 'web' von Mitgliedern der Gruppe
    'kde', wenn Sie Gruppenmitglied sind.  
    
-   **[ /group/kde/web%20api](group/kde/web%20api "wikilink")**  
    Zeigt alle Einträge mit dem Tag 'web' und dem Tag 'api' von
    Mitgliedern der Gruppe 'kde', wenn Sie Gruppenmitglied sind.  
    
-   **[/relevantfor/group/kde](relevantfor/group/kde "wikilink")**  
    Zeigt alle Einträge, die für die Teilnehmer der Gruppe relevant
    sind.  
    
-   **[ /followers](followers "wikilink")**  
    Zeigt die neuesten Einträge aller Benutzer, denen Sie folgen. Diese
    Einträge werden mittels eines Rankings so umsortiert, dass die für
    Sie relevantesten Einträge ganz oben stehen.  

----------------------------

### Tagseiten/Schlagwortseiten

-   **[ /tag/web](tag/web "wikilink")**  
    Zeigt alle öffentlichen Einträge mit dem Tag (Schlagwort) 'web'.  
    
-   **[ /tag/web%20api](tag/web%20api "wikilink")**  
    Zeigt alle öffentlichen Einträge mit dem Tag 'web' und dem Tag
    'api'.  
    
-   **[/tag/web?order=folkrank](tag/web?order=folkrank "wikilink")**  
    Zeigt alle öffentlichen Einträge mit dem Tag 'web' und ordnet die
    Ergebnisse nach dem [FolkRank-Algorithmus](https://www.bibsonomy.org/bibtex/24d8b4f79814691fbe6db8357d63206a1/stumme).  
    
------------------------------

### Relationen und Tags

-   **[/api/tags/data](http://www.bibsonomy.org/api/tags/data)**  
    Zeigt alle Tags, die in Relation zu 'data' stehen.  
    
-   **[/api/tags/data?relation=related](http://www.bibsonomy.org/api/tags/data?relation=related)**  
    Zeigt alle Tags, die in Relation zu 'data' stehen.  
    
-   **[/api/tags/data?relation=similar](http://www.bibsonomy.org/api/tags/data?relation=similar)**  
    Zeigt alle Tags, die 'data' ähnlich sind.  

-------------------------------------

### Relationen und Begriffsseiten

-   **[/relations/schmitz](http://www.bibsonomy.org/relations/schmitz)**  
    Zeigt alle Relationen des Benutzers *schmitz*.  
    
-   **[/concept/user/schmitz/event](http://www.bibsonomy.org/concept/user/schmitz/event)**  
    Zeigt alle Lesezeichen und Publikationen des Benutzers *schmitz*,
    denen das Schlagwort (Tag) 'event' oder eines der Unterschlagwörter
    der Relation als Tag zugeordnet ist.  

----------------------------------

### Suchseiten

-   **[ /search/web](search/web "wikilink")**  
    Zeigt alle öffentlichen Einträge, die im Volltext (nicht in
    den Schlagwörtern!) das Wort 'web' enthalten. Bei Lesezeichen
    enthält der Volltext die URL, den Titel und die Beschreibung. Bei
    Publikationen sind der Titel, die Beschreibung und alle
    BibTex-Felder enthalten.  
    
-   **[ /search/web%20api](search/web%20api "wikilink")**  
    Zeigt alle öffentlichen Einträge, die im Volltext (nicht in
    den Schlagwörtern!) das Wort 'web' und das Wort 'api' enthalten. Bei
    Lesezeichen enthält der Volltext die URL, den Titel und
    die Beschreibung. Bei Publikationen sind der Titel, die Beschreibung
    und alle BibTex-Felder enthalten.  
    
-   **[ /search/web%20-api](search/web%20-api "wikilink")**  
    Zeigt alle öffentlichen Einträge, die im Volltext (nicht in
    den Schlagwörtern!) das Wort 'web', aber nicht das Wort
    'api' enthalten. Bei Lesezeichen enthält der Volltext die URL, den
    Titel und die Beschreibung. Bei Publikationen sind der Titel, die
    Beschreibung und alle BibTex-Felder enthalten.  
    
-   **[/search/web%20user:jaeschke](search/web%20user:jaeschke "wikilink")**  
    Zeigt alle öffentlichen Einträge des Benutzers *jaeschke*, die im
    Volltext (nicht in den Schlagwörtern!) das Wort 'web' enthalten. Bei
    Lesezeichen enthält der Volltext die URL, den Titel und
    die Beschreibung. Bei Publikationen sind der Titel, die Beschreibung
    und alle BibTex-Felder enthalten.  
    
-   **[/search/web%20api%20user:jaeschke](search/web%20api%20user:jaeschke "wikilink")**  
    Zeigt alle öffentlichen Einträge des Benutzers *jaeschke*, die im
    Volltext (nicht in den Schlagwörtern!) das Wort 'web' und das Wort
    'api' enthalten. Bei Lesezeichen enthält der Volltext die URL, den
    Titel und die Beschreibung. Bei Publikationen sind der Titel, die
    Beschreibung und alle BibTex-Felder enthalten.  
    
-   **[/search/web%20-api%20user:jaeschke](search/web%20-api%20user:jaeschke "wikilink")**  
    Zeigt alle öffentlichen Einträge des Benutzers *jaeschke*, die im
    Volltext (nicht in den Schlagwörtern!) das Wort 'web', aber nicht
    das Wort 'api' enthalten. Bei Lesezeichen enthält der Volltext die
    URL, den Titel und die Beschreibung. Bei Publikationen sind der
    Titel, die Beschreibung und alle BibTex-Felder enthalten.  
    
-   **[ /mySearch](mySearch "wikilink")**  
    Diese Seite bietet eine Schnellsuche in Ihrer Sammlung.  
    

Für eine Übersicht der weiteren Optionen, die Ihnen bei der Suche auf ${project.name} zur Verfügung stehen, besuchen Sie die [Hilfeseite
für die Suche](SuchenHilfe "wikilink").

---------------------------------

### Sichtbare Seiten

Eine Einführung in die Sichtbarkeitseinstellungen finden Sie unter [
Gruppen](Gruppenfunktionen "wikilink") sowie unter [Freunde](FreundeHinzufügen "wikilink"). Im folgenden Abschnitt werden
die wichtigsten Sichtbarkeits-URLs aufgeführt.  


-   **[ /viewable/public](viewable/public "wikilink")**  
    Zeigt alle Ihre Einträge, die Sie als öffentlich sichtbar
    eingestellt haben.  
    
-   **[ /viewable/public/web](viewable/public/web "wikilink")**  
    Zeigt alle Ihre Einträge mit dem Tag (Schlagwort) 'web', die Sie als
    öffentlich sichtbar eingestellt haben.  
    
-   **[/viewable/public/web%20api](viewable/public/web%20api "wikilink")**  
    Zeigt alle Ihre Einträge mit dem Tag 'web' und dem Tag 'api', die
    Sie als öffentlich sichtbar eingestellt haben.  
    
-   **[ /viewable/private](viewable/private "wikilink")**  
    Zeigt alle Ihre Einträge, die Sie als privat sichtbar eingestellt
    haben.  
    
-   **[ /viewable/private/web](viewable/private/web "wikilink")**  
    Zeigt alle Ihre Einträge mit dem Tag 'web', die Sie als privat
    sichtbar eingestellt haben.  
    
-   **[/viewable/private/web%20api](viewable/private/web%20api "wikilink")**  
    Zeigt alle Ihre Einträge mit dem Tag 'web' und dem Tag 'api', die
    Sie als privat sichtbar eingestellt haben.  
    
-   **[ /viewable/friends](viewable/friends "wikilink")**  
    Zeigt alle Ihre Einträge, die Sie als für Freunde sichtbar
    eingestellt haben.  
    
-   **[ /viewable/friends/web](viewable/friends/web "wikilink")**  
    Zeigt alle Ihre Einträge mit dem Tag 'web', die Sie als für Freunde
    sichtbar eingestellt haben.  
    
-   **[/viewable/friends/web%20api](viewable/friends/web%20api "wikilink")**  
    Zeigt alle Ihre Einträge mit dem Tag 'web' und dem Tag 'api', die
    Sie als für Freunde sichtbar eingestellt haben.  
    
-   **[ /viewable/kde](viewable/kde "wikilink")**  
    Zeigt alle Einträge, die für die Gruppe 'kde' als sichtbar
    eingestellt wurden.  
    
-   **[ /viewable/kde/web](viewable/kde/web "wikilink")**  
    Zeigt alle Einträge mit dem Tag 'web', die für die Gruppe 'kde' als
    sichtbar eingestellt wurden.  
    
-   **[/viewable/kde/web%20api](viewable/kde/web%20api "wikilink")**  
    Zeigt alle Einträge mit dem Tag 'web' und dem Tag 'api', die für die
    Gruppe 'kde' als sichtbar eingestellt wurden.  

-------------------------------

### Behandlung von Duplikaten

Auf mehreren Seiten werden Einträge (Publikationen) angezeigt, die von
mehreren Benutzern stammen. Ein Beispiel hierfür sind Gruppenseiten.
Wenn innerhalb einer Gruppe zwei oder mehr Benutzer denselben Eintrag in
Ihrer Sammlung haben, wird dieser ebenfalls mehrfach angezeigt.  
Falls dies nicht gewünscht ist, kann das Verhalten mittels des
Parameters *duplicates* wie folgt angepasst werden:  


-   **[ /group/kde/myown](group/kde/myown "wikilink")**  
    Zeigt alle Einträge der Gruppe 'kde' an, die mit dem Tag 'myown'
    annotiert sind (auch Duplikate).  
    
-   **[/group/kde/myown?duplicates=no](group/kde/myown?duplicates=no "wikilink")**  
    Zeigt alle Einträge der Gruppe 'kde' an, die mit dem Tag 'myown'
    annotiert sind; für jedes Duplikat wird nur der erste Eintrag
    angezeigt.  
    
-   **[/group/kde/myown?duplicates=merged](group/kde/myown?duplicates=merged "wikilink")**  
    Zeigt alle Einträge der Gruppe 'kde' an, die mit dem Tag 'myown'
    annotiert sind; für jedes Duplikat werden alle Tags "aufgesammelt"
    und aggregiert an einem einzelnen Eintrag angezeigt.  

---------------------------

### Export von Seiten

-   **[ /clipboard](clipboard "wikilink")**  
    Hier können Sie Publikationseinträge mit der Schaltfläche "Auswahl"
    verwalten.  
    
-   **[ /export/](export/ "wikilink")**  
    Zeigt eine Auswahl von Formaten an, die Sie zum Export von
    Publikationsmetadaten wählen können.  
    

Die folgenden URLs sind Befehle, die auch auf der Export-Seite
angeklickt werden können.  

-----------------------------

### RSS Feeds

-   **[ /publrss/](publrss/ "wikilink")**  
    Zeigt einen RSS-Feed der Publikationen-Hauptseite.  
    
-   **[ /burst/](burst/ "wikilink")**  
    ${project.name} [BuRST](http://www.cs.vu.nl/pmika/research/burst/BuRST.html)-Feed für die Publikationen-Hauptseite.  
    
-   **[ /aparss/](aparss/ "wikilink")**  
    RSS Feed im [APA](http://www.apa.org/)-Format.  

--------------------------

### Referenz-Metadaten und Formatierung

-   **[ /bib/](bib/ "wikilink")**  
    BibTeX-Format aller Publikationen der Publikationshauptseite.  
    
-   **[ /bib/user/beate](bib/user/beate "wikilink")**  
    BibTeX-Format aller Publikationen der Publikationsseite des
    Benutzers *beate*.  
    
-   **[ /endnote/](endnote/ "wikilink")**  
    EndNote-Format der Publikationen der Hauptseite.  
    
-----------------------------------

### HTML-Formatierung

-   **[ /publ/](publ/ "wikilink")**  
    Eine einfache Übersicht, in der jeder Eintrag als Zeile in einer
    Tabelle dargestellt ist.  
    
-   **[ /publ/?notags=1](publ/?notags=1 "wikilink")**  
    Unterdrückt die ${project.name}-Schlagwörter in der HTML-Ausgabe.  
    
-   **[ /publ/user/beate](publ/user/beate "wikilink")**  
    Die Publikationen des Nutzers *beate*.  
    
-   **[/publ/user/beate/myown](publ/user/beate/myown "wikilink")**  
    Die Publikationen des Nutzers *beate* mit dem Tag *myown*.  

--------------------------------

### Semantic Web-Formatierung

-   **[ /swrc/](swrc/ "wikilink")**  
    RDF-Ausgabe gemäß der [SWRC-Ontologie](http://ontoware.org/swrc/).  

------------------------------

### URL- oder BibTeX-Seiten

-   **[/url/d1bb7b3f6cafafa7b418f9f356ff2e83](url/d1bb7b3f6cafafa7b418f9f356ff2e83 "wikilink")**  
    Zeigt alle öffentlichen ${project.name}-Lesezeicheneinträge der URL
    mit dem MD5-Hash d1bb7b3f6cafafa7b418f9f356ff2e8.  
    
-   **[/url/d1bb7b3f6cafafa7b418f9f356ff2e83/jaeschke](url/d1bb7b3f6cafafa7b418f9f356ff2e83/jaeschke "wikilink")**  
    Zeigt die ${project.name}-Lesezeicheneinträge des Benutzers
    *jaeschke* mit dem MD5-Hash d1bb7b3f6cafafa7b418f9f356ff2e83.  
    
-   **[/bibtex/1d28c9f535d0f24eadb9d342168836199](bibtex/1d28c9f535d0f24eadb9d342168836199 "wikilink")**  
    Zeigt alle öffentlichen ${project.name}-Publikationseinträge mit
    dem [Hashkey](help/doc/inside.html "wikilink") 1d28c9f535d0f24eadb9d342168836199.
    Der benutzte Hash ist der Inter-Hash.  
    
-   **[/bibtex/25854a71547051543dd3d3d5e2e2f2b67/steff83](bibtex/25854a71547051543dd3d3d5e2e2f2b67/steff83 "wikilink")**  
    Zeigt den ${project.name}-Publikationseintrag des Benutzers
    *steff83* mit dem Hashkey 25854a71547051543dd3d3d5e2e2f2b67. Der
    benutzte Hash ist der Intra-Hash. ${project.name} liefert einen
    Tag-JSON-Feed, der zu einem BibTeX-Eintrag gehört.  
    
-   **[/json/tags/bibtex/1d28c9f535d0f24eadb9d342168836199](json/tags/bibtex/1d28c9f535d0f24eadb9d342168836199 "wikilink")**  
    Liefert eine JSON-Ausgabe. Diese enthält alle Schlagwörter, welche
    in Beziehung mit der [Publikation](bibtex/1d28c9f535d0f24eadb9d342168836199?lang=de "wikilink") stehen, die dem Inter-Hash
    1d28c9f535d0f24eadb9d342168836199 entsprechen. ${project.name}
    bietet die Möglichkeit, eine Publikation anhand ihres
    BibTex-Schlüssels abzurufen.  
    
-   **[ /bibtexkey/Wille82](bibtexkey/Wille82 "wikilink")**  
    Liefert Publikationen mit dem BibTex-Schlüssel *Wille82*.  
    
-   **[/bibtexkey/wille82+sys:user:Stumme](bibtexkey/wille82+sys:user:stumme "wikilink")**  
    Liefert Publikationen mit dem BibTex-Schlüssel *Wille82* aus der
    Sammlung des Benutzers *Stumme*.  
    
-   **[/bibtexkey/hjss06bibsonomy+sys%3Auser%3Ajaeschke](bibtexkey/hjss06bibsonomy+sys%3Auser%3Ajaeschke "wikilink")**  
    Zeigt alle Einträge mit dem vorgegebenen BibTex-Schlüssel
    `hjss06bibsonomy` des Benutzers *jaeschke*. Haben Sie mehr als einen
    Eintrag mit dem gleichen BibTeX-Schlüssel, so erhalten Sie eine
    Liste aller Treffer.  
    
-   **[/bibtexkey/journals/jacm/HopcroftU69/dblp](bibtexkey/journals/jacm/HopcroftU69/dblp "wikilink")**  
    Sie können die BibTex-Semantik benutzen, um auf Einträge zu
    verweisen, die wir von DBLP spiegeln, sobald Sie gelernt haben, wie
    DBLP seine BibTeX-Schlüssel erzeugt.  

---------------------------

### Inhaltsvereinbarungsseiten

Inhaltsvereinbarung (Content Negotiation) hilft dabei, eine Quelle mit
derselben URL auf verschiedene Arten anzuzeigen, unter Berücksichtigung
der Einstellungen des abfragenden Benutzers. Das Hinzufügen des
Schlüsselworts `uri` zu Ihrer URL ermöglicht Ihnen die
Inhaltsvereinbarung. Das Verfahren wird für die folgenden Seiten
unterstützt:  


-   URL- und BibTeX-Seiten
-   Autoren-Seiten
-   BibTeX-Schlüssel-Seiten  
    

**Beispiele:**  


-   **[/uri/url/d1bb7b3f6cafafa7b418f9f356ff2e83](uri/url/d1bb7b3f6cafafa7b418f9f356ff2e83 "wikilink")**  
    Zeigt alle Lesezeichen mit dem bestimmten Hash.  
    
-   **[/uri/bibtex/2b8b87c78e9e27a44aacde0402c642bff](uri/bibtex/2b8b87c78e9e27a44aacde0402c642bff "wikilink")**  
    Zeigt alle BibTeX-Einträge mit dem bestimmten Hash.  
    
-   **[/uri/bibtexkey/hjss06bibsonomy/jaeschke](uri/bibtexkey/hjss06bibsonomy/jaeschke "wikilink")**  
    Zeigt alle BibTeX-Einträge mit dem bestimmten Schlüssel des
    Benutzers *jaeschke*.  
    
-   **[/uri/author/hotho](uri/author/hotho "wikilink")**  
    Zeigt alle BibTeX-Einträge des Autors *hotho*.  
    

Die gegenwärtig unterstützten Ausgabeformate sind HTML, XML, RSS, RDF
und BibTeX. Der User Agent legt im HTML Accept Header ein bevorzugtes
Ausgabeformat fest, im dem die bevorzugte Reihenfolge mit Hilfe von
*q-values* definiert ist.  
Beispielsweise bedeutet die Header-Definition

`Accept:text/xml;q=1,text/html;q=0.9,text/plain;q=0.8,image/png;q=1,*/*;q=0.5`

die folgende Reihenfolge:

-   text/xml
-   image/png
-   text/html
-   text/plain
-   \*/*

-------------------------------

### Jabref-Layouts

Einen kompletten Überblick zu allen verfügbaren Jabref-Layouts erhalten
Sie auf der [ Export-Seite](export/ "wikilink") von ${project.name}.  

-   **[ /layout/simplehtml/](layout/simplehtml/ "wikilink")**  
    HTML-Übersicht ohne Kopf- oder Fußzeile nützlich für die Einbindung
    von Publikationslisten in andere HTML-Seiten.  
    
-   **[ /layout/html/](layout/html/ "wikilink")**  
    Eine einfache Übersicht, in der jeder Eintrag als Zeile in einer
    Tabelle dargestellt ist.  
    
-   **[ /layout/tablerefs/](layout/tablerefs/ "wikilink")**  
    HTML-Ausgabe mit jedem Eintrag als Zeile in einer Tabelle und einer
    zusätzlichen JavaScript-Suchfunktion.  
    
-   **[/layout/tablerefsabsbib/](layout/tablerefsabsbib/ "wikilink")**  
    Ähnelt TableRefs. Enthält auch die BibTeX-Quelle und die
    Kurzbeschreibung der Publikation.  
    
-   **[ /layout/docbook/](layout/docbook/ "wikilink")**  
    Dies ist eine XML-Ausgabe gemäß dem DocBook-Schema.  
    
-   **[ /layout/endnote/](layout/endnote/ "wikilink")**  
    Ausgabe in RIS, welches von dem Literaturverwaltungsprogramm EndNote
    verwendet wird.  
    
-   **[ /layout/dblp/](layout/dblp/ "wikilink")**  
    DBLP exportiert Ihre Datensätze in eine DBLP-konforme XML-Struktur.  
    
-   **[ /layout/text/](layout/text/ "wikilink")**  
    BibTeX-Ausgabe.  
    

------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink") 


