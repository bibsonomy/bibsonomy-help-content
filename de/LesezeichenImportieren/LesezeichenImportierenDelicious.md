<!-- en: ImportBookmarks/ImportBookmarksDelicious -->

-----------------

[`Zurück zur Übersicht des Importierens von Lesezeichen geht es hier.`](LesezeichenImportieren "wikilink")

---------------------

## Lesezeichen aus Delicious importieren
------------------------------------

Sie können Ihre Lesezeichen auf der Seite [Importieren](${project.home}import/bookmarks?lang=de "wikilink") hinzufügen. Dazu müssen Sie
Ihre [Delicious-Nutzerdaten](http://www.delicious.com/) eingeben. Wenn
Sie Ihre **Lesezeichen** importieren möchten, wählen Sie die
dazugehörige Option. Zusammen mit Ihren Lesezeichen werden die
dazugehörigen Tags und Sichtbarkeitsdefinitionen ebenfalls hinzugefügt.

Sie können ebenfalls Ihre **Tag Bundles** als *Relationen* innerhalb von
${project.name} importieren lassen, indem Sie beim Importieren die
andere Option wählen.  

![ <File:Bib1.png>](LesezeichenImportierenDelicious/Bib1.png  "fig: File:Bib1.png")  
