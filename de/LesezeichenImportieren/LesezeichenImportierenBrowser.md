<!-- en: ImportBookmarks/ImportBookmarksBrowser -->

-----------------

[`Zurück zur Übersicht des Importierens von Lesezeichen geht es hier.`](LesezeichenImportieren "wikilink")

---------------------

## Lesezeichen aus dem Browser importieren
-----------------------------------------------

### Chrome
Um Ihre Lesezeichen in Chrome als HTML-Datei zu
exportieren, klicken Sie im Menü ganz oben rechts auf *Lesezeichen* und
dann auf *Lesezeichenmanager*. Dort klicken Sie auf *Organisieren* und
schließlich auf *Lesezeichen in HTML-Datei exportieren...*.  

![ <File:Chrome1.png>](LesezeichenImportierenBrowser/Chrome1.png  "fig: File:Chrome1.png")  

![ <File:Chrome2.png>](LesezeichenImportierenBrowser/Chrome2.png  "fig: File:Chrome2.png")  

---------------------------------

### Firefox

Um Ihre Lesezeichen in Firefox als HTML-Datei zu exportieren, klicken
Sie auf das Lesezeichensymbol rechts neben der Suchleiste und dann auf
*Lesezeichen verwalten*. Danach klicken Sie auf *Importieren und
Sichern* und wählen *Lesezeichen nach HTML exportieren*.  

![ <File:Firefox1.png>](LesezeichenImportierenBrowser/Firefox1.png  "fig: File:Firefox1.png")  

![ <File:Firefox2.png>](LesezeichenImportierenBrowser/Firefox2.png  "fig: File:Firefox2.png")

---------------------------------

### HTML-Datei in ${project.name} importieren

Die gespeicherte HTML-Datei können Sie in ${project.name} importieren,
indem Sie auf der Seite [ Importieren
](Bib:/import/bookmarks?lang=de "wikilink") unter der Überschrift
"Importieren Sie Ihre Lesezeichen aus Ihrem Browser" die Datei auswählen
und hochladen. Außerdem können Sie definieren, für wen die Lesezeichen
sichtbar sein sollen. Wählen Sie **öffentlich**, wenn die Lesezeichen
für alle Nutzer sichtbar sein sollen, oder wählen Sie **privat**, wenn
die Lesezeichen nur für Sie selbst sichtbar sein sollen.  

![ <File:Bib2.png>](LesezeichenImportierenBrowser/Bib2.png  "fig: File:Bib2.png")  

