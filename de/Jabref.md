<!-- en: Jabref -->

## JabRef-Plugin - Datenaustausch
------------------------------

**Achtung:** Seit der JabRef-Version 2.12 werden **keine Plugins** mehr in JabRef unterstützt. Das ${project.name}-Plugin funktioniert in den neueren Versionen daher leider nicht mehr.

Um Daten zwischen dem Literaturverwaltungsprogramm JabRef und
${project.name} austauschen zu können, müssen Sie das ${project.name}-Plugin
installiert haben. Informationen zur Installation finden Sie [hier](JabrefInstallieren "wikilink").
Wenn Sie Fragen zu dem Plugin haben, besuchen Sie unsere [ Kontaktseite](Contact "wikilink").


------------------------------------------------------------------------

### Die ${project.name}-Sidebar

Um mit JabRef arbeiten zu können, öffnen Sie eine neue oder bereits
existierende Datenbank.  
Das ${project.name}-Plugin für JabRef enthält eine
${project.name}-Sidebar, die wichtige Funktionen anbietet. Um die
Sidebar zu öffnen, klicken Sie auf den *BibSonomy-Button* in der
JabRef-Toolbar.

![<File:jabrefSidebar.png>](jabref/jabrefSidebar.png  "fig: File:jabrefSidebar.png")  

In der Sidebar können Sie folgende Aktionen durchführen:

-   **Suche (Search)**  
    Hier können Sie nach Publikationen, die in ${project.name}
    gespeichert sind, suchen. Geben Sie einfach den Suchterm in das
    Textfeld ein. Sie können wählen, ob Sie nach *ganzem Text
    (Full text)* oder nach *Tags* suchen möchten. Drücken Sie dann den
    *Such-Button*.

<!-- -->

-   **Tags**  
    Das Plugin zeigt Ihnen eine Tagwolke (tagcloud) mit den
    meistgenutzten Tags von Ihnen, allen Nutzern oder einer bestimmten
    Gruppe, in der Sie Mitglied sind. Die Tagwolke kann durch Klicken
    auf den *Refresh-Button* aktualisiert werden. Wenn Sie auf einen Tag
    klicken, werden Ihnen die Einträge, die mit diesem Tag versehen
    sind, angezeigt.

<!-- -->

-   **Einträge importieren (Import posts)**  
    Hier können Sie wählen, ob Sie nur Ihre eigenen Einträge aus
    ${project.name} oder die Einträge aller Nutzer importieren möchten.
    Klicken Sie auf den *Refresh-Button*, um die Tagwolke
    zu aktualisieren.

<!-- -->

-   **Einstellungen (Settings)**  
    Klicken Sie hier, um Ihre Einstellungen für das
    ${project.name}-Plugin anzusehen und zu ändern.

------------------------------------------------------------------------

### Import von Daten aus ${project.name} nach JabRef

Sie können Ihre Einträge, die Sie in ${project.name} gesammelt haben,
nach JabRef importieren.

1.  Klicken Sie auf *BibSonomy* im Menü *Plugins*. Wählen Sie
    anschließend *Import all my posts*.  
      
    ![
    <File:importPosts.png>](jabref/importPosts.png  "fig: File:importPosts.png")  
      
2.  Wählen Sie in der Liste die Einträge aus, die Sie
    importieren möchten. Klicken Sie abschließend auf *OK*. Die Einträge
    werden nun in Ihre JabRef-Datenbank importiert.

------------------------------------------------------------------------

### Export von Daten aus JabRef nach ${project.name}

Sie können Ihre Einträge, die Sie in JabRef gesammelt haben, nach
${project.name} exportieren.

1.  Wählen Sie in JabRef die Einträge aus, die nach ${project.name}
    exportiert werden sollen. Sie können mehrere Einträge auswählen,
    indem Sie die STRG-Taste gedrückt halten und dann auf die Einträge
    klicken.  
      
2.  Klicken Sie auf *BibSonomy* im Menü *Plugins* und wählen Sie *Export
    selected entries*. Bereits existierende Einträge werden aktualisiert.  
      
    ![
    <File:exportPosts.png>](jabref/exportPosts.png  "fig: File:exportPosts.png")  
      

**Anmerkung**: Wenn Sie für einen Eintrag kein Schlagwort (Tag) vergeben
haben, erhalten Sie die Fehlermeldung "The following selected entries
have no keywords assigned". Wenn Sie mit **"yes"** bestätigen, wird
automatisch das Schlagwort "noKeywordAssigned" eingetragen. Wenn Sie
**"no"** anklicken, können Sie eigene Schlagworte vergeben.

Schlagworte werden bei JabRef üblicherweise durch Kommata getrennt. Beim
Export nach ${project.name} werden die Kommata durch Leerzeichen ersetzt.  
  

#### Das Trennsymbol in JabRef ändern

JabRef ermöglicht die Einstellung des Trennsymbols (Delimiter) für Tags,
die in das Keyword-Feld eingegeben werden. Gehen Sie zu *Options &gt;
Preferences &gt; Groups*, um diese Einstellung zu ändern.  
  
![ <File:delimiter.png>](jabref/delimiter.png  "fig: File:delimiter.png")  
  
Dort finden Sie ein Textfeld namens *When adding/removing keywords,
separate them by*, in welches Sie Ihr bevorzugtes Trennsymbol
(Delimiter) eingeben. Achten Sie darauf, dass in ${project.name} keine
Leerzeichen innerhalb eines Tags erlaubt sind. Befolgen Sie deshalb
folgende Schritte, wenn Sie einen Eintrag auf ${project.name}
hochladen:

1.  Unterteilen Sie den Inhalt des Tag-Feldes je nach Ihrem
    eingestellten JabRef-Trennsymbol.
2.  Löschen Sie alle übrigen Leerzeichen.

Wir empfehlen generell die Verwendung von Leerzeichen als
Tag-Trennsymbol.

------------------------------------------------------------------------

### Einträge löschen

In diesem Plugin ist es auch mögich, Einträge aus Ihrer eigenen
${project.name}-Sammlung zu löschen.

1.  Wählen Sie in JabRef die Einträge aus, die gelöscht werden sollen.
    Sie können mehrere Einträge auswählen, indem Sie die STRG-Taste
    gedrückt halten und dann auf die Einträge klicken.  
      
2.  Klicken Sie auf *BibSonomy* im Menü *Plugins* und wählen Sie *Delete
    selected entries*. Bitte beachten Sie, dass der Eintrag aus Ihrer
    ${project.name}-Sammlung gelöscht wird - eine lokale Kopie des
    Eintrages bleibt jedoch in JabRef erhalten!

------------------------------------------------------------------------

### Einträge synchronisieren

Seit der JabRef-Version 1.5 ist es möglich, Ihre lokalen BibTeX-Einträge
mit den Remote-Einträgen (den Einträgen auf ${project.name}) mit einem
Klick zu synchronisieren. Um eine Synchronisation durchzuführen, gehen
Sie zu *Plugins -&gt; Bibsonomy -&gt; Synchronize*.  
  
Wenn es einen Unterschied zwischen den lokalen und den Remote-Einträgen
gibt, erscheint ein Fenster, das sowohl den lokalen als auch den
Remote-Eintrag anzeigt. Felder, die sich unterscheiden, sind gelb markiert.

![<File:synchronize.png>](jabref/synchronize.png  "fig: File:synchronize.png")  

Wenn Sie *Keep local* oder *Always keep local* wählen, wird Ihr lokaler
Eintrag nach ${project.name} exportiert. Außerdem speichert *Always
keep local* Ihre Auswahl für die aktuelle Session.  
Wenn Sie *Keep remote* oder *Always keep remote* wählen, wird Ihr
lokaler BibTeX-Eintrag von dem Remote-Eintrag aus ${project.name}
überschrieben. Außerdem speichert *Always keep remote* Ihre Auswahl für
die aktuelle Session.

------------------------------------------------------------------------

### Private Dokumente

Mit der JabRef-Version 2.5 hat sich die Handhabung von Dokumenten
komplett geändert. Alle Dokumente werden automatisch hoch- und
heruntergeladen, sofern Sie die Option in den Einstellungen *Plugins
-&gt; BibSonomy -&gt; Settings* aktiviert haben.

Um es zu vermeiden, all Ihre Eintrage erneut zu importieren, können Sie
*Download my documents* im BibSonomy-Menü auswählen, falls Sie zuvor die
Download-Option in den Einstellungen deaktiviert haben.

Um neu hinzugefügte Dokumente hochzuladen, exportieren Sie einfach den
dazugehörigen Eintrag. Vergessen Sie aber nicht, die Option in den
Einstellungen zu aktivieren.

------------------------------------------------------------------------

### Proxy-Einstellungen

Auch wenn Sie JabRef mit einer Konfiguration hinter einem Proxy
benutzen, können Sie das BibSonomy-Plugin nutzen. Es benutzt
denselben Proxy-Mechanismus wie JabRef selbst; das heißt, wenn Sie
JabRef mit diesen Optionen starten:

`java -Dhttp.proxyHost="hostname" -Dhttp.proxyPort="portnumber"`

dann benutzt das ${project.name}-Plugin auch diese Parameter (ersetzen
Sie hostname mit dem hostname Ihres Proxys und portnumber mit der
portnumber Ihres Proxys).

------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink") 
  

