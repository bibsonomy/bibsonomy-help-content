<!-- en: SystemTags -->

## Systemtags
----------

Systemtags sind spezielle [Tags (Schlagworte)](Tags "wikilink"), die eine feste Bedeutung
haben. Derzeit bietet ${project.name} drei Typen von Systemtags an:

-------------------------

### Ausführbare Systemtags

Ausführbare Systemtags werden zu einem Eintrag hinzugefügt, um eine
spezielle Aktion mit diesem Eintrag auszuführen.

-   `for:<Gruppenname>`: kopiert den Post in die Sammlung der Gruppe.
    Dabei wird der Tag durch `from:IhrBenutzerName` ersetzt. Wenn Sie
    ihren Eintrag löschen oder bearbeiten, so bleibt der in die Gruppe
    kopierte Eintrag unverändert. Nur Mitglieder der Gruppe können
    Einträge für die Gruppe kopieren.
    
-   `send:<Benutzername>`: Damit senden Sie den Eintrag in den [
    Eingang](Eingang "wikilink") eines anderen Benutzers. Damit das
    funktioniert, muss der andere Benutzer Sie als [
    Freund](FreundeHinzufügen "wikilink") eingetragen haben oder Sie
    müssen Mitglied in der gleichen [Gruppe](Gruppenfunktionen "wikilink") sein.

------------------------

### Markup-Systemtags

Markup-Systemtags markieren Ihre Einträge. Derzeit werden folgende Tags
unterstützt:

-   `myown`: Ein Eintrag, der mit dem Tag `myown` versehen wurde,
    erscheint auf Ihrer CV-Seite.
    
-   `sys:relevantFor:<Gruppenname>`: Einträge mit dem Tag
    `sys:relevantFor:xygruppe` werden auf der "Interessant für"-Seite
    der Gruppe "xygruppe" angezeigt. Damit hat dieser Tag den gleichen
    Effekt wie die Auswahl von "xygruppe" in der "Interessant für"-Box
    beim Bearbeiten eines Eintrages.
-   `sys:hidden:<tag>`: Benutzen Sie diesen Tag mit `sys:hidden:xyz`, damit Ihr Post als "hidden" gilt. Damit ist er unsichtbar und kann nur von Ihnen eingesehen werden.

---------------------

### Such-Systemtags

Such-Systemtags sind nicht dazu da, an einen Eintrag geschrieben zu
werden, sondern um Einträge in Suchanfragen zu filtern. Sie haben alle
die gleiche Syntax: `sys:<Feldname><Feldwert>`. Beispielsweise gibt eine
Suche mit `sys:author:xyz` nur jene Einträge zurück, in denen `xyz` der
Autor ist. Folgende Filter werden unterstützt (die Suche beschränkt sich
hierbei um Publikationseinträge):

-   `sys:author:<Autorenname>` filtert die Suche nach dem Autor.  
-   `sys:bibtexkey:<bibtexkey>` filtert die Suche nach einem
    bestimmten BibTeX-Schlüssel.
    
-   `sys:entrytype:<Eintragstyp>` filtert die Suche nach dem [Eintragstypen](Eintragstypen "wikilink").
    
-   `sys:group:<group>` filtert die Suche nach einer bestimmten Gruppe.

-   `sys:user:<user>` sucht nach Einträgen eines Nutzers.

-   `sys:title:<title>` sucht nach Einträgen mit diesem Titel.

-   `sys:not:<tag>` filtert die Suche, indem alle Ergebnisse ignoriert
    werden, die diesen Tag enthalten. An dieser Stelle können Sie auch
    Platzhalter verwenden, z.B. werden bei `sys:not:news_*` alle
    Ergebnisse ignoriert, die Tags enthalten, die mit `news_` beginnen.
    
-   `sys:year:<Jahr>` filtert die Suche nach dem Erscheinungsjahr. Dabei
    sind mehrere Schreibweisen für das Jahr möglich:
    -   `2000`: alle Einträge aus dem Jahr 2000
    -   `2000-`: alle Einträge aus dem Jahr 2000 oder einem Jahr danach
    -   `-2000`: alle Einträge aus dem Jahr 2000 oder einem Jahr davor
    -   `1990-2000`: alle Einträge aus den Jahren 1990 bis 2000

------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink") 


