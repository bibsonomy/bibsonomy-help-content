 
[ Übersicht über Hilfethemen](Übersicht "wikilink")

### Anmelden

${project.theme == "bibsonomy"}{
  -   [ Mit OpenID-Account anmelden](MitOpenIDAnmelden "wikilink")
  -   [ Mit Benutzeraccount anmelden](MitNutzeraccountAnmelden "wikilink")
  -   [ Benutzeraccount erstellen](NeuenNutzeraccountAnlegen "wikilink")
  -   [ Passwort vergessen?](PasswortVergessen "wikilink")
}

${project.theme == "puma"}{
-   [ Bibliotheksausweis für PUMA freischalten](BibliotheksausweisfürPUMAfreischalten "wikilink")
}

### Basisfunktionen

${project.theme == "bibsonomy"}{

-   [ Die Benutzeroberfläche](Programmoberfläche "wikilink")
-   [ Lesezeichen / Publikationen](LesezeichenPublikationenManagen "wikilink")
-   [ Schlagworte nutzen](Tags "wikilink")
-   [ Einzelnes Lesezeichen hinzufügen](LesezeichenPublikationenManagen/LesezeichenManagen "wikilink")
-   [ Mehrere Lesezeichen importieren](LesezeichenImportieren "wikilink")
-   [ Lesezeichen exportieren](LesezeichenExportieren "wikilink")
-   [ Lesezeichen/Publikationen bearbeiten](EigeneEinträgeBearbeiten "wikilink")
-   [ Einzelne Publikation hinzufügen](EintragPublikationen "wikilink")
-   [ Mehrere Publikationen importieren](EintragPublikationen/EintragMehrerePublikationen "wikilink")
-   [ Publikationen exportieren](LiteraturlisteExportieren "wikilink")
-   [ Browser-Addon installieren](${project.home}buttons)
-   [ Discovery Service](DiscoveryService "wikilink")

}

${project.theme == "puma"}{

-   [ Die Benutzeroberfläche](Programmoberfläche "wikilink")
-   [ Lesezeichen / Publikationen](LesezeichenPublikationenManagen "wikilink")
-   [ Schlagworte nutzen](Tags "wikilink")
-   [ Einzelnes Lesezeichen hinzufügen](LesezeichenPublikationenManagen/LesezeichenManagen "wikilink")
-   [ Mehrere Lesezeichen importieren](LesezeichenImportieren "wikilink")
-   [ Lesezeichen exportieren](LesezeichenExportieren "wikilink")
-   [ Lesezeichen/Publikationen bearbeiten](EigeneEinträgeBearbeiten "wikilink")
-   [ Einzelne Publikation hinzufügen](EintragPublikationen "wikilink")
-   [ Mehrere Publikationen importieren](EintragPublikationen/EintragMehrerePublikationen "wikilink")
-   [ Publikationen exportieren](LiteraturlisteExportieren "wikilink")
-   [ Browser-Addon installieren](http://puma.uni-kassel.de/buttons)
-   [ Discovery Service](DiscoveryService "wikilink")

}

### Erweiterte Funktionen

-   [ Benutzerprofil/Lebenslauf](LebenslaufÄndern "wikilink")
-   [ Personeneinträge](Personeneinträge "wikilink")
-   [ RSS-Feeds abonnieren](RSS-FeedsAbonnieren "wikilink")
-   [ Publikationen durchstöbern](PublikationenDurchstöbern "wikilink")
-   [ Die Zwischenablage](DieAblage "wikilink")
-   [ Private Dateien anhängen](DokumentAnhängen "wikilink")
-   [ URL-Syntax](URL-Syntax "wikilink")
-   (BETA) [ Addon für Google Docs](AddonGoogleDocs "wikilink")

### Soziale Funktionen

-   [ Freunde hinzufügen](FreundeHinzufügen "wikilink")
-   [ Gruppenfunktionen](Gruppenfunktionen "wikilink")
-   [ Nutzern folgen](Follower "wikilink")
-   [ Kommentar, Rezension,
    Bewertung](KommentarRezensionBewertung "wikilink")
-   [ Community Posts](CommunityPosts "wikilink")
-   [ OpenURL-Resolver](OpenURL-Resolver "wikilink")

${project.theme == "puma"}{

### Funktionen für Entwickler

-   [Entwicklerdokumentation](https://bibsonomy.bitbucket.io/)
-   [ JavaScript-Codesnippet](JS-Codeschnipsel "wikilink")
-   [ Integration](Integration "wikilink")

}

${project.theme == "bibsonomy"}{

### Funktionen für Entwickler

-   [Entwicklerdokumentation](https://bibsonomy.bitbucket.io/)
-   [REST-API](https://bitbucket.org/bibsonomy/bibsonomy/wiki/documentation/api/REST%20API)
-   [ Integration in andere Systeme](Integration "wikilink")
    -   [ In TYPO3 einbinden](Typo3 "wikilink")
    -   [ JavaScript-Codesnippet](JS-Codeschnipsel "wikilink")
-   [Anonymisierte
    Datensätze](https://www.kde.cs.uni-kassel.de/bibsonomy/dumps/)

}

### Presseinfos

-   [ Projekte/Kooperation - Eine Übersicht](Projekte "wikilink")
-   [ Presse zu BibSonomy/PUMA](Presse "wikilink")  
      

### Weitere Infos

-   [Entwickler-Blog BibSonomy/PUMA](http://bibsonomy.blogspot.com/)
