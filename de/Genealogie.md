<!-- en: Genealogie -->

## Die ${project.name} Genealogie
-------------------------------

Die ${project.name} [Genealogie](${project.home}persons "wikilink") ist ein
dissertationsbasierter Stammbaum der Forschung an deutschen
Universitäten.  
Ausgangspunkt ist der Dissertationskatalog der [Deutschen
Nationalbibliothek](http://www.dnb.de/DE/Home/home_node.html). Nutzerbasiert werden Beziehungen zwischen den an der
Dissertation beteiligten Personen ergänzt, sodass ein
fächerübergreifender Stammbaum der Forschung in Deutschland erstellt
werden kann.
Forscherinnen und Forscher aller Fächer und Disziplinen können dabei
mithelfen, indem sie die an ihren wissenschaftlichen Arbeiten
beteiligten Personen (Betreuer\_in, Doktorvater/Doktormutter,
Gutachter\_in) eintragen.  
Um die Genealogie zu nutzen, genügt es, ein Nutzerkonto bei
${project.name} zu haben (wie man ein Konto erstellt, erfahren Sie [
hier](NeuenNutzeraccountAnlegen "wikilink")). Die Daten werden nur zu
Forschungszwecken im Bereich der Hochschul- und Wissenschaftsforschung
verwendet.  

Sie können nach den wissenschaftlichen Veröffentlichungen eines Autors
suchen:  

![ <File:1.png>](genealogie/1.png  "fig: File:1.png")  

Dann können Sie Informationen über seine wissenschaftlichen Arbeiten
sehen und ggf. ergänzen:  

![ <File:2.png>](genealogie/2.png  "fig: File:2.png")  

![ <File:3.png>](genealogie/3.png  "fig: File:3.png")  


Sie können die angezeigten Publikationen nach Jahr, Titel oder Autor sortieren, indem Sie auf das Pfeilsymbol, das
Sie in der oberen rechten Ecke finden, klicken.

Um die Genealogie zu nutzen und weitere Informationen zu erhalten,
folgen Sie [ diesem Link](${project.home}persons "wikilink").

------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink") 


