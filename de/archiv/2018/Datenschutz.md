<!-- en: archive/2018/Privacy -->

<div class="alert alert-warning" style="margin-top:10px">
	HINWEIS: Dies ist die alte Version der Nutzungsbedingungen und Datenschutzerklärung, gültig 
	bis zum 23.05.18. Die aktuelle Version finden Sie hier:
	<a href="/help_de/Datenschutz">Datenschutz</a>
</div>


Nutzungsbedingungen
-------------------


1.  Die Benutzung von ${project.name} impliziert das Einverständnis zu
    diesen Nutzungsbedingungen und der Datenschutzerklärung.

2.  Die Nutzungsbedingungen und Datenschutzerklärung können fristlos
    geändert werden. Es liegt in der Verantwortlichkeit des Benutzers
    sich regelmäßig über Änderungen der Nutzungsbedingungen und der
    Datenschutzerklärung zu informieren.

3.  ${project.name} ist ein Sozialer-Lesezeichen-Service, der
    gebührenfrei benutzt werden kann.
    
4.  ${project.name} garantiert keine Datenintegrität, Verfügbarkeit des
    Service, Tauglichkeit für einen bestimmten Zweck oder Qualität
    des Inhalts.

5.  Der Benutzer gibt ${project.name} das Recht, den Inhalt, den er in
    das System einstellt, zu veröffentlichen.

6.  ${project.name} trägt keine Verantwortung für den vom Benutzer
    eingestellten Inhalt.

7.  Der Benutzer trägt allein die Verantwortung für den von ihm
    eingestellten Inhalt.

8.  Der Benutzer darf ${project.name} nicht derart benutzen, dass
    hierdurch der Service beschädigt oder der Datenschutz anderer
    Benutzer bedroht werden könnte. Dies bezieht sich insbesondere, aber
    nicht ausschließlich auf:

    1.  die Veröffentlichung von Inhalten mit dem Ziel Webseiten im
        Ranking von Suchmaschinen zu fördern (Spam)

    2.  Inhalte einzustellen, die kommerziellen Transaktionen
        dienen (Werbung)

    3.  Das System zu hacken oder zu versuchen, die Kennwörter der
        anderen Benutzer zu erhalten

    4.  Denial-of-Service-Angriffe gegen ${project.name} auszuführen

    5.  Eine automatische Erfassung der in ${project.name}
        eingestellten Inhalte durchzuführen

    6.  Konteninformationen an Dritte zu übergeben.



9.  Der Benutzer kann von dem System zeitweise oder dauerhaft
    ausgeschlossen werden, wenn ein Verstoß gegen die
    Nutzungsbedingungen oder die Datenschutzerklärung vorliegt.

10. Der vom Benutzer eingestellte Inhalt kann gelöscht oder von den
    öffentlichen Seiten von ${project.name} entfernt werden, wenn ein
    Verstoß gegen die Nutzungsbedingungen und
    Datenschutzerklärung vorliegt.
    

Datenschutzerklärung
--------------------

${project.name} wird vom Fachgebiet Wissensverarbeitung der Universität
Kassel als Forschungsprojekt betrieben. Hinsichtlich der Erhebung und
Verwendung personenbezogener Daten der Nutzer unterliegt
${project.name} den Vorschriften des [Hessischen Datenschutzgesetz](http://www.datenschutz.hessen.de/hdsg99.htm) und des [Telemediengesetz](http://bundesrecht.juris.de/tmg/BJNR017910007.html#BJNR017910007BJNG000400000),
von denen für ${project.name} insbesondere § 33 HDSG bedeutsam ist.


Um seine Forschungstätigkeit durchführen zu können, erhebt
${project.name} mehr Daten von seinen Nutzern und analysiert diese
weitergehend, als es bei kommerziellen Web-Angeboten üblich oder
jedenfalls zulässig wäre. Diese Daten werden dazu verwendet, u.a.
Empfehlungs-, Spammentdeckungs, oder Ranking- Funktionen zu entwickeln
oder zu verbessern und auch gerade die datenschutzgerechte Gestaltung
von Social Bookmarking-Systemen voranzubringen, die in einigen Bereichen
sogar auf andere Web 2.0-Angebote übertragbar sein wird. Da die
wissenschaftliche Arbeit ein offener Prozess ist, können die möglichen
Datenverwendungen hier nicht endgültig und abschließend aufgezählt
werden. Es besteht jedoch auf Seiten ${project.name}s bei der Analyse
von Nutzerdaten kein Interesse an der hinter dem Nutzer stehenden realen
Person und Daten der Nutzer werden auf keinen Fall für Werbezwecke
verwendet. ${project.name} gibt keine Nutzerdaten an Dritte weiter. Die
einzige Ausnahme stellen andere Forschungsgruppen dar, die einen Teil
der Daten als Datensatz von ${project.name} beziehen können, um ihre
eigenen Forschungsarbeiten damit durchzuführen und die
Forschungsergebnisse des ${project.name}-Projektes nachvollziehen und
überprüfen zu können. Diese Datensätze werden von ${project.name}
jedoch zuvor pseudonymisiert.

### Nicht registrierte oder nicht angemeldete Nutzer

Im Rahmen der Nutzung erhebt und verwendet ${project.name} zur
Durchführung des Angebotes und zu statistischen Zwecken folgende Daten:
den Namen der aufgerufenen Seite, den Zeitpunkt des Abrufs, die
übertragene Datenmenge, die Meldung, ob der Abruf erfolgreich war, die
Referrer-URL sowie ein Cookie mit Sitzungs-Identifikationsnummer.
Daneben verwendet ${project.name} die IP-Adresse des Rechners, von dem
die Anfrage abgeschickt wurde, zu Forschungszwecken, etwa der
Entwicklung von Algorithmen zur Spam-Abwehr. Zur Analyse des
Nutzerverhaltens, die zur Entwicklung von neuen Algorithmen notwendig
ist und zu deren Evaluation herangezogen wird, besitzt ${project.name}
eine Klick-Log-Funktion, die jeden Klick eines Nutzers auf einen Link in
${project.name} speichert. ${project.name} verfügt jedoch nicht über
die Mittel und hat auch kein Interesse, diese Daten der hinter dem
Nutzer stehenden Person zuzuordnen.

### Registrierte Nutzer

#### Bestandsdaten

Im Rahmen der Registrierung erhebt ${project.name} zur Begründung des
Nutzungsverhältnisses folgende Daten des Nutzers, die gegebenenfalls
Rückschlüsse auf dessen Identität erlauben und damit personenbezogene
Daten des Nutzers darstellen können: selbst gewählten Nutzernamen,
Passwort und E-Mail-Adresse. Nutzername und Passwort dienen der
Einrichtung eines geschützten Nutzerkontos. Der Nutzername ist zudem
Bestandteil der vom Nutzer als öffentlich gekennzeichneten Lesezeichen-
und Literatureinträge. Die E-Mail-Adresse wird im Rahmen von
Forschungsarbeiten zur Spam-Entdeckung analysiert. Daneben dient sie der
Zusendung eines Bestätigungs-Links zur Aktivierung des Nutzerkontos, um
den Missbrauch fremder Daten zu verhindern, sowie der Zusendung von
Login-Daten im Falle des Verlusts durch den Nutzer. Eine Weitergabe der
E-Mail-Adresse oder anderer Registrierungsdaten an Dritte erfolgt nicht.

#### Nutzungsdaten

Während der Nutzung erhebt und verwendet ${project.name} folgende Daten
zur Durchführung des Angebotes, die gegebenenfalls Rückschlüsse auf die
Identität des Nutzers erlauben und damit personenbezogene Daten des
Nutzers darstellen können: Merkmale zur Identifikation, wie Nutzername
und Passwort, sowie Angaben zu Beginn und Ende der Nutzung, daneben den
Namen der abgerufenen Datei, Zeitpunkt des Abrufs oder Eintrags,
übertragene Datenmenge, die Meldung, ob der Abruf erfolgreich war, die
Referrer-URL sowie ein Cookie mit Sitzungs-Identifikationsnummer und
Nutzerkennung. Zu Forschungszwecken wird auch die IP-Adresse des
anfragenden Systems gespeichert. Im Rahmen der Forschungstätigkeit
werden alle diese Daten zur Forschung im Bereich der
datenschutzgerechten Gestaltung, der Verbesserung des Systems sowie der
Entwicklung neuer Algorithmen, u.a. zur Spam-Entdeckung, zum Ranking
oder für neue Empfehlungssystemen eingesetzt. ${project.name} stellt
die erhobenen Daten, jedoch nicht Nutzernamen, Passwort oder IP-Adresse,
anderen Forschungseinrichtungen in pseudonymisierter Form in Form eines
Datensatzes zur Verfügung, die diese nur im Rahmen ihrer
Forschungstätigkeit verwenden dürfen. Dabei lässt sich auch durch
Pseudonymisierungsverfahren nicht immer ausschließen, dass der Bezug zu
einem Nutzerkonto im Einzelfall durch einen Abgleich der
pseudonymisierten Daten mit den veröffentlichten Einträgen aufgedeckt
werden kann. Handelt der Nutzer etwa unter seinem Realnamen oder ergeben
sich aus seinen öffentlichen Einträgen Hinweise zu seiner Identität,
kann ${project.name} in einem solchen Fall den möglichen Personenbezug
nicht ausschließen.

#### Einträge

Die vom Nutzer im Rahmen der Systemnutzung eingegebenen Einträge, d. h.
Lesezeichen sowie Angaben und Links zu eingestellter Literatur, werden
entsprechend den angebotenen Funktionalitäten dazu verwendet, sie dem
einstellenden Nutzer und im Falle öffentlicher Einträge auch anderen
Nutzern zur Einsicht und zur Übernahme in die eigene Sammlung bereit zu
halten. Öffentliche Einträge werden für Forschungszwecke, z.B. zur
Erstellung von Rankings und Bildung von Tag-Clouds, herangezogen und
werden im Rahmen der internen Suchfunktion in Zusammenhang mit dem zum
Eintrag gehörenden Nutzernamen verarbeitet und dem suchenden Nutzer
angezeigt. Daneben besteht für registrierte Nutzer die Möglichkeit,
öffentliche Einträge über die angebotene [Programmier-Schnittstelle](https://bitbucket.org/bibsonomy/bibsonomy/wiki/documentation/api/REST%20API) (API) einzusehen und auf den eigenen Rechner herunter zu laden. Die
Einträge werden auch zu Forschungszwecken, etwa auf den Gebieten der
Spam-Entdeckung und der Recommender-Funktionen analysiert und sind Teil
des Datensatzes, die ${project.name} in pseudonymisierter Form an
andere Forschungseinrichtungen weitergibt. Beendet der Nutzer seine
Mitgliedschaft durch Löschen seines Nutzerkontos, werden seine Einträge
gesperrt. Ab diesem Zeitpunkt können weder andere Nutzer noch der Nutzer
selbst diese Einträge sehen. Die Einträge bleiben bei ${project.name}
gespeichert, werden jedoch ausschließlich zu Forschungszwecken
verwendet.

#### Werbung

${project.name} ist werbefrei. Es werden keine Nutzerdaten für die
Zuordnung von personalisierter Werbung verwendet und sie werden auch
nicht zu Werbezwecken an Dritte weitergegeben.

#### Geltung

Diese Datenschutzerklärung gilt nur für die Erhebung und Verwendung von
Nutzerdaten durch ${project.name}, nicht jedoch für den etwaigen
Datenumgang durch die jeweiligen Anbieter der in Einträgen oder
anderweitig verlinkten externen Web-Sites.

#### Auskunftsrecht

Nutzer haben das Recht, von ${project.name} kostenlos Auskunft über die
zu ihrer Person oder ihrem Nutzernamen gespeicherten Daten zu verlangen.
[webmaster@bibsonomy.org](mailto:webmaster@bibsonomy.org)



------------------------------------------------------------------------

