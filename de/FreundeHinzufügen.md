<!-- en: AddFriends -->

## Freunde
-------------

In ${project.name} können Sie andere Nutzer zu Ihrer Freundesliste
hinzufügen. Das macht es Ihnen leichter, mit ihnen zu interagieren. Auf
dieser Seite lernen Sie, wie Sie Freunde zu Ihrer Kontaktliste
hinzufügen und wie Sie Ihren Freunden Lesezeichen oder Publikationen
senden können.

  
<!-- TODO: implement toc \_\_TOC\_\_-->

------------------------------------------------------------------------

### Freunde hinzufügen

${project.name} bietet zwei unterschiedliche Wege an, um Freunde und
Kollegen der eigenen Kontaktliste hinzuzufügen.  
![ <File:1.png>](freundehinzufügen/1.png  "fig: File:1.png")

- **A**: Klicken Sie auf den Nutzernamen (diese beginnen mit einem
"@"). Dieser wird z. B. bei allen Einträgen angezeigt, die der Nutzer
erstellt hat (weiter mit **C** - siehe unten).

-   **B**: Sie kennen den Nutzernamen. In diesem Fall können Sie direkt
    zum Nutzerprofil springen, indem Sie die Suche nutzen.

	1.  Klicken Sie auf **"Suche"** und wählen Sie **"Benutzer"** aus.
    2.  Geben Sie den Benutzernamen in das Suchfeld ein.
    3.  Drücken Sie Enter oder klicken Sie auf die Lupe (weiter mit
        **C** - siehe unten).  

-   **C:** In der rechten Spalte wird Ihnen der Benutzer angezeigt.
    Klicken Sie auf **"als Freund hinzufügen"**.

------------------------------------------------------------------------

### Freunden Lesezeichen/Publikationen senden

Sie können Lesezeichen und Publikationen sehr einfach an Ihre Freunde
senden, indem Sie den [ Systemtag](SystemTags "wikilink") `send:<username>` (z.B. `send:hotho`) zu dem Lesezeichen/der Publikation hinzufügen.  
  
Am einfachsten geht das, indem Sie zu **Ihrer Nutzerseite** gehen
(klicken Sie auf Ihren Nutzernamen im rechten Hauptmenü). Klicken Sie
dann bei dem Lesezeichen/der Publikation, das/die gesendet werden soll,
auf das **kleine blaue Stiftsymbol**, um die Tags zu editieren.  

![ <File:editTags.png>](freundehinzufügen/editTags.png  "fig: File:editTags.png")  
  
Jetzt können Sie den Tag `send:<username>` (ersetzen Sie &lt;username&gt;
durch den Nutzernamen Ihres Freundes) eingeben und abschließend auf
**Speichern** klicken.  

![ <File:editTags3.png>](freundehinzufügen/editTags3.png  "fig: File:editTags3.png")  

Das Lesezeichen/die Publikation wird nun an Ihren Freund gesendet.
Außerdem erscheint ein neues Symbol nebem dem blauen Stift, ein
**kleiner blauer Stern**. Wenn Sie auf dieses Symbol klicken, sehen Sie
die **versteckten Systemtags** (z.B. `sent:<username>`).  
![ <File:sent.png>](freundehinzufügen/sent.png  "fig: File:sent.png")  
  
Lesezeichen/Publikationen, die Ihnen von Freunden zugesendet worden
sind, können Sie im [ Eingang](Eingang "wikilink") finden. Um Ihren
Eingang aufzurufen, klicken Sie auf das **Personensymbol** im rechten
Hauptmenü und dann auf **Eingang**. Hier finden Sie die Ihnen
zugesendeten Lesezeichen/Publikationen, versehen mit `from:<username>`.  
  
![ <File:inbox.png>](freundehinzufügen/inbox.png  "fig: File:inbox.png")  
  
------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink") 

