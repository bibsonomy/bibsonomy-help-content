<!-- en: PersonEntities -->


## Personeneinträge
----------------

${project.theme == "bibsonomy"}{

### Was sind "Personeneinträge"?

Personeneinträge sind Repräsentationen von **Autoren von Publikationen**
im System von ${project.name}. Personeneinträge repräsentieren **nicht
die Nutzer** von ${project.name}, sondern die Personen, die mit
Publikationen assoziiert sind.  
Beispielsweise können Studierende  Nutzer von ${project.name} sein.
Diese haben sich für einen persönlichen Benutzeraccount registriert und damit Zugang zur eigenen [Nutzerseite](UserSeite "wikilink").
Falls sie noch nichts publiziert haben, werden sie allerdings von ${project.name} nicht als **Autor** erkannt.
In diesem Fall werden sie nicht in speziellen **Personeneinträgen** repräsentiert.

------------------------------------------------------------------------

### Überblick
  
Um eine Autorenseite aufzurufen, klicken Sie auf den Namen des Autors,
der zusammen mit der Publikation angezeigt wird. Um nach einem Autor zu
suchen, benutzen Sie die [ Suchleiste](SuchenHilfe "wikilink") oben
rechts. Wählen Sie das Feld "Autor" aus und geben Sie dann den Namen des
Autors ein. Dann können Sie auf den Namen des Autors klicken, der
zusammen mit den Publikationen angezeigt wird.    

![ <File:searchName.png>](personeneinträge/searchName.png  "fig: File:searchName.png") 
![ <File:searchBar.png>](personeneinträge/searchBar.png  "fig: File:searchBar.png")

------------------------------------------------------------------------

### Neue Personen hinzufügen

Nachdem Sie auf den Namen des Autors geklickt haben, kommen Sie zu einer
Seite, auf der Sie den Eintrag (Publikation) mit einem Personeneintrag
(Autor) verknüpfen können.  
Falls der Autor noch nicht durch einen Personeneintrag im System
repräsentiert wird, können Sie eine **neue Person mit dem Namen des
Autors hinzufügen**. Klicken Sie dazu auf den entsprechenden Button am
Ende der Seite.  
    
![ <File:NoPersonFound.png>](personeneinträge/NoPersonFound.png  "fig: File:NoPersonFound.png")
  
Nachdem Sie auf den Button geklickt haben, erscheint ein PopUp-Fenster,
in dem Sie gefragt werden, ob Sie einen **neuen Personeneintrag
erstellen** möchten oder ob Sie den Namen des Autors mit einem **bereits
bestehenden Personeneintrag verknüpfen** möchten (einer Person mit
anderem Namen).  
*(Dies kann der Fall sein, wenn der Autor zuvor ausschließlich unter
einem anderen Namen veröffentlicht hat (einem Pseudonym oder einem
früheren Namen). In diesem Fall klicken Sie auf "Person mit anderem
Namen" und suchen Sie dann die Person, mit der Sie den Autor verknüpfen
möchten.)*.  

![ <File:AddOrSelectPerson.png>](personeneinträge/AddOrSelectPerson.png  "fig: File:AddOrSelectPerson.png")

------------------------------------------------------------------------

### Autoren mit Publikationen verknüpfen

Nachdem Sie auf den Namen des Autors geklickt haben, kommen Sie zu einer
Seite, auf der Sie den Eintrag (Publikation) mit einem Personeneintrag
(Autor) verknüpfen können.  
Falls der Name des Autors bereits durch einen Personeneintrag im System
repräsentiert wird, sehen Sie eine **Liste von Autoren** mit diesem Namen.  

![ <File:PersonFound.png>](personeneinträge/PersonFound.png  "fig: File:PersonFound.png")
  
In dieser Liste können Sie den Personeneintrag auswählen, der zu dem
Autor passt. Sie werden dann in einem PopUp-Fenster gefragt, ob Sie
diese **Verknüpfung speichern** möchten ("Ja, speichern") oder ob Sie
**nur die Seite des Autors anzeigen** möchten, ohne zu speichern ("Nein,
nur die Person anzeigen").    

![ <File:CommunityChoice.png>](personeneinträge/CommunityChoice.png  "fig: File:CommunityChoice.png")
  
Wenn keine Person in der Liste zum Autor passt, klicken Sie auf "andere
Person", um einen **neuen Personeneintrag** für den Namen des Autors zu
erstellen.

------------------------------------------------------------------------

### Die Autorenseite

Nachdem Sie einen neuen Personeneintrag erstellt oder den Namen des
Autors mit einem bestehenden Personeneintrag verknüpft haben, wird die
Autorenseite angezeigt.  
Sie können diese Seite auch über die URL aufrufen:  
`/person/<erster Buchstabe des Vornamens>.<Nachname>`,
z.B. [/person/f.jannidis](${project.home}person/f.jannidis "wikilink").   

![ <File:overview.png>](personeneinträge/overview.png  "fig: File:overview.png")
  
Auf dieser Seite bekommen Sie einen Überblick über den akademischen Grad
des Autors, den alternativen Namen, Abschlussarbeiten und andere
Publikationen, die mit dem Autor assoziiert sind.  
Sie können hier ebenfalls fehlende Informationen hinzufügen. Wenn dieser
Personeneintrag Sie selbst sind, können Sie die Person beanspruchen, indem Sie
auf das Feld "Das bin ich", das Sie ganz rechts finden, klicken.

}

${project.theme == "puma"}{

### Was sind "Personeneinträge"?

Personeneinträge sind Repräsentationen von **Autoren von Publikationen**
im System von ${project.name}. Personeneinträge repräsentieren **nicht
die Nutzer** von ${project.name}, sondern die Personen, die mit
Publikationen assoziiert sind.  
Beispielsweise können Studierende Nutzer von ${project.name} sein.
Diese haben sich für einen persönlichen Benutzeraccount registriert und damit Zugang zur eigenen [Nutzerseite](UserSeite "wikilink").
Falls sie noch nichts publiziert haben, werden sie allerdings von ${project.name} nicht als **Autor** erkannt.
In diesem Fall werden sie nicht in speziellen **Personeneinträgen** repräsentiert.

------------------------------------------------------------------------

### Überblick
  
Um eine Autorenseite aufzurufen, klicken Sie auf den Namen des Autors,
der zusammen mit der Publikation angezeigt wird. Um nach einem Autor zu
suchen, benutzen Sie die [ Suchleiste](SuchenHilfe "wikilink") oben
rechts. Wählen Sie das Feld "Autor" aus und geben Sie dann den Namen des
Autors ein. Dann können Sie auf den Namen des Autors klicken, der
zusammen mit den Publikationen angezeigt wird.    

![ <File:searchName.png>](personeneinträge/searchName.png  "fig: File:searchName.png") 
![ <File:searchBar.png>](personeneinträge/searchBar.png  "fig: File:searchBar.png")

------------------------------------------------------------------------

### Neue Personen hinzufügen

Nachdem Sie auf den Namen des Autors geklickt haben, kommen Sie zu einer
Seite, auf der Sie den Eintrag (Publikation) mit einem Personeneintrag
(Autor) verknüpfen können.  
Falls der Autor noch nicht durch einen Personeneintrag im System
repräsentiert wird, können Sie eine **neue Person mit dem Namen des
Autors hinzufügen**. Klicken Sie dazu auf den entsprechenden Button am
Ende der Seite.  
    
![ <File:NoPersonFound.png>](personeneinträge/NoPersonFound.png  "fig: File:NoPersonFound.png")
  
Nachdem Sie auf den Button geklickt haben, erscheint ein PopUp-Fenster,
in dem Sie gefragt werden, ob Sie einen **neuen Personeneintrag
erstellen** möchten oder ob Sie den Namen des Autors mit einem **bereits
bestehenden Personeneintrag verknüpfen** möchten (einer Person mit
anderem Namen).  
*(Dies kann der Fall sein, wenn der Autor zuvor ausschließlich unter
einem anderen Namen veröffentlicht hat (einem Pseudonym oder einem
früheren Namen). In diesem Fall klicken Sie auf "Person mit anderem
Namen" und suchen Sie dann die Person, mit der Sie den Autor verknüpfen
möchten.)*.  

![ <File:AddOrSelectPerson.png>](personeneinträge/AddOrSelectPerson.png  "fig: File:AddOrSelectPerson.png")

------------------------------------------------------------------------

### Autoren mit Publikationen verknüpfen

Nachdem Sie auf den Namen des Autors geklickt haben, kommen Sie zu einer
Seite, auf der Sie den Eintrag (Publikation) mit einem Personeneintrag
(Autor) verknüpfen können.  
Falls der Name des Autors bereits durch einen Personeneintrag im System
repräsentiert wird, sehen Sie eine **Liste von Autoren** mit diesem Namen.  

![ <File:PersonFound.png>](personeneinträge/PersonFound.png  "fig: File:PersonFound.png")
  
In dieser Liste können Sie den Personeneintrag auswählen, der zu dem
Autor passt. Sie werden dann in einem PopUp-Fenster gefragt, ob Sie
diese **Verknüpfung speichern** möchten ("Ja, speichern") oder ob Sie
**nur die Seite des Autors anzeigen** möchten, ohne zu speichern ("Nein,
nur die Person anzeigen").    

![ <File:CommunityChoice.png>](personeneinträge/CommunityChoice.png  "fig: File:CommunityChoice.png")
  
Wenn keine Person in der Liste zum Autor passt, klicken Sie auf "andere
Person", um einen **neuen Personeneintrag** für den Namen des Autors zu
erstellen.

------------------------------------------------------------------------

### Die Autorenseite

Nachdem Sie einen neuen Personeneintrag erstellt oder den Namen des
Autors mit einem bestehenden Personeneintrag verknüpft haben, wird die
Autorenseite angezeigt.  
Sie können diese Seite auch über die URL aufrufen:  
`/person/<erster Buchstabe des Vornamens>.<Nachname>`,
z.B. [/person/f.jannidis](${project.home}person/f.jannidis "wikilink").   

![ <File:overview.png>](personeneinträge/overview.png  "fig: File:overview.png")
  
Auf dieser Seite bekommen Sie einen Überblick über den akademischen Grad
des Autors, den alternativen Namen, Abschlussarbeiten und andere
Publikationen, die mit dem Autor assoziiert sind.  
Sie können hier ebenfalls fehlende Informationen hinzufügen. Wenn dieser
Personeneintrag Sie selbst sind, können Sie die Person beanspruchen, indem Sie
auf das Feld "Das bin ich", das Sie ganz rechts finden, klicken.

}

${project.theme == "wueresearch"}{

### Was sind "Personeneinträge"?

Personeneinträge sind Repräsentationen von **Autoren von Publikationen**
im System von ${project.name}. Personeneinträge repräsentieren **nicht
die Nutzer** von ${project.name}, sondern die Personen, die mit
Publikationen assoziiert sind.  
Beispielsweise können Studierende Nutzer von ${project.name} sein.
Diese haben sich für einen persönlichen Benutzeraccount registriert und damit Zugang zur eigenen [Nutzerseite](UserSeite "wikilink").
Falls sie noch nichts publiziert haben, werden sie allerdings von ${project.name} nicht als **Autor** erkannt.
In diesem Fall werden sie nicht in speziellen **Personeneinträgen** repräsentiert.

------------------------------------------------------------------------

### Überblick
  
Um einen Personeneintrag aufzurufen, klicken Sie auf den Namen des Autors,
der zusammen mit der Publikation angezeigt wird. Um nach einem bestimmten Autor zu
suchen, wählen Sie in der obigen Leiste das Feld "**Personen**" aus. Hier 
können Sie im Suchfeld einen konkreten Namen suchen oder Ihn per Klick auf die
Buchstaben aus einer alphabetisch geordneten Liste auswählen. Auch können Sie per Schaltfläche (unten rechts) 
nicht nur die Personen der Universität, sondern alle Personeneinträge anzeigen lassen.

![ <File:searchwue.png>](personeneinträge/searchwue.png  "fig: File:searchwue.png")

Alternativ benutzen Sie die [Suchleiste](SuchenHilfe "wikilink") oben
rechts. Wählen Sie dort das Feld "Autor" aus und geben dann den Namen des
Autors ein. Dann können Sie auf den Namen des Autors klicken, der
zusammen mit den Publikationen angezeigt wird.  

![ <File:searchBar.png>](personeneinträge/searchBar.png  "fig: File:searchBar.png")
![ <File:searchName.png>](personeneinträge/searchName.png  "fig: File:searchName.png") 


------------------------------------------------------------------------

### Neue Personen hinzufügen

Wenn Sie auf den Namen eines Autors ohne bestehende Personenseite geklickt haben, kommen Sie zu einer
Seite, auf der Sie den Eintrag (Publikation) mit einem Personeneintrag
(Autor) verknüpfen können.
Falls der Autor noch nicht durch einen Personeneintrag im System
repräsentiert wird, können Sie eine **neue Person mit dem Namen des
Autors hinzufügen**. Klicken Sie dazu auf den entsprechenden Button  "**Andere Person**"
am Ende der Auswahlliste.
    
![ <File:PersonAuswahlwue.png>](personeneinträge/PersonAuswahlwue.png  "fig: File:PersonAuswahlwue.png")
  
Nachdem Sie auf den Button geklickt haben, erscheint ein PopUp-Fenster,
in dem Sie gefragt werden, ob Sie einen **neuen Personeneintrag
erstellen** möchten oder ob Sie den Namen des Autors mit einem **bereits
bestehenden Personeneintrag verknüpfen** möchten (einer Person mit
anderem Namen). Wählen Sie hier erstere Möglichkeit aus, um einen neuen Personeneintrag zu erstellen.
*(Dies kann der Fall sein, wenn der Autor zuvor ausschließlich unter
einem anderen Namen veröffentlicht hat (einem Pseudonym oder einem
früheren Namen). In diesem Fall klicken Sie auf "Person mit anderem
Namen" und suchen Sie dann die Person, mit der Sie den Autor verknüpfen
möchten.)*.  

![ <File:AddOrSelectPerson.png>](personeneinträge/AddOrSelectPerson.png  "fig: File:AddOrSelectPerson.png")

------------------------------------------------------------------------

### Autoren mit Publikationen verknüpfen

Nachdem Sie auf den Namen des Autors geklickt haben, kommen Sie zu einer
Seite, auf der Sie den Eintrag (Publikation) mit einem Personeneintrag
(Autor) verknüpfen können.
Falls der Name des Autors bereits durch einen Personeneintrag im System
repräsentiert wird, sehen Sie eine **Liste von Autoren** mit diesem Namen. 
Benötigen Sie zur Zuordnung weitere Informationen, können Sie auf das Augensymbol 
rechts vom Eintrag klicken, um zur zugehörigen Personenseite zu gelangen.

![ <File:PersonAuswahlwue.png>](personeneinträge/PersonAuswahlwue.png  "fig: File:PersonAuswahlwue.png")
  
In dieser Liste können Sie den Personeneintrag auswählen, der zu dem
Autor passt. Sie werden dann in einem PopUp-Fenster gefragt, ob Sie
diese **Verknüpfung speichern** möchten ("Ja, speichern") oder ob Sie
**nur die Seite des Autors anzeigen** möchten, ohne zu speichern ("Nein,
nur die Person anzeigen").    

![ <File:CommunityChoice.png>](personeneinträge/CommunityChoice.png  "fig: File:CommunityChoice.png")
  
Wenn keine Person in der Liste zum Autor passt, klicken Sie auf "**Andere Person**", 
um einen **neuen Personeneintrag** für den Namen des Autors zu erstellen (siehe oben).

------------------------------------------------------------------------

### Die Personenseite

Nachdem Sie einen neuen Personeneintrag erstellt oder den Namen des
Autors mit einem bestehenden Personeneintrag verknüpft haben, wird die
Personenseite angezeigt.  
Sie können diese Seite auch über die URL aufrufen:  
`/person/<erster Buchstabe des Vornamens>.<Nachname>`,
z.B. [/person/a.hotho](${project.home}person/a.hotho "wikilink").   

![ <File:overviewwue.png>](personeneinträge/overviewwue.png  "fig: File:overviewwue.png")
  
Auf dieser Seite gelangen Sie an folgende Informationen zur Person:
* Name der Einrichtung mit evtl. Link zur Organisation
* E-Mail-Adresse
* Homepage
* evtl. ORCID als eindeutige Identifizierungsnummer

Zusätzlich werden alle Veröffentlichungen angegeben, die mit der Person assoziiert sind. Diese können Sie nach Publikationstypen filtern.
Außerdem können Sie hier fehlende Informationen zur Personenseite hinzufügen.

}

------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink") 
  
