<!-- de: ExportBookmarks -->

## Lesezeichen exportieren
----------------------------------

Mithilfe von ${project.name} können Sie Ihre Lesezeichenlisten in verschiedenen Formaten exportieren und sie anschließend mit anderen Programmen verwenden. Sie können statt jedes Lesezeichen einzeln auch mehrere Lesezeichen auf einmal exportieren. 

Um Ihre Lesezeichen zu exportieren, gehen Sie auf Ihre **persönliche Nutzerseite**, wo die Lesezeichen, die Sie bereits zu Ihrer Sammlung hinzugefügt haben, angezeigt werden. 
(Klicken Sie [hier](LesezeichenPublikationenManagen "wikilink") für eine Anleitung zum Hinzufügen von Lesezeichen. )

Klicken Sie nun auf das **Pfeilsymbol** ganz rechts, das sich neben der Spalte mit den Lesezeichen
befindet.
Sie können nun ein Exportformat wählen, in dem Sie Ihre Lesezeichen exportieren wollen.

${project.theme == "bibsonomy"}{

![ <File:1.png>](lesezeichenexportieren/1.png  "fig: File:1.png")

}

${project.theme == "puma"}{

![ <File:1.png>](lesezeichenexportieren/1.png  "fig: File:1.png")

}

${project.theme == "wueresearch"}{

![ <File:exportbmswue.png>](wueresearch/LesezeichenExportieren/exportbmswue.png  "fig: File:exportbmswue.png")

}

Nachdem Sie ein Exportformat ausgewählt haben, werden Ihre Lesezeichen in diesem angezeigt. Sie können nun die Daten in das von Ihnen gewünschte Programm importieren.

Um mehr darüber zu erfahren, wie Sie Ihre gespeicherten Lesezeichenlisten in häufig benutzte Programmen exportieren, klicken Sie [hier](LiteraturlisteExportierenSpezifisch "wikilink").


---------------------------------------

### RSS

In ${project.name} können Sie eigene/fremde Lesezeichen- und Publikationslisten als RSS-Feed abonnieren.
Sobald etwas Neues geschieht (Änderungen/Neueinträge), werden Sie informiert.
[Hier](RSS-FeedsAbonnieren "wikilink") erfahren Sie mehr über die Verwendung von RSS-Feeds.

-----------------------------

### BibTeX

Das international einheitliche Datenformat, welches von allen gängigen Programmen zur Literaturverwaltung unterstützt wird, heißt BibTeX. 
Nachdem Sie dieses Format ausgewählt haben, drücken Sie die rechte Maustaste und wählen Sie **"Seite speichern unter..."**, um Ihre Lesezeichenliste zu speichern.

-----------------------------

### XML

Nachdem Sie das Format *"XML"* gewählt haben, drücken Sie die rechte Maustaste und wählen Sie **"Seite speichern unter..."**, um Ihre 
Lesezeichenliste im XML-Format zu speichern.

-----------------------------

### Bookpubl

Im *bookpubl*-Format werden die Titel der Lesezeichen Ihrer Liste zusammen mit den dazugehörigen Links zu den Webseiten, auf denen das 
Lesezeichen gesetzt wurde, angezeigt.
Um sich ihre Lesezeichenliste im *bookpubl*-Format anzeigen zu lassen, fügen Sie den Text *"bookpubl/"* direkt nach *"bibsonomy.org/"* an die URL an, wie in diesem Beispiel: 

`https://www.bibsonomy.org/bookpubl/?lang=de`

`https://www.bibsonomy.org/bookpubl/user/ihrnutzername?lang=de`

Um die Liste zu speichern, drücken Sie die rechte Maustaste und wählen Sie **"Seite speichern unter..."**. 

------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink")

