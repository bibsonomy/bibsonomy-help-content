<!-- en: JS-Codesnippet -->

## Javascript-Codeschnipsel
------------------------

Erleichtern Sie Ihren Webseitenbesuchern das Vermerken und Arbeiten und
vergrößern Sie dadurch die eigene Reichweite. ${project.name} macht es
möglich, mit ein paar Zeilen JavaScript. Fügen Sie den folgenden Code in
Ihre Webseite ein und schon gelangen Besucher mit einem Klick zu
${project.name} und können dort ganz einfach Lesezeichen und Kommentare
hinterlegen.  
  
**Java Script Code** 

	<!-- post bookmark to link code -->
      <script type="text/JavaScript">
      <!--
      var url=encodeURIComponent(document.location.href);
      var title=encodeURIComponent(document.title);
      document.write("<a href=http://www.bibsonomy.org/ShowBookmarkEntry?c=b&jump=yes&url="+url+ "&description="+title +"\" title=\"Bookmark this page to BibSonomy.\">Bookmark to BibSonomy!</a>");
      //-->
      </script>
      <!-- end post bookmark to link code -->

------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink") 
