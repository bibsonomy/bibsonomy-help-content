<!-- en: Downloads -->

## Downloads
---------

Auf dieser Seite finden Sie die Download-Links von Features,
Erweiterungen und Plugins für ${project.name}.


<!-- \_\_TOC\_\_ -->



------------------------------------------------------------------------



### Browser



-   Browser-Addons: [Download](http://www.bibsonomy.org/buttons?lang=de)



------------------------------------------------------------------------



### Programme



-   JabRef-Plugin: [ Tutorial](JabrefInstallieren "wikilink"), [Download](https://gforge.cs.uni-kassel.de/frs/download.php/76/jabref-bibsonomy-plugin-2.5.2-bin.jar)

-   JabRef Layout-Dateien: [Tutorial](Integration#section-Integration-JabRef "wikilink"), [Download](http://dev.bibsonomy.org/maven2/org/bibsonomy/bibsonomy-layout/)

-   TeXlipse-Erweiterung: [Tutorial](TeXlipseBibSonomyExtension "wikilink"), [Download](https://bitbucket.org/bibsonomy/bibsonomy-texlipse/downloads/org.bibsonomy.texlipseextension_0.1.1.1.jar)



------------------------------------------------------------------------



### Webseiten



-   Confluence-Plugin: [Tutorial](http://www.christianschenk.org/projects/confluence-bibsonomy-plugin/), [Download](http://data.christianschenk.org/confluence-bibsonomy-plugin/confluence-bibsonomy-plugin-1.0.jar)



-   Addon für Google Docs: [ Tutorial](AddonGoogleDocs "wikilink"), [Download](https://docs.google.com/document/d/1O9h5zqcd8UAOnJszDnilr4-_0uBfCk2Pw-qdDtphtPE/edit?pref=2&pli=1)



-   Google Scholar-Plugin: [Tutorial](http://blog.bibsonomy.org/2015/12/feature-of-week-bibsonomy-scholar.html), [Download](https://chrome.google.com/webstore/detail/bibsonomy-scholar/nfncjdnkilenkgnhchaapiiboaimpoon)



-   Moodle-Plugin: [ Tutorial](Moodle "wikilink"), [Download](https://moodle.org/plugins/pluginversions.php?plugin=mod_pbm)



------------------------------------------------------------------------



### Eigene Webseite



-   Tagclouds (Tagwolken): [Tutorial](Flash-JavaScript-Tag-Cloud "wikilink"), [swfobject und tagcloud.swf](http://www.schoenwandt.info/download/wpcumulus.zip), [tagCloud.js](TagCloud.js "wikilink"), [tagCloud.css](TagCloud.css "wikilink")



-   TYPO3-Erweiterung: [ Tutorial](Typo3 "wikilink"), [Download für TYPO3 v1.0-v5.9](https://typo3.org/extensions/repository/view/ext_bibsonomy),

    [Download für TYPO3 v6.0 oder höher](https://typo3.org/extensions/repository/view/ext_bibsonomy_csl)



-   Wordpress-Plugin: [ Tutorial](Wordpress "wikilink"), [Download](https://wordpress.org/plugins/bibsonomy-csl/)



------------------------------------------------------------------------



### Anwendungen



-   Bookmark Exporter: [Tutorial](http://www.christianschenk.org/projects/bibsonomyexporter/), [Download](http://data.christianschenk.org/bibsonomyexporter/bibsonomyexporter-1.0.jar)



-   Link Status Checker: [Tutorial](http://www.christianschenk.org/projects/linkstatuschecker/), [Download](http://data.christianschenk.org/linkstatuschecker/LinkStatusChecker-1.1.zip)



------------------------------------------------------------------------



### REST-API



-   REST-API: [Dokumentation](https://bitbucket.org/bibsonomy/bibsonomy/wiki/documentation/api/REST%20API), [Repository](http://dev.bibsonomy.org/maven2/org/bibsonomy/bibsonomy-rest-client/)



-   Java: [Repository](https://bitbucket.org/bibsonomy/bibsonomy/src/tip/bibsonomy-rest-client/?at=default)



-   PHP: [Repository](https://bitbucket.org/bibsonomy/restclient-php)



-   Python: [Download](https://gforge.cs.uni-kassel.de/frs/download.php/46/bibsonomy-client-python.zip)



------------------------------------------------------------------------



### Sonstige



-   Bibsonomy Poster: [Info and Download](https://play.google.com/store/apps/details?id=net.gromgull.android.bibsonomyposter)



-   OAuth: [ Tutorial](OAuth "wikilink"), [Running example](https://bitbucket.org/bibsonomy/bibsonomy-help-content/src/b8d20eb1bde387f1a8772d91943452f64d5edda2/code-samples/oauth-rest-demo/src/main/java/org/bibsonomy/OAuthRestApiDemo.java?at=default&fileviewer=file-view-default)



-   Maven: [Repository](http://dev.bibsonomy.org/maven2/)



-   Bitbucket-Repository: [Download](https://bitbucket.org/bibsonomy/bibsonomy/downloads)


------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink")
  

  



