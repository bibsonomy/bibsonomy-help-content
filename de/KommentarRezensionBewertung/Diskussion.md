<!-- en: DiscussionReviewsComments/Discussion -->

-----------------

[`Zurück zur Übersicht vom Diskutieren, Bewerten und Kommentieren geht es hier.`](KommentarRezensionBewertung "wikilink")

---------------------

## Diskussion in ${project.name}
------------------------------------------------------------------------

Jeder angemeldete Benutzer kann über **jede beliebige Ressource**
(Webseite oder Publikation) diskutieren, auch wenn er diese nicht selbst
eingetragen hat. Zu jeder Ressource können mehrere Einträge im System
sein (z. B. von verschiedenen Benutzern). Die Diskussionen gehören
jedoch immer zur Ressource, nicht zu einem der Einträge. Daher führen
die Diskutieren-Links von Einträgen zur gleichen Ressource immer zur
selben Diskussionsseite.  

**Einträge mit eingeschränkter Sichtbarkeit**: Auch private Einträge
oder solche mit eingeschränkter Sichtbarkeit (auf Gruppen oder Freunde)
können diskutiert werden. Jedoch wird in solchen Fällen eine öffentlich
sichtbare Kopie dieses Eintrags angelegt, sobald der erste Kommentar/die
erste Rezension erstellt wird. Die Kopie gehört weder dem Besitzer des
Originaleintrages, noch demjenigen, der die Diskussion beginnt. Sie
gehört einfach dem System. Bevor eine Kopie eines
sichtbarkeitsbeschränkten Eintrags erzeugt wird, wird eine Warnung
angezeigt, bevor der Benutzer diskutiert. Private Notizen werden nicht
mitkopiert. 

**Wichtig:** Es wird unterschieden zwischen Rezensionen
(mit Bewertungen) und Kommentaren (ohne Bewertungen). Jeder Nutzer kann
genau **eine Rezension** zu einem Artikel verfassen, aber **beliebig
viele Kommentare**. Rezensionen und Kommentare können jederzeit vom
Nutzer bearbeitet und gelöscht werden.