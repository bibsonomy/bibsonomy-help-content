<!-- en: DiscussionReviewsComments/Rate -->

-----------------

[`Zurück zur Übersicht vom Diskutieren, Bewerten und Kommentieren geht es hier.`](KommentarRezensionBewertung "wikilink")

---------------------

## Publikation/Lesezeichen bewerten
------------------------------------------------------------------------

**Ziel:** Sie lernen, wie Sie einen bestehenden Eintrag
(Lesezeichen/Publikation) bewerten.  

${project.theme == "puma"}{

**Voraussetzung:** Sie müssen sich bereits bei PUMA angemeldet haben ([Bibliotheksausweis für PUMA
freischalten](BibliotheksausweisFür_PUMAFreischalten "wikilink")).

} 

${project.theme == "bibsonomy"}{

**Voraussetzung:** Sie müssen über einen BibSonomy-Benutzeraccount
verfügen ([ neuen Nutzeraccount anlegen](NeuenNutzeraccountAnlegen "wikilink")) und aktuell mit diesem
angemeldet sein ([ bei BibSonomy mit Nutzeraccount anmelden](MitNutzeraccountAnmelden "wikilink")).

}

1.  Klicken Sie auf die Sternleiste (diese befindet sich sowohl bei
    Lesezeichen als auch bei Publikationen unterhalb des Eintrags).  
    
    ![ <File:post1.png>](Bewerten/post1.png  "fig: File:post1.png")  
2.  Auf der Detailansicht sehen Sie sowohl die Details des Lesezeichens/
    der Publikation als auch weiter unten die **Kommentare und Rezensionen**.  
    
    Im Folgenden eine kurze Erklärung der Bereiche A, B und C:

    -   **A (Bewertungsverteilung):** Die zwei Diagramme fassen alle
        bisherigen Bewertungen zusammen und stellen sie dar. Im linken
        Diagramm ist die Bewertungsverteilung angezeigt.  
        Im rechten Diagramm ist die Benutzerbewertung nochmals in der
        Form von Sternen zu sehen (0 Sterne = sehr schlecht, 5 Sterne = sehr gut).
        
    -   **B (Kommentar erstellen):** Hier können Sie Ihren eigenen
        Kommentar/Ihre eigene Bewertung zu diesem Artikel erstellen.
        
    -   **C (bisherige Kommentare):** In diesem Bereich werden alle für
        Sie sichtbaren Kommentare und/oder Bewertungen angezeigt.
        Außerdem können Sie zu jeden Eintrag sehen, wer diesen
        Kommentar/diese Bewertung abgegeben hat und wann. Unter jedem
        Eintrag finden Sie die Möglichkeit, auf diesen zu antworten
        **(schwarzer Pfeil)**. Damit können Sie auf bestehende
        Kommentare/Bewertungen reagieren.  
        
        ![<File:overview_neu1.png>](Bewerten/overview_neu1.png  "fig: File:overview_neu1.png")  

3.  Um eine Rezension zu schreiben, füllen Sie den Abschnitt **B (Kommentar erstellen)** aus. Siehe Bild unten.

    1.  **Rezension oder Kommentar:** Wählen Sie aus, ob Sie eine
        Rezension oder einen Kommentar verfassen möchten. Sie können mit
        der Abgabe einer Rezension einen Eintrag bewerten (auf einer
        Skala von 0 bis 5). Je mehr Sterne Sie vergeben, desto besser
        bewerten Sie den Eintrag.
    2.  **Schreiben Sie Ihr Review:** Hier geben Sie den Text Ihrer
        Rezension ein.
    3.  **Anonym:** Wenn Sie dieses Kästchen anklicken, ist Ihre
        Rezension öffentlich sichtbar, jedoch wird Ihr Benutzername
        nicht veröffentlicht.
    4.  **Sichtbarkeit:** Legen Sie fest, für wen Ihre Rezension
        sichtbar sein soll.
        - *öffentlich*: Jeder Nutzer kann die Rezension sehen.
    	- *privat*: Nur Sie können die Rezension sehen. Diese Einstellung ist praktisch für eigene Notizen.
         	    - *Freunde*: Die Rezension wird nur für Ihre Freunde
               sichtbar sein.
                	- *Gruppen*: Wählen Sie die Gruppen aus, für die die Rezension sichtbar gemacht werden soll.  

4.  Klicken Sie abschließend auf **Bewerten**, um Ihre Rezension zu
    speichern und zu veröffentlichen.

     ![ <File:review2.png>](Bewerten/review2.png  "fig: File:review2.png")  

**Hinweis:** Die Durchschnittsbewertung eines Eintrags
wird über **alle abgegebenen Bewertungen** berechnet, auch über die
privaten. Das heißt, wenn Sie eine private Rezension schreiben, ist zwar
Ihr Text nur für Sie sichtbar, Ihre Bewertung jedoch wird zur
Durchschnittsbewertung mit eingerechnet. 

