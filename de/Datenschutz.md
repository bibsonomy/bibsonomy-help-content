<!-- en: Privacy -->

${project.theme == "puma"}{

	Siehe: <a href="http://www.ub.uni-stuttgart.de/impressum/index.html">Datenschutz</a>

}

${project.theme == "bibsonomy"}{

<div class="alert alert-warning" style="margin-top:10px">HINWEIS: Dies ist die aktuelle Version der Nutzungsbedingungen und Datenschutzerklärung, gültig seit dem 24.05.18. Die alte Version finden Sie hier:
<a href="archiv/2018/Datenschutz">Datenschutz</a></div>


Nutzungsbedingungen
-------------------


1.  Die Benutzung von ${project.name} impliziert das Einverständnis zu
    diesen Nutzungsbedingungen und der Datenschutzerklärung.

2.  ${project.name} ist ein Sozialer-Lesezeichen-Service, der
    gebührenfrei benutzt werden kann.
    
3.  ${project.name} garantiert keine Datenintegrität, Verfügbarkeit des
    Service, Tauglichkeit für einen bestimmten Zweck oder Qualität
    des Inhalts.

4.  Der Benutzer gibt ${project.name} das Recht, den Inhalt, den er in
    das System einstellt, zu veröffentlichen.

5.  ${project.name} trägt keine Verantwortung für den vom Benutzer
    eingestellten Inhalt.

6.  Der Benutzer trägt allein die Verantwortung für den von ihm
    eingestellten Inhalt.

7.  Der Benutzer darf ${project.name} nicht derart benutzen, dass
    hierdurch der Service beschädigt oder der Datenschutz anderer
    Benutzer bedroht werden könnte. Dies bezieht sich insbesondere, aber
    nicht ausschließlich auf:

    1.  die Veröffentlichung von Inhalten mit dem Ziel Webseiten im
        Ranking von Suchmaschinen zu fördern (Spam)

    2.  Inhalte einzustellen, die kommerziellen Transaktionen
        dienen (Werbung)

    3.  Das System zu hacken oder zu versuchen, die Kennwörter der
        anderen Benutzer zu erhalten

    4.  Denial-of-Service-Angriffe gegen ${project.name} auszuführen

    5.  Eine automatische Erfassung der in ${project.name}
        eingestellten Inhalte durchzuführen

    6.  Konteninformationen an Dritte zu übergeben.

8.  Der Benutzer kann von dem System zeitweise oder dauerhaft
    ausgeschlossen werden, wenn ein Verstoß gegen die
    Nutzungsbedingungen oder die Datenschutzerklärung vorliegt.

9. Der vom Benutzer eingestellte Inhalt kann gelöscht oder von den
    öffentlichen Seiten von ${project.name} entfernt werden, wenn ein
    Verstoß gegen die Nutzungsbedingungen und
    Datenschutzerklärung vorliegt.
    

Datenschutzerklärung und Information
--------------------

${project.name} ist ein gemeinsam betriebener Bookmarking-Dienst der Data Mining und Information Retrieval Gruppe der Universität Würzburg, vom Fachgebiet Wissensverarbeitung der Universität
Kassel und vom Institut für Bibliotheks- und Informationswissenschaft 
Sachgebiet Information Processing and Analytics der Humboldt-Universität zu Berlin.

Die wesentlichen Inhalte der Vereinbarung können Sie [hier](datenschutz/Vereinbarung) abrufen.  

---------------------

Ihre Anlaufstelle für Fragen des Datenschutzes ist die  
**Julius-Maximilians-Universität Würzburg**.  
Fragen richten Sie bitte an:  
${project.email}  

-----------------------

Die einzelnen Verantwortlichen sind:  

**Julius-Maximilians-Universität Würzburg (Körperschaft des öffentlichen Rechts)**  
Am Hubland  
D-97074  
Würzburg  
Tel: +49 931 / 31 – 86731  
Fax: +49 931 / 31 - 86732  
${project.email}  
Vertretungsberechtigter für die Data Mining und Information Retrieval Gruppe am LS VI  
und diesem Dienst ist Prof. Dr. Andreas Hotho.  

**Kontaktdaten des bestellten behördlichen Datenschutzbeauftragten:**  
Behördlicher Datenschutzbeauftragter der Julius-Maximilians-Universität Würzburg  
Sanderring 2  
Tel. 0931/31-88131 oder 0931/31-81446  
Fax 0931/31-86880  
[datenschutz@uni-wuerzburg.de](mailto:datenschutz@uni-wuerzburg.de)  

**Universität Kassel (Körperschaft des öffentlichen Rechts)**  
Wilhelmshöher Allee 73  
34121 Kassel  
Tel.: +49 561 804 - 6250  
Fax: +49 561 804 - 6259  
${project.email}  
Vertretungsberechtigter für das Fachgebiet Wissensverarbeitung ist Prof. Dr. Gerd Stumme.  


**Kontaktdaten der behördlichen Datenschutzbeauftragten der Universität Kassel:**  
Behördliche Datenschutzbeauftragte der Universität Kassel  
Mönchebergstraße 19  
34109 Kassel  
Tel.: +49 561 804-2756  
[datenschutz@uni-kassel.de](mailto:datenschutz@uni-kassel.de)  


**Humboldt-Universität zu Berlin (Körperschaft des öffentlichen Rechts)**  
Unter den Linden 6  
10099 Berlin  
Telefon: +49 30 2093–0  
Fax: +49 30 2093–2770  
${project.email}  
Vertretungsberechtigter für das Institut für Bibliotheks- und Informationswissenschaft  
Sachgebiet Information Processing and Analytics ist Prof. Dr. Robert Jäschke.  

**Kontaktdaten der behördlichen Datenschutzbeauftragten der Humboldt-Universität Berlin:**  
Behördliche Datenschutzbeauftragte der Humboldt-Universität zu Berlin  
Unter den Linden 6  
10099 Berlin  
Tel: +49 (30) 2093-2591  
[datenschutz@uv.hu-berlin.de](mailto:datenschutz@uv.hu-berlin.de)  

----------------------------------------

###Zwecke der Verarbeitung

${project.name} wird von der Data Mining und Information Retrieval Gruppe der Universität Würzburg, vom Fachgebiet Wissensverarbeitung der Universität
Kassel und vom Institut für Bibliotheks- und Informationswissenschaft 
Sachgebiet Information Processing and Analytics der Humboldt-Universität Berlin als Forschungsprojekt betrieben. 

Um seine Forschungstätigkeit durchführen zu können, erhebt
${project.name} mehr Daten von seinen Nutzern und analysiert diese
weitergehend, als es bei kommerziellen Web-Angeboten üblich oder
jedenfalls zulässig wäre. Diese Daten werden dazu verwendet, u.a.
Empfehlungs-, Spammentdeckungs, oder Ranking- Funktionen zu entwickeln
oder zu verbessern und auch gerade die datenschutzgerechte Gestaltung
von Social Bookmarking-Systemen voranzubringen, die in einigen Bereichen
sogar auf andere Web 2.0-Angebote übertragbar sein wird. Da die
wissenschaftliche Arbeit ein offener Prozess ist, können die möglichen
Datenverwendungen hier nicht endgültig und abschließend aufgezählt
werden. Es besteht jedoch auf Seiten ${project.name}s bei der Analyse
von Nutzerdaten kein Interesse an der hinter dem Nutzer stehenden realen
Person und Daten der Nutzer werden auf keinen Fall für Werbezwecke
verwendet. ${project.name} gibt keine Nutzerdaten an Dritte weiter. Die
einzige Ausnahme stellen andere Forschungsgruppen dar, die einen Teil
der Daten als Datensatz von ${project.name} beziehen können, um ihre
eigenen Forschungsarbeiten damit durchzuführen und die
Forschungsergebnisse des ${project.name}-Projektes nachvollziehen und
überprüfen zu können. Diese Datensätze werden von ${project.name}
jedoch zuvor pseudonymisiert.

__Rechtsgrundlagen der Verarbeitung__

Einwilligung, Art. 6 Abs. 1 a) DSGVO 
Aufgabenerfüllung, Art. 6 Abs. 1 e) DSGVO i.V.m. Art. 2 Abs. 4 S. 1 BayHSchG; Art. 7 BayHSchG (+ andere Hochschulgesetze); Art. 11 Abs. 1 BayEGovG; § 13 Abs. 7 TMG

###Cookies

${project.name} verwendet sogenannte [Cookies](Cookies "wikilink"). Diese dienen dazu, unser Angebot nutzerfreundlicher, effektiver und sicherer zu machen. Cookies sind kleine Textdateien, die auf Ihrem Rechner abgelegt werden und die Ihr Browser speichert. Die meisten der von uns verwendeten Cookies 
sind sogenannte Session-Cookies. Sie werden nach Ende Ihres Besuchs automatisch gelöscht,
es sei denn Sie haben bei der Anmeldung die Option "Eingeloggt bleiben" gewählt. 
In diesem Fall bleibt der Cookie für ein Jahr bestehen, sofern dieser nicht manuell gelöscht wird.
Die durch einen Cookie erzeugten Informationen über Ihre Benutzung werden nur auf Servern von
BibSonomy gespeichert. Die so erfassten Daten werden nur zum Zweck der eigenen internen 
Statistik genutzt und im Anschluss an die Auswertung gelöscht.

Liste der von ${project.name} verwendeten Cookies:

- `JSESSIONID`: wird von ${project.name}s Applikationsstack verwendet und speichert eine Session ID, die Ihnen für die Dauer des Besuches der Seite zugeordnet wird. Es dient dazu, verschiedene Anfragen während der Session zu speichern
- mit `_pk_id` beginnende Cookies: Cookie des Statistiktools [Matomo](https://matomo.org) zur Erfassung von generellen Zugriffsstatistiken

Bei eingeloggten Benutzern werden zusätzlich die folgenden Cookies verwendet:

- `_user`: enthält den Benutzernamen des eingeloggten Benutzers
- `db_user`: enthält gehashte Informationen, um den Benutzer nach einiger Zeit wieder automatisch anmelden zu können
- `openID_user`: enthält gehashte Informationen, um den Benutzer nach einiger Zeit wieder automatisch mit OpenID anmelden zu können
- `_lPost`: enthält zufällige, nicht personenbezogene Zeichen, die zur Spamerkennung und -abwehr dienen

### Nicht registrierte oder nicht angemeldete Nutzer

Im Rahmen der Nutzung erhebt und verarbeitet ${project.name} zur
Durchführung des Angebotes und zu statistischen Zwecken folgende Daten:
den Namen der aufgerufenen Seite, den Zeitpunkt des Abrufs, die
übertragene Datenmenge, die Meldung, ob der Abruf erfolgreich war, die
Referrer-URL sowie ein Cookie mit Sitzungs-Identifikationsnummer.
Daneben verarbeitet ${project.name} die IP-Adresse des Rechners, von dem
die Anfrage abgeschickt wurde, zu Forschungszwecken, etwa der
Entwicklung von Algorithmen zur Spam-Abwehr. Zur Analyse des
Nutzerverhaltens, die zur Entwicklung von neuen Algorithmen notwendig
ist und zu deren Evaluation herangezogen wird, besitzt ${project.name}
eine Klick-Log-Funktion, die jeden Klick eines Nutzers auf einen Link in
${project.name} speichert. ${project.name} verfügt jedoch nicht über
die Mittel und hat auch kein Interesse, diese Daten der hinter dem
Nutzer stehenden Person zuzuordnen.

### Registrierte Nutzer

#### Bestandsdaten

Im Rahmen der Registrierung erhebt ${project.name} zur Begründung des
Nutzungsverhältnisses folgende Daten des Nutzers, die gegebenenfalls
Rückschlüsse auf dessen Identität erlauben und damit personenbezogene
Daten des Nutzers darstellen können: selbst gewählten Nutzernamen,
Passwort und E-Mail-Adresse. Nutzername und Passwort dienen der
Einrichtung eines geschützten Nutzerkontos. Der Nutzername ist zudem
Bestandteil der vom Nutzer als öffentlich gekennzeichneten Lesezeichen-
und Literatureinträge. Die E-Mail-Adresse wird im Rahmen von
Forschungsarbeiten zur Spam-Entdeckung analysiert. Daneben dient sie der
Zusendung eines Bestätigungs-Links zur Aktivierung des Nutzerkontos, um
den Missbrauch fremder Daten zu verhindern, sowie der Zusendung von
Login-Daten im Falle des Verlusts durch den Nutzer. Eine Weitergabe der
E-Mail-Adresse oder anderer Registrierungsdaten an Dritte erfolgt nicht.

#### Nutzungsdaten

Während der Nutzung erhebt und verarbeitet ${project.name} folgende Daten
zur Durchführung des Angebotes, die gegebenenfalls Rückschlüsse auf die
Identität des Nutzers erlauben und damit personenbezogene Daten des
Nutzers darstellen können: Merkmale zur Identifikation, wie Nutzername
und Passwort, sowie Angaben zu Beginn und Ende der Nutzung, daneben den
Namen der abgerufenen Datei, Zeitpunkt des Abrufs oder Eintrags,
übertragene Datenmenge, die Meldung, ob der Abruf erfolgreich war, die
Referrer-URL sowie ein Cookie mit Sitzungs-Identifikationsnummer und
Nutzerkennung. Zu Forschungszwecken wird auch die IP-Adresse des
anfragenden Systems gespeichert. Im Rahmen der Forschungstätigkeit
werden alle diese Daten zur Forschung im Bereich der
datenschutzgerechten Gestaltung, der Verbesserung des Systems sowie der
Entwicklung neuer Algorithmen, u.a. zur Spam-Entdeckung, zum Ranking
oder für neue Empfehlungssystemen eingesetzt. ${project.name} stellt
die erhobenen Daten, jedoch nicht Nutzernamen, Passwort oder IP-Adresse,
anderen Forschungseinrichtungen in pseudonymisierter Form in Form eines
Datensatzes zur Verfügung, die diese nur im Rahmen ihrer
Forschungstätigkeit verarbeiten dürfen. Dabei lässt sich auch durch
Pseudonymisierungsverfahren nicht immer ausschließen, dass der Bezug zu
einem Nutzerkonto im Einzelfall durch einen Abgleich der
pseudonymisierten Daten mit den veröffentlichten Einträgen aufgedeckt
werden kann. Handelt der Nutzer etwa unter seinem Realnamen oder ergeben
sich aus seinen öffentlichen Einträgen Hinweise zu seiner Identität,
kann ${project.name} in einem solchen Fall den möglichen Personenbezug
nicht ausschließen.

####Sicherheit

Um unsere Webseite vor Spam zu schützen, verwenden wir den Dienst ["reCAPTCHA"](https://www.google.com/recaptcha/intro/v3beta.html)
von Google für eine erhöhte Sicherheit. Hierfür wird die IP-Adresse
des Nutzers sowie gegebenenfalls weitere, von Google für "reCAPTCHA"
benötige Daten, übertragen.

#### Einträge

Die vom Nutzer im Rahmen der Systemnutzung eingegebenen Einträge, d. h.
Lesezeichen sowie Angaben und Links zu eingestellter Literatur, werden
entsprechend den angebotenen Funktionalitäten dazu verwendet und verarbeitet, sie dem
einstellenden Nutzer und im Falle öffentlicher Einträge auch anderen
Nutzern zur Einsicht und zur Übernahme in die eigene Sammlung bereit zu
halten. Öffentliche Einträge werden für Forschungszwecke, z.B. zur
Erstellung von Rankings und Bildung von Tag-Clouds, herangezogen und
werden im Rahmen der internen Suchfunktion in Zusammenhang mit dem zum
Eintrag gehörenden Nutzernamen verarbeitet und dem suchenden Nutzer
angezeigt. Daneben besteht für registrierte Nutzer die Möglichkeit,
öffentliche Einträge über die angebotene [Programmier-Schnittstelle](https://bitbucket.org/bibsonomy/bibsonomy/wiki/documentation/api/REST%20API) (API) einzusehen und auf den eigenen Rechner herunter zu laden. Die
Einträge werden auch zu Forschungszwecken, etwa auf den Gebieten der
Spam-Entdeckung und der Recommender-Funktionen analysiert und sind Teil
des Datensatzes, die ${project.name} in pseudonymisierter Form an
andere Forschungseinrichtungen weitergibt. Beendet der Nutzer seine
Mitgliedschaft durch Löschen seines Nutzerkontos, werden seine Einträge
gesperrt. Ab diesem Zeitpunkt können weder andere Nutzer noch der Nutzer
selbst diese Einträge sehen. Die Einträge bleiben bei ${project.name}
gespeichert, werden jedoch ausschließlich zu Forschungszwecken
verarbeitet.

#### Werbung

${project.name} ist werbefrei. Es werden keine Nutzerdaten für die
Zuordnung von personalisierter Werbung verarbeitet und sie werden auch
nicht zu Werbezwecken an Dritte weitergegeben.

#### Empfänger oder Kategorien von Empfängern der personenbezogenen Daten

- Rechenzentrum der Universität Würzburg
- IT Servicezentrum der Universität Kassel
- Computer- und Medienservice der Humboldt-Universität zu Berlin
- IT-Dienstleister
- Forschungseinrichtungen

#### Dauer der Speicherung der personenbezogenen Daten

Die Kriterien für Dauer der Speicherung werden in den Zwecken der Verarbeitung festgelegt.
Vor der Löschung der personenbezogenen Daten müssen diese den Universitätsarchiven angeboten werden.
Sofern die Daten archivwürdig sind, werden die in die Archive zu Archivzwecken übernommen,
andernfalls werden die personenbezogen Daten gelöscht.

#### Betroffenenrechte

Diese Rechte können Sie gegenüber jedem einzelnen der Verantwortlichen geltend machen, jedoch auch gegenüber der genannten Anlaufstelle im Rahmen unserer gemeinsamen Verantwortung.
Werden Ihre personenbezogenen Daten verarbeitet, so haben Sie das Recht Auskunft über die zu Ihrer Person gespeicherten Daten zu erhalten (Art. 15 DSGVO).
Sollten unrichtige personenbezogene Daten verarbeitet werden, steht Ihnen ein Recht auf Berichtigung zu (Art. 16 DSGVO).
Liegen die gesetzlichen Voraussetzungen vor, so können Sie die Löschung oder Einschränkung der Verarbeitung verlangen sowie Widerspruch gegen die Verarbeitung einlegen (Art. 17, 18 und 21 DSGVO).
Wenn Sie in die Datenverarbeitung eingewilligt haben oder ein Vertrag zur Datenverarbeitung besteht und die Datenverarbeitung mithilfe automatisierter Verfahren durchgeführt wird, steht Ihnen gegebenenfalls ein Recht auf Datenübertragbarkeit zu (Art. 20 DSGVO).
Sollten Sie von Ihren oben genannten Rechten Gebrauch machen, prüft die öffentliche Stelle, ob die gesetzlichen Voraussetzungen hierfür erfüllt sind.
Weiterhin besteht ein Beschwerderecht bei den zuständigen Aufsichtsbehörden:

- Bayerische Landesbeauftragte für den Datenschutz
- Hessische Datenschutzbeauftragte
- Berliner Beauftragte für Datenschutz und Sicherheit

Ihr Beschwerderecht können Sie gegenüber jeder der Aufsichtsbehörden ausüben.

####Widerrufsrecht bei Einwilligung

Wenn Sie in die Verarbeitung durch die Universität Kassel, Julius-Maximilians-Universität Würzburg und Humboldt-Universität Berlin durch eine entsprechende Erklärung eingewilligt haben, können Sie die Einwilligung jederzeit für die Zukunft widerrufen. Die Rechtmäßigkeit der aufgrund der Einwilligung bis zum Widerruf erfolgten Datenverarbeitung wird durch diesen nicht berührt. 

####Pflicht zur Bereitstellung der Daten

Wir benötigen Ihre Daten, um den Nutzungsvertrag mit Ihnen abschließen zu können und unsere Forschungsvorhaben durchführen zu können. Wenn Sie die erforderlichen Daten nicht angeben, kann der Nutzungsvertrag mit Ihnen nicht abgeschlossen werden und das Forschungsvorhaben nicht durchgeführt werden.

####Recht auf Datenübertragbarkeit

Die betroffene Person hat nach Art. 20 DSGVO das Recht, die sie betreffenden personenbezogenen Daten, 
die sie einem Verantwortlichen bereitgestellt hat, in einem strukturierten, gängigen und 
maschinenlesbaren Format zu erhalten, und sie hat das Recht, diese Daten einem 
anderen Verantwortlichen ohne Behinderung durch den Verantwortlichen, dem die 
personenbezogenen Daten bereitgestellt wurden, zu übermitteln. Dies soll immer dann gelten, 
wenn die Grundlage der Datenverarbeitung die Einwilligung oder ein Vertrag ist und die Daten 
automatisiert verarbeitet werden.
Ihre Daten können Sie mittels unserer API exportieren.

#### Geltung

Diese Datenschutzerklärung gilt nur für die Erhebung und Verarbeitung von
Nutzerdaten durch ${project.name}, nicht jedoch für den etwaigen
Datenumgang durch die jeweiligen Anbieter der in Einträgen oder
anderweitig verlinkten externen Webseiten.

}

------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink")