<!-- en: SignInWithUseraccount -->

## Mit einem ${project.name}-Benutzeraccount anmelden
---------------------------------------------------

**Ziel:** Diese Anleitung zeigt Ihnen, wie Sie sich mit einem
bestehenden ${project.name}-Benutzeraccount anmelden.

**Voraussetzung:** Sie müssen bereits über einen ${project.name}-Benutzeraccount verfügen.
Haben Sie noch keinen ${project.name}-Benutzeraccount? - Diese Anleitung zeigt Ihnen, wie
Sie einen [neuen Benutzeraccount anlegen.](NeuenNutzeraccountAnlegen "wikilink")

--------------

### Anleitung

1.  Klicken Sie auf der Startseite auf **"Anmelden"**.

    ![ <File:1.png>](mitnutzeraccountanmelden/1.png  "fig: File:1.png")  
    
2.  Ein neues Fenster öffnet sich. Tragen Sie hier Ihren Benutzernamen und das von Ihnen gewählte Passwort ein.  

    ![ <File:2.png>](mitnutzeraccountanmelden/2.png  "fig: File:2.png")  
    
3.  Klicken Sie abschließend auf **"Anmelden"**, um sich bei
    ${project.name} anzumelden. Waren Ihre Angaben korrekt, dann werden
    Sie zu ${project.name} weitergeleitet.

------------------------------------------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink")
