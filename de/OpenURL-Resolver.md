<!-- en: OpenURL -->

## Verwenden Sie den OpenURL-Resolver Ihrer Bibliothek
---------------------------------------------------

**Ziel:** Diese Anleitung hilft Ihnen dabei, Ihr
${project.name}-Nutzerkonto mit dem URL-Resolver Ihrer Bibliothek zu
verknüpfen. Damit können alle URLs, auch in Publikationseinträgen, über
Ihre Bibliothek aufgelöst werden.  

**Voraussetzung:** Sie müssen die URL des OpenURL-Resolvers kennen. Dies
können Sie entweder bei der Bibliothek erfragen, oder aber Sie rufen den
folgenden Link aus ihrem Universitäts-/Bibliotheksnetzwerk auf:
[OpenURL-Resolver ermitteln](http://www.openurl.de/info/).  
  
Hierbei erhalten Sie folgende (beispielhafte) Ausgabe:  

![<File:Resolver.png>](openurl-resolver/Resolver.png  "fig: File:Resolver.png") 

Wenn Sie die URL des Resolvers kennen, gehen Sie wie folgt vor:  
  
![ <File:Resolver1.png>](openurl-resolver/Resolver1.png  "fig: File:Resolver1.png")  
  
1.  Öffnen Sie im rechten [ Hauptmenü](Programmoberfläche "wikilink") Ihr **"persönliches Menü"**.

2.  Klicken Sie auf **"Einstellungen"**.

3.  Geben Sie in der Rubrik **"Kontakt"** in das Feld **"OpenURL"** die
    URL des OpenURL-Resolvers ein. Im Beispiel ist der OpenURL-Resolver
    der Universitätsbibliothek der Universität Kassel zu sehen.
    
4.  Klicken Sie am Ende der Seite auf **"Änderungen speichern"**.

------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink") 
  

