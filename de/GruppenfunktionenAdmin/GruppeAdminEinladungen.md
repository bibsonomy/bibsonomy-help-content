<!-- en: GroupFunctionsAdmin/GroupAdminInvites -->

-----------------

[`Zurück zur Übersicht der Gruppenfunktionen von Admins geht es hier.`](GruppenfunktionenAdmin "wikilink")

---------------------

## Einladungen
-------------------

Unter **Mitgliederliste** können Administratoren und Moderatoren der Gruppe
**Nutzer zur Gruppe einladen** und **Beitrittsgesuche verwalten**.

![ <File:adminJoin.png>](GruppeAdminEinladungen/adminJoin.png  "fig: File:adminJoin.png")  


**Einladungen zu Gruppen** können bei den Einstellungen unter **"Gruppen"**
eingesehen, akzeptiert oder abgelehnt werden.  

![ <File:invite.png>](GruppeAdminEinladungen/invite.png  "fig: File:invite.png")  