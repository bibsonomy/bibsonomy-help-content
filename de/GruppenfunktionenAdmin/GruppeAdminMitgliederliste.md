<!-- en: GroupFunctionsAdmin/GroupAdminMembers -->

-----------------

[`Zurück zur Übersicht der Gruppenfunktionen von Admins geht es hier.`](GruppenfunktionenAdmin "wikilink")

---------------------

## Mitgliederliste verwalten
----------------------------------

Reguläre Mitglieder der Gruppe können nur die Mitgliederliste sehen. Sie
können daran nichts verändern.  

![<File:adminMembers.png>](GruppeAdminMitgliederliste/adminMembers.png  "fig: File:adminMembers.png")  

Administratoren der Gruppe dagegen können **Nutzer zur Gruppe
einladen**, **Beitrittsgesuche verwalten** und den Gruppenmitgliedern
**Rollen zuweisen**.  

Verfügbare Rollen:

-   **Administrator:** Dieser Nutzer hat die meisten Rechte in
    einer Gruppe. Er kann die Gruppeneinstellungen ändern und
    Beitrittsgesuche verwalten.
-   **Moderator:** Moderatoren können Beitrittsgesuche verwalten, können
    jedoch nicht die Gruppeneinstellungen ändern.
-   **User:** Reguläre Nutzer können alle verfügbaren Gruppenfunktionen
    nutzen, können jedoch weder Gruppeneinstellungen ändern noch
    Beitrittsgesuche verwalten.