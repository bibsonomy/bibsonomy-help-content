<!-- en: GroupFunctionsAdmin/GroupAdminTags -->

-----------------

[`Zurück zur Übersicht der Gruppenfunktionen von Admins geht es hier.`](GruppenfunktionenAdmin "wikilink")

---------------------

## Vordefinierte Tags hinterlegen
-----------------------------------

Über den Reiter „Gruppen“ -> GRUPPENNAME -> Einstellungen können Gruppenadmins die Registerkarte „Vordefinierte Tagliste“ anwählen:

![<File:TagsAnlegen.png>](GruppeAdminTags/TagsAnlegen.png "fig: File:TagsAnlegen.png") 

In der Eingabemaske könne Tags hinzugefügt und beschrieben werden. Die Beschreibung wird später als Mouseover angezeigt. 
Sobald sie alle Informationen eingegeben haben, klicken sie auf Hinzufügen.
Das vordefinierte Tag erscheint dann in der Liste.
Sie haben die Möglichkeit, bereits bestehende vordefinierte Tags zu bearbeiten, indem sie ein bestimmtes Tag in der Dropdownliste „Vordefinierte Tags bearbeiten“ auswählen und anschließend direkt editieren.
Mit „Aktualisieren“ werden die geänderten Informationen gespeichert:
