<!-- en: GroupFunctionsAdmin/GroupAdminDisband -->

-----------------

[`Zurück zur Übersicht der Gruppenfunktionen von Admins geht es hier.`](GruppenfunktionenAdmin "wikilink")

---------------------

## Gruppe auflösen
---------------------

Um eine Gruppe aufzulösen, tippen Sie **"yes"** in das Eingabefeld bei "Sind
Sie sicher?". 
**Achtung:** Wenn Sie eine Gruppe auflösen, werden **alle
Dokumente gelöscht**, die der Gruppe zugeordnet sind.  

![<File:delete.png>](GruppeAdminAufloesen/delete.png  "fig: File:delete.png")  

