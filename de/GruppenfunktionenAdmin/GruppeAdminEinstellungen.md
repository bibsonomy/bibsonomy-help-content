<!-- en: GroupFunctionsAdmin/GroupAdminSettings -->

-----------------

[`Zurück zur Übersicht der Gruppenfunktionen von Admins geht es hier.`](GruppenfunktionenAdmin "wikilink")

---------------------

## Einstellungen bearbeiten
-----------------------------------

Nur der Administrator einer Gruppe kann die Gruppeneinstellungen verändern.

![<File:settings_neu.png>](GruppeAdminEinstellungen/settings_neu.png  "fig: File:settings_neu.png")

- **Allgemeine Informationen** und **Kontaktdaten** hinzufügen
-   **Gruppeneinstellungen**
    -   Mitgliederliste: Sie können festlegen, ob die Mitgliederliste
        öffentlich, privat oder öffentlich für Mitglieder sein soll.
    -   Dokumentenfreigabe: Legen Sie fest, ob Gruppenmitglieder Zugriff
        auf die Dokumente anderer Mitglieder haben sollen (aktiviert)
        oder nicht (deaktiviert).
    -   Gruppenbeitrittsgesuche: Wählen Sie, ob andere Nutzer die
        Möglichkeit haben sollen, Ihnen Beitrittsgesuche zu
        senden (aktiviert) oder nicht (deaktiviert).
-   Auf den **API-Key** der Gruppe zugreifen
-   Das **Gruppenbild ändern**

