<!-- en: ChangeCV -->

## Eigenes Benutzerprofil und Lebenslauf anlegen
---------------------------------------------

Das CVWiki ist ein Feature von ${project.name}, mit dem Sie Ihren
Lebenslauf auf Ihrem Benutzerprofil selbst erstellen oder verändern
können. Sie haben die Wahl zwischen vorgegebenen Layouts oder der
Möglichkeit, Ihr persönliches Design zu erstellen. In Ihrem Lebenslauf
(alias "Curriculum Vitae") können Sie einige Informationen zu sich und
ihren Interessen (wissenschaftlich/privat) veröffentlichen. Andere
Benutzer können diese Informationen einsehen.



------------------------------------------------------------------------

### Ihren Lebenslauf aufrufen

  
![ <File:1.png>](lebenslaufändern/1.png  "fig: File:1.png")  

1. Klicken Sie auf Ihren **Benutzernamen** im [Hauptmenü](Programmoberfläche "wikilink").

2.  Klicken Sie rechts neben ihrem Profilbild (rechte blaue Leiste) auf den **CV-Button**.

3.  Ihr Lebenslauf wird angezeigt (so wie ihn andere Benutzer sehen).

    ![ <File:2.png>](lebenslaufändern/2.png  "fig: File:2.png")  
    
4.  Um den Lebenslauf zu verändern, klicken Sie auf das schwarze **Zahnradsymbol**.

5.  Wählen Sie **"Lebenslauf bearbeiten"**. Sie können so Ihren
    Lebenslauf frei nach Ihren Wünschen gestalten. Wählen Sie hierzu unter **"Layout editieren"** 		ein Layout und klicken Sie anschließend auf **"Layout editieren"**. Zur Gestaltung
    verwenden Sie nun die [MediaWiki-Syntax](http://en.wikipedia.org/wiki/Help:Wiki_markup), die zum 	Beispiel aus Wikipedia bekannt ist.

  
**Alternative**: Sie können Ihren Lebenslauf auch bearbeiten, indem Sie
im rechten Hauptmenü auf das Personensymbol klicken, dann auf
**"Einstellungen"** klicken und schließlich den Reiter **"Lebenslauf"** auswählen.  
  

------------------------------------------------------------------------

### Erweiterte Funktionen

Zur Gestaltung Ihres Layouts bieten wir außerdem einige XHTML-Tags an, um Ihnen Zeit zu ersparen.
Einige der Tags können jedoch ausschließlich für den Lebenslauf von Gruppen bzw. Benutzern 
verwendet werden. 
Folgender Tabelle können Sie entnehmen, welche Tags für Benutzer bzw. Gruppen genutzt werden können.

Tag | Bedeutung | Gruppe | Benutzer
--- | --- | --- | ---
`<name />` | Ihr Name, mit Link auf Ihre Publikationsseite | ja | ja
`<image />` | Ihr Profilbild | ja | ja 
`<publications />` | Eine Liste Ihrer Publikationen mit dem Tag "myown" | ja | ja 
`<bookmarks />` | Eine Liste Ihrer Lesezeichen mit dem Tag "myown" | ja | ja 
`<profession />` | Ihr Beruf | nein | ja 
`<institution />` | Ihre beschäftigende Institution | nein | ja 
`<hobbies />` | Eine Liste Ihrer Hobbies | nein | ja
`<interests />` | Ihre persönlichen Interessen | nein | ja
`<birthday />` | Ihr Geburtsdatum | nein | ja 
`<location />` | Ihr momentaner Wohnort | nein | ja 
`<regdate />` | Ihr Registrierungsdatum in ${project.name} | nein | ja
`<members />` | Liste aller Gruppenmitglieder | ja | nein
`<groupimage/>` | Gruppenicon| ja | nein
`<tags />` | Ihre Tags | ja | ja  



Für die Tags `<publications />;`, `<bookmarks />` und `<tags/>` können Sie außerdem eigene Tags als Ressourcenselektoren benutzen, wenn Sie `tags="tag1 tag2\[ ...\]"` als Attribute anfügen. Beispielsweise liefert `<publications tags="data mining" />` alle Ihre Publikationen zurück, die Sie sowohl mit `data` als auch mit `mining` getaggt haben.

Für `<tags />` gibt es folgende Selektoren: `style` mit den Optionen
`cloud` und `list`; `order` mit den Optionen `alpha` und `freq`, `minfreq` mit einer
Zahl als Parameter, und `type` mit den Optionen `bookmarks` und `publications`.  
  
------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink") 
