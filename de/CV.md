<!-- redirect:Eigenes Benutzerprofil und Lebenslauf anlegen-->
<!-- en: CV -->

Diese Seite zeigt Ihren Lebenslauf in ${project.name}. Dort zu sehen
sind Ihre persönliche Daten, wissenschaftliche Interessen und Hobbys,
die Sie in den [ Einstellungen](Einstellungen "wikilink") eintragen
können, sowie die Liste der Einträge, die Sie mit *myown* getaggt haben.
Die Profildaten werden entsprechend Ihrer Einstellung für die
Sichtbarkeit Ihres Profils nur Freunden, niemandem, oder allen Benutzern
angezeigt.

------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink")