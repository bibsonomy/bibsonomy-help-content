<!-- en: Followers -->

## Follower
-----------------------------

Ein guter Weg, um in ${project.name} interessante Lesezeichen und Publikationen 
zu finden, ist Nutzer zu suchen, die sich für ähnliche Themen 
wie Sie selbst interessieren.
Um dies zu erleichtern, gibt es die Möglichkeit, einem oder mehreren Nutzern zu **folgen**.
Wenn Sie einem Nutzer folgen, können Sie sehr einfach dessen Einträge verfolgen.
Die Liste der Follower kann also als Liste von Nutzern verstanden werden, die 
dieselben Interessen mit Ihnen teilen.


---------------------------------------------------------------------------------------

### Nutzern folgen


![ <File:follow.png>](follower/follow.png  "fig: File:follow.png")


Um einem Nutzer zu folgen, müssen Sie lediglich auf das Feld **"Folgen"** klicken,
das Sie auf jeder Nutzerseite in der Sidebar unter dem Namen des jeweiligen Nutzers finden.


![ <File:unfollow.png>](follower/unfollow.png  "fig: File:unfollow.png")


Die Namen der Nutzer, denen Sie folgen, werden  auf der Seite ["Follower"](https://www.bibsonomy.org/followers) in der Sidebar 
auf der rechten Seite angezeigt. Sie finden diese Seite, indem Sie im linken Hauptmenü auf **"mein BibSonomy"**. dann auf **"verfolgte Einträge"** klicken.

Wenn Sie einem Nutzer **nicht mehr folgen** möchten, klicken Sie auf das rote **"Entfernen"-Feld** neben 
dem Nutzernamen.
In der Sidebar werden außerdem Nutzer angezeigt, die Ihnen folgen und Ihnen ähnliche Nutzer, 
denen zu folgen für Sie relevant sein könnte.


---------------------------------------------------------------------------------------

### Nutzer zum Folgen finden

Es gibt mehrere Möglichkeiten Nutzer zu finden, denen Sie folgen können.


 -  Wenn Sie den Nutzernamen kennen, können Sie diesen direkt mit der **Suchfunktion** suchen, 
	die Sie auf der rechten oberen Seite finden. Wählen Sie die Option **Benutzer** aus und tippen Sie anschließend
	den Namen des Nutzers, den Sie finden wollen, ein.


![ <File:suchename.png>](follower/suchename.png  "fig: File:suchename.png")


 -  In der Liste der Publikationen und Lesezeichen können Sie die Namen der Nutzer, die einen für Sie 
	interessanten Eintrag erstellt haben, heraussuchen.


![ <File:namepublikation.png>](follower/namepublikation.png  "fig: File:namepublikation.png")


 -  Sie finden Personen mit ähnlichen Interessen, indem Sie auf den Namen einer Publikation klicken, die Sie interessiert. 
	In der rechten Sidebar sehen Sie eine Liste der Nutzer, die diese Publikation zu Ihrer Kollektion hinzugefügt haben.


![ <File:collection.png>](follower/collection.png  "fig: File:collection.png")



-----------------------------------------------------------------------------------

### Einträge von Nutzern, denen Sie folgen

Auf der [Followerseite](https://www.bibsonomy.org/followers) werden die Einträge aller Nutzer, denen Sie folgen, zusammengefasst.
Die relevantesten Einträge für Sie stehen hierbei ganz oben auf der Liste.

Wenn Sie nur Einträge einer bestimmten Person der Sie folgen sehen wollen, klicken Sie auf deren Nutzernamen, 
den Sie in der rechten Sidebar finden: 
dort sind alle Nutzer, denen Sie folgen, aufgelistet.
Wählen Sie von dort eine bestimmte Person aus, werden nur deren Einträge angezeigt.


![ <File:sortierung.png>](follower/sortierung.png  "fig: File:sortierung.png")


Sie können außerdem das **Ranking** verändern, das angewendet werden soll. Klicken Sie 
auf das **Symbol mit dem Pfeil und den vier Balken**. 
In dem Menü, das nun erscheint, können Sie die einträge nach Datum, Name oder Ranking sortieren.


![ <File:ranking.png>](follower/ranking.png  "fig: File:ranking.png")


Wenn Sie die Option **Ranking** wählen, finden Sie in der rechten Sidebar den Abschnitt **Einstellungen**. 
Dort können Sie als Rankingmethode [**"tf-idf"**](https://de.wikipedia.org/wiki/Tf-idf-Ma%C3%9F) oder **"Tag-Überlapp"** wählen. 
Sie können auswählen, ob bei der Erstellung des Rankings eine Normalisierung angewendet werden soll.

Um Ihre Auswahl zu bestätigen, klicken Sie auf das Feld **"Los"**.

------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink")