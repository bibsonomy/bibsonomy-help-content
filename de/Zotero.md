<!-- en: Zotero -->

## Zotero
-----------------------------

[Zotero](http://www.zotero.org/) ist eine Erweiterung für Firefox und
sammelt, verwaltet und zitiert Publikationen. Wir unterstützen den
Export von Literaturangaben aus ${project.name} zu Zotero sowie den
Import von Einträgen in der Zotero-Bibliothek nach ${project.name}.

Wenn Zotero einen Publikationseintrag auf einer Seite entdeckt, dann
zeigt es ein Symbol in der Firefox-Adressleiste an. Wenn Sie auf das
Symbol klicken, speichert Zotero die Information in einer Ihrer
Zotero-Bibliotheken. Auch auf ${project.name}-Seiten, die
BibTeX-Einträge enthalten, wird das Symbol für Zotero-Benutzer
angezeigt.


------------------------------------------------------------------------

### Export von ${project.name} nach Zotero

Wenn Sie auf einer ${project.name}-Seite (zum Beispiel der Home-Seite
oder Ihrer Benutzerseite) eine Publikation in Ihre Zotero-Bibliothek
übernehmen möchten, klicken Sie auf den **schwarzen Pfeil** neben dem
Zotero-Symbol und anschließend auf **"In Zotero mit "unAPI"
speichern"**. Daraufhin öffnet sich ein **Popup-Fenster**, in dem Sie
den gewünschten Eintrag zum Export auswählen können. Klicken Sie auf OK
und der ausgewählte Eintrag erscheint in Ihrer Zotero-Bibliothek.

![ <File:speichern1.png>](zotero/speichern1.png  " File:speichern1.png")

![ <File:zoteroentry.png>](zotero/zoteroentry.png  " File:zoteroentry.png")

------------------------------------------------------------------------

### Import von Zotero nach ${project.name}

Andersherum funktioniert es noch nicht völlig automatisch, denn Zotero
muss zunächst für ${project.name} konfiguriert werden.

Gehen Sie zu den Zotero-Einstellungen. Dazu klicken Sie zunächst auf das
**"Z"-Symbol** in Firefox oben rechts, daraufhin wird Zotero in einem
Fenster im unteren Browserbereich geöffnet. Dort klicken Sie auf das
**Zahnradsymbol** und schließlich auf **"Einstellungen"**. Ein
Popup-Fenster öffnet sich. Wählen Sie den Menüpunkt **Export** aus.
Fügen Sie zu den **website-spezifischen Einstellungen** durch Klicken
auf das '+'-Symbol einen neuen Eintrag hinzu. Geben Sie in dem
Popup-Fenster `bibsonomy.org` ein und wählen Sie `BibTeX` als Ausgabeformat.
Bestätigen Sie den Eintrag durch OK.

![<File:einstellungen.png>](zotero/einstellungen.png  " File:einstellungen.png")

![<File:zoterosettings.png>](zotero/zoterosettings.png  "fig: File:zoterosettings.png") 

Nun können Sie, wenn Sie sich bei ${project.name} eingeloggt haben, im
[ linken Hauptmenü](Programmoberfläche "wikilink") auf **"Eintragen"**,
dann auf **"Publikation eintragen"** klicken und den Reiter
**"BibTeX/EndNote-Schnipsel"** wählen. 

Auf der folgenden Seite können Sie in das Feld **"Auswahl"** einen
Eintrag aus Ihrer Zotero-Bibliothek durch **drag & drop** hineinziehen
(klicken Sie auf den Zotero-Eintrag, halten Sie die linke Maustaste
gedrückt, bewegen Sie den Mauszeiger in das Feld und lassen Sie dann die
linke Maustaste los). Durch Klicken auf **"Weiter"** werden die Daten
aus dem Zotero-Eintrag extrahiert und in die entsprechenden Felder
eingetragen.

![<File:zoterobibsonomy.png>](zotero/zoterobibsonomy.png  "fig: File:zoterobibsonomy.png")  

------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink") 
