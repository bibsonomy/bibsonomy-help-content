<!-- en: MainAdvanced -->

Hilfe für Fortgeschrittene
--------------------------

**Hinweis:** Auf dieser Seite finden Sie die **Hilfe für fortgeschrittene Nutzer**.
Für weitere Informationen besuchen Sie die [Hilfe für Einsteiger](MainEinsteiger "wikilink") und die
[ Hilfe für Softwareentwickler](MainEntwickler "wikilink").

${project.name} ist ein kostenloser Service, der Recherche,
Literaturverwaltung und Publikationsmanagement vereint. Darüberhinaus
ermöglicht ${project.name} das gemeinsame Arbeiten im Team (z. B. durch
den Austausch von Internet-Lesezeichen oder gerade gelesener Literatur).
Diese Seite fasst für Sie alle Funktionen für fortgeschrittene Nutzer
zusammen.

----

![<File:Student-Laptop.png>](mainfortgeschrittene/Student-Laptop.png  " File:Student-Laptop.png")

### Erweiterte Funktionen von ${project.name}


In diesem Abschnitt finden Sie Anleitungen, die Ihnen zeigen, wie Sie
noch mehr aus ${project.name} herausholen können. Bevor Sie mit diesen
Anleitungen starten, empfehlen wir, die [ erweiterten Funktionen
freizuschalten](ErweiterteFunktionenFreischalten "wikilink"), damit Sie noch mehr Möglichkeiten haben, effektiv mit Ihren Lesezeichen und
Publikationen zu arbeiten.  

-   [ Eigenes Benutzerprofil und Lebenslauf anlegen](LebenslaufÄndern "wikilink")

<!-- -->

-   [ Immer auf dem neusten Stand: RSS-Feeds abonnieren](RSS-FeedsAbonnieren "wikilink")

<!-- -->

-   [ Publikationen durchstöbern](PublikationenDurchstöbern "wikilink")

<!-- -->

-   [ Die Ablage verwenden und Publikationen merken](Die_Ablage "wikilink")

<!-- -->

-   [ Private Dateien an eine Publikation anhängen](DokumentAnhängen "wikilink")

<!-- -->

-   [ URL-Syntax und Parameter verstehen und nutzen](URL-Syntax "wikilink")

<!-- -->

-   (BETA) [ Addon für Google Docs](AddonGoogleDocs "wikilink")


----

![ <File:User-Group.png>](mainfortgeschrittene/User-Group.png  " File:User-Group.png")

### Soziale Funktionen von ${project.name}


${project.name} ermöglicht es allen Benutzern, sich untereinander zu
verknüpfen. Verknüpfte Benutzer werden in ${project.name} als *Freunde*
bezeichnet. Außerdem können *Gruppen* gebildet und Publikationen
*diskutiert* werden.  

-   [ Freunde hinzufügen](FreundeHinzufügen "wikilink")

<!-- -->

-   [ Gruppen für gemeinsame (Lese-)Interessen finden und beitreten](Gruppenfunktionen "wikilink")
<!-- -->

-   [ Anderen Nutzern folgen](Follower "wikilink")

<!-- -->

-   [ Kommentar, Rezension, Bewertung - Gemeinsam mit anderen diskutieren](KommentarRezensionBewertung "wikilink")

<!-- -->

-   [ Verwende Sie den OpenURL-Resolver ihrer Bibliothek](OpenURL-Resolver "wikilink")


----

![ <File:News.png>](mainfortgeschrittene/News.png  " File:News.png")

### ${project.name} - Informationen für Journalisten/Presse


Informationen für Pressevertreter und Interessierte zu ${project.name}
und den Schwester-Projekten finden Sie hier:  

-   [ Projekte/Kooperation - Eine Übersicht](Projekte "wikilink")

<!-- -->

-   [ Presse zu BibSonomy/PUMA](Presse "wikilink")


----

${project.theme == "bibsonomy"}{

![<File:Messages-Information-01.png>](mainfortgeschrittene/Messages-Information-01.png  " File:Messages-Information-01.png")

### Direkter Kontakt zu den Entwicklern von ${project.name}


Haben Sie Fragen? Brauchen Sie Hilfe? Oder haben Sie vielleicht
Vorschläge, was wir noch besser machen könnten? Hier finden Sie unsere
Kontaktdaten. Wir freuen uns auf Ihr Feedback.  

------------------

![<File:Message-Mail.png>](mainfortgeschrittene/Message-Mail.png  "fig: File:Message-Mail.png")
<webmaster@bibsonomy.org>

-   Direkte Hilfe bei Fragen und Problemen zu ${project.name}.  

-------------------

![<File:WordPress.png>](mainfortgeschrittene/WordPress.png  "fig: File:WordPress.png")
[blog.bibsonomy.org](http://blog.bibsonomy.org)

-   "Feature of the week"
-   Meldungen zu Releases und aktuellen Entwicklungen
-   Kommentare, die beantwortet werden

------------------------

![<File:Twitter-Bird.png>](mainfortgeschrittene/Twitter+Bird.png  "fig: File:Twitter-Bird.png")
[Join "BibSonomyCrew" on Twitter](https://twitter.com/BibSonomyCrew)

-   Updates zu Entwicklungen
-   Direkter Kontakt

-------------------

![<File:Conference-Call.png>](mainfortgeschrittene/Conference-Call.png  "fig: File:Conference-Call.png")
Für Freunde der guten alten Mailinglisten:
<bibsonomy-discuss-de@cs.uni-kassel.de>

-   Diskussionen rund um BibSonomy
-   Diskussionen sind öffentlich und alle Nutzer sehen die Fragen und Antworten

}

-----------------------

Auf dieser Seite finden Sie die **Hilfe für fortgeschrittene Nutzer**.
Für weitere Informationen besuchen Sie die [Hilfe für Einsteiger](MainFortgeschrittene "wikilink") und die
[ Hilfe für Softwareentwickler](MainEntwickler "wikilink").

  


