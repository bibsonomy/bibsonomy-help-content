<!-- en: TeXlipseBibSonomyExtension -->

## TeXlipseBibSonomyExtension
-----------------------------

Die TeXlipseBibSonomyExtension ermöglicht es, die Publikationen Ihres
BibSonomy Accounts mithilfe des Eclipse Plugins
[TeXlipse](http://texlipse.sourceforge.net/) in ihre aktuellen Arbeiten zu importieren.

Zum Installieren des Plugins in Eclipse verwenden Sie bitte die [Update Seite](http://texlipse.bibsonomy.org/) (`http://texlipse.bibsonomy.org/`). Bitte beachten Sie, dass ältere Versionen des Plugins auch über die TeXlipse Update Site
verfügbar sind, neuere Versionen nur über die BibSonomy Update Site.

Alternativ können Sie unten das Plugin herunterladen und manuell installieren.

-----------------------------

### Funktionen

#### Einstellungen

Über die Eclipse Einstellungen lassen sich Nutzername und Api-Key
setzen. Zusätzlich können sämtliche Funktionen des Plugins an- und
abgeschaltet werden.  

![preferences.png](texlipsebibsonomyextension/pref.png)  

-------------------

#### Zitierfunktion

Importieren Sie Einträge vom BibSonomy-Server direkt in die .bib-Datei
Ihres LateX Projekts.  
In der Completion erscheinen:

-   Einträge, die nur in der .bib-Datei vorhanden sind (gelb)
-   Einträge, die nur auf dem BibSonomy-Server vorhanden sind (rot)
-   Einträge, die auf dem BibSonomy-Server und in der .bib-Datei
    vorhanden sind (blau).


![preferences.png](texlipsebibsonomyextension/cite.png)  

---------------

#### Tags benutzen

Mithilfe von Tags lassen sich die Einträge, die vom BibSonomy-Server
geholt werden, regulieren.  

![preferences.png](texlipsebibsonomyextension/tags.png)  

--------------------------

#### Synchronisierung

Über die Synchronisierungsfunktion lassen sich einzelne Einträge auf
Unterschiede zur Serverversion prüfen, diese Unterschiede rückgängig
machen, oder die lokalen Änderungen auf den Server übertragen (nur
halbwegs funktional, wird in Zukunft durch eine effektivere Version
ersetzt).  

![preferences.png](texlipsebibsonomyextension/sync.png)  


------------------------------------------------------------------------

### Aktuelle Version

[TeXlipseBibSonomyExtension\_0.1.1.1](https://bitbucket.org/bibsonomy/bibsonomy-texlipse/downloads/org.bibsonomy.texlipseextension_0.1.1.1.jar)

----------------------

### Ältere Versionen

[TeXlipseBibSonomyExtension\_0.1](https://bitbucket.org/bibsonomy/bibsonomy-texlipse/downloads/org.bibsonomy.texlipseextension_0.1.0.jar)

-------------------------

### Bekannte Probleme

-   Manchmal geht die Synchronisierung zwischen .bib-Datei und
    autocomplete Container des `\cite`-Befehls verloren. In dem Fall
    hilft neu einbinden der Datei mit dem `\bibliography` Befehl.
    
-   Die Synchronisierungsfunktion zwischen BibSonomy-Server und .bib-Datei
    ist momentan nur provisorisch implementiert und wird in
    Zukunft komplett überarbeitet.

------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink") 