<!-- en: MainBeginner -->

Hilfe für Einsteiger
--------------------

**Hinweis:** Auf dieser Seite finden Sie die **Hilfe für Einsteiger**.
Für weitere Informationen besuchen Sie die [ Hilfe für fortgeschrittene Nutzer](MainFortgeschrittene "wikilink") und die 
[ Hilfe für Softwareentwickler](MainEntwickler "wikilink").

${project.name} ist ein kostenloser Service, der Recherche,
Literaturverwaltung und Publikationsmanagement vereint. Darüberhinaus
ermöglicht ${project.name} das gemeinsame Arbeiten im [Team](Gruppenfunktionen "wikilink") (z. B. durch
den Austausch von Internet-Lesezeichen oder gerade gelesener Literatur).
Diese Seite fasst für Sie alle grundlegenden Funktionen von
${project.name} zusammen.

----

${project.theme == "puma"}{

![ <File:Race-Flag.png>](maineinsteiger/Race-Flag.png  " File:Race-Flag.png")

### Benutzeraccount freischalten

Um PUMA aktiv verwenden zu können (z. B. neue Publikationen eintragen),
müssen Sie einmalig Ihren Bibliotheksausweis für die Nutzung von PUMA
freischalten. Die folgende Anleitung zeigt Ihnen, wie Sie Ihren [
Bibliotheksausweis für PUMA freischalten](BibliotheksausweisFür_PUMAFreischalten "wikilink"). Danach
können Sie in PUMA einfach auf Anmelden klicken, um sich mit den Daten
Ihres Bibliotheksausweises anmelden.

}

${project.theme == "bibsonomy"}{

![ <File:Race-Flag.png>](maineinsteiger/Race-Flag.png  " File:Race-Flag.png")

### Bei BibSonomy anmelden / neuen BibSonomy-Benutzeraccount erstellen

Sie können sich auf zwei Wegen bei BibSonomy anmelden:  

-   [ Mit einem bestehenden OpenID-Account anmelden](MitOpenIDAnmelden "wikilink")

<!-- -->

-   [ Mit BibSonomy-Benutzeraccount anmelden](MitNutzeraccountAnmelden "wikilink")


Falls Sie *noch keinen BibSonomy-Benutzeraccount besitzen*, dann erklärt
Ihnen die folgende Anleitung, wie Sie  

-   [ Einen neuen BibSonomy-Benutzeraccount erstellen](NeuenNutzeraccountAnlegen "wikilink")  


Weitere Hilfe zu Benutzeraccounts finden Sie hier:  

-   [ Passwort vergessen?](PasswortVergessen "wikilink")  

<!-- -->

-   [ Benutzerkonto endgültig löschen](BenutzerkontoEndgültigLöschen "wikilink")

} 

------------------------

![<File:Student-Read-04.png>](maineinsteiger/Student-Read-04.png  " File:Student-Read-04.png")

### Die grundlegenden Funktionen von ${project.name}


Hier finden Sie einfache und schnelle Erklärungen für die wichtigsten
Funktionen, die es in ${project.name} gibt.  

-   [ Was finde ich wo? - Die Benutzeroberfläche kurz und bündig erklärt](Programmoberfläche "wikilink")

<!-- -->

-   [ Lesezeichen und Publikationen verwalten](LesezeichenPublikationenManagen "wikilink")

<!-- -->

-   [ Schlagworte (Tags) nutzen um Literaturangaben schneller wiederzufinden](Tags "wikilink")

<!-- -->

-   [Lesezeichen importieren](LesezeichenImportieren "wikilink")

<!-- -->

-   [Lesezeichen exportieren](LesezeichenExportieren "wikilink")

<!-- -->

-   [ Literaturliste importieren (z. B. aus Citavi, EndNote, uvm.)](EintragPublikationen "wikilink")

<!-- -->

-   [ Literaturliste exportieren (z. B. als EndNote, RDF, HTML oder BibTeX)](LiteraturlisteExportieren "wikilink")

<!-- -->

-   [ "Google Chrome", "Mozilla Firefox" oder "Apple Safari"-Addon installieren](${project.home}buttons "wikilink")

<!-- -->

-   [ Discovery Service und HeBIS-Integration Ihrer Bibliothek nutzen](DiscoveryService "wikilink")


----

![ <File:News.png>](maineinsteiger/News.png  " File:News.png")

### ${project.name} - Informationen für Journalisten/Presse


Informationen für Pressevertreter und Interessierte zu ${project.name}
und den Schwester-Projekten finden Sie hier:  

-   [ Projekte/Kooperation - Eine Übersicht](Projekte "wikilink")

<!-- -->

-   [ Presse zu BibSonomy/PUMA](Presse "wikilink")


----

${project.theme == "bibsonomy"}{

![<File:Messages-Information-01.png>](maineinsteiger/Messages-Information-01.png  " File:Messages-Information-01.png")

### Direkter Kontakt zu den Entwicklern von ${project.name}


Haben Sie Fragen? Brauchen Sie Hilfe? Oder haben Sie vielleicht
Vorschläge, was wir noch besser machen könnten? Hier finden Sie unsere
Kontaktdaten. Wir freuen uns auf Ihr Feedback.  

------------------

![<File:Message-Mail.png>](maineinsteiger/Message-Mail.png  "fig: File:Message-Mail.png")
<webmaster@bibsonomy.org>

-   Direkte Hilfe bei Fragen und Problemen zu ${project.name}.  

-------------------

![<File:WordPress.png>](maineinsteiger/WordPress.png  "fig: File:WordPress.png")
[blog.bibsonomy.org](http://blog.bibsonomy.org)

-   "Feature of the week"
-   Meldungen zu Releases und aktuellen Entwicklungen
-   Kommentare, die beantwortet werden

------------------------

![<File:Twitter-Bird.png>](maineinsteiger/Twitter+Bird.png  "fig: File:Twitter-Bird.png")
[Join "BibSonomyCrew" on Twitter](https://twitter.com/BibSonomyCrew)

-   Updates zu Entwicklungen
-   Direkter Kontakt

------------------

![<File:Conference-Call.png>](maineinsteiger/Conference-Call.png  "fig: File:Conference-Call.png")
Für Freunde der guten alten Mailinglisten:
<bibsonomy-discuss-de@cs.uni-kassel.de>

-   Diskussionen rund um BibSonomy
-   Diskussionen sind öffentlich und alle Nutzer sehen die Fragen und Antworten

}

-----------------------

Auf dieser Seite finden Sie die **Hilfe für Einsteiger**.
Für weitere Informationen besuchen Sie die [ Hilfe für fortgeschrittene Nutzer](MainFortgeschrittene "wikilink") und die
[ Hilfe für Softwareentwickler](MainEntwickler "wikilink"). 


