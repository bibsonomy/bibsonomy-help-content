<!-- en: TagcloudOnZopePages -->

## Tag-Wolken auf Zope-Seiten
--------------------------

${project.name}-Schlagwörter können auf einer
[Zope](http://zope.org/)-Seite angezeigt werden. Ein Beispiel finden Sie [hier](http://www.kde.cs.uni-kassel.de/macek).

-   Sie müssen auf eine ${project.name}-Seite aus Zope
    heraus zugreifen. Hierfür benötigen Sie das Produkt [Kebas Data](http://sourceforge.net/projects/kebasdata/).
   
-   Für jede Tag-Wolke, die Sie anzeigen lassen wollen, benötigen Sie
    ein KebasData-Objekt. Bitte konfigurieren Sie es wie folgt
    (Benutzername etc. muss natürlich ersetzt werden):  
      
    ![<File:kebas_config.png>](tagwolkenaufzope-seiten/kebas_config.png  "fig: File:kebas_config.png")  
      
    Nun werden alle Tags in Ihrer Tag-Wolke angezeigt, die sich zwischen
    den Start- &lt;ul...> - und den Ende- &lt;/ul> Schemata bewegen.

-   Sie müssen jedoch die von ${project.name} ausgegebenen URLs
    überarbeiten, da diese sich auf das
    ${project.name}-Hauptverzeichnis und nicht auf Ihre Seite beziehen.
    Hierfür fügen Sie bitte ein "Script (Python)"-Objekt namens
    **`render_fixbaseurl`** in Zope an beliebiger Stelle oberhalb des
    Ordners, der Ihre Tag-Wolke enthält, ein. Lassen Sie es zwei
    Parameter haben und folgendermaßen aussehen
    ("http://www.bibsonomy.org/" ist ggf. durch die
    ${project.name}-Adresse zu ersetzen):
    

        ul = context.match[0]
        ul = ul.replace('href="/', 'href="http://www.bibsonomy.org/')
        print ul
        return printed
        some code block


-   Für die Anzeige Ihrer Tag-Wolke von *DTML* aus müssen Sie diesen
    Befehl eingeben:
    
        <ul class="tagbox">
        <dtml-var tagcloud>
        </ul>


-   Für die Anzeige Ihrer Tag-Wolke von einem *Page Template* aus können
    Sie diesen Befehl benutzen:
    
        <ul class="tagbox">
        <div tal:replace="structure here/tagcloud"/>
        </ul>


-   Nutzen Sie CSS zur Formatierung der Tag-Wolke nach Ihrem Geschmack.
    Hier sehen Sie, was wir benutzen; bitte beachten Sie, dass dies die
    selten vorkommenden Tags verbirgt. Sie können **`display: none`**
    durch **`display: inline`** ersetzen, um deren Anzeige zu
    aktivieren:
    
        ul.tagbox { list-style: none; text-align: justify; }
        ul.tagbox li { display: inline; }
        ul.tagbox li a { display: none; text-decoration: none; color: #e05698; font-size: 60% }
        ul.tagbox li.tagone a {  display: none; text-decoration: none; color: #a3004e; font-size: 80% } 
        ul.tagbox li.tagten a {  display: inline; text-decoration: none; color: #830030; font-size: 100% }

------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink") 

