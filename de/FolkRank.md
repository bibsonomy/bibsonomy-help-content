<!-- en: FolkRank -->

## FolkRank
---------------

Durch Ihre bisherigen Erfahrung mit ${project.name} wissen Sie
vielleicht, dass ein typischer Weg, Publikationen oder Lesezeichen zu
einem Thema zu finden, die Suche mit Tags (Schlagwörtern) ist. Die
Ergebnisse dieser Suche sind Lesezeichen- bzw. Publikationslisten,
**geordnet nach Datum**. Dadurch können Sie die **neusten** Einträge
finden; jedoch werden nicht die **relevantesten** Einträge angezeigt.

Um die relevantesten Einträge zu einem Tag zu finden, hat unsere
Entwicklergruppe den [FolkRank-Algorithmus](${project.home}bibtex/24d8b4f79814691fbe6db8357d63206a1/stumme "wikilink")
entwickelt. Die Idee hinter diesem Algorithmus ist dem
PageRank-Algorithmus von Google ähnlich, denn er analysiert die
Link-Struktur zwischen Nutzern, Tags und Einträgen iterativ, um die
**Relevanz zu berechnen**.

Um die Ergebnisse nach FolkRank zu ordnen, fügen Sie `?order=folkrank` der URL hinzu
oder klicken Sie auf einer Tag-Seite auf **"Sortiert nach Folkrank"**
(rechts unter der Suchleiste). Wenn Sie beispielsweise die Ergebnisse
der Suche mit dem Tag "www" analysieren, können Sie feststellen, dass
sehr relevante Lesezeichen, wie die WWW-Konferenz-Webseiten, am Anfang
der Liste stehen.  

Außerdem berechnet der FolkRank-Algorithmus **verwandte ("relevante")
Benutzer** für einen Tag:

![ <File:relatedusers.png>](folkrank/relatedusers.png  " File:relatedusers.png")

Diese relevanten Nutzer sind möglicherweise Experten in einem bestimmten
Forschungsfeld und somit ein guter Startpunkt, um ein Netzwerk aus
Personen zu bilden, die an denselben Forschungsthemen interessiert sind
wie Sie selbst.

------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink") 


