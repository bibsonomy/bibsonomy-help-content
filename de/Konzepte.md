<!-- en: Concepts -->

## Konzepte
-----------------------------

### Was sind Konzepte?

Durch Konzepte können Sie Ihre Tags in Gruppen organisieren. Sie können
beispielsweise den Tag *"Europa"* als Supertag nehmen (also als Name für
das Konzept), und diesem dann die Tags *"Deutschland"*, *"Frankreich"*
und *"Italien"* als Subtags zuordnen. Auf diese Weise werden diese Tags
gruppiert. Das heißt, wenn Sie mit dem Konzept *"Europa"* suchen,
bekommen Sie als Ergebnis alle Lesezeichen/Publikationen, die mit einem
der Subtags getagged wurden (also *"Deutschland"*, *"Frankreich"* oder *"Italien"*).  
Diese Funktion ist nützlich, wenn Sie oft nach Lesezeichen/Publikationen
zu einem bestimmten Thema suchen.  

-------------------------

### Konzepte ansehen

Um Ihre Konzepte anzusehen, klicken Sie auf **mein ${project.name}** im [ linken Hauptmenü](Programmoberfläche "wikilink"), dann auf **Konzepte**. Sie sehen alle Ihre Konzepte mit den dazugehörigen Subtags.  

![ <File:view.png>](konzepte/view.png  "fig: File:view.png")  

Um Konzepte, die gerade **beliebt** in ${project.name} sind, zu sehen, klicken Sie auf **Beliebt** im [ linken Hauptmenü](Programmoberfläche "wikilink"), dann auf **Konzepte**. Sie sehen die populärsten Konzepte und ihre Subtags.  

-------------------------

### Hinzufügen/Bearbeiten von Konzepten

 Um ein neues Konzept hinzuzufügen oder ein bereits existierendes Konzept zu bearbeiten, klicken Sie auf das **Personensymbol** im [ rechten Hauptmenü](Programmoberfläche "wikilink"), dann auf **Tags bearbeiten**.  

Unter **Subtags zu Konzepten hinzufügen** können Sie ein neues Konzept hinzufügen oder neue Tags zu einem bereits bestehenden Konzept hinzufügen. Um ein neues Konzept hinzuzufügen, geben Sie den gewünschten Tag, der als Name des Konzepts stehen soll, in das Feld 'Supertag' ein und den Tag, der diesem Konzept hinzugefügt werden soll, in das Feld 'Subtag'. Wiederholen Sie diesen Schritt für alle Tags, die dem Konzept hinzugefügt werden sollen.  

Unter **Subtags von Konzepten löschen** können Sie Subtags von bereits bestehenden Konzepten löschen. Dafür geben Sie den Namen des Konzepts in das Feld 'Supertag' ein und den Tag, der gelöscht werden soll, in das Feld 'Subtag'.  

![ <File:edit.png>](konzepte/edit.png  "fig: File:edit.png")  

-------------------------

### Navigation mit Konzepten 
Um mit Konzepten zu suchen, benutzen Sie einfach die Suchleiste rechts oben.
Klicken Sie auf den **blauen Pfeil** neben 'Suche' und wählen Sie im Dropdown-Menü **Konzepte**.
Geben Sie den Namen des Konzepts, mit dem Sie suchen möchten, in das Suchfeld ein
und klicken Sie auf das **Lupensymbol** oder drücken Sie die **Enter-Taste**.  

![ <File:search.png>](konzepte/search.png  "fig: File:search.png")  

Die Lesezeichen/Publikationen, die mit einem der Subtags des Konzepts
getagged worden sind, werden angezeigt.  
In der Sidebar auf der rechten Seite werden alle Subtags, die dem
Supertag des Konzepts (dem Namen) zugeordnet sind, angezeigt.

------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink") 


