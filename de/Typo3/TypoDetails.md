<!-- en: Typo3/TypoDetails-->



---------------------------

[`Zurück zur Typo3-Übersicht geht es hier.`](Typo3 "wikilink")

---------------------

## Typo3 - Detail-Seite
------------------

Nachdem Sie die Typo3-Erweiterung importiert und aktiviert haben, können Sie
an den Einstellungen arbeiten.
Der Tab **"Detail-Seite"** bietet Ihnen dazu folgende Möglichkeit:

--------------------

![ <File:plugindetails.jpg>](TypoDetails/plugindetails.jpg "fig: File:plugindetails.jpg") 

---------------------------------

Wenn Sie die Box ankreuzen, wird das Plugin unter jedem Publikationseintrag einen Link zu einer Detail-Seite erstellen. Diese Seite wird dann spezielle Informationen anzeigen.

---------------------


