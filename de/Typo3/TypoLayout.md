<!-- en: Typo3/TypoLayout-->



---------------------------

[`Zurück zur Typo3-Übersicht geht es hier.`](Typo3 "wikilink")

---------------------

## Typo3 - Layout
------------------

Nachdem Sie die Typo3-Erweiterung importiert und aktiviert haben, können Sie
an den Einstellungen arbeiten.
Der Tab **"Layout"** bietet mehrere Möglichkeiten, um das Layout der Publikationsliste zu verändern.

--------------------

![ <File:pluginlayout1.jpg>](TypoLayout/pluginlayout1.jpg  "fig: File:pluginlayout1.jpg") 

---------------------------------

1. Zuerst können Sie Ihren bevorzugten Zitations-Stil für die Publikationliste auswählen. Dazu existiert bereits eine lange Liste.
2. Anschließend wählen Sie Ihre bevorzugte Sprache zur Darstellung der Publikationen.
3. Sie können alternativ auch Ihren Stil per Citation Style Language (CSL) anpassen. Dies ist eine open source XML-basierte Markup-Sprache. Eine lange Liste freiverfügbarer Stylesheets ist [hier](http://www.zotero.org/styles/) verfügbar.
4. Dies zwei Boxen erlauben es Ihnen, Links anzuzeigen. Diese führen zur Zusammenfassung (abstract) der Publikationen, bzw. zum BibTeX-Export und erscheinen unter jeder Publikation.

-------------------------

![ <File:pluginlayout2.jpg>](TypoLayout/pluginlayout2.jpg  "fig: File:pluginlayout2.jpg") 

-------------------------------

1. Gleiches ist auch mit EndNote, der Publikations-URL, einem DOI-Link und einem Link zum Post im Publikations-Host möglich (z.B. ${project.name}).
2. Ein experimenteller Modus, der es erlaubt, persönliche Webseiten der Autoren zu erkennen und zu verlinken. Dazu müssen die Links direkt in der Publikation erscheinen.
3. Auch können Sie die persönlichen Webseiten der Autoren mit E-Mailadressen angeben.

-------------------------

![ <File:pluginlayout3.jpg>](TypoLayout/pluginlayout3.jpg  "fig: File:pluginlayout3.jpg") 

-------------------------------

1. Möchten Sie Notizen der Publikationen anzeigen, dann setzen Sie hier ein Kreuz.
2. In diesem Feld können Sie eine Liste an Keys angeben, die URLs beinhalten müssen. Diese werden dann als Links dargestellt.
3. Wählen Sie, ob und wie Vorschaubilder und Links zu privaten Dokumenten innerhalb Ihrer Publikationen angezeigt werden sollen. (Beachten Sie die Warnung auf der [Login-Seite](Typo3/TypoLogin "wikilink").)
4. Wie Backslashes und geschweifte Klammern behandelt werden, können Sie hier bestimmen. BibTeX kann zudem Sonderzeichen bereinigen.
5. Schließlich können Sie Layout-Modifikatoren mit CSS definieren.

-------------------------

