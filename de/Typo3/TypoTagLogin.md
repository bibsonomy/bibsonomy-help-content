<!-- en: Typo3/TypoTagLogin -->



---------------------------

[`Zurück zur Typo3-Übersicht geht es hier.`](Typo3 "wikilink")

---------------------

## Typo3 - Tags Login
------------------

Nachdem Sie die Typo3-Erweiterung importiert und aktiviert haben, können Sie an den Einstellungen arbeiten. 

-------------------------

![ <File:tagcloudlogin.png>](TypoTagLogin/tagcloudlogin.png  "fig:File:tagcloudlogin.png")  

-------------------------

Hier müssen Sie Ihre ${project.name} API Zugangsdaten eingeben. Diese finden Sie [hier](${project.home}settings "wikilink").

---------------------------------------