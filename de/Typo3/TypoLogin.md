<!-- en: Typo3/TypoLogin-->



---------------------------

[`Zurück zur Typo3-Übersicht geht es hier.`](Typo3 "wikilink")

---------------------

## Typo3 - Login
--------------------

Nachdem Sie die Typo3-Erweiterung importiert und aktiviert haben, können Sie
an den Einstellungen arbeiten.
Hier müssen Sie Ihre ${project.name} API Zugangsdaten eingeben. Diese finden Sie [hier](${project.home}settings "wikilink").

--------------------

![ <File:pluginlogin.jpg>](TypoLogin/pluginlogin.jpg "fig: File:pluginlogin.jpg") 

---------------------

**WARNUNG:** Bitte beachten Sie, dass die Erweiterung Ihre gesamte
Publikationsliste darstellt, wie Sie sie in ${project.name} sehen (auch
**private** Publikationen). Wenn Sie z. B. an eine Publikation **ein
Dokument angehängt** haben, dann wird dieses auch in der
Publikationsliste angezeigt. Dadurch können für Sie urheberrechtliche
Konsequenzen entstehen. Wir empfehlen Ihnen daher für die Nutzung einen
zusätzlichen Account anzulegen, mit dem Sie in ${project.name} nur die
TYPO3-Publikationen verwalten.

-----------------------



