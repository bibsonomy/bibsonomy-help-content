<!-- en: Typo3/TypoContent-->



---------------------------

[`Zurück zur Typo3-Übersicht geht es hier.`](Typo3 "wikilink")

---------------------

## Typo3 - Inhalte auswählen
------------------

Nachdem Sie die Typo3-Erweiterung importiert und aktiviert haben, können Sie
an den Einstellungen arbeiten. Der Tab **"Inhalte auswählen"** bietet dazu mehrere Möglichkeiten.

--------------------

![ <File:plugininhalt.png>](TypoInhalt/plugininhalt.png  "fig: File:plugininhalt.png") 

---------------------------------

1. Quelle für Inhalte: Wählen Sie hier die Quelle, aus der das Plugin Daten bekommen soll. Diese wird es nutzen um eine Publikationsliste zu erstellen. Sie können einen bestimmten Nutzer, eine Gruppe oder einen Viewable auswählen. 
2. Geben Sie nun die ID der vorherigen Wahl an.

   
    **Beispiel:** Möchten Sie die Publikationen der Gruppe "testgroup" anzeigen, dann        wählen Sie zuerst "group" und geben dann die ID "testgroup" an.
    
3. Nun können Sie Ihre Auswahl per Tags filtern. Um nur eigene Publikationen anzuzeigen, geben Sie den Tag **"myown"** an. Wenn Sie mehr als einen Tag nutzen wollen, trennen Sie diese durch einen space character. Möglich ist es auch, die Anzahl der Publikationen zu limitieren oder nach freiem Text zu filtern. 

4. Zuletzt haben Sie auch die Möglichkeit, den Inhalt per Volltextsuche zu filtern.

-------------------------------------