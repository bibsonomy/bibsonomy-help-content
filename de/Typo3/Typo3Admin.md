<!-- en: Typo3/Typo3Admin -->



---------------------------

[`Zurück zur Typo3-Übersicht geht es hier.`](Typo3 "wikilink")

---------------------

## Typo3 - Administration
------------------

Auf dieser Seite finden Sie Hilfestellungen zur Administration des Typo3-Plugins. Dies beinhaltet Anleitungen zur Installation der Erweiterung und zur Verwaltung von CSL-Styles.

-------------------

### Installation

Um BibSonomy CSL zu installieren, melden Sie sich bei ihrer
Typo3-Instanz als Administrator an und suchen Sie über **Extension
Manager =&gt; Import Extensions** nach der Erweiterung
**ext\_bibsonomy\_csl** und importieren Sie diese.  
![ <File:1.png>](Typo3Admin/1.png  "fig: File:1.png")  
Nach erfolgreichem Import wird die Erweiterung unter **"Verfügbare
Extensions"** angezeigt. Klicken Sie auf das **"+"-Symbol**, um es zu
aktivieren.  

![ <File:2.png>](Typo3Admin/2.png  "fig: File:2.png")  

------------------------

### CSL-Styles mit dem Backend Module verwalten

Typo3-Erweiterungen sind klassisch in Frontend- und Backend-Module
unterteilt. Die Frontend-Module **"Publikationsliste hinzufügen"** und
**"Tagcloud hinzufügen"** wurden bereits erklärt. In diesem Abschnitt
soll es um das Backend-Modul gehen, mit dem Sie die CSL-Stylesheets
verändern können.

Nach der Installation der Erweiterung haben Sie die Möglichkeit, eine Reihe von
bereits hinzugefügten, vorinstallierten CSL-Stylesheets zu verwenden. Um weitere Stylesheets hinzuzufügen,
erstellen Sie einen neuen Ordner im Seitenbaum und nennen diesen **"CSL Styles"**. Wählen Sie diesen Ordner dann aus, klicken Sie auf **"Neu"**
und wählen Sie **"Add a custom style"**.

Es gibt drei Wege, um neue Stylesheets hinzuzufügen.

1.  Geben Sie den XML-Quellcode direkt in das Textfeld ein und klicken
    Sie dann auf **"Save"**.
2.  Geben Sie in das Textfeld die URL des CSL-Stylesheets ein und
    klicken Sie auf **"Import"**.
3.  Laden Sie ein CSL-Stylesheet von Ihrem Computer hoch und klicken Sie
    auf **"Upload"**.

![ <File:9.png>](Typo3Admin/9.png  "fig: File:9.png")  
Um eine Vorschau zu erhalten, klicken Sie auf **"Show Styles"**.  

![ <File:10.png>](Typo3Admin/10.png  "fig: File:10.png")  
CSL-Stylesheets **löschen** Sie wie gewohnt über das **Papierkorbsymbol**, das sich auf der
linken Seite der Einträge befindet.

---------------------------------------