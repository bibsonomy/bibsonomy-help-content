<!-- en: Typo3/Typo3Publist -->



---------------------------

[`Zurück zur Typo3-Übersicht geht es hier.`](Typo3 "wikilink")

---------------------

## Typo3 - Publikationslisten
------------------

Nachdem Sie die Typo3-Erweiterung importiert und aktiviert haben, können Sie
Publikationslisten erstellen.

----------------------------

Wenn Sie nur Hilfe zu den verschiedenen Tabs benötigen, besuchen Sie bitte unsere Hilfeseiten: 
[Inhalte auswählen](Typo3/TypoInhalt "wikilink"), [Gruppieren und Sortieren](Typo3/TypoGroupsort "wikilink"), [Layout](Typo3/TypoLayout "wikilink"), [Detail-Seite](Typo3/TypoDetails "wikilink") and [API-Zugang](Typo3/TypoLogin "wikilink").)

--------------------------

Um eine Publikationsliste zu erstellen, folgen Sie bitte diesen Schritten:

Fügen Sie dazu der Seite, auf der die
Publikationsliste erscheinen soll, ein neues Plugin hinzu - wählen Sie
aus der Liste **"${project.name} Publication List"**.

--------------------------

Wählen Sie anschließend **"Publication List from ${project.name}"** aus der Liste aus. 

![ <File:selectplugin.png>](Typo3Publist/selectplugin.png  "fig: File:selectplugin.png")  

--------------------------

Nachdem Sie die Liste korrekt in Ihre Seite eingefügt haben, haben Sie zahlreiche Möglichkeiten, das Erscheinungsbild und die Funktion anzupassen.
Es gibt fünf Tabs, die jeweils mehrere Einstellungen bieten.

![ <File:pluginoptions.jpg>](Typo3Publist/pluginoptions.jpg  "fig: File:pluginoptions.jpg") 

--------------------------------------

Wenn Sie Hilfe zu den verschiedenen Tabs benötigen, besuchen Sie bitte unsere Hilfeseiten: 
[Inhalte auswählen](Typo3/TypoInhalt "wikilink"), [Gruppieren und Sortieren](Typo3/TypoGroupsort "wikilink"), [Layout](Typo3/TypoLayout "wikilink"), [Detail-Seite](Typo3/TypoDetails "wikilink") and [API-Zugang](Typo3/TypoLogin "wikilink").

**Achtung:** Manche der Einstellungen sind nötig, damit das Plugin problemfrei funktionieren kann.

-------------------------

Es könnte zuletzt nützlich sein, der Publikationsliste einen Titel als Überschrift zu vergeben. Dies ist nur im Tab **"Allgemein"** möglich.

![ <File:plugingeneral.jpg>](Typo3Publist/plugingeneral.jpg  "fig: File:plugingeneral.jpg")  

------------------------------

**WARNUNG:** Bitte beachten Sie, dass die Erweiterung Ihre gesamte
Publikationsliste darstellt, wie Sie sie in ${project.name} sehen (auch
**private** Publikationen). Wenn Sie z. B. an eine Publikation **ein
Dokument angehängt** haben, dann wird dieses auch in der
Publikationsliste angezeigt. Dadurch können für Sie urheberrechtliche
Konsequenzen entstehen. Wir empfehlen Ihnen daher für die Nutzung einen
zusätzlichen Account anzulegen, mit dem Sie in ${project.name} nur die
TYPO3-Publikationen verwalten.

-----------------------