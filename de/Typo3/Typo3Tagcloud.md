<!-- en: Typo3/Typo3Tagcloud -->



---------------------------

[`Zurück zur Typo3-Übersicht geht es hier.`](Typo3 "wikilink")

---------------------

## Typo3 - Tag Clouds
------------------

Sie können Ihren Webseiten mit dem Frontend-Plugin nicht nur Publikationslisten, sondern auch Ihre ${project.name} Tagcloud hinzufügen.
Fügen Sie dazu der Seite, auf der die Tagcloud erscheinen soll, ein neues Plugin hinzu. Wählen Sie dazu aus der Liste **"Tag Cloud from ${project.name}"**.  

---------------------

![ <File:tagcloudplugin.png>](Typo3Tagcloud/tagcloudplugin.png  "fig: File:tagcloudplugin.png")  

-----------------------

Wie bei ["Publikationsliste hinzufügen"](Typo3/Typo3Publist "wikilink") können Sie auch hier zwischen verschiedenen Modi wählen.  

Zudem können Sie die Tag Cloud an Ihre spezifischen Bedürfnisse anpassen. Besuchen Sie dazu die Anleitungen zu den Tabs [Content Selection](Typo3/TypoTagContent "wikilink") und [Login](Typo3/TypoTagLogin "wikilink").

---------------------------

Das Ergebnis kann beispielsweise so aussehen:

![ <File:tagcloudex.png>](Typo3Tagcloud/tagcloudex.png  "fig: File:tagcloudex.png")  

---------------------------------------