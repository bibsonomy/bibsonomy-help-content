<!-- en: Typo3/TypoTagContent -->



---------------------------

[`Zurück zur Typo3-Übersicht geht es hier.`](Typo3 "wikilink")

---------------------

## Typo3 - Tags Content Selection
------------------

Nachdem Sie die Typo3-Erweiterung importiert und aktiviert haben, können Sie an den Einstellungen arbeiten. Der Tab **"Inhalte auswählen"** bietet dazu mehrere Möglichkeiten.

-------------------------

![ <File:tagcloudcontent.png>](TypoTagContent/tagcloudcontent.png  "fig:File:tagcloudcontent.png")  

-------------------------

Wählen Sie zuerst die Quelle für die Inhalte der Tag Cloud. Dies kann ein bestimmter Nutzer oder eine Gruppe sein. Geben Sie dann die zugehörige ID an.
Anschließend können Sie die Auswahl der angezeigten Tags einschränken, indem Sie nur bestimmte Tags anzeigen lassen, manche Tags ignorieren und die Anzahl der Tags beschränken.

---------------------------------------