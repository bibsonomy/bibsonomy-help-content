<!-- en: Typo3/TypoGroupsort-->



---------------------------

[`Zurück zur Typo3-Übersicht geht es hier.`](Typo3 "wikilink")

---------------------

## Typo3 - Gruppieren und Sortieren
--------------------

Nachdem Sie die Typo3-Erweiterung importiert und aktiviert haben, können Sie
an den Einstellungen arbeiten. Der Tab **"Gruppieren und Sortieren"** bietet Ihnen dazu folgende Möglichkeiten:

--------------------

![ <File:plugingrouping.jpg>](TypoGroupSort/plugingrouping.jpg "fig: File:plugingrouping.jpg") 

---------------------------------

1. Wählen Sie zuerst, ob Sie die Publikationen gruppieren möchten und falls ja, nach welcher Kategorie. Sie können zwischen "nichts", "Jahr (aufsteigend)", "Jahr (absteigend)", "Publikationstyp" und "DBLP" wählen.
2. Dann müssen Sie entscheiden, nach was die Publikationen sortiert werden sollen. Falls gruppiert, werden SIe innerhalb der Gruppe sortiert. Wählen Sie aus "Jahr", "Titel", "Autor", "Publikationstyp" und "Monat".
3. Hier können Sie die Sortierungsreihenfolge ändern (absteigend oder aufsteigend).
4. Möchten Sie nur Publikationen eines bestimmten Jahres anzeigen lassen, können Sie dies hier angeben.
5. Die folgenden Boxen können angekreuzt werden, um nicht-numerische Jahresangaben und Publikationsfilter zu erlauben.
6. Zuletzt ist es möglich, die Härte des Filters zu bestimmen. So können Duplikate entfernt werden.

--------------------------

Mehr Informationen zur Benutzung von Hashes bei ${project.name} gibt es auf der [Hilfsseite](InterIntraHash "wikilink").