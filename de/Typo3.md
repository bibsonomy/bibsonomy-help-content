<!-- en: Typo3 -->

## In Typo3 einbinden
------------------

Wenn Sie Typo3 in der Version 1.0 bis 5.9 verwenden, dann können Sie die
[BibSonomy Typo3-Erweiterung](http://typo3.org/extensions/repository/view/ext_bibsonomy) nutzen, um Publikationslisten aus ${project.name} in Ihre Webseite zu
integrieren. In Typo3 v6.0 gab es viele bedeutende Änderungen am
Typo3-System. Daher haben wir eine komplett neue Erweiterung entwickelt,
die auf Typo3 Flow basiert. Bitte nutzen Sie für alle Typo3-Instanzen ab
v6.0 die folgende Erweiterung: [BibSonomy CSL (ext\_bibsonomy\_csl)](http://typo3.org/extensions/repository/view/ext_bibsonomy_csl).

Mit der Erweiterung können Sie nicht nur Publikationslisten in Typo3
laden, Sie können auch das Aussehen mittels CSL-Stylesheets (Citation
Style Language) definieren. Damit Sie schnell starten können, haben wir
der Erweiterung bereits ein paar vordefinierte CSL-Stylesheets hinzugefügt.
Sie können die Stylesheets aber auch gerne jederzeit anpassen oder aber eigene
entwickeln. Außerdem haben Sie ebenfalls die Möglichkeit, Ihre Tagcloud aus
${project.name} in Ihre Webseite integrieren.

-----------------------------------------

### Administration

Nachdem Sie das Typo3-Plugin installiert haben, gibt es mehrere Möglichkeiten zur Verwaltung. Informationen zur Installation und zur Verwaltung von CSL-Styles finden Sie darum [hier](Typo3/Typo3Admin "wikilink").

------------------------------------------------------------------------

### Publikationslisten hinzufügen mit dem Frontend Plugin

Nachdem Sie die Erweiterung importiert und aktiviert haben, können Sie
Publikationslisten erstellen. [Diese Anleitung](Typo3/Typo3Publist "wikilink") kann Ihnen dabei behilflich sein.

------------------------------------------------------------------------

### Tagcloud hinzufügen mit dem Frontend Plugin

Sie können Ihren Webseiten mit dem Frontend-Plugin nicht nur Publikationslisten, sondern auch Ihre ${project.name} Tagcloud hinzufügen. Folgen Sie dazu [diesen Schritten](Typo3/Typo3Tagcloud "wikilink").

------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink") 