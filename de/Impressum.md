<!-- en: Contact -->

## Impressum
---------

**_Hinweis:_** Wenn Sie Hilfe bei der Verwendung von BibSonomy benötigen, besuchen Sie bitte zuerst die [Hilfeseiten](Übersicht "wikilink"). Die folgenden Kontaktoptionen sind hierfür nicht vorgesehen. Wenn Sie jedoch weitere Fragen haben, können Sie uns gerne per E-Mail ${project.email} erreichen.

${project.theme == "bibsonomy"}{

BibSonomy ist ein gemeinsam betriebener Bookmarking-Dienst folgender Einrichtungen:

- [Fachgebiet Wissensverarbeitung](https://www.kde.cs.uni-kassel.de/) der Universität Kassel, Deutschland
- [Data Mining und Information Retrieval Gruppe](http://www.dmir.uni-wuerzburg.de/) der Universität Würzburg, Deutschland
- [Institut für Bibliotheks- und Informationswissenschaft](https://www.ibi.hu-berlin.de/de) Information Processing and Analytics, Humboldt-Universität zu Berlin, Deutschland  

Wir sind nicht verantwortlich für den Inhalt unserer Nutzer.  

Verantwortlich für die Webseite und den Dienst sind:  

Julius-Maximilians-Universität Würzburg (Körperschaft des öffentlichen Rechts)  
Am Hubland  
D-97074  Würzburg  
Tel: +49 931 / 31 - 89675  
Fax: +49 931 / 31 - 86732  
${project.email}

Vertretungsberechtigter  
für Data Mining und Information Retrieval Gruppe am LS VI  
Prof. Dr. Andreas Hotho  

Umsatzsteuer-Identifikationsnummer:  
Die Umsatzsteuer-Identifikationsnummer gemäß § 27a Umsatzsteuergesetz lautet DE 134187690  

Zuständige Aufsichtsbehörde:  
Bayerisches Staatsministerium für Wissenschaft und Kunst  
Briefanschrift:  
Salvatorstraße 2  
80327 München  

--------------------------------

Universität Kassel (Körperschaft des öffentlichen Rechts)  
Wilhelmshöher Allee 73  
34121 Kassel  
Tel.: +49 561 / 804 - 6250  
Fax: +49 561 / 804 - 6259  
${project.email}

Vertretungsberechtigter  
Fachbereich Elektrotechnik/Informatik  
Fachgebiet Wissensverarbeitung  
Prof. Dr. Gerd Stumme  

Umsatzsteuer-Identifikationsnummer:  
Die Umsatzsteuer-Identifikationsnummer gemäß § 27a Umsatzsteuergesetz lautet DE 113057055  

Zuständige Aufsichtsbehörde:  
Hessisches Ministerium für Wissenschaft und Kunst  
Rheinstraße 23-25  
65185 Wiesbaden  

----------------------------------

Humboldt-Universität zu Berlin (Körperschaft des öffentlichen Rechts)  
Unter den Linden 6  
10099 Berlin  
Telefon: +49 30 / 2093 – 0  
Fax: +49 30 / 2093 – 2770  

Umsatzsteuer-Identifikationsnummer:  
Die Umsatzsteuer-Identifikationsnummer gemäß § 27a Umsatzsteuergesetz lautet DE 137176824   

------------------------------------------------------------------------

### Adressen

[Prof. Dr. Andreas Hotho](http://www.dmir.uni-wuerzburg.de/staff/hotho/)  
Julius-Maximilians-Universität Würzburg  
Data Mining und Information Retrieval Gruppe am LS VI  
Am Hubland  
D-97074 Würzburg  
Tel: +49 931 / 31 - 89675  
Fax: +49 931 / 31 - 86732  
${project.email}

[Prof. Dr. Robert Jäschke](https://amor.cms.hu-berlin.de/~jaeschkr/)  
Humboldt-Universität zu Berlin
Institut für Bibliotheks- und Informationswissenschaft 
Sachgebiet Information Processing and Analytics
Dorotheenstraße 26  
10099 Berlin  
Phone.: +49 30 / 2093 - 70960  
Fax: +49 30 / 2093 - 4335  
${project.email}

[Prof. Dr. Gerd Stumme](http://www.kde.cs.uni-kassel.de/stumme)  
Universität Kassel  
Fachbereich Elektrotechnik/Informatik  
Fachgebiet Wissensverarbeitung  
Wilhelmshöher Allee 73  
34121 Kassel  
Tel.: +49 561 / 804 - 6250  
Fax: +49 561 / 804 - 6259  
${project.email}

------------------------------------------------------------------------

### Mailinglisten

Folgende Mailinglisten wurden für dieses Projekt eingerichtet.


**BibSonomy Discuss**  
Für BibSonomy-Nutzer, auch für Fragen zum JabRef-Plugin

[Abonnieren](https://mail.cs.uni-kassel.de/mailman/listinfo/bibsonomy-discuss)

[Deabonnieren](https://mail.cs.uni-kassel.de/mailman/listinfo/bibsonomy-discuss)

[Schreiben](mailto:bibsonomy-discuss@cs.uni-kassel.de)

Archiv: [mail.cs.uni-kassel.de](https://mail.cs.uni-kassel.de/pipermail/bibsonomy-discuss)

Archiv: [mail.cs.uni-kassel.de](https://mail.cs.uni-kassel.de/pipermail/api-discuss)

**Genealogie**  
Liste für Nutzer der BibSonomy-Genealogie

[Abonnieren](https://mail.cs.uni-kassel.de/mailman/listinfo/genealogie)

[Deabonnieren](https://mail.cs.uni-kassel.de/mailman/listinfo/genealogie)

[Schreiben](mailto:genealogie@cs.uni-kassel.de)

Archiv: [mail.cs.uni-kassel.de](https://mail.cs.uni-kassel.de/pipermail/genealogie/)

------------------------------------------------------------------------

### Repository

BibSonomy wird auf [Bitbucket](https://bitbucket.org) verwaltet. Es ist Teil des Repositorys des [BibSonomy-Teams](https://bitbucket.org/bibsonomy/).

-   [Repository](https://bitbucket.org/bibsonomy/bibsonomy)
-   [Quellcode](https://bitbucket.org/bibsonomy/bibsonomy/src)
-   [Issues (Probleme)](https://bitbucket.org/bibsonomy/bibsonomy/issues?status=new&status=open)
-   [Wiki](https://bitbucket.org/bibsonomy/bibsonomy/wiki/Home)
-   [Lizenzen](https://bitbucket.org/bibsonomy/bibsonomy/wiki/License)

}

${project.theme == "puma"}{

Dieser Service wird von der [Universitätsbibliothek der Universität Kassel](http://www.ub.uni-kassel.de/) betrieben und wurde in Kooperation mit dem [Fachgebiet
Wissensverarbeitung](http://www.kde.cs.uni-kassel.de/) in Kassel und der DMIR Gruppe der [Universität Würzburg](http://www.dmir.uni-wuerzburg.de/) entwickelt.

Mit PUMA betreiben wir nur einen Bookmarking-Dienst. Wir sind nicht verantwortlich für den Inhalt unserer Nutzer.

------------------------------------------------------------------------

### Adressen

Universitätsbibliothek Kassel  
Diagonale 10 34127 Kassel  
Telefon: 0561-804-2117  
Telefax: 0561-804-2125  
E-Mail: direktion(at)bibliothek.uni-kassel.de

Internet: [www.ub.uni-kassel.de](http://www.ub.uni-kassel.de)


Die Universitätsbibliothek Kassel ist eine Zentrale Betriebseinheit der
Universität Kassel, vertreten durch Dr. Axel Halle, Leitender
Bibliotheksdirektor. Gesetzlicher Vertreter der Universität Kassel ist
der Präsident Prof. Dr. Rolf-Dieter Postlep.

Zuständige Aufsichtsbehörde:

Hessisches Ministerium für Wissenschaft und Kunst  
Rheinstr. 23-25  
65185 Wiesbaden

------------------------------------------------------------------------

### Mailinglisten

Folgende Mailinglisten wurden für dieses Projekt eingerichtet. Die Listen gehören zwar zum Schwesternprojekt von PUMA, BibSonomy, sind jedoch auch für PUMA gültig.


**BibSonomy Discuss**  
Für BibSonomy-Nutzer, auch für Fragen zum JabRef-Plugin 

[Abonnieren](https://mail.cs.uni-kassel.de/mailman/listinfo/bibsonomy-discuss)

[Deabonnieren](https://mail.cs.uni-kassel.de/mailman/listinfo/bibsonomy-discuss)

[Schreiben](mailto:bibsonomy-discuss@cs.uni-kassel.de)

Archiv: [mail.cs.uni-kassel.de](https://mail.cs.uni-kassel.de/pipermail/bibsonomy-discuss)

**Genealogie**  
Liste für Nutzer der BibSonomy-Genealogie

[Abonnieren](https://mail.cs.uni-kassel.de/mailman/listinfo/genealogie)

[Deabonnieren](https://mail.cs.uni-kassel.de/mailman/listinfo/genealogie)

[Schreiben](mailto:genealogie@cs.uni-kassel.de)

Archiv: [mail.cs.uni-kassel.de](https://mail.cs.uni-kassel.de/pipermail/genealogie/)

------------------------------------------------------------------------

### Repository

PUMA wird auf [Bitbucket](https://bitbucket.org) verwaltet. Es ist Teil des Repositorys des [BibSonomy-Teams](https://bitbucket.org/bibsonomy/).

-   [Repository](https://bitbucket.org/bibsonomy/puma/overview)
-   [Quellcode](https://bitbucket.org/bibsonomy/puma/src)
-   [Issues (Probleme)](https://bitbucket.org/bibsonomy/puma/issues?status=new&status=open)
-   [Wiki](https://bitbucket.org/bibsonomy/puma/wiki/Home)
-   [Lizenz](https://bitbucket.org/bibsonomy/puma/src/stable/bibsonomy-webapp-puma/LICENSE.txt)
}

------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink") 