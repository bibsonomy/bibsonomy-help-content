<!-- en: SystemTools -->

## Veraltete Werkzeuge
-------------------

### GoogleSonomy (Firefox Add-on)

**Dieses Add-on ist nur mit Firefox 1.5 bis 3.6 kompatibel.**  
GoogleSonomy ist eine Firefox Erweiterung, die Ihre ${project.name}
Suchergebnisse zusammen mit Ihrer Google Suche anzeigt. In der aktuellen
Version gibt es vier Suchmodi. Globale Volltext- oder Tagsuche, sowie
Nutzer Volltext- oder Tagsuche. Des Weiteren fügt GoogleSonomy einen "In
${project.name} speichern" Link zum schnellen Eintrag in
${project.name} zu jedem Suchergebnis hinzu.

Im Einstellungsfenster unten können Sie wählen, ob Sie nur Lesezeichen
oder Publikationen oder beides angezeigt bekommen möchten. Außerdem ist
es möglich, die Anzahl der Listenelemente einzustellen. Die nötige
Erweiterung können Sie auf der [Mozilla Addon Seite](https://addons.mozilla.org/de/firefox/addon/8855) herunterladen .

---------------------------

### BibTeX-Einträge aus einer LaTeX-Aux-Datei holen

**Das Perl-Skript ist leider nicht mehr verfügbar.**  
Der Perl-Skript, den Sie unten downloaden können, holt alle
referenzierten BibTeX-Einträge in einer bestimmten LaTeX-Datei in
aux-Format. Dann fragt er die korrespondierenden BibTeX-Einträge von
${project.name} ab.

Befehl zum Start des Skripts:

`grabbib.pl Benutzername Name der LaTeX aux-Datei > bibtexfile.bib`

[ Download](${project.home}help_de/tools/toolbox/downloads/grabbib.pl "wikilink")

-------------------------

### Integration in Gnome-Deskbar

**Die Entwicklung der Gnome-Deskbar wurde 2010 eingestellt.**  
Das Ziel der Deskbar ist es, eine Suchschnittstelle zu verschiedenen
Suchsystemen wie Amazon, eBay oder Ihren Dateisystem zu bieten. Sie
können ${project.name} in der [Gnome Deskbar](http://raphael.slinckx.net/deskbar/) mit dem [ Gnome Deskbar
Handler](${project.home}help_de/attach/Werkzeug/bibsonomy.py "wikilink") für ${project.name}, unter Lizenz des GPL 2, integrieren.

-   Skript downloaden.
-   Skript nach `$HOME/.gnome2/deskbar-applet/handlers` verschieben.
-   Rechts-Klick auf das Deskbar-Element und
    ${project.name}-Lesezeichen aktivieren.
-   *more* anklicken. Es erscheint ein Menü, in das Sie Ihren
    ${project.name}-Benutzernamen eingeben müssen (siehe Bild unten).

[ Gnome Deskbar Handler](${project.home}help_de/attach/Werkzeug/Deskbar.png "wikilink")

Wenn Sie einen Begriff in die Deskbar eingeben, kann es einige Sekunden
dauern, bis das Ergebnis in ${project.name} erscheint.

--------------------------

### CiteSmart

**Die Webseite von CiteSmart ist nicht mehr verfügbar.**  
[CiteSmart](http://www.miresoft.net/citesmart/) ist eine Zitatsoftware,
die von ${project.name} unterstützt wird. Diese Software bietet einen
einfachen Weg zum Übernehmen der Dateien aus unserer Webseite und zum
Verschaffen der Referenzen für Artikel, die in Word geschrieben sind.
Beispielsweise zum Verfassen wissenschaftlicher Artikel, aber auch im
gesamten Forschungsprozess, wird somit ein unkomplizierteres Arbeiten
ermöglicht.

------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink") 


