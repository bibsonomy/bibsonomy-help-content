<!-- en: Team -->

## Team
-------

Wir arbeiten ohne Unterlass, um Ihnen den besten kostenlosen und freien webbasierten Dienst zu bieten, der ihre Lesezeichen und Publikationen organisiert.

----------------------

### Andreas Hotho

![<File:http://www.bibsonomy.org/picture/user/hotho>](http://www.bibsonomy.org/picture/user/hotho  " File:http://www.bibsonomy.org/picture/user/hotho")

[ Andreas Hotho](${project.home}user/hotho "wikilink") ist Projektleiter von
BibSonomy. Sein Master-Diplom in Informationssystemen erwarb er sich an der
Universität Braunschweig 1998. Von 1999 bis 2004 arbeitete er am Institut für
Angewandte Informatik und Formale Beschreibungsverfahren (AIFB) an der
Universität Karlsruhe in den Bereichen des Text-, Data- und Web-Minings, des
Semantic Webs und des Information Retrievals, wo er auch promovierte. Seit 2009
ist er Professor an der Julius-Maximilians-Universität Würzburg. Sein
Schwerpunkt ist die Kombination von Machine Learning/Data Mining mit dem
Semantic Web, das sogenannte Semantic Web Mining, und insbesondere das Text
Clustering/Classification mit Hintergrundwissen. Er war eingebunden in die
Organisation verschiedener Workshops auf dem Gebiet von Knowledge Discovery und
Ontology Learning, in Verbindung mit den ECML/PKDD- und KDD-Konferenzen.

--------------------------

### Robert Jäschke

![https://amor.cms.hu-berlin.de/~jaeschkr/img/me.jpg](https://amor.cms.hu-berlin.de/~jaeschkr/img/me.jpg  " http://www.kbs.uni-hannover.de/~jaeschke/img/me.jpg")

[ Robert Jäschke](${project.home}user/jaeschke "wikilink") ist ehemaliger Leiter
unseres Entwicklerteams und weiterhin innovative Kraft für die Weiterentwicklung
von BibSonomy. Sein Forschungsschwerpunkt ist die Einbindung von Algorithmen zum
Auffinden von Gemeinschaften, Ranking und Empfehlungen in
Folksonomie-Systeme. Er ist Professor für *Information Processing and Analytics*
an der Humboldt-Universität zu Berlin. 

------------------------

### Gerd Stumme

![ <File:Stumme.jpg>](team/Stumme.jpg  " File:Stumme.jpg")

[ Gerd Stumme](${project.home}user/stumme "wikilink") leitet den
Hertie-Stiftungslehrstuhl Wissensverarbeitung an der Universität Kassel und ist
Vollmitglied des Forschungszentrums L3S. Gerd Stumme promovierte 1997 an der
Technischen Universität Darmstadt und habilitierte 2002 am Institut für
Angewandte Informatik und Formale Beschreibungsverfahren (AIFB) an der
Universität Karlsruhe.  1999/2000 war er Gastprofessor an der Universität
Clermont-Ferrand, Frankreich, und 2003 Vertretungsprofessor für Maschinelles
Lernen und Wissensverarbeitung an der Universität Magdeburg. Gerd Stumme hat
über 100 Artikel anläßlich nationaler und internationaler Konferenzen und in
Zeitschriften veröffentlicht und saß dem Programmkomitee zahlreicher 
Konferenzen vor. Er ist Mitglied der Redaktion des Intl. Journal on Data
Warehousing and Mining und der International Conference on Conceptual Structures
sowie von Programmkomitees mehrerer Konferenzen und Workshops. 

---------------------------

### Daniel Zoller

![ <File:csm_zoller_17d1047376.jpg>](team/csm_zoller_17d1047376.jpg)

[ Daniel Zoller](${project.home}user/nosebrain "wikilink") ist einer der *Senior
Developer* von BibSonomy und Doktorrand am Lehrstuhl für *Data Science* an der
Julius-Maximilians-Universität Würzburg.  Seine Forschungsinteressen liegen in
den Gebieten Empfehlungssysteme und *Information Retrieval*. Er nutzt für seine
Forschung unter anderem Methoden des *Deep Learning* und untersucht Anwendungen
seiner Resultate in *Software Engineering*. 

---------------------------

### Tom Hanika

![ <File:hanika.jpg>](team/hanika_small.jpg)

[ Tom Hanika](${project.home}user/tomhanika "wikilink") ist einer der *Senior
Developer* von BibSonomy und PostDoc am Lehrstuhl für Wissensverarbeitung an der
Universität Kassel. Sein Forschungsschwerpunkt ist die Extraktion von
Implikationswissen aus Daten. Insbesondere finden seine Arbeiten in relationalen
Strukturen Anwendung, wofür Strukturen der angewandeten Algebra nutzt und unter
Zuhilfenahmen von topologischen und geometrischen Methoden weiterentwickelt. Er
ist außerdem *maintainer* und *senior developer* der
[conexp-clj](https://github.com/tomhanika/conexp-clj) Software, einem Werkzeug
zur konzeptuellen Analyse von Daten, basierend auf der Formalen
Begriffsanalyse. 

---------------------------

### Mario Holtmüller

![ <File:Holtmueller.jpg>](team/Holtmueller_small.jpg)

[ Mario Holtmueller](${project.home}user/mho "wikilink") ist einer der *Senior
Developer* von BibSonomy. Dazu ist er die treibende Kraft hinter dem
[PUMA-Projekt](https://www.academic-puma.de/ "https://www.academic-puma.de/").
Er studierte Informatik an der Leopold-Franzens-Universität Innsbruck und der
Universität Kassel.

-------------------------------

### Ehemalige Mitarbeiter

-----------------------

### Stephan Doerfel

![ <File:doerfel.png>](team/doerfel.png  " File:doerfel.png")

[ Stephan Doerfel](${project.home}user/sdo "wikilink") studierte Mathematik in
Dresden und Lund (Schweden). Die Schwerpunkte seiner Arbeit lagen dort
in den Gebieten Algebraische Strukturen und Formale Begriffsanalyse. Im
Jahr 2009 wechselte er für seine Promotion zum Fachgebiet
Wissensverarbeitung der Universität Kassel.

---------------------------

### Christoph Scholz

![ <File:scholz.png>](team/scholz.png  " File:scholz.png")


-------------------

### Björn-Elmar Macek

![<File:bjoern-elmar_macek.png>](team/bjoern-elmar_macek.png  " File:bjoern-elmar_macek.png")

[ Björn-Elmar Macek](${project.home}user/macek "wikilink") studierte
Computerwissenschaft an der Technischen Universität Ilmenau mit den
Schwerpunkten Komplexitätstheorie, Datenbanken und Datenströme. Nach der
Absolvierung arbeitete er in der industriellen Web-Entwicklung und
Beratung und hat Erfahrung in Design und Programmierung vongraphorientierten
Datenbanken gemacht. Von 2009 bis 2014 promovierte er am Fachgebiets
Wissensverarbeitung bei. 

-----------------------

### Martin Atzmüller

![ <File:martin.png>](team/martin.png  " File:martin.png")

[ Martin Atzmueller](${project.home}user/atzmueller "wikilink") Er promovierte
2006 in Würzburg und habilitierte sich 2013 am Fachbereich
Elektrotechnik/Informatik der Universität Kassel. Er studierte Informatik an der
Universität Texas in Austin (UT), Texas, USA, und der Universität Würzburg.

-----------------------

### Dominik Benz

![ <File:benz.png>](team/benz.png  " File:benz.png")

[ Dominik Benz](${project.home}user/dbenz "wikilink") erlangte sein
Informatik-Diplom mit Nebenfach Psychologie an der Universität Freiburg 2007.
Momentan ist er als Doktorand der Informatik im Fachgebiet Wissensverarbeitung
der Universität Kassel beschäftigt gewesen. Seine Forschungsschwerpunkte liegen
auf dem Gebiet des kollaborativen Wissensmanagements, des Ontology Learnings und
der kollaborativen Schlagwortsysteme.

-----------------------

### Folke Mitzlaff

![ <File:folke.png>](team/folke.png  " File:folke.png")

----------------

### Beate Navarro Bullock

![ <File:krause.png>](team/krause.png  " File:krause.png")

[ Beate Navarro Bullock](${project.home}user/beate "wikilink") studierte
Wirtschaftsinformatik an der Berufsakademie Stuttgart (BA) und an der
Humboldt Universität Berlin (M.Sc.). 

------------------

### Studierende

Folgende Studierende helfen uns gegenwärtig bzw. halfen uns in der
Vergangenheit, unsere Anwendung zu entwickeln:

-   [ Mohammed Abed](${project.home}"wikilink") ![<File:MohammedAbed.jpg>](team/MohammedAbed.jpg  "fig: File:MohammedAbed.jpg")
-   [ Philipp Beau](${project.home}"wikilink") ![<File:PhilippBeau.png>](team/PhilippBeau.png  "fig: File:PhilippBeau.png")
-   [ Waldemar Biller](${project.home}"wikilink") ![<File:waldemar_biller.png>](team/waldemar_biller.png  "fig: File:waldemar_biller.png")
-   [ Manuel Bork](${project.home}"wikilink")
-   [ Christian Claus](${project.home}user/christian_claus "wikilink") ![<File:claus.jpg>](team/claus.jpg  "fig: File:claus.jpg")
- 	[ Jennifer Häfner](${project.home}"wikilink")
-   [ Lukas Hanke](${project.home}"wikilink") ![ <File:Lukas+Hanke.jpg](team/Lukas+Hanke.jpg  "fig: File:Lukas+Hanke.jpg") 
-   [ Johannes Hirth](${project.home}"wikilink") ![ <File:johannes.png](team/johannes.png  "fig: File:johannes.png")
-   [ Jens Illig](${project.home}"wikilink")
-   [ Lutful Kabir](${project.home}"wikilink") ![ <File:Lutful+Kabir.jpg](team/Lutful+Kabir.jpg  "fig: File:Lutful+Kabir.jpg")
-   [ Christian Kramer](${project.home}"wikilink")
-   [ Ibrahim Mahmoud](${project.home}"wikilink") ![ <File:Ibrahim+Mahmoud.jpg](team/Ibrahim+Mahmoud.jpg  "fig: File:Ibrahim+Mahmoud.jpg")
-   [ Markus Matz](${project.home}user/cupodmac "wikilink") ![<File:matz.jpg>](team/matz.jpg  "fig: File:matz.jpg")
-   [ Nils Raabe](${project.home}"wikilink") ![<File:Nils_Raabe_96x133.jpg>](team/Nils_Raabe_96x133.jpg  "fig: File:Nils_Raabe_96x133.jpg")
-   [ Serak Rezane](${project.home}user/siko "wikilink") ![<File:rezane.jpg>](team/rezane.jpg  "fig: File:rezane.jpg")
-   [ Jan Oliver Rüdiger](${project.home}"wikilink") ![ <File:Jan+Oliver+Ruediger.jpg](team/Jan+Oliver+Ruediger.jpg  "fig: File:Jan+Oliver+Ruediger.jpg")
-   [ Bastian Schäfermeier](${project.home}"wikilink") ![<File:Bastian_schaefermeier_klein2.jpg>](team/Bastian_schaefermeier_klein2.jpg  "fig: File:Bastian_schaefermeier_klein2.jpg")
-   [ Christian Schenk](${project.home}user/cschenk "wikilink")
-   [ Sven Stefani](${project.home}"wikilink") ![<File:stefani.jpg>](team/stefani.jpg  "fig: File:stefani.jpg")
-   [ Thomas Steuber](${project.home}user/thomas "wikilink")
-   [ Stefan Stützer](${project.home}user/steff83 "wikilink") ![<File:stuetzer.jpg>](team/stuetzer.jpg  "fig: File:stuetzer.jpg")
-   [ Daniil Tanygin](${project.home}"wikilink") ![ <File:Daniil+Tanygin.jpg](team/Daniil+Tanygin.jpg  "fig: File:Daniil+Tanygin.jpg")
-   [ Bernd Terbrack](${project.home}"wikilink")
-   [ Michael Velimirov](${project.home}"wikilink") ![<File:michael_velimirov.jpg>](team/michael_velimirov.jpg  "fig: File:michael_velimirov.jpg")
-   [ Michael Wagner](${project.home}user/michi "wikilink") ![<File:wagner.jpg>](team/wagner.jpg  "fig: File:wagner.jpg")

------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink") 
  

