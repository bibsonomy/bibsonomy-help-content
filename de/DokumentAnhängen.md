<!-- en: AttachDocuments -->

## Ein privates Dokument an eine Publikation anhängen
--------------------------------------------------

Sie können an jede Ihrer Publikationen ein Dokument anhängen (max. 50 MB
pro Datei - erlaubte Dateiendungen: *pdf, ps, djv, djvu, txt, tex, doc,
docx, ppt, pptx, xls, xlsx, ods, odt, odp, jpg, jpeg, svg, tif, tiff,
png, htm, html, epub*). Dieser Anhang kann z. B. eine Datei mit privaten
Notizen sein oder eine kurze Zusammenfassung des Inhalts. Der Anhang ist
aus urheberrechtlichen Gründen nur für Sie selbst sichtbar. Es ist
außerdem möglich, die Dokumente mit anderen Wissenschaftlern zu teilen.
  
![ <File:1.png>](dokumentanh%c3%a4ngen/1.png  "fig: File:1.png")  
  
Um ein Dokument hochzuladen, klicken Sie auf den Titel der Publikation.
Klicken Sie nun auf das große **Dokumentsymbol mit dem Plus** auf der rechten Seite.
Klicken Sie nun auf den
**Durchsuchen-Button**, der unter dem Dokumentsymbol erscheint und wählen Sie
die Datei, die Sie hochladen möchten, aus. 
  
![ <File:2.png>](dokumentanh%c3%a4ngen/2.png  "fig: File:2.png")  
  
Nachdem Ihre Datei hochgeladen wurde, wird automatisch eine Vorschau angezeigt.
Unter dieser wird der Name der Datei angezeigt, sowie die Optionen die Datei zu bearbeiten, zu
löschen oder herunterzuladen.
Sie können außerdem noch weitere Dokumente hinzufügen.

------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink")