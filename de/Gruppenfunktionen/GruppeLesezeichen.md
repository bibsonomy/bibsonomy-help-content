<!-- en: GroupFunctions/GroupBookmarks -->

-----------------

[`Zurück zur Übersicht der Gruppenfunktionen geht es hier.`](Gruppenfunktionen "wikilink")

---------------------

## Lesezeichen/Publikationen mit der Gruppe teilen
--------------------------------------------------------

Im Dialog **"Bearbeiten Sie Ihren
Publikationseintrag/Lesezeicheneintrag"**, der erscheint, wenn Sie ein
neues Lesezeichen/eine neue Publikation [ hinzufügen oder
bearbeiten](LesezeichenPublikationenManagen "wikilink"), haben Sie die
Möglichkeit, den Eintrag für eine/mehrere Gruppen freizugeben.  

![<File:3.png>](GruppeLesezeichen/3.png "fig: File:3.png")  

1. Wählen Sie dazu unter dem Punkt **"Sichtbarkeit"** die Option
   **"andere"**.
2. Wählen Sie die Gruppe, mit denen Sie das Lesezeichen/die Publikation
   teilen möchten, aus.
3. Klicken Sie am Ende der Seite auf **"Speichern"**.

Eine **andere Möglichkeit**, Lesezeichen/Publikationen mit einer Gruppe
zu teilen, ist, das Lesezeichen/die Publikation mit dem [ System Tag](SystemTags "wikilink")
`for:<group name>` zu taggen. Auf diese Weise wird der Eintrag in die Sammlung der Gruppe kopiert.

**Hinweis:** Wenn ein Mitglied einer Gruppe seinen
${project.name}-Account **löscht**, so bleibt ein Lesezeichen/eine
Publikation für eine Gruppe verfügbar, wenn er mit dem System Tag
`for:<group name>` getagged wurde. Ansonsten geht er auch für die Gruppe
verloren.  
Auch, wenn ein Eintrag für **mehrere Gruppen** sichtbar gemacht werden
soll, ist die Verwendung des System Tags zu empfehlen.