<!-- en: GroupFunctions/GroupCreate -->

-----------------

[`Zurück zur Übersicht der Gruppenfunktionen geht es hier.`](Gruppenfunktionen "wikilink")

---------------------

## Eine neue Gruppe gründen
---------------------------------

**Ziel:** Diese Anleitung zeigt Ihnen, wie Sie eine neue Gruppe
erstellen können. Befolgen Sie dazu folgende Schritte: 

![<File:4.png>](GruppeGruenden/4.png "fig: File:4.png")  

1.  Klicken Sie im Hauptmenü auf **"Gruppen"** - ein Menü erscheint.
2.  Klicken Sie auf **"Eine neue Gruppe erstellen"**.
3.  Füllen Sie das Formular aus. Erklären Sie bitte auch (unter
    **"Begründung"**), warum bzw. wofür die neue Gruppe genutzt
    werden soll. Um Spamgruppen zu vermeiden, werden alle Gruppenanträge
    manuell geprüft.
4.  Klicken Sie abschließend auf **"Gruppe beantragen"**.
5.  Die Administratoren werden Ihren Antrag für eine neue Gruppe prüfen.
    Sobald die Gruppe freigeschaltet ist, erhalten Sie eine E-Mail.