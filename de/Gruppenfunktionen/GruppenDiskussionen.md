<!-- en: GroupFunctions/GroupDiscussions -->

-----------------

[`Zurück zur Übersicht der Gruppenfunktionen geht es hier.`](Gruppenfunktionen "wikilink")

---------------------

## Gruppendiskussionen
----------------------------------

Bei der wissenschaftlichen Arbeit ist es wichtig, Gedanken und Ideen mit
Kollegen auszutauschen. In ${project.name} haben Sie neben öffentlicher
[ Diskussion](KommentarRezensionBewertung "wikilink") auch die Möglichkeit, gruppenintern zu diskutieren.  
Wenn Sie einen Kommentar zu einem Eintrag schreiben, können Sie die
**Sichtbarkeit** dieses Kommentars auf eine Gruppe beschränken. Dazu
klicken Sie neben "Sichtbarkeit" auf "andere" und dann auf den Namen der
Gruppe. Ihr Kommentar ist dann nur für Gruppenmitglieder sichtbar, auch
**nicht** für den Eigentümer des Eintrags (die Durchschnittsbewertung
bleibt jedoch öffentlich).  
Auf diese Weise können Sie innerhalb der Gruppe über relevante
wissenschaftliche Veröffentlichungen diskutieren oder den Mitgliedern
lesenswerte Artikel empfehlen. Um zu sehen, welche Einträge in der
Gruppe diskutiert wurden, klicken Sie auf der [Gruppenseite](Gruppenfunktionen/GruppenSeite "wikilink") auf **Zeige kürzlich
diskutierte Einträge von &lt;Gruppenname&gt;**.