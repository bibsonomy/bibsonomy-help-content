<!-- en: GroupFunctions/GroupTags -->

-----------------

[`Zurück zur Übersicht der Gruppenfunktionen geht es hier.`](Gruppenfunktionen "wikilink")

---------------------
## Vordefinierte Gruppentags
---------------------

Vordefinierte Gruppentags ermöglichen Tags in einer Gruppe auszuwählen, die ein Gruppenadmin zuvor angelegt hat [`Zurück zur Übersicht der Gruppenfunktionen geht es hier.`](Gruppenfunktionen "wikilink"). 

Beim Eintragen einer einzelnen Publikation können
durch Auswahl der Option „senden an", Gruppentags ausgewählt werden:

![<File:TagsAuswählen.png>](GruppenTags/TagsAuswählen.png "fig: File:TagsAuswählen.png") 


Hier können sie die Gruppe wählen, an die der Eintrag gesendet werden soll und die vordefinierten
Tags erscheinen. Diese können per Klick hinzugefügt und wieder entfernt
werden; ausgewählte Tags werden ausgegraut. Ein Mouseover zeigt
zusätzlich die hinterlegte Beschreibung der einzelnen Tags an.
Nur die vordefinierte Gruppentags werden an den Gruppeneintrag hinzugefügt, in der eigenen Sammlung
werden die Tags nicht angezeigt. So ist es möglich, dass in der Gruppe nur ein definiertes Tagset an den Einträgen stehen, in der eigenen Sammlung werden nur die eigenen Tags angezeigt.
