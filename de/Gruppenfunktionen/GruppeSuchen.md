<!-- en: GroupFunctions/GroupSearch -->

-----------------

[`Zurück zur Übersicht der Gruppenfunktionen geht es hier.`](Gruppenfunktionen "wikilink")

---------------------

## Gruppen suchen und beitreten
----------------------------------------

![<File:1.png>](GruppeSuchen/1.png "fig: File:1.png")  

Möchten Sie Gruppen suchen und ihnen beitreten, dann befolgen Sie die folgenden Schritte:

1. Klicken Sie im [ Hauptmenü](Programmoberfläche "wikilink") auf **"Gruppen"** - ein Menü erscheint.
2. Wählen Sie **"Alle Gruppen"** per Mausklick aus.
3. In der ersten Tabellenspalte sehen Sie den Gruppennamen sowie eine
   kurze Gruppenbeschreibung.
4. Die letzte Spalte (die Sie nur sehen, wenn Sie bei ${project.name}
   angemeldet sind) zeigt für jede Gruppe den Button **"beitreten"**.
   Klicken Sie auf den entsprechenden **"beitreten"**-Button, um einen
   Beitritt zu beantragen.  
   ![<File:joinrequest1.png>](GruppeSuchen/joinrequest1.png "fig: File:joinrequest1.png")
5. Geben Sie in das Feld **"Begründung"** ein, warum
   Sie dieser Gruppe beitreten möchten.
6. Klicken Sie auf die Checkbox neben **"Captcha"**, damit wir
   ausschließen können, dass Sie eine Maschine/ein Roboter sind.
7. Wenn Sie möchten, dass andere Gruppenmitglieder Zugriff auf Ihre
   Dokumente haben sollen, klicken Sie auf die Checkbox neben "Ihre
   Dokumente mit dieser Gruppe teilen".
8. Klicken Sie abschließend auf **"Anfrage senden"**.
9. Der Gruppen-Administrator erhält eine Nachricht, dass Sie Mitglied
   dieser Gruppe werden möchten. Daher ist ein plausibler
   Begründungstext (siehe 5.) sinnvoll. Über eine Aufnahme in eine
   Gruppe entscheidet allein der Gruppen-Administrator.