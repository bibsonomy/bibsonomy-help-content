<!-- en: GroupFunctions/GroupPage -->

-----------------

[`Zurück zur Übersicht der Gruppenfunktionen geht es hier.`](Gruppenfunktionen "wikilink")

---------------------

## Die Gruppenseite
-----------------------

Wenn Sie auf den Namen einer Gruppe klicken, kommen Sie zur
**Gruppenseite**. Dort sehen Sie Lesezeichen/Publikationen der Gruppe,
ihre Mitglieder und verwendete Tags.  
![<File:overview.png>](GruppenSeite/overview.png "fig: File:overview.png")  

- **Kürzlich diskutierte Einträge:** Klicken Sie in der Sidebar unter "Diskussion" auf **Zeige kürzlich diskutierte Einträge von &lt;Gruppenname&gt;**. Auf der folgenden Seite sehen Sie alle Einträge, die kürzlich von Mitgliedern der Gruppe rezensiert oder kommentiert wurden. Außerdem sehen Sie die **durchschnittliche Bewertung** der Einträge.
- **CV:** Klicken Sie auf den **CV-Button** links in der Sidebar. Auf
  der folgenden Seite bekommen Sie eine Übersicht über die Gruppe
  (Gruppenbild, Mitglieder, kürzlich
  hinzugefügte Lesezeichen/Publikationen).