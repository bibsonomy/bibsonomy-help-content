<!-- en: GroupFunctions/GroupLeave -->

-----------------

[`Zurück zur Übersicht der Gruppenfunktionen geht es hier.`](Gruppenfunktionen "wikilink")

---------------------

## Aus einer Gruppe austreten
------------------------------------

Um aus einer Gruppe auszutreten, gehen Sie zur **Gruppenseite** (klicken
Sie auf den Namen der Gruppe ODER klicken Sie auf "Gruppen" im linken
Hauptmenü, dann auf den Namen der Gruppe und schließlich auf
"Einträge").  
In der Sidebar auf der rechten Seite befindet sich ein **grüner Button**
mit dem Text **"Mitglied"**. Wenn Sie den Mauszeiger über diesen Button
bewegen, wird er **rot** und der Text ändert sich zu **"Gruppe
verlassen"**. Klicken Sie darauf, falls Sie sicher sind, dass Sie diese
Gruppe nicht mehr benötigen.  

![ <File:leave.png>](GruppeAustreten/leave.png "fig: File:leave.png")  

