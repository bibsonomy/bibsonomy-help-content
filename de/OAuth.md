<!-- en: OAuth -->

## ${project.name} via OAuth
--------------------------

**Hinweis:** Ein Codebeispiel für den Zugriff auf ${project.name} via OAuth
können Sie [hier](https://bitbucket.org/bibsonomy/bibsonomy-help-content/src/b8d20eb1bde387f1a8772d91943452f64d5edda2/code-samples/oauth-rest-demo/src/main/java/org/bibsonomy/OAuthRestApiDemo.java?at=default&fileviewer=file-view-default) herunterladen.

----------------

Die [API](https://bitbucket.org/bibsonomy/bibsonomy/wiki/documentation/api/REST%20API) von ${project.name} erlaubt Ihnen systematisch Zugriff auf Ihre
Einträge. Die Hilfeseite der API beschreibt, wie Sie Ihre Anfragen auf
Ihre Einträge mit Hilfe Ihres API-Keys und Ihres API-Secrets
autorisieren können. Wenn Sie von Ihrer Anwendung aus für einen anderen
Nutzer auf ${project.name} zugreifen möchten, ist dieser Weg nicht
sinnvoll, da der Nutzer seinen API-Key und sein API-Secret in Ihrer
Anwendung speichern müsste.

[OAuth](https://oauth.net/) ist ein etabliertes Protokoll für sichere
API-Autorisierung, die es Nutzern ermöglicht, einer dritten Anwendung
den Zugriff auf ihre Daten zu erlauben, ohne, dass sie ihre
Anmeldeinformationen außerhalb von ${project.name} angeben müssen.

Auf dieser Seite erfahren Sie, wie Sie mithilfe von OAuth von Ihrer Anwendung auf ${project.name} zugreifen können
und [OAuth Anfragen](OAuthAnfragen "wikilink") an die API von ${project.name} machen.

------------------------------------------------------------------------

### So erhalten Sie durch OAuth in Ihrer Anwendung Zugriff auf ${project.name}

#### 1) Beantragen Sie einen OAuth Consumer Key und Consumer Secret

Bevor Ihre Anwendung auf die API von ${project.name} zugreifen kann,
müssen beide Anwendungen einen gesicherten Kommunikationskanal aufbauen.
Dies wird durch den Austausch von Anmeldedaten, dem sogenannten
*consumer key* und dem *consumer secret*, erreicht. Der *consumer key*
identifiziert Ihre Anwendung, und durch den *consumer secret* werden
Ihre Anfragen verifiziert. Sowohl symmetrische ([HMAC](https://de.wikipedia.org/wiki/Keyed-Hash_Message_Authentication_Code)) als auch Public Key
([RSA](https://de.wikipedia.org/wiki/Public-Key-Verschl%C3%BCsselungsverfahren))-Verschlüsselung wird unterstützt.

Um einen *consumer key* und ein *consumer secret* zu beantragen,
schreiben Sie bitte eine E-Mail an <${project.apiemail}> mit folgenden Informationen:

- Name der Anwendung
- Eine kurze Beschreibung Ihrer Anwendung
- Typ der Verschlüsselung (HMAC oder RSA)
- Die Callback Adresse Ihrer Anwendung (wenn anwendbar)
  
  

**Hinweis: Um eine sichere Kommunikation per E-Mail zu garantieren, wird dringend empfohlen, 
einen Verschlüsselungsstandard (wie beispielsweise [PGP](https://de.wikipedia.org/wiki/Pretty_Good_Privacy)) zu verwenden, 
um die Daten vor unberechtigtem Zugriff zu schützen und die Identität des Absenders verifizieren zu können.**

------------------------------------------------------------------------

#### 2) Implementieren Sie den Autorisierungsprozess von OAuth

Wenn ein Nutzer Ihrer Anwendung in ${project.name} Zugriff auf seine
Daten gewährt, wird der Nutzer zwischen Ihrer Anwendung und
${project.name} hin- und wieder zurückgelenkt, bis am Ende der
sogenannte *access token* Ihre Anwendung erreicht. Dieser wird dann dazu
genutzt, um Ihre Anfragen an die API zu autorisieren. Dieser Prozess
wird in der [OAuth-Anleitung](https://hueniverse.com/protocol-workflow-266b6a975cbf) genauer beschrieben.

Im Wesentlichen muss Ihre Anwendung den Nutzer zu der
${project.name}-OAuth-Autorisierungsseite weiterleiten, mit den vorher
erhaltenen Anmeldedaten als Request-Parameter (z.B.
[${project.home}oauth/authorize?oauth_token=xxxxxxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx](${project.home}oauth/authorize?oauth_token=xxxxxxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx])):

![ <File:access.png>](oauth/access.png  "fig: File:access.png")  

Wenn der Nutzer Ihre temporären Anmeldedaten autorisiert, wird er
entweder zu Ihrer Seite weitergeleitet (falls Sie eine Callback-URL
angegeben haben), oder der Nutzer muss manuell zu Ihrer Seite wechseln.
Die autorisierten Anmeldedaten können dann dazu genutzt werden, um den
*access token* zu erhalten, mit dem Anfragen autorisiert werden.  

![ <File:success.png>](oauth/success.png  "fig: File:success.png")  

Die OAuth-Rest-API von ${project.name} für Java erleichtert diesen
Prozess. Falls Sie [Maven](http://maven.apache.org/) nutzen, fügen Sie
einfach Ihrer *pom.xml*-Datei den folgenden Code hinzu:

${project.theme == "puma"}{ 

(Hinweis: PUMA basiert auf dem BibSonomy System, 
deshalb können die Libraries für BibSonomy auch für PUMA verwendet werden.)

}

    <project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
    	<modelVersion>4.0.0</modelVersion>
    
    	<groupId>org.bibsonomy</groupId>
    	<artifactId>oauth-rest-demo</artifactId>
    	<version>1.0-SNAPSHOT</version>
    
    	<repositories>
    		<repository>
    			<id>bibsonomy-repo</id>
    			<name>Releases von BibSonomy-Modulen</name>
    			<url>http://dev.bibsonomy.org/maven2/</url>
    		</repository>
    	</repositories>
    
    	<build>
    		<plugins>
    			<plugin>
    				<groupId>org.apache.maven.plugins</groupId>
    				<artifactId>maven-compiler-plugin</artifactId>
    				<version>2.0</version>
    				<configuration>
    					<source>1.7</source>
    					<target>1.7</target>
    					<compilerArguments>
    						<encoding>UTF-8</encoding>
    					</compilerArguments>
    				</configuration>
    			</plugin>
    		</plugins>
    	</build>
    
    	<dependencies>
    		<dependency>
    			<groupId>org.bibsonomy</groupId>
    			<artifactId>bibsonomy-rest-client-oauth</artifactId>
    			<version>3.5.0</version>
    		</dependency>
    	</dependencies>
    
    </project>



Alternativ können Sie die jar-Dateien auch [direkt herunterladen](https://dev.bibsonomy.org/maven2/).

Temporäre Anmeldedaten zu erhalten funktioniert dann folgendermaßen:

    BibSonomyOAuthAccesssor accessor = new BibSonomyOAuthAccesssor("<YOUR CONSUMER KEY>", "<YOUR CONSUMER SECRET>", "<YOUR CALLBACK URL>");
    String redirectURL = accessor.getAuthorizationUrl();

Nun müssen Sie den Nutzer zu `redirectURL` weiterleiten. Danach werden
die vorher erhaltenen temporären Anmeldedaten zu einem *access token*
umgeformt:

    accessor.obtainAccessToken();

---------------------------------------------------------------

Nachdem der Autorisierungsprozess abgeschlossen ist, können Sie OAuth Anfragen an die API von ${project.name} machen.
Erfahren Sie mehr darüber [hier](OAuthAnfragen "wikilink").

------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink") 