<!-- en: User_Interface -->

## Die Benutzeroberfläche - Übersicht
------------------

${project.theme == "bibsonomy"}{
    
Diese Anleitung gibt Ihnen einen ersten Überblick zum Aufbau
von ${project.name}. Nach dieser Anleitung wissen Sie, wo Sie z. B. das
Hauptmenü oder die Suche finden.

![<File:user_interface_markierungen_de.png>](programmoberfläche/user_interface_markierungen_de.png  "fig: /File:user_interface_markierungen_de.png")  

1. **Die Suche** - bietet Ihnen viele Möglichkeiten, ${project.name}
    nach Informationen zu durchsuchen. Klicken Sie mit der linken Maustaste
    auf den blauen Pfeil bei *"Suche"* und wählen Sie im darauf
    erscheinenden Menü den Datensatz aus, den Sie durchsuchen möchten, z. B.
    Autor, um nach einem bestimmten Autor zu suchen. Geben Sie dann in das
    weiße Feld Ihren Suchbegriff ein. Klicken Sie abschließend auf
    Lupensymbol rechts oder drücken Sie *"Enter"* auf Ihrer Tastatur.
    ${project.name} gibt Ihnen in wenigen Sekunden alle verfügbaren
    Ergebnisse aus.  
    Auf der [ Suche-Hilfeseite](SuchenHilfe "wikilink") finden Sie außerdem
    Informationen über die verschiedenen Optionen, die Sie beim Suchen in
    ${project.name} haben.  
  
2. **Die Sprachleiste** - ermöglicht es Ihnen, zwischen den drei
    verfügbaren Sprachversionen in ${project.name} zu wechseln. Klicken Sie
    auf *"en"*, um ${project.name} mit einer englischsprachigen Oberfläche
    zu verwenden, auf *"de"*, um ${project.name} auf die deutsche
    Sprachversion umzustellen, oder auf *"ru"* für die russische Version.  
  
3. **Das linke Hauptmenü** - stellt Ihnen die wichtigsten Funktionen von
    ${project.name} zur Verfügung. Wenn Sie auf die Pfeile neben den
    Menüpunkten klicken, sehen Sie die jeweiligen Unterpunkte des Menüs. Die
    einzelnen Menüs des Hauptmenüs (Auflistung von links nach rechts, mit
    Unterpunkten) sind:

    - **Home:** Damit gelangen Sie zur Startseite

    - **mein ${project.name}:** Ihre persönliche Startseite.
        Unterpunkte, die *kursiv* dargestellt sind, werden nur in der
        erweiterten Ansicht angezeigt. Wie sie die erweiterte Ansicht
        freischalten, [ lesen Sie hier](ErweiterteFunktionenFreischalten "wikilink").
        
		-   **Meine Einträge:** Zeigt Ihre Einträge an.
        
		-   ***Private Einträge:*** Zeigt alle Einträge an, die nur Sie selbst sehen können.
		
		-   ***Einträge für Freunde:*** Zeigt die Einträge an, die nur Sie selbst oder Ihre Freunde 			sehen können.
    
		-   ***Dokumente:*** Wenn Sie Ihren Einträgen [Dokumente (z. B. als PDF) angefügt](DokumentAnhängen "wikilink") haben, 				können Sie hier eine Übersicht sehen.
		
		-   ***Duplikate:*** Zeigt Einträge an, die wahrscheinlich Duplikate sind. So können Sie 				Ihre Literaturliste ganz einfach bereinigen.
        
		-   ***Konzepte:*** [ Konzepte](Konzepte "wikilink") ermöglichen es Ihnen, mehrere 						Schlagwörter zu gruppieren.
		
    	-   **Diskutierte Einträge:** Zeigt die Einträge an, zu denen Sie einen [Diskussionsbeitrag](DiskutierteEinträge "wikilink") 				geleistet haben.

    	-   **Verfolgte Einträge:** Zeigt die Einträge von Nutzern, denen Sie [folgen](Follower "wikilink") an.

    	-   **Einträge von Freunden:** Zeigt Einträge an, die nur für Freunde sichtbar sind und von den Benutzern stammen, auf deren Freundesliste Sie stehen.
    
    	-   ***Lebenslauf:*** Hier können Sie einen [Text/Lebenslauf/Vita hinterlegen](LebenslaufÄndern "wikilink"), den andere                 Nutzer in ${project.name} einsehen können.
    	
    	-    ***Publikationen durchstöbern:*** Durchsuchen Sie Ihre gespeicherten Publikation, indem               Sie die Filteroptionen benutzen. In [ diesem Tutorial](PublikationenDurchstöbern "wikilink") finden Sie weitere Informationen dazu.
    	
    	-   ***BibTeX exportieren:*** Exportiert ihre Literaturdaten im BibTeX-Format.
    
    - **Eintragen:** Dieses Menü hilft Ihnen beim Eintragen neuer Lesezeichen/Publikationen.
	  
        -   **Lesezeichen hinzufügen:** Fügen Sie ein neues Lesezeichen hinzu. In [ diesem Tutorial](LesezeichenPublikationenManagen "wikilink") lesen Sie, wie Sie Lesezeichen und Publikationen verwalten.
        
        -   **Publikation hinzufügen:** Fügen Sie eine neue Publikation hinzu. In [ diesem Tutorial ](LesezeichenPublikationenManagen "wikilink") lesen Sie, wie Sie Lesezeichen und Publikationen verwalten.
            
        -   **Lesezeichen importieren:** Sie können Lesezeichen aus Ihrem Browser oder [Delicious](https://delicious.com/) importieren (eine Anleitung dazu [ finden Sie hier](LesezeichenImportieren "wikilink")).

        -   **Publikationen importieren:** Sie können Publikatonen importieren, indem Sie eine bestehende BibTeX- oder EndNote-Datei hochladen. 
        
    - **Gruppen:** Zeigt Ihnen die [ Funktionen zu Gruppen](Gruppenfunktionen "wikilink") an.
    
        -   **Alle Gruppen:** Zeigt Ihnen eine Übersicht zu allen Gruppen in ${project.name} an.
        
        -   **Eine neue Gruppe erstellen:** Hier können Sie eine neue Gruppe erstellen.
       
    - **Beliebt:** Ermöglicht Ihnen, die aktuell beliebtesten Einträge in ${project.name} zu 				erforschen.
    
        -    **Einträge:** Zeigt die beliebtesten Einträge an.
        
        -   **Tags:** Zeigt eine Schlagwortwolke an, die die beliebtesten [ Schlagwörter (Tags)](Tags "wikilink") in ${project.name} darstellt. Je größer ein Schlagwort, desto beliebter ist der Tag.
          
        -   **Autoren:** Zeigt die beliebtesten Autoren an.
        
        -  **Konzepte:** Zeigt die beliebtesten [ Konzepte](Konzepte "wikilink") und deren Zuordnungen an.
        
        -  **Diskussionen:** Zeigt die stark frequentierten [Diskussionen](DiskutierteEinträge "wikilink") zu Lesezeichen und Publikationen an.  
    
4.  **Das rechte Hauptmenü:** Hier finden Sie weitere wichtige
    Funktionen von ${project.name}.

    -   **Ihr Benutzername:** Unter diesem Namen sind Sie gerade bei
        ${project.name} angemeldet. Wenn Sie auf Ihren Namen klicken,
        kommen Sie auf Ihre [ Nutzerseite](UserSeite "wikilink") und
        Ihnen werden Ihre Lesezeichen und Publikationen angezeigt.
        
    -   **Das Personensymbol:** Klicken Sie auf dieses Symbol rechts
        neben Ihrem Namen, um weitere Funktionen sehen zu können.
        
        -   **Eingang:** Ihr [ Lesezeichen/Publikation-Posteingang](Eingang "wikilink").
            Freunde und Gruppen können Ihnen Lesezeichen/Publikationen
            zusenden, diese Eingänge landen hier.
            
        -   **Ablage:** In der [ Ablage](Ablage "wikilink") können Sie eine aktuelle Literaturliste zusammenstellen.
        
        -   **Freunde:** Zeigt Ihre [Freunde](FreundeHinzufügen "wikilink") an.
        
        -   **Tags bearbeiten:** Hier können Sie Ihre Schlagworte (Tags) und [ Konzepte](Konzepte "wikilink") bearbeiten.
        
        -   **Einstellungen:** Hier können Sie Ihre [ persönlichen Benutzereinstellungen ansehen und ändern](Einstellungen "wikilink").
        
        -   **Weblog:** Leitet Sie auf [den Weblog von  ${project.name} ]((http://bibsonomy.blogspot.com/)) weiter.
        
        -   **Hilfe:** Hier gelangen Sie zu den Hilfeseiten von ${project.name}.
        
        -   **Abmelden:** Klicken Sie hier, um sich von ${project.name} abzumelden.  
    
5.  **Der Inhaltsbereich** - hier sehen Sie eine Auswahl der momentan
    verfügbaren Lesezeichen und Publikationen. Um mehr über diesen
    Bereich, Lesezeichen und Publikationen zu erfahren, lesen Sie [
    dieses Tutorial](LesezeichenPublikationenManagen "wikilink").
    
6.  **Die Neuigkeiten** - hier publizieren die Entwickler von
    ${project.name} aktuelle Neuigkeiten, z. B. neue Funktionen oder
    auch Hinweise zu Wartungsarbeiten am System.  
    
7.  **Beliebte Tags** - zeigt Ihnen die aktuell beliebtesten [Tags](Tags "wikilink").

}

${project.theme == "puma"}{
    
Diese Anleitung gibt Ihnen einen ersten Überblick zum Aufbau
von ${project.name}. Nach dieser Anleitung wissen Sie, wo Sie z. B. das
Hauptmenü oder die Suche finden.

![<File:user_interface_markierungen_de.png>](programmoberfläche/user_interface_markierungen_de.png  "fig: /File:user_interface_markierungen_de.png")  

1. **Die Suche** - bietet Ihnen viele Möglichkeiten, ${project.name}
    nach Informationen zu durchsuchen. Klicken Sie mit der linken Maustaste
    auf den blauen Pfeil bei *"Suche"* und wählen Sie im darauf
    erscheinenden Menü den Datensatz aus, den Sie durchsuchen möchten, z. B.
    Autor, um nach einem bestimmten Autor zu suchen. Geben Sie dann in das
    weiße Feld Ihren Suchbegriff ein. Klicken Sie abschließend auf
    Lupensymbol rechts oder drücken Sie *"Enter"* auf Ihrer Tastatur.
    ${project.name} gibt Ihnen in wenigen Sekunden alle verfügbaren
    Ergebnisse aus.  
    Auf der [ Suche-Hilfeseite](SuchenHilfe "wikilink") finden Sie außerdem
    Informationen über die verschiedenen Optionen, die Sie beim Suchen in
    ${project.name} haben.  
  
2. **Die Sprachleiste** - ermöglicht es Ihnen, zwischen den drei
    verfügbaren Sprachversionen in ${project.name} zu wechseln. Klicken Sie
    auf *"en"*, um ${project.name} mit einer englischsprachigen Oberfläche
    zu verwenden, auf *"de"*, um ${project.name} auf die deutsche
    Sprachversion umzustellen, oder auf *"ru"* für die russische Version.  
  
3. **Das linke Hauptmenü** - stellt Ihnen die wichtigsten Funktionen von
    ${project.name} zur Verfügung. Wenn Sie auf die Pfeile neben den
    Menüpunkten klicken, sehen Sie die jeweiligen Unterpunkte des Menüs. Die
    einzelnen Menüs des Hauptmenüs (Auflistung von links nach rechts, mit
    Unterpunkten) sind:

    - **Home:** Damit gelangen Sie zur Startseite

    - **mein ${project.name}:** Ihre persönliche Startseite.
        Unterpunkte, die *kursiv* dargestellt sind, werden nur in der
        erweiterten Ansicht angezeigt. Wie sie die erweiterte Ansicht
        freischalten, [ lesen Sie hier](ErweiterteFunktionenFreischalten "wikilink").
        
		-   **Meine Einträge:** Zeigt Ihre Einträge an.
        
		-   ***Private Einträge:*** Zeigt alle Einträge an, die nur Sie selbst sehen können.
		
		-   ***Einträge für Freunde:*** Zeigt die Einträge an, die nur Sie selbst oder Ihre Freunde 			sehen können.
    
		-   ***Dokumente:*** Wenn Sie Ihren Einträgen [Dokumente (z. B. als PDF) angefügt](DokumentAnhängen "wikilink") haben, 				können Sie hier eine Übersicht sehen.
		
		-   ***Duplikate:*** Zeigt Einträge an, die wahrscheinlich Duplikate sind. So können Sie 				Ihre Literaturliste ganz einfach bereinigen.
        
		-   ***Konzepte:*** [ Konzepte](Konzepte "wikilink") ermöglichen es Ihnen, mehrere 						Schlagwörter zu gruppieren.
		
    	-   **Diskutierte Einträge:** Zeigt die Einträge an, zu denen Sie einen [Diskussionsbeitrag](DiskutierteEinträge "wikilink") 				geleistet haben.

    	-   **Verfolgte Einträge:** Zeigt die Einträge von Nutzern, denen Sie [folgen](Follower "wikilink") an.

    	-   **Einträge von Freunden:** Zeigt Einträge an, die nur für Freunde sichtbar sind und von den Benutzern stammen, auf deren Freundesliste Sie stehen.
    
    	-   ***Lebenslauf:*** Hier können Sie einen [Text/Lebenslauf/Vita hinterlegen](LebenslaufÄndern "wikilink"), den andere                 Nutzer in ${project.name} einsehen können.
    	
    	-    ***Publikationen durchstöbern:*** Durchsuchen Sie Ihre gespeicherten Publikation, indem               Sie die Filteroptionen benutzen. In [ diesem Tutorial](PublikationenDurchstöbern "wikilink") finden Sie weitere Informationen dazu.
    	
    	-   ***BibTeX exportieren:*** Exportiert ihre Literaturdaten im BibTeX-Format.
    
    - **Eintragen:** Dieses Menü hilft Ihnen beim Eintragen neuer Lesezeichen/Publikationen.
	  
        -   **Lesezeichen hinzufügen:** Fügen Sie ein neues Lesezeichen hinzu. In [ diesem Tutorial](LesezeichenPublikationenManagen "wikilink") lesen Sie, wie Sie Lesezeichen und Publikationen verwalten.
        
        -   **Publikation hinzufügen:** Fügen Sie eine neue Publikation hinzu. In [ diesem Tutorial ](LesezeichenPublikationenManagen "wikilink") lesen Sie, wie Sie Lesezeichen und Publikationen verwalten.
            
        -   **Lesezeichen importieren:** Sie können Lesezeichen aus Ihrem Browser oder [Delicious](https://delicious.com/) importieren (eine Anleitung dazu [ finden Sie hier](LesezeichenImportieren "wikilink")).

        -   **Publikationen importieren:** Sie können Publikatonen importieren, indem Sie eine bestehende BibTeX- oder EndNote-Datei hochladen. 
        
    - **Gruppen:** Zeigt Ihnen die [ Funktionen zu Gruppen](Gruppenfunktionen "wikilink") an.
    
        -   **Alle Gruppen:** Zeigt Ihnen eine Übersicht zu allen Gruppen in ${project.name} an.
        
        -   **Eine neue Gruppe erstellen:** Hier können Sie eine neue Gruppe erstellen.
       
    - **Beliebt:** Ermöglicht Ihnen, die aktuell beliebtesten Einträge in ${project.name} zu 				erforschen.
    
        -    **Einträge:** Zeigt die beliebtesten Einträge an.
        
        -   **Tags:** Zeigt eine Schlagwortwolke an, die die beliebtesten [ Schlagwörter (Tags)](Tags "wikilink") in ${project.name} darstellt. Je größer ein Schlagwort, desto beliebter ist der Tag.
          
        -   **Autoren:** Zeigt die beliebtesten Autoren an.
        
        -  **Konzepte:** Zeigt die beliebtesten [ Konzepte](Konzepte "wikilink") und deren Zuordnungen an.
        
        -  **Diskussionen:** Zeigt die stark frequentierten [Diskussionen](DiskutierteEinträge "wikilink") zu Lesezeichen und Publikationen an.  
    
4.  **Das rechte Hauptmenü:** Hier finden Sie weitere wichtige
    Funktionen von ${project.name}.

    -   **Ihr Benutzername:** Unter diesem Namen sind Sie gerade bei
        ${project.name} angemeldet. Wenn Sie auf Ihren Namen klicken,
        kommen Sie auf Ihre [ Nutzerseite](UserSeite "wikilink") und
        Ihnen werden Ihre Lesezeichen und Publikationen angezeigt.
        
    -   **Das Personensymbol:** Klicken Sie auf dieses Symbol rechts
        neben Ihrem Namen, um weitere Funktionen sehen zu können.
        
        -   **Eingang:** Ihr [ Lesezeichen/Publikation-Posteingang](Eingang "wikilink").
            Freunde und Gruppen können Ihnen Lesezeichen/Publikationen
            zusenden, diese Eingänge landen hier.
            
        -   **Ablage:** In der [ Ablage](Ablage "wikilink") können Sie eine aktuelle Literaturliste zusammenstellen.
        
        -   **Freunde:** Zeigt Ihre [Freunde](FreundeHinzufügen "wikilink") an.
        
        -   **Tags bearbeiten:** Hier können Sie Ihre Schlagworte (Tags) und [ Konzepte](Konzepte "wikilink") bearbeiten.
        
        -   **Einstellungen:** Hier können Sie Ihre [ persönlichen Benutzereinstellungen ansehen und ändern](Einstellungen "wikilink").
        
        -   **Weblog:** Leitet Sie auf [den Weblog von  ${project.name} ]((http://bibsonomy.blogspot.com/)) weiter.
        
        -   **Hilfe:** Hier gelangen Sie zu den Hilfeseiten von ${project.name}.
        
        -   **Abmelden:** Klicken Sie hier, um sich von ${project.name} abzumelden.  
    
5.  **Der Inhaltsbereich** - hier sehen Sie eine Auswahl der momentan
    verfügbaren Lesezeichen und Publikationen. Um mehr über diesen
    Bereich, Lesezeichen und Publikationen zu erfahren, lesen Sie [
    dieses Tutorial](LesezeichenPublikationenManagen "wikilink").
    
6.  **Die Neuigkeiten** - hier publizieren die Entwickler von
    ${project.name} aktuelle Neuigkeiten, z. B. neue Funktionen oder
    auch Hinweise zu Wartungsarbeiten am System.  
    
7.  **Beliebte Tags** - zeigt Ihnen die aktuell beliebtesten [Tags](Tags "wikilink").

}

${project.theme == "wueresearch"}{

    
Diese Anleitung gibt Ihnen einen ersten Überblick zum Aufbau der Benutzeroberfläche von ${project.name}.
Diese besteht aus den folgenden Bestandteilen:

![<File:userinterfacewue.png>](wueresearch/Programmoberfläche/userinterfacewue.png  "fig: File:userinterfacewue.png")  

1. **Die Suche** - bietet Ihnen mehrere Möglichkeiten, ${project.name}
    nach Informationen zu durchsuchen. Klicken Sie mit der linken Maustaste
    auf den blauen Pfeil bei *"Suche"* und wählen Sie im darauf
    erscheinenden Menü den Datensatz aus, nach welchem Sie suchen möchten. Wollen Sie beispielsweise einen bestimmten Autor suchen, dann nutzen Sie das Feld "*Autor*".
    Geben Sie dann in das weiße Feld Ihren Suchbegriff ein. Klicken Sie abschließend auf das
    Lupensymbol rechts oder drücken Sie *"Enter"* auf Ihrer Tastatur.
    ${project.name} gibt Ihnen in wenigen Sekunden alle verfügbaren
    Ergebnisse aus.  
    Auf der [Suche-Hilfeseite](SuchenHilfe "wikilink") finden Sie weitere
    Informationen über die verschiedenen Optionen, die Sie beim Suchen in
    ${project.name} haben. Sie erreichen diese und weitere Hilfsseiten per Klick auf das Infosymbol im Suchfeld.
  
2. **Die Sprachleiste** - ermöglicht es Ihnen, zwischen den zwei
    verfügbaren Sprachversionen in ${project.name} zu wechseln. Klicken Sie
    auf *"en"*, um ${project.name} mit einer englischsprachigen Oberfläche
    zu verwenden, auf *"de"*, um ${project.name} auf die deutsche
    Sprachversion umzustellen.
  
3. **Das Menü in der oberen Leiste** - stellt Ihnen die wichtigsten Funktionen von
    ${project.name} zur Verfügung. Wenn Sie auf einen Menüpunkt mit Pfeil klicken, sehen Sie dessen Unterpunkte. 
    Unterpunkte, die *kursiv* dargestellt sind, werden nur in der
    erweiterten Ansicht angezeigt. Wie sie die erweiterte Ansicht
    freischalten, [lesen Sie hier](ErweiterteFunktionenFreischalten "wikilink"). 
    Die einzelnen Optionen (Auflistung von links nach rechts, mit Unterpunkten) sind:

    - **Home:** Damit gelangen Sie zur Startseite von ${project.name}.

    - **MEIN${project.name}:** Ihre persönliche Startseite.
        
		-   **Meine Einträge:** Zeigt Ihre Einträge auf Ihrer persönlichen Nutzerseite an.
        
		-   ***Private Einträge:*** Zeigt alle Einträge an, die nur Sie selbst sehen können.
		
		-   ***Einträge für Freunde:*** Zeigt die Einträge an, die nur Sie selbst oder Ihre Freunde sehen können.
    
		-   ***Dokumente:*** Wenn Sie Ihren Einträgen [Dokumente (z. B. als PDF) angefügt](DokumentAnhängen "wikilink") haben, können Sie hier eine Übersicht sehen.
		
		-   ***Duplikate:*** Zeigt Einträge an, die wahrscheinlich Duplikate sind. So können Sie Ihre Literaturliste ganz einfach bereinigen.
        
		-   ***Konzepte:*** [Konzepte](Konzepte "wikilink") ermöglichen es Ihnen, mehrere Schlagwörter zu gruppieren.
		
    	-   **Diskutierte Einträge:** Zeigt die Einträge an, zu denen Sie einen [Diskussionsbeitrag](DiskutierteEinträge "wikilink") geleistet haben.

    	-   **Verfolgte Einträge:** Zeigt die Einträge von Ihren [gefolgten](Follower "wikilink") Nutzern an.

    	-   **Einträge von Freunden:** Zeigt Einträge an, die nur für Freunde sichtbar sind und von den Benutzern stammen, auf deren Freundesliste Sie stehen.
    
    	-   ***Lebenslauf:*** Hier können Sie einen [Text/Lebenslauf/Vita hinterlegen](LebenslaufÄndern "wikilink"), den andere Nutzer in ${project.name} einsehen können.
    	
    	-   ***Publikationen durchstöbern:*** Durchsuchen Sie Ihre gespeicherten Publikation, indem Sie die Filteroptionen benutzen. In [diesem Tutorial](PublikationenDurchstöbern "wikilink") finden Sie weitere Informationen dazu.
    	
    	-   ***BibTeX exportieren:*** Exportiert ihre Literaturdaten im BibTeX-Format.
    
    - **Eintragen:** Dieses Menü hilft Ihnen beim Eintragen neuer Lesezeichen/Publikationen.
	  
        -   **Lesezeichen hinzufügen:** Fügen Sie ein neues Lesezeichen hinzu. In [ diesem Tutorial](LesezeichenPublikationenManagen "wikilink") lesen Sie, wie Sie Lesezeichen und Publikationen verwalten.
        
        -   **Publikation hinzufügen:** Fügen Sie eine neue Publikation hinzu. In [ diesem Tutorial ](LesezeichenPublikationenManagen "wikilink") lesen Sie, wie Sie Lesezeichen und Publikationen verwalten.
            
        -   **Lesezeichen importieren:** Sie können Lesezeichen aus Ihrem Browser oder [Delicious](https://delicious.com/) importieren (eine Anleitung dazu [ finden Sie hier](LesezeichenImportieren "wikilink")).

        -   **Publikationen importieren:** Sie können Publikatonen importieren, indem Sie eine bestehende BibTeX- oder EndNote-Datei hochladen. 

    - **Einrichtungen:** Hier finden Sie eine Liste aller Einrichtungen der Universität. Nach diesen können Sie auch suchen und weitere Informationen finden.
        
    - **Gruppen:** Zeigt Ihnen die [Funktionen zu Gruppen](Gruppenfunktionen "wikilink") an.
    
        -   **Alle Gruppen:** Zeigt Ihnen eine Übersicht aller Gruppen in ${project.name} an. Hier können Sie diesen auch beitreten.
        
        -   **Eine neue Gruppe erstellen:** Hier können Sie eine neue Gruppe erstellen.

    - **Personen:** Hier finden Sie eine Liste aller [Personeneinträge](Personeneinträge "wikilink") von ${project.name}. Darüber können Sie bestimmte Namen suchen und an weitere Informationen zu ausgewählten Personen gelangen.
       
    - **Publikationen:** Hier finden Sie die Publikationen, welche als letztes zu ${project.name} hinzugefügt wurden. Zudem können Sie nach bestimmten Publikationen suchen.
    
4.  **Das rechte Hauptmenü:** Hier finden Sie weitere wichtige
    Funktionen von ${project.name}.

    -   **Ihr Benutzername:** Unter diesem Namen sind Sie gerade bei
        ${project.name} angemeldet. Wenn Sie auf Ihren Namen klicken,
        kommen Sie auf Ihre [ Nutzerseite](UserSeite "wikilink") und
        Ihnen werden Ihre Lesezeichen und Publikationen angezeigt.
        
    -   **Das Personensymbol:** Klicken Sie auf dieses Symbol rechts
        neben Ihrem Namen, um weitere Funktionen sehen zu können.
        
        -   **Eingang:** Ihr [Lesezeichen/Publikation-Posteingang](Eingang "wikilink").
            Freunde und Gruppen können Ihnen Lesezeichen/Publikationen
            zusenden, diese Eingänge landen hier.
            
        -   **Ablage:** In der [Ablage](Ablage "wikilink") können Sie eine aktuelle Literaturliste zusammenstellen.
        
        -   **Freunde:** Zeigt Ihre [Freunde](FreundeHinzufügen "wikilink") an.
        
        -   **Schlagworte bearbeiten:** Hier können Sie Ihre Schlagworte (Tags) und [ Konzepte](Konzepte "wikilink") bearbeiten.
        
        -   **Einstellungen:** Hier können Sie Ihre [persönlichen Benutzereinstellungen ansehen und ändern](Einstellungen "wikilink").
        
        -   **Weblog:** Leitet Sie auf [den Weblog von  ${project.name} ]((http://bibsonomy.blogspot.com/)) weiter.
        
        -   **Hilfe:** Hier gelangen Sie zu den Hilfeseiten von ${project.name}.
        
        -   **Abmelden:** Klicken Sie hier, um sich von ${project.name} abzumelden.  
        
5.  **Der Inhaltsbereich** - hier sehen Sie eine Auswahl der momentan
    verfügbaren Lesezeichen und Publikationen. Um mehr über diesen
    Bereich, Lesezeichen und Publikationen zu erfahren, lesen Sie [
    dieses Tutorial](LesezeichenPublikationenManagen "wikilink").          
    
}

--------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink")
