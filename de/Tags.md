<!-- en: Tags --> 

## Schlagworte (Tags) nutzen
-------------------------

Schlagworte (engl. tags) ermöglichen es, Inhalte (und somit auch
Literaturangaben) zu organisieren. Mit ${project.name} haben Sie
außerdem die Möglichkeit, nach Schlagwörtern zu suchen und diese zu
vergleichen.


------------------------------------------------------------------------

### Wie funktioniert das Schlagwort-System?

Sicherlich kennen Sie von Ihrem Computer oder aus dem realen Leben die
Möglichkeit, Dateien/Dokumente in Ordnern und Unterordnern zu
organisieren. Eine Datei liegt dabei in einem ganz bestimmten Ordner.  
Das Schlagwort-System unterscheidet sich davon grundlegend. Einer
Datei/Literaturangabe können Sie so viele Schlagworte zuordnen, wie Sie
möchten. Auf diese Weise können Sie ganz leicht alle Dateien zu einem
bestimmten Thema finden, und zwar, indem Sie einfach nach dem
dazugehörigen Schlagwort suchen. Der Vorteil des Schlagwort-Systems ist,
dass man bei der Suche nach Literatur die Schlagwörter beliebig
kombinieren kann und so ganz spezifische Ergebnisse erzielen kann.

Wollen Sie beispielsweise Literatur zum Thema *"Politik in Frankreich"*
finden, suchen Sie einfach mit den Schlagwörtern *"Politik"* UND
*"Frankreich"*, und schon werden Ihnen alle Artikel angezeigt, die beide
Themen behandeln.  
Durch diese Funktion erleichtert es Ihnen ${project.name} erheblich,
Ihre Literaturangaben zu organisieren, zu verwalten und mit anderen
auszutauschen.


**Tipp:** Gute Schlagworte sind kurz und prägnant. Außerdem empfehlen
wir immer Singular (Einzahl) zu verwenden (also *"Baum"* anstatt *"Bäume"* -
**oder noch besser:** gleich zwei Schlagwörter: *"Baum"* UND *"Wald"*),
somit vermeiden Sie unnötige Wortformen. Wenn Sie ein Schlagwort nutzen
wollen, das aus zwei Wörtern besteht (z.B. *"Computer Science"*), dann
verwenden Sie [PascalCase](http://c2.com/cgi/wiki?PascalCase) (z.B. *"ComputerScience"*).

------------------------------------------------------------------------

### Tags zu Lesezeichen/Publikationen hinzufügen

Sie können Ihren Lesezeichen/Publikationen Tags zuordnen, um sie
effizienter zu organisieren und den Überblick zu behalten.  
Immer, wenn Sie [ ein Lesezeichen/eine Publikation Ihrer Sammlung
hinzufügen](LesezeichenPublikationenManagen "wikilink") (egal, ob Sie
einen neuen Eintrag erstellen oder einen bestehenden Eintrag
übernehmen), können Sie diesen Einträgen Tags zuordnen (taggen). Beim
Hinzufügen macht Ihnen das System **Empfehlungen** für Tags, basierend
auf Ihren häufig genutzten Tags. Außerdem zeigt Ihnen das System die
**Tags des kopierten Eintrags an** (falls Sie einen bestehenden Eintrag
in Ihre Sammlung übernehmen).  

${project.theme == "bibsonomy"}{

![ <File:addTags.png>](tags/addTags.png  "fig: File:addTags.png")  

------------------------------------------------------------------------

### Tags von Lesezeichen/Publikationen bearbeiten

Wenn Sie einen Eintrag bereits in Ihre Sammlung übernommen haben, ist es
weiterhin möglich, die angegebenen Tags zu bearbeiten.  
Es gibt mehrere Möglichkeit, die Tags zu bearbeiten. 

1.  Tags bearbeiten via **"Eintrag bearbeiten"**
2.  Tags bearbeiten via **"Schnellbearbeitung"**
3.  Tags bearbeiten via **"Tags bearbeiten"**

![ <File:editPost2.png>](tags/editPost2.png  "fig: File:editPost2.png")  

-----------------------------

#### Tags bearbeiten via "Eintrag bearbeiten" 
Klicken Sie auf das **schwarze Stiftsymbol** (dieses Lesezeichen/diese Publikation
bearbeiten) auf der rechten Seite eines Eintrags. Sie kommen auf eine
Seite, auf der Sie alle Informationen über diesen Eintrag,
einschließlich der Tags, bearbeiten können. Wenn Sie fertig sind mit dem
Bearbeiten, vergessen Sie bitte nicht, am Ende der Seite auf
**Speichern** zu klicken.  

#### Tags bearbeiten via "Schnellbearbeitung"

Klicken Sie auf das **blaue Stiftsymbol** (Tags bearbeiten) auf der
linken Seite eines Eintrags. Ein Pop-Up-Fenster erscheint, in dem Sie
die Tags eines Eintrags direkt bearbeiten können. Entfernen Sie Tags,
indem Sie auf das X-Symbol klicken, und fügen Sie Tags hinzu, indem Sie
diese in das Textfeld schreiben (durch Leerzeichen getrennt). Klicken
Sie auf **Speichern**, um Ihre Änderungen zu speichern, oder auf
**schließen**, um Ihre Änderungen zu verwerfen. 

#### Tags bearbeiten via "Tags bearbeiten"

In ${project.name} können Sie nicht nur die Tags eines einzelnen
Eintrags bearbeiten, sondern auch alle Ihre Tags, die Sie nutzen, auf
einmal. Dazu klicken Sie auf das **Personensymbol** im [ rechten
Hauptmenü](Programmoberfläche "wikilink") und dann auf **Tags bearbeiten**. Auf der folgenden Seite können Sie Ihre Tags und [
Konzepte](Konzepte "wikilink") bearbeiten.  

- **Umbenennen/Ersetzen von Tags:** Hier können Sie einen bestimmten Tag
durch einen anderen Tag ersetzen. Das ist nützlich, falls Sie zwei
ähnliche Tags benutzt haben, die Sie zu einem Tag zusammenfügen wollen,
z.B. können Sie *augmentedreality* durch *augmentedReality* ersetzen. 

- **Subtags zu Konzepten hinzufügen:** Um einen Subtag zu einem [
Konzept](Konzepte "wikilink") hinzuzufügen, geben Sie einfach den Namen
des Konzepts in das Feld 'Supertag' ein und den Namen des Tags, den Sie
hinzufügen wollen, in das Feld 'Subtag'.  

- **Subtags von Konzepten löschen:** Um einen Subtag von einem [
Konzept](Konzepte "wikilink") zu löschen, geben Sie einfach den Namen
des Konzepts in das Feld 'Supertag' ein und den Namen des Tags, den Sie
löschen wollen, in das Feld 'Subtag'.  

------------------------------------------------------------------------

### Navigation via Tags

Sie können in ${project.name} mit Hilfe von Tags Lesezeichen und
Publikationen suchen.  

- **Option 1:**
	Um nach **Lesezeichen/Publikationen mit einem bestimmten
	Tag** zu suchen, benutzen Sie einfach die Suchleiste rechts oben.
	Klicken Sie auf den **blauen Pfeil** neben 'Suche' und wählen Sie im
	Dropdown-Menü **Tag**. Geben Sie den Tag, mit dem Sie suchen möchten, in
	das Suchfeld ein und klicken Sie auf das **Lupensymbol** oder drücken
	Sie die **Enter-Taste**.  
	![ <File:search.png>](tags/search.png  "fig: File:search.png")  

}

${project.theme == "puma"}{

![ <File:addTags.png>](tags/addTags.png  "fig: File:addTags.png")  

------------------------------------------------------------------------

### Tags von Lesezeichen/Publikationen bearbeiten

Wenn Sie einen Eintrag bereits in Ihre Sammlung übernommen haben, ist es
weiterhin möglich, die angegebenen Tags zu bearbeiten.  
Es gibt mehrere Möglichkeit, die Tags zu bearbeiten. 

1.  Tags bearbeiten via **"Eintrag bearbeiten"**
2.  Tags bearbeiten via **"Schnellbearbeitung"**
3.  Tags bearbeiten via **"Tags bearbeiten"**

![ <File:editPost2.png>](tags/editPost2.png  "fig: File:editPost2.png")  

-----------------------------

#### Tags bearbeiten via "Eintrag bearbeiten" 
Klicken Sie auf das **schwarze Stiftsymbol** (dieses Lesezeichen/diese Publikation
bearbeiten) auf der rechten Seite eines Eintrags. Sie kommen auf eine
Seite, auf der Sie alle Informationen über diesen Eintrag,
einschließlich der Tags, bearbeiten können. Wenn Sie fertig sind mit dem
Bearbeiten, vergessen Sie bitte nicht, am Ende der Seite auf
**Speichern** zu klicken.  

#### Tags bearbeiten via "Schnellbearbeitung"

Klicken Sie auf das **blaue Stiftsymbol** (Tags bearbeiten) auf der
linken Seite eines Eintrags. Ein Pop-Up-Fenster erscheint, in dem Sie
die Tags eines Eintrags direkt bearbeiten können. Entfernen Sie Tags,
indem Sie auf das X-Symbol klicken, und fügen Sie Tags hinzu, indem Sie
diese in das Textfeld schreiben (durch Leerzeichen getrennt). Klicken
Sie auf **Speichern**, um Ihre Änderungen zu speichern, oder auf
**schließen**, um Ihre Änderungen zu verwerfen. 

#### Tags bearbeiten via "Tags bearbeiten"

In ${project.name} können Sie nicht nur die Tags eines einzelnen
Eintrags bearbeiten, sondern auch alle Ihre Tags, die Sie nutzen, auf
einmal. Dazu klicken Sie auf das **Personensymbol** im [ rechten
Hauptmenü](Programmoberfläche "wikilink") und dann auf **Tags bearbeiten**. Auf der folgenden Seite können Sie Ihre Tags und [
Konzepte](Konzepte "wikilink") bearbeiten.  

- **Umbenennen/Ersetzen von Tags:** Hier können Sie einen bestimmten Tag
durch einen anderen Tag ersetzen. Das ist nützlich, falls Sie zwei
ähnliche Tags benutzt haben, die Sie zu einem Tag zusammenfügen wollen,
z.B. können Sie *augmentedreality* durch *augmentedReality* ersetzen. 

- **Subtags zu Konzepten hinzufügen:** Um einen Subtag zu einem [
Konzept](Konzepte "wikilink") hinzuzufügen, geben Sie einfach den Namen
des Konzepts in das Feld 'Supertag' ein und den Namen des Tags, den Sie
hinzufügen wollen, in das Feld 'Subtag'.  

- **Subtags von Konzepten löschen:** Um einen Subtag von einem [
Konzept](Konzepte "wikilink") zu löschen, geben Sie einfach den Namen
des Konzepts in das Feld 'Supertag' ein und den Namen des Tags, den Sie
löschen wollen, in das Feld 'Subtag'.  

------------------------------------------------------------------------

### Navigation via Tags

Sie können in ${project.name} mit Hilfe von Tags Lesezeichen und
Publikationen suchen.  

- **Option 1:**
	Um nach **Lesezeichen/Publikationen mit einem bestimmten
	Tag** zu suchen, benutzen Sie einfach die Suchleiste rechts oben.
	Klicken Sie auf den **blauen Pfeil** neben 'Suche' und wählen Sie im
	Dropdown-Menü **Tag**. Geben Sie den Tag, mit dem Sie suchen möchten, in
	das Suchfeld ein und klicken Sie auf das **Lupensymbol** oder drücken
	Sie die **Enter-Taste**.  
	![ <File:search.png>](tags/search.png  "fig: File:search.png")  

}

${project.theme == "wueresearch"}{

![ <File:addTagswue.png>](wueresearch/Tags/addTagswue.png  "fig: File:addTagswue.png")  

------------------------------------------------------------------------

### Tags von Lesezeichen/Publikationen bearbeiten

Wenn Sie einen Eintrag bereits in Ihre Sammlung übernommen haben, ist es
weiterhin möglich, die angegebenen Tags zu bearbeiten.  
Es gibt mehrere Möglichkeit, die Tags zu bearbeiten. 

1.  Tags bearbeiten via **"Eintrag bearbeiten"**
2.  Tags bearbeiten via **"Schnellbearbeitung"**
3.  Tags bearbeiten via **"Tags bearbeiten"**

![ <File:editTagswue.png>](wueresearch/Tags/editTagswue.png  "fig: File:editTagswue.png")  

-----------------------------

#### Tags bearbeiten via "Eintrag bearbeiten" 
Klicken Sie auf das **schwarze Stiftsymbol** (dieses Lesezeichen/diese Publikation
bearbeiten) auf der rechten Seite eines Eintrags. Sie kommen auf eine
Seite, auf der Sie alle Informationen über diesen Eintrag,
einschließlich der Tags, bearbeiten können. Wenn Sie fertig sind mit dem
Bearbeiten, vergessen Sie bitte nicht, am Ende der Seite auf
**Speichern** zu klicken.  

#### Tags bearbeiten via "Schnellbearbeitung"

Klicken Sie auf das **blaue Stiftsymbol** (Tags bearbeiten) auf der
linken Seite eines Eintrags. Ein Pop-Up-Fenster erscheint, in dem Sie
die Tags eines Eintrags direkt bearbeiten können. Entfernen Sie Tags,
indem Sie auf das X-Symbol klicken, und fügen Sie Tags hinzu, indem Sie
diese in das Textfeld schreiben (durch Leerzeichen getrennt). Klicken
Sie auf **Speichern**, um Ihre Änderungen zu speichern, oder auf
**schließen**, um Ihre Änderungen zu verwerfen. 

#### Tags bearbeiten via "Tags bearbeiten"

In ${project.name} können Sie nicht nur die Tags eines einzelnen
Eintrags bearbeiten, sondern auch alle Ihre Tags, die Sie nutzen, auf
einmal. Dazu klicken Sie auf das **Personensymbol** im [ rechten
Hauptmenü](Programmoberfläche "wikilink") und dann auf **Tags bearbeiten**. Auf der folgenden Seite können Sie Ihre Tags und [
Konzepte](Konzepte "wikilink") bearbeiten.  

- **Umbenennen/Ersetzen von Tags:** Hier können Sie einen bestimmten Tag
durch einen anderen Tag ersetzen. Das ist nützlich, falls Sie zwei
ähnliche Tags benutzt haben, die Sie zu einem Tag zusammenfügen wollen,
z.B. können Sie *augmentedreality* durch *augmentedReality* ersetzen. 

- **Subtags zu Konzepten hinzufügen:** Um einen Subtag zu einem [
Konzept](Konzepte "wikilink") hinzuzufügen, geben Sie einfach den Namen
des Konzepts in das Feld 'Supertag' ein und den Namen des Tags, den Sie
hinzufügen wollen, in das Feld 'Subtag'.  

- **Subtags von Konzepten löschen:** Um einen Subtag von einem [
Konzept](Konzepte "wikilink") zu löschen, geben Sie einfach den Namen
des Konzepts in das Feld 'Supertag' ein und den Namen des Tags, den Sie
löschen wollen, in das Feld 'Subtag'.  

------------------------------------------------------------------------

### Navigation via Tags

Sie können in ${project.name} mit Hilfe von Tags Lesezeichen und
Publikationen suchen.  

- **Option 1:**
	Um nach **Lesezeichen/Publikationen mit einem bestimmten
	Tag** zu suchen, benutzen Sie einfach die Suchleiste rechts oben.
	Klicken Sie auf den **blauen Pfeil** neben 'Suche' und wählen Sie im
	Dropdown-Menü **Tag**. Geben Sie den Tag, mit dem Sie suchen möchten, in
	das Suchfeld ein und klicken Sie auf das **Lupensymbol** oder drücken
	Sie die **Enter-Taste**.  
	![ <File:searchTagswue.png>](wueresearch/Tags/searchTagswue.png  "fig: File:searchTagswue.png")  

}

	Die Lesezeichen/Publikationen, die mit diesem Tag getaggt wurden, werden
	angezeigt.  
	In der Sidebar rechts werden außerdem zusätzliche Informationen zu
	diesem Tag angezeigt: [ Konzepte](Konzepte "wikilink") mit diesem Tag, [verwandte 	Tags](Tags#Verwandte_Tags "wikilink") und [ähnliche Tags](Tags#Ähnliche_Tags "wikilink").  

- **Option 2:**
	Wenn Sie auf einen Tag, der bei einem Eintrag angezeigt
	wird (wie z.B. auf der [ Homepage](Bib:\ "wikilink")), klicken, sehen
	Sie **alle Einträge eines Nutzers** mit diesem bestimmten Tag. In der
	Sidebar auf der rechten Seite sehen Sie zusätzliche Informationen über  
	-  diesen Tag als Tag von allen Nutzern
    -   diesen Tag als Konzept von diesem bestimmten Nutzer
    -   diesen Tag als Konzept von allen Nutzern
    -   Verwandte Tags
    -   die Konzepte des Nutzers
    -   die Tags des Nutzers.

------------------------------------------------------------------------

### Verwandte Tags

Verwandte Tags sind gemeinsam einem bestimmten Eintrag zugeordnet. Wenn
ein Benutzer zum Beispiel einen Eintrag mit *"Java"* und *"Programmieren"*
getaggt hat, dann sind diese beiden Tags verwandt.

------------------------------------------------------------------------

### Ähnliche Tags

Ähnliche Tags werden nach einer komplexen Ähnlichkeitsmessung berechnet.
Im Prinzip sind ähnliche Tags Synonyme.

------------------------------------------------------------------------

### Systemtags

Systemtags sind spezielle Tags (Schlagworte), die eine feste Bedeutung
haben. Derzeit bietet ${project.name} drei Typen von Systemtags an:

-   Ausführbare Systemtags
-   Markup-Systemtags
-   Such-Systemtags

Klicken Sie [hier](SystemTags "wikilink") um mehr über Systemtags und deren Funktionen zu erfahren.

------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink")
