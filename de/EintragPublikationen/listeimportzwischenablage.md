<!-- en: AddPublications/listimportclipboard -->

-----------------

[`Zurück zur Übersicht zum Importieren einer Literaturliste es hier.`](EintragPublikationen/LiteraturlisteImportieren "wikilink")

---------------------

## Importieren aus der Zwischenablage
------------------

1. Bewegen Sie Ihren Mauszeiger auf den Menüpunkt **"Eintragen"** im [
Hauptmenü](Programmoberfläche "wikilink"). Ein Untermenü klappt auf.

2.  Klicken Sie im Untermenü mit der linken Maustaste auf **"Publikation
   hinzufügen"**.
   
3.  Klicken Sie auf den Reiter **"BibTeX/EndNote-Schnipsel"**.

4.  Fügen Sie den Text aus der Zwischenablage in das große Textfeld
    namens **"Schnipsel"** ein.  
    **Hinweis:** Dies können Sie zum Beispiel dadurch erreichen, indem
    Sie mit der rechten Maustaste auf das Textfeld klicken und im darauf
    erscheinenden Menü mit der linken Maustaste auf
    *"Einfügen"* klicken.
    
5.  Klicken Sie auf **"Weiter"**.

6.  Anschließend zeigt Ihnen ${project.name} eine Übersicht
    aller Daten. Überprüfen Sie, ob die Daten erfolgreich
    importiert wurden.
    
7.  Klicken Sie auf **"Speichern"**, um den Import
    vollständig abzuschließen. 
    
${project.theme == "bibsonomy"}{

![<File:1.png>](listeimportzwischenablage/1.png  "fig: File:1.png")

}

${project.theme == "puma"}{

![<File:1.png>](listeimportzwischenablage/1.png  "fig: File:1.png")

}

${project.theme == "wueresearch"}{

![<File:clipboardwue.png>](wueresearch/EintragPublikationen/listeimportzwischenablage/clipboardwue.png  "fig: File:clipboardwue.png")

}
