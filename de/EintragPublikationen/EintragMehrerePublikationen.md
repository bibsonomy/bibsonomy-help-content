<!-- en: AddPublications/ImportMultiplePublications -->

-----------------

[`Zurück zur Übersicht zum Hinzufügen von Publikationen geht es hier.`](EintragPublikationen "wikilink")

---------------------

## Mehrere Publikationen importieren
--------------------------

Publikationen lassen sich nicht nur einzeln in ${project.name} importieren.
Per BibTeX- oder EndNote-Datei ist es möglich, diese in größeren Mengen einzuführen.
Jene Dateien können als einheitliche Datenformate in allen gängigen Programmen zur
Literaturverwaltung erstellt werden. Exportieren Sie dazu die gewünschte Publikationsliste
aus Ihrem Programm. [Diese Anleitung](EintragPublikationen/listeimportcitavi "wikilink") 
kann Ihnen dabei behilflich sein.
Wenn Sie eine entsprechende Datei zum Import besitzen, dann folgen Sie einfach den nächsten Schritten.

-------------------------------------

1. Klicken Sie im ${project.name}-Menü auf die Schaltfläche **"Eintragen"** und dann auf **"Publikationen importieren"**.
Alternativ können Sie auch [diesem Link](https://www.bibsonomy.org/import/publications) folgen.

2. Hier müssen Sie zunächst per Knopfdruck auf **"Durchsuchen"** den Speicherort Ihrer Datei auswählen. Anschließend können Sie bestimmen, ob dieser Import öffentlich oder nur für Sie sichtbar sein soll. Möglich ist auch, dass Sie bei **"andere"** eine Gruppe auswählen, die den Import sehen darf (bspw. Freunde).

3. Schließlich können Sie noch folgende Einstellungen vornehmen:
	- Wenn Sie einen Haken bei **"Vor Import bearbeiten"** setzen, dann werden Ihre Importe nicht direkt gespeichert. Stattdessen haben Sie die Möglichkeit, die Publikationen zu bearbeiten, bevor sie zu Ihrer Sammlung hinzugefügt werden.
	- Wenn Sie darunter einen Haken setzen, dann überschreiben Sie ältere Einträge, die dieselbe Publikation referenzieren.
	- Bei **"Trennzeichen"** müssen Sie auswählen, durch welches Zeichen die Publikationen in Ihrer Datei getrennt wurden. Möglich sind Leerzeichen, Kommata und Semikola.
	- Haben Sie keine Leerzeichen angegeben, so können Sie darunter selber angeben, durch welches Zeichen die Einträge getrennt sind. Beispielsweise Bindestriche oder Unterstriche.
	- Zuletzt müssen Sie die Kodierung Ihrer Datei angeben. Häufig ist dies "UTF-8".

4. Haben Sie alle Einstellungen vorgenommen, dann drücken Sie auf **"Weiter"**. Hiermit ist der Import abgeschlossen.

------------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink")