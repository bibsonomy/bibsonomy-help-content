<!-- en: AddPublications/AddManual -->

-----------------

[`Zurück zur Übersicht zum Hinzufügen von Publikationen geht es hier.`](EintragPublikationen "wikilink")

---------------------

## Hinzufügen von Publikation per Hand
-------------------

Sie können einzelne Publikationen in ${project.name}  auch manuell eintragen. Klicken Sie dazu im Hauptmenü auf den Button "Eintragen" und auf "Publikation hinzufügen". Folgen Sie anschließend diesen Schritten:

------------------

1. Klicken Sie auf den Button "Per Hand" um das manuelle Hinzufügen einer Publikation auszuwählen. Daraufhin werden mehrere Felder erscheinen.

2. Wählen Sie den Eintragstyp der Publikation (z. B. Artikel, Buch, Sammlung). 

3. Geben Sie den Titel der Publikation ein.

4. Geben Sie den/die Namen des/der Autoren ein. Nennen Sie einen Autor pro Zeile im Format "Nachname, Vorname" (z. B. Hotho, Andreas). Wenn der Autor eine Institution o. ä. ist, die nicht zu dem Format passt, umgeben Sie den Namen mit geschweiften Klammern (z. B. {Bundesministerium für Bildung und Forschung} ). Dadurch wird der Name nicht getrennt. 

5. Wenn Sie Autor oder Herausgeber der Publikation sind, können Sie dies in der Box markieren. Das System-Tag "myown" wird dem Eintrag dann beigefügt. 

6.  Die Angabe des "Herausgebers" ist nur notwendig, wenn kein Autor angegeben wurde. Geben Sie die Herausgeber erneut in unterschiedlichen Zeilen und im Format "Nachname, Vorname" ein. 

7. Nennen Sie zuletzt das Jahr der Publikation. Es sollte aus vier Ziffern bestehen (z. B. 2021). Drücken Sie dann auf "Weiter". Hiermit ist die Publikation zu ${project.name} hinzugefügt. 

----------------

![ <File:DEManuell.png>](EintragManuell/DEManuell.png  "fig: File:DEManuell.png")

---------------------


[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink")
