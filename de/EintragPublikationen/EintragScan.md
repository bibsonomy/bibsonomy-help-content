<!-- en: AddPublications/AddScan -->

-----------------

[`Zurück zur Übersicht zum Hinzufügen von Publikationen geht es hier.`](EintragPublikationen "wikilink")

---------------------

## Eintrag einer Publikation per Scannen des ISBN-Codes
---------------------

${project.theme == "bibsonomy"}{

**Vorraussetzung**: Für diese Methode benötigen Sie eine Webcam oder ein anderes aufnahmefähiges Gerät. Besitzen Sie dies nicht, dann wählen Sie bitte eine andere Methode, um Ihre Publikation bei ${project.name} einzutragen. Andere Möglichkeiten sind [hier](EintragPublikationen "wikilink") aufgelistet.

---------------------

Mit Ihrer Webcam können Sie die Barcodes Ihrer Bücher scannen, um neue Publikationen hinzuzufügen. Als Ergebnis bekommen Sie ein vorausgefülltes Formular mit allen verfügbaren Metadaten zum Buch. Befolgen Sie dazu die folgende Schritte:

1. Bewegen Sie ihren Mauszeiger auf den Menüpunkt "Eintragen" (im Hauptmenü). Ein Untermenü klappt auf.

2. Klicken Sie im Untermenü mit der linken Maustaste auf "Publikation hinzufügen".

3. Wählen Sie "Code scannen". Ihr Webbrowser zeigt wahrscheinlich eine Warnmeldung an, dass die Webseite ${project.name} versucht, auf Ihre Webcam zuzugreifen. Falls eine derartige Meldung erscheint, erlauben Sie den Zugriff.

4. Halten Sie den ISBN-Strichcode der Publikation ruhig und gut sichtbar vor die Kamera. Sobald BibSonomy den ISBN-Strichcode erkennt, ertönt ein Kameraklicken.

5. Die automatisch erkannten Daten werden angezeigt. Kontrollieren Sie die Daten und ergänzen/korrigieren Sie, wenn nötig. Klicken Sie abschließend auf "Speichern".

---------------------

![ <File:Scan.jpg>](EintragScan/Scan.jpg "fig: File:Scan.jpg")

}

${project.theme == "puma"}{

**Vorraussetzung**: Für diese Methode benötigen Sie eine Webcam oder ein anderes aufnahmefähiges Gerät. Besitzen Sie dies nicht, dann wählen Sie bitte eine andere Methode, um Ihre Publikation bei ${project.name} einzutragen. Andere Möglichkeiten sind [hier](EintragPublikationen "wikilink") aufgelistet.

---------------------

Mit Ihrer Webcam können Sie die Barcodes Ihrer Bücher scannen, um neue Publikationen hinzuzufügen. Als Ergebnis bekommen Sie ein vorausgefülltes Formular mit allen verfügbaren Metadaten zum Buch. Befolgen Sie dazu die folgende Schritte:

1. Bewegen Sie ihren Mauszeiger auf den Menüpunkt "Eintragen" (im Hauptmenü). Ein Untermenü klappt auf.

2. Klicken Sie im Untermenü mit der linken Maustaste auf "Publikation hinzufügen".

3. Wählen Sie "Code scannen". Ihr Webbrowser zeigt wahrscheinlich eine Warnmeldung an, dass die Webseite ${project.name} versucht, auf Ihre Webcam zuzugreifen. Falls eine derartige Meldung erscheint, erlauben Sie den Zugriff.

4. Halten Sie den ISBN-Strichcode der Publikation ruhig und gut sichtbar vor die Kamera. Sobald BibSonomy den ISBN-Strichcode erkennt, ertönt ein Kameraklicken.

5. Die automatisch erkannten Daten werden angezeigt. Kontrollieren Sie die Daten und ergänzen/korrigieren Sie, wenn nötig. Klicken Sie abschließend auf "Speichern".

---------------------

![ <File:Scan.jpg>](EintragScan/Scan.jpg "fig: File:Scan.jpg")

}

${project.theme == "wueresearch"}{

**Warnung:** Das Hinzufügen von Publikationen per Scannen des ISBN-Codes ist zurzeit leider nicht möglich.
Weitere Möglichkeiten um Ihre Publikationen einzutragen finden Sie [hier](EintragPublikationen "wikilink").

}

---------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink")
