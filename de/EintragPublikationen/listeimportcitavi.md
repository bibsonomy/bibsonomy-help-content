<!-- en: AddPublications/listimportcitavi -->

-----------------

[`Zurück zur Übersicht zum Importieren einer Literaturliste es hier.`](EintragPublikationen/LiteraturlisteImportieren "wikilink")

---------------------

## BibTeX-Export aus Citavi
------------------

  1.  Klicken Sie im oberen Menü auf *"Datei"*, dann auf *"Exportieren"*.
      
  2.  Ein Dialog erscheint. Wählen Sie, ob Sie nur bestimmte oder alle Artikel exportieren möchten. Klicken Sie dann im Dialog unten auf *"Weiter"*.
      
  3.  Sie werden nach dem *Export-Format* gefragt. Wählen Sie *"BibTeX
        (ohne Erweiterung)"* aus und klicken Sie anschließend auf
        *"Weiter"*.
      
  4.  Sie werden nach dem Speicherort gefragt. Wählen Sie *"Textdatei
        in der Zwischenablage speichern"* aus und klicken Sie auf *"Weiter"*.
      
  5.  Abschließend werden Sie gefragt, ob Sie die vorgenommenen Export-Einstellungen speichern möchten.  **Tipp:** Wir empfehlen Ihnen folgende Vorgehensweise: Wählen Sie *"Ja, unter dem Namen"* aus. Tragen Sie in das Textfeld: *"BibTeX"* ein. Setzen Sie das Häkchen bei *"Automatisch exportieren beim Speichern"*. **Hintergrund:** Sollten Sie dieser Empfehlung folgen, dann exportiert Citavi automatisch alle Daten, sobald Sie das Citavi-Projekt speichern (z. B. über *"Datei"* &gt; *"Speichern"*).
  
  6.  Klicken Sie auf *"Weiter"* - Sie erhalten eine Meldung *"Export erfolgreich abgeschlossen"*. Bestätigen Sie diese Meldung mit *"OK"*.  

**Resultat:** Die von Ihnen exportierten Daten befinden sich nun in der Zwischenablage.  

-----------------------------

Sie können nun mit der Anleitung ["BibTeX aus der Zwischenablage importieren"](EintragPublikationen/listeimportzwischenablage "wikilink") fortfahren.
	   