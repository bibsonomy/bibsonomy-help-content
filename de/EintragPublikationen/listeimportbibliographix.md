<!-- en: AddPublications/listimportbibliographix -->

-----------------

[`Zurück zur Übersicht zum Importieren einer Literaturliste es hier.`](EintragPublikationen/LiteraturlisteImportieren "wikilink")

---------------------

## BibTeX-Export aus Bibliographix
------------------

  1.  Starten Sie Bibliographix und laden Sie das zu
       exportierende Projekt.
      
  2.  Klicken Sie auf den Reiter *"mehr..."*, dann auf *"Daten
       austauschen"*.  
          

 ![ <File:biblio1.png>](listeimportbibliographix/biblio1.png  "fig: File:biblio1.png")  
      
  3.  Wählen Sie im linken Menü die Option *"Export"*.
  
  4.  Klicken Sie abschließend auf *"Datenbank im RIS-Format
        exportieren!"*.  
          

![ <File:biblio2.png>](listeimportbibliographix/biblio2.png  "fig: File:biblio2.png")  
       
  5.  Die exportiere Datei mit den BibTeX-Datensätzen können Sie nun
        bei ${project.name} importieren, indem Sie im linken Hauptmenü
        auf **"Eintragen“** klicken und im Untermenü auf **"Publikation
        eintragen“**. Wählen Sie daraufhin den Reiter **"Datei
        hochladen“**. Hier können Sie die Datei von Ihrem Computer
        auswählen, außerdem können Sie bestimmen, für wen die
        Publikationen sichtbar sein sollen. Durch Klicken auf
        **"Weiter"** wird die Datei hochgeladen.
