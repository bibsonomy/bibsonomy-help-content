<!-- en: AddPublications/ImportData -->

-----------------

[`Zurück zur Übersicht zum Hinzufügen von Publikationen geht es hier.`](EintragPublikationen "wikilink")

---------------------

## Literaturliste importieren
--------------------------

Der Import bereits erstellter Literaturlisten aus anderen Programmen in
${project.name} ist jederzeit problemlos möglich. Das international
einheitliche Datenformat, welches von allen gängigen Programmen zur
Literaturverwaltung unterstützt wird, heißt BibTeX. Der Import erfolgt
in zwei Schritten. Exportieren Sie zuerst aus ihrem bisherigen
Literaturverwaltungsprogramm die gewünschten Publikationen, dann
importieren Sie die BibTeX-Daten in ${project.name}.

---------------------

### BibTeX-Export aus Ihrem bisherigen Literaturverwaltungsprogramm

Sie können einen BibTeX-Export Ihres bisherigen Literaturverwaltungsorigramms durchführen. Betrachten Sie dazu die 
Anleitungen für den Export aus [Citavi](EintragPublikationen/listeimportcitavi "wikilink")
und aus [Bibliographix](EintragPublikationen/listeimportbibliographix "wikilink").


**Hinweis:** Bitte beachten Sie, dass es neben den unterschiedlichen
Programmen auch unterschiedliche Programmversionen gibt, daher kann es
für ein Programm zwei oder mehr Möglichkeiten geben, Daten zu
exportieren. Probieren Sie ggf. alle Möglichkeiten einmal aus.

---------------------------------

### BibTeX aus der Zwischenablage importieren

Haben Sie Daten aus Ihrem bisherigen Literaturverwaltungsprogramm in das BibTeX-Format exportiert
und in die Zwischenablage kopiert? Dann können Sie diese nun in ${project.name} importieren. Folgen Sie dazu
[dieser Anleitung](EintragPublikationen/listeimportzwischenablage "wikilink").

**Voraussetzung:** Exportieren Sie zunächst Ihre Literaturliste aus
Ihrem bisherigen Literaturverwaltungsprogramm in die Zwischenablage. Wie
das geht, finden Sie unter dem Punkt **BibTeX-Export aus meinem
bisherigen Literaturverwaltungsprogramm** am Anfang dieser Seite.  

