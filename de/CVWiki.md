## CVWiki
--------------

Mit ${project.name} ist es nicht nur möglich, seine eigenen
Publikationen mit anderen Benutzern zu teilen. Sie können auch einen
Lebenslauf (im Englischen auch Curriculum Vitae, kurz: CV) erstellen, in
dem Sie sich anderen Besuchern Ihrer Seite vorstellen können. Sie können
persönliche Informationen wie Ihr Geburtsdatum und Interessen angeben,
aber auch einen Überblick über Ihre beruflichen Aspekte sowie Ihre
eigenen veröffentlichten Publikationen schaffen. Im Folgenden möchten
wir Ihnen einen kurzen Überblick über das CVWiki geben.

-----------------

### Was ist das CVWiki?

Das CVWiki ist ein neues Feature von ${project.name}, mit dem Sie Ihre
Lebenslaufseite selbst erstellen oder verändern können. Sie haben die
Wahl zwischen vorgegebenen Layouts und der Möglichkeit, Ihr
persönliches Design zu erstellen.

Falls Sie Sich für die zweite Möglichkeit entscheiden, so können Sie
sowohl die weitgehend bekannte Mediawiki-Syntax verwenden als auch von
uns vorgegebene XHTML-Tags. Wir werden auf beide Techniken im Folgenden
kurz eingehen.

---------------

### Wo finde ich es?

Ihr CVWiki befindet sich grundsätzlich auf der Seite
`http://www.bibsonomy.org/cv/user/YourUserName`. Um von dort aus zum
Editor zu gelangen, können Sie entweder über das Zahnrad in der rechten
oberen Ecke navigieren oder Ihre Einstellungs-Seite aufrufen.
<http://www.bibsonomy.org/settings?selTab=5>.

----------------

### Benutzungsdetails

#### Grundlegende Funktionen

Auf der Einstellungsseite können Sie eine Vorschau des aktuellen Designs
sehen. Unter dem Selektor für die Standard-Layouts liegt der
Quellcode-Editor versteckt. Öffnen Sie diesen, so können Sie Ihren
Lebenslauf frei nach Ihren Wünschen gestalten. Hierfür können Sie die
[MediaWiki-Syntax](http://en.wikipedia.org/wiki/Help:Wiki_markup), die zum Beispiel aus Wikipedia bekannt ist, benutzen.

-----------------

#### Ein kurzer Überblick über unsere Tags

Wir bieten außerdem einige XHTML-Tags an, um Ihnen Zeit zu ersparen.

Tag | Bedeutung | Gruppe | Benutzer
--- | --- | --- | ---
&lt;name /&gt; | Ihr Name, mit Link auf Ihre Publikationsseite | nein | ja
&lt;image /&gt; | Ihr Profilbild | ja | ja 
&lt;publications /&gt; | Eine Liste Ihrer Publikationen mit dem Tag "myown" | ja | ja 
&lt;bookmarks /&gt; | Eine Liste Ihrer Lesezeichen mit dem Tag "myown" | ja | ja 
&lt;profession /&gt; | Ihr Beruf | nein | ja 
&lt;institution /&gt; | Ihre beschäftigende Institution | nein | ja 
&lt;hobbies /&gt; | Eine Liste Ihrer Hobbies | nein | ja
&lt;interests /&gt; | Ihre persönlichen Interessen | nein | ja
&lt;birthday /&gt; | Ihr Geburtsdatum | nein | ja 
&lt;location /&gt; | Ihr momentaner Wohnort | nein | ja 
&lt;regdate /&gt; | Ihr Registrierungsdatum in ${project.name} | nein | ja
&lt;members/&gt; | Liste aller Gruppenmitglieder | ja | nein
&lt;groupimage/&gt; | Gruppenicon| ja | nein
&lt;tags /&gt; | Ihre Tags | ja | ja  



Für die Tags `<publications />;`, `<bookmarks />` und `<tags/>` können Sie außerdem eigene Tags als Ressourcenselektoren benutzen, wenn Sie tags="tag1 tag2\[ ...\]" als Attribute anfügen. Beispielsweise liefert `<publications tags="data mining" />` alle Ihre Publikationen zurück, die Sie sowohl mit `data` als auch mit `mining` getaggt haben.

Für `<tags />` gibt es folgende Selektoren: `style` mit den Optionen
`cloud` und `list`; `order` mit den Optionen `alpha` und `freq`, `minfreq` mit einer
Zahl als Parameter, und `type` mit den Optionen `bookmarks` und `publications`.  

------------------------------------

[Klicken Sie hier, um zum Einsteiger-Bereich zurückzugelangen und mehr
über die grundlegenden Funktionen zu erfahren.](MainEinsteiger "wikilink")