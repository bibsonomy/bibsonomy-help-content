package org.bibsonomy;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.UUID;

import org.bibsonomy.common.enums.PostUpdateOperation;
import org.bibsonomy.model.Bookmark;
import org.bibsonomy.model.Post;
import org.bibsonomy.model.Resource;
import org.bibsonomy.model.User;
import org.bibsonomy.model.logic.LogicInterface;
import org.bibsonomy.rest.RESTConfig;
import org.bibsonomy.rest.auth.OAuthAPIAccessor;
import org.bibsonomy.rest.client.RestLogicFactory;
import org.bibsonomy.rest.renderer.RenderingFormat;

import net.oauth.OAuthException;


public class OAuthRestApiDemo {
	
	public static void main(String[] args) throws IOException, OAuthException, URISyntaxException {
		final OAuthAPIAccessor accessor = new OAuthAPIAccessor("<YOUR CONSUMER KEY>", "<YOUR CONSUMER SECRET>", "<YOUR CALLBACK URL>");
		
		// step one: ask user to authorize access
		System.out.println("Paste this in a browser: " + accessor.getAuthorizationUrl());
		System.out.println("<enter>");
		Scanner in = new Scanner(System.in);
		in.nextLine();
		
		// step two: obtain access token
		accessor.obtainAccessToken();
		System.out.println("Accesss Token: " + accessor.getAccessToken());
		System.out.println("Token Secret : " + accessor.getTokenSecret());

		// create logic interface
		RestLogicFactory rlf = new RestLogicFactory("https://www.bibsonomy.org/api", RenderingFormat.XML);
		LogicInterface rl = rlf.getLogicAccess(accessor);
		
		Post<Bookmark> testPost = generatePost(accessor.getRemoteUserId());
		
		//
		// publish first test post
		//
		List<Post<? extends Resource>> uploadPosts = new LinkedList<Post<? extends Resource>>();
		uploadPosts.add(testPost);
		testPost.getResource().recalculateHashes();
		String firstHash = testPost.getResource().getIntraHash();
		try {
			rl.createPosts(uploadPosts);
		} catch (org.bibsonomy.rest.exceptions.BadRequestOrResponseException e) {
			System.err.println(e.getMessage());
		}
		
		//
		// publish second test post
		//
		testPost.getResource().setTitle("Ein zweiter Post mittels OAuth ueber die Rest-API");
		testPost.getResource().setUrl("http://blog.bibsonomy.org/tmp" + UUID.randomUUID().toString());
		testPost.getResource().recalculateHashes();
		try {
			rl.createPosts(uploadPosts);
		} catch (org.bibsonomy.rest.exceptions.BadRequestOrResponseException e) {
			System.err.println(e.getMessage());
		}
		
		//
		// update second post
		//
		List<Post<? extends Resource>> updatePosts = new LinkedList<Post<? extends Resource>>();
		testPost.getResource().setTitle("Wenn es einen ersten Post gibt, ist hier ein fehler [UPDATED]");
		testPost.getResource().setUrl("http://blog.bibsonomy.org/tmp" + UUID.randomUUID().toString());
		updatePosts.add(testPost);
		try {
			rl.updatePosts(updatePosts, PostUpdateOperation.UPDATE_ALL);
		} catch (org.bibsonomy.rest.exceptions.BadRequestOrResponseException e) {
			System.err.println(e.getMessage());
		}
		
		//
		// delete first post
		//
		List<String> hashes = new LinkedList<String>();
		hashes.add(firstHash);
		try {
			rl.deletePosts(RESTConfig.USER_ME, hashes);
		} catch (org.bibsonomy.rest.exceptions.BadRequestOrResponseException e) {
			System.err.println(e.getMessage());
		}
	}

	/**
	 * generate test post
	 * @param username
	 * @return
	 */
	private static Post<Bookmark> generatePost(String username) {
		User uploadUser = new User(username);
		
		Bookmark testBookmark = new Bookmark();
		testBookmark.setTitle("Ein erster Post mittels OAuth ueber die Rest-API");
		testBookmark.setUrl("http://blog.bibsonomy.org/tmp" + UUID.randomUUID().toString());
		testBookmark.recalculateHashes();
		
		Post<Bookmark> testPost = new Post<Bookmark>();
		testPost.setUser(uploadUser);
		testPost.setResource(testBookmark);
		testPost.addTag("test");
		testPost.addTag("oauth");
		testPost.addTag("upload");
		
		return testPost;
	}
}
